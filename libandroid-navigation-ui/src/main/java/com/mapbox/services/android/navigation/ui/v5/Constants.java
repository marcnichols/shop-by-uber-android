package com.mapbox.services.android.navigation.ui.v5;

/**
 * Created by mac on 9/15/17.
 */

public class Constants {
    public static String SBU_Latitude = "latitude";
    public static String SBU_Longitude = "longitude";
    public static String SBU_Bearing = "bearing";
    public static String SBU_ChannelName = "channel";

    public static int ARRIVAL_MILESTONE_METER = 15;
}
