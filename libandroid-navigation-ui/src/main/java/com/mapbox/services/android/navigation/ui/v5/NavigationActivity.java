package com.mapbox.services.android.navigation.ui.v5;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.mapbox.geojson.Point;
import com.mapbox.services.android.navigation.v5.navigation.NavigationConstants;
import com.mapbox.services.android.navigation.v5.navigation.NavigationUnitType;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.models.consumer.PNPublishResult;
import com.pubnub.api.models.consumer.PNStatus;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Serves as a launching point for the custom drop-in UI, {@link NavigationView}.
 * <p>
 * Demonstrates the proper setup and usage of the view, including all lifecycle methods.
 */
public class NavigationActivity extends AppCompatActivity implements NavigationViewListener, NavigationView.OnLocationChangeCalled {

    private NavigationView navigationView;
    NavigationView.OnLocationChangeCalled onLocationChangeCalled;
    PNCallback<PNPublishResult> pnPublishResultPNCallback;
    PubNub pubnub;
    String pubChannelName;

    public static NavigationActivity mContext;

    public static NavigationActivity getInstance() {
        return mContext;
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.Theme_AppCompat_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        navigationView = findViewById(R.id.navigationView);
        navigationView.onCreate(savedInstanceState);
        mContext = this;
        navigationView.getNavigationAsync(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
            pubChannelName = bundle.getString(Constants.SBU_ChannelName);
        initPubNub();

        onLocationChangeCalled = this;
        navigationView.setListener(onLocationChangeCalled);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        navigationView.onLowMemory();
    }

    @Override
    public void onBackPressed() {
        // If the navigation view didn't need to do anything, call super
        if (!navigationView.onBackPressed()) {
            super.onBackPressed();
            try {
                if (this.pubnub != null) {
                    this.pubnub.unsubscribe().channels(Arrays.asList(pubChannelName))
                            .channelGroups(Arrays.asList(pubChannelName))
                            .execute();
                    this.pubnub.destroy();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        navigationView.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        navigationView.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onNavigationReady() {
        NavigationViewOptions.Builder options = NavigationViewOptions.builder();
        extractRoute(options);
        extractCoordinates(options);
        extractConfiguration(options);
        navigationView.startNavigation(options.build());
    }

    @Override
    public void onNavigationFinished() {
        finish();
    }

    @Override
    public void destinationArrived() {
        FinishOK();
    }

    private void extractRoute(NavigationViewOptions.Builder options) {
        options.directionsRoute(NavigationLauncher.extractRoute(this));
    }

    private void FinishOK() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    private void extractCoordinates(NavigationViewOptions.Builder options) {
        HashMap<String, Point> coordinates = NavigationLauncher.extractCoordinates(this);
        if (coordinates.size() > 0) {
            options.origin(coordinates.get(NavigationConstants.NAVIGATION_VIEW_ORIGIN));
            options.destination(coordinates.get(NavigationConstants.NAVIGATION_VIEW_DESTINATION));
        }
    }

    private void extractConfiguration(NavigationViewOptions.Builder options) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        options.awsPoolId(preferences
                .getString(NavigationConstants.NAVIGATION_VIEW_AWS_POOL_ID, null));
        options.shouldSimulateRoute(preferences
                .getBoolean(NavigationConstants.NAVIGATION_VIEW_SIMULATE_ROUTE, false));
        options.unitType(preferences
                .getInt(NavigationConstants.NAVIGATION_VIEW_UNIT_TYPE, NavigationUnitType.TYPE_IMPERIAL));
    }

    private void initPubNub() {
        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey(getString(R.string.SBU_Subscribe_key));
        pnConfiguration.setPublishKey(getString(R.string.SBU_Publish_key));
        pubnub = new PubNub(pnConfiguration);
    }

    @Override
    public void locationChanges(Location location) {
        broadcastLocation(location);
    }

    private void broadcastLocation(final Location location) {
        try {
            Map message = new HashMap();
            message.put(Constants.SBU_Latitude, location.getLatitude());
            message.put(Constants.SBU_Longitude, location.getLongitude());
            message.put(Constants.SBU_Bearing, location.getBearing());

            pnPublishResultPNCallback = new PNCallback<PNPublishResult>() {
                @Override
                public void onResponse(PNPublishResult result, PNStatus status) {
                    if (!status.isError()) {

                    }
                }
            };
            pubnub.publish().message(message).channel(pubChannelName).async(pnPublishResultPNCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
