package com.shopbyuber.quickblox;


import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.quickblox.auth.session.QBSettings;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialogCustomData;
import com.quickblox.chat.utils.DialogUtils;
import com.quickblox.core.LogLevel;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.QBPagedRequestBuilder;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.model.UserModel;
import com.shopbyuber.quickblox.Utils.QbDialogHolder;
import com.shopbyuber.quickblox.model.Users;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

import static com.quickblox.users.QBUsers.signIn;

public class ChatHelper {
    private QBChatService qbChatService;
    Activity activity;

    Integer opponentId;
    boolean getall;
    String dialogId;
    Globals globals = (Globals) Globals.getContext();

    OnQBChatServicesListener onQBChatServicesListener;
    private static ChatHelper instance;

    QBChatDialog activeQbChatDialog;

    public void setActiveChatDialog(QBChatDialog chatDialog) {
        this.activeQbChatDialog = chatDialog;
    }

    public void resetActiveChatDialog() {
        this.activeQbChatDialog = null;
    }

    public QBChatDialog getCurrentChatDialog() {
        return activeQbChatDialog;
    }

    public static synchronized ChatHelper getInstance() {
        if (instance == null) {
            QBSettings.getInstance().setLogLevel(LogLevel.DEBUG);
            QBChatService.setDebugEnabled(true);
            QBChatService.setConfigurationBuilder(buildChatConfigs());
            instance = new ChatHelper();
        }
        return instance;
    }

    public static QBChatService.ConfigurationBuilder buildChatConfigs() {
        QBChatService.ConfigurationBuilder configurationBuilder = new QBChatService.ConfigurationBuilder();
        SampleConfigs qbConfigs = Globals.getSampleConfigs();

        configurationBuilder.setPort(qbConfigs.getChatPort());
        configurationBuilder.setSocketTimeout(qbConfigs.getChatSocketTimeout()); //Sets chat socket's read timeout in seconds
        configurationBuilder.setKeepAlive(qbConfigs.isKeepAlive()); //Sets connection socket's keepAlive option.
        configurationBuilder.setAutojoinEnabled(qbConfigs.isAutoJoinEnabled());
        configurationBuilder.setAutoMarkDelivered(qbConfigs.isAutoMarkDelivered());
        configurationBuilder.setUseTls(qbConfigs.isUseTls()); //Sets the TLS security mode used when making the connection. By default TLS is disabled.
        QBChatService.setConfigurationBuilder(configurationBuilder);

        return configurationBuilder;
    }


    private ChatHelper() {
        qbChatService = QBChatService.getInstance();
        qbChatService.setUseStreamManagement(true);
    }

    public boolean isLogged() {
        return QBChatService.getInstance().isLoggedIn();
    }

    private QBUser getQBUser() {
        UserModel userLoginDetail = globals.getUserData();
        QBUser qbUser = new QBUser();
        qbUser.setLogin(String.valueOf(userLoginDetail.Data.userId));
        qbUser.setPassword(Constants.SBU_QBPasssword);
        qbUser.setFullName(userLoginDetail.Data.firstName +
                (globals.getUserData().Data.lastName == null ? "" : " " + globals.getUserData().Data.lastName));
        qbUser.setEmail(userLoginDetail.Data.email);
        return qbUser;
    }


    public void init(Activity activity, boolean getall, String dialogId, OnQBChatServicesListener onQBChatServicesListener) {
        this.activity = activity;
        this.dialogId = dialogId;
        this.getall = getall;

        this.onQBChatServicesListener = onQBChatServicesListener;

        if (isLogged()) {
            retriveDialogs();
        } else {
            signInUser(getQBUser(), true);
        }
    }

    private void retriveDialogs() {
        if (getall)
            getDialogs();
        else
            getDialogById(dialogId);
    }

    int totalDialogs;
    ArrayList<QBChatDialog> qbDialogs = new ArrayList<>();

    public void getDialogs() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                // code goes here
                QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();
                requestBuilder.setLimit(100);
                if (qbDialogs.size() > 0)
                    requestBuilder.setSkip(qbDialogs.size());
                QBRestChatService.getChatDialogs(null, requestBuilder).performAsync(new QBEntityCallback<ArrayList<QBChatDialog>>() {
                    @Override
                    public void onSuccess(ArrayList<QBChatDialog> qbChatDialogs, Bundle bundle) {
                        if (qbChatDialogs.size() > 0) {
                            totalDialogs = bundle.getInt(Constants.IM_total_entries);
                            qbDialogs.addAll(qbChatDialogs);
                            if (qbDialogs.size() >= totalDialogs) {
                                getUserDetails(qbDialogs);
                            } else
                                getDialogs();
                        } else {
                            getUserDetails(qbDialogs);
                        }
                    }

                    @Override
                    public void onError(QBResponseException errors) {
                        onQBChatServicesListener.onServiceProcessFailed(errors.getMessage());
                    }
                });
            }
        });
    }

    public void getUserDetails(final ArrayList<QBChatDialog> qbChatDialogs) {
        if (!qbChatDialogs.isEmpty()) {
            JSONObject params = HttpRequestHandler.getInstance().getUsersFromQuickbloxIds(
                    globals.getUserData().Data.userId,
                    globals.getUserData().Data.sessionId,
                    prepareQBIds(qbChatDialogs));

            HttpRequestHandler.getInstance().post(activity, activity.getString(R.string.getUsersFromQuickbloxIds), params, new JsonHttpResponseHandler() {

                @Override
                public void onStart() {
                    super.onStart();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Logger.json(response.toString());
                    Users users = new Gson().fromJson(response.toString(), Users.class);
                    if (users.Result == true && users.Data != null)
                        setUpFinalDialogList(users, qbChatDialogs);
                    else if (onQBChatServicesListener != null)
                        onQBChatServicesListener.onServiceProcessFailed(users.Message);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    Toaster.shortToast(R.string.error_went_wrong);
                    if (onQBChatServicesListener != null)
                        onQBChatServicesListener.onServiceProcessSuccess(qbChatDialogs);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Toaster.shortToast(R.string.error_went_wrong);
                    if (onQBChatServicesListener != null)
                        onQBChatServicesListener.onServiceProcessSuccess(qbChatDialogs);
                }
            });
        } else {
            onQBChatServicesListener.onServiceProcessSuccess(qbChatDialogs);
        }
    }

    private void setUpFinalDialogList(Users users, ArrayList<QBChatDialog> qbChatDialogs) {
        for (QBChatDialog qbChatDialog : qbChatDialogs) {
            Users.Data userDetails = getUser(users, Integer.valueOf(getRecipient(qbChatDialog)));
            if (userDetails != null) {
                qbChatDialog.setName(userDetails.name);
                qbChatDialog.setPhoto(userDetails.profileImage);

                QBDialogCustomData qbDialogCustomData = new QBDialogCustomData();
                qbDialogCustomData.putString(Constants.SBU_UserId, userDetails._id);
                qbChatDialog.setCustomData(qbDialogCustomData);

                qbChatDialog.initForChat(QBChatService.getInstance());
                QbDialogHolder.getInstance().addDialog(qbChatDialog);
            }
        }
        if (onQBChatServicesListener != null)
            onQBChatServicesListener.onServiceProcessSuccess(qbChatDialogs);
        clearTemps();
    }

    private Users.Data getUser(Users users, Integer recipientId) {
        for (Users.Data userDetails : users.Data) {
            if (userDetails.quickBloxId == recipientId)
                return userDetails;
        }
        return null;
    }

    private void clearTemps() {
        totalDialogs = 0;
        qbDialogs.clear();
    }

    private JSONArray prepareQBIds(ArrayList<QBChatDialog> qbChatDialogs) {
        ArrayList<Integer> userIds = new ArrayList<>();
        for (QBChatDialog qbChatDialog : qbChatDialogs) {
            userIds.add(getRecipient(qbChatDialog));
        }
        return new JSONArray(userIds);
    }

    public void signInUser(final QBUser qbuser, final boolean fromDialogs) {
        if (qbChatService == null) {
            qbChatService = QBChatService.getInstance();
        }
        signInQBUser(qbuser, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                loginToChat(qbuser, fromDialogs);
            }

            @Override
            public void onError(QBResponseException e) {
                if (e.getHttpStatusCode() == 401)
                    registerQuickBloxUser(qbuser, fromDialogs);
                else
                    onQBChatServicesListener.onServiceProcessFailed(e.getMessage());
            }
        });
    }

    public void registerQuickBloxUser(final QBUser qbUser, final boolean fronDialogs) {
        signUpQBUser(qbUser, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser user, Bundle bundle) {
                qbUser.setId(user.getId());
                signInUser(qbUser, fronDialogs);
            }

            @Override
            public void onError(QBResponseException e) {
                if (e.getErrors() != null && !e.getErrors().isEmpty()) {
                    if (e.getHttpStatusCode() == Constants.SBU_err_qb_login_taken) {
                        getExistingQBUser(qbUser, new QBEntityCallback<QBUser>() {
                            @Override
                            public void onSuccess(QBUser qbUser, Bundle bundle) {
                                qbUser.setPassword(Constants.SBU_QBPasssword);
                                qbUser.setId(qbUser.getId());
                                signInUser(qbUser, fronDialogs);
                            }

                            @Override
                            public void onError(QBResponseException e) {
                                onQBChatServicesListener.onServiceProcessFailed(e.getMessage());
                            }
                        });
                    } else
                        onQBChatServicesListener.onServiceProcessFailed(e.getMessage());
                }
            }
        });
    }

    public void getExistingQBUser(final QBUser qbUser, QBEntityCallback<QBUser> callback) {
        QBUsers.getUserByLogin(qbUser.getLogin()).performAsync(callback);
    }

    public void signUpQBUser(final QBUser qbUser, QBEntityCallback<QBUser> callback) {
        QBUsers.signUp(qbUser).performAsync(callback);
    }

    public void signInQBUser(final QBUser qbUser, QBEntityCallback<QBUser> callback) {
        signIn(qbUser).performAsync(callback);
    }

    public void loginToChat(final QBUser user, final boolean fromDialogs) {
        if (qbChatService == null) {
            return;
        }
        chatLogin(user, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                if (fromDialogs)
                    retriveDialogs();
                else
                    createDialogWithSelectedUsers(activity, opponentId, onQBChatServicesListener);
            }

            @Override
            public void onError(QBResponseException e) {
                if (e.getMessage().equalsIgnoreCase(Constants.SBU_err_qb_logged_in)) {
                    retriveDialogs();
                } else
                    loginToChat(user, fromDialogs);
            }
        });
    }

    public void chatLogin(final QBUser qbUser, QBEntityCallback<QBUser> callback) {
        qbChatService.login(qbUser, callback);
    }

    public void createDialogWithSelectedUsers(Activity activity, Integer opponentId, final OnQBChatServicesListener onQBChatServicesListener) {
        this.activity = activity;
        this.opponentId = opponentId;
        this.onQBChatServicesListener = onQBChatServicesListener;

        if (isLogged()) {
            createDialog(opponentId, new QBEntityCallback<QBChatDialog>() {
                @Override
                public void onSuccess(QBChatDialog qbChatDialog, Bundle bundle) {
                    ArrayList<QBChatDialog> qbChatDialogs = new ArrayList<QBChatDialog>();
                    qbChatDialogs.add(qbChatDialog);
                    onQBChatServicesListener.onServiceProcessSuccess(qbChatDialogs);
                }

                @Override
                public void onError(QBResponseException e) {
                    onQBChatServicesListener.onServiceProcessFailed(e.getMessage());
                }
            });
        } else {
            signInUser(getQBUser(), false);
        }
    }

    private void createDialog(Integer opponentId, QBEntityCallback<QBChatDialog> callback) {
        QBRestChatService.createChatDialog(prepareDialog(opponentId)).performAsync(callback);
    }

    public void getDialogById(String dialogId) {
        QBRestChatService.getChatDialogById(dialogId).performAsync(new QBEntityCallback<QBChatDialog>() {
            @Override
            public void onSuccess(QBChatDialog qbChatDialog, Bundle bundle) {
                ArrayList<QBChatDialog> qbChatDialogs = new ArrayList<QBChatDialog>();
                qbChatDialogs.add(qbChatDialog);
                getUserDetails(qbChatDialogs);
               /* onQBChatServicesListener.onServiceProcessSuccess(qbChatDialogs);*/
            }

            @Override
            public void onError(QBResponseException e) {
                if (onQBChatServicesListener != null)
                    onQBChatServicesListener.onServiceProcessFailed(e.getMessage());
            }
        });
    }

    public Integer getRecipient(QBChatDialog qbChatDialog) {
        if (globals.getUserData() == null)
            return 0;
        for (Integer id : qbChatDialog.getOccupants()) {
            if (!String.valueOf(id).equalsIgnoreCase(String.valueOf(globals.getUserData().Data.quickBloxId)))
                return id;
        }
        return qbChatDialog.getRecipientId();
    }

    public void getDialogMessage(QBChatDialog qbChatDialog, boolean forReferesh, long dateSent, final OnQBChatMeesageListener onQBChatMeesageListener) {
        QBRequestGetBuilder customObjectRequestBuilder = new QBRequestGetBuilder();
        customObjectRequestBuilder.setLimit(Constants.NumberOfMessagesPerPage);
        if (forReferesh)
            customObjectRequestBuilder.lt("date_sent", dateSent);
        customObjectRequestBuilder.sortDesc("date_sent");
        QBRestChatService.getDialogMessages(qbChatDialog, customObjectRequestBuilder)
                .performAsync(new QBEntityCallback<ArrayList<QBChatMessage>>() {
                    @Override
                    public void onSuccess(ArrayList<QBChatMessage> qbChatMessages, Bundle bundle) {
                        onQBChatMeesageListener.messageRetriveSuccess(qbChatMessages);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        onQBChatMeesageListener.messageRetriveFailed(e.getMessage());
                    }
                });
    }

    public static QBChatDialog prepareDialog(Integer opponentId) {
        List<QBUser> users = new ArrayList<>();
        QBUser qbUser = new QBUser();
        qbUser.setId(opponentId);
        users.add(qbUser);
        return DialogUtils.buildDialog(users.toArray(new QBUser[users.size()]));
    }

    public void getUsersFromDialog(QBChatDialog dialog,
                                   final QBEntityCallback<ArrayList<QBUser>> callback) {
        List<Integer> userIds = dialog.getOccupants();
        QBPagedRequestBuilder requestBuilder = new QBPagedRequestBuilder(userIds.size(), 1);
        QBUsers.getUsersByIDs(userIds, requestBuilder).performAsync(callback);
    }

    public void logoutFromChat(Context context) {
        if (qbChatService == null)
            return;
        QbDialogHolder.getInstance().clearDialogs();
        NotificationManager nMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancelAll();

        try {
            qbChatService.logout(new QBEntityCallback() {

                @Override
                public void onSuccess(Object o, Bundle bundle) {
                    qbChatService.destroy();
                }

                @Override
                public void onError(QBResponseException errors) {
                    Logger.e(errors.getMessage());
                }
            });
        } catch (Exception e) {

        }
    }

    public static QBUser getCurrentUser() {
        return QBChatService.getInstance().getUser();
    }

    public interface OnQBChatServicesListener {
        public void onServiceProcessSuccess(ArrayList<QBChatDialog> qbChatDialogs);

        public void onServiceProcessFailed(String errorMessage);
    }

    public interface OnQBChatMeesageListener {
        public void messageRetriveSuccess(ArrayList<QBChatMessage> qbChatMessages);

        public void messageRetriveFailed(String message);
    }
}
