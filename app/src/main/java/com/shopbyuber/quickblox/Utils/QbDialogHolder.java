package com.shopbyuber.quickblox.Utils;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


public class QbDialogHolder {

    private static QbDialogHolder instance;
    private Map<String, QBChatDialog> dialogsMap;
    Intent intentUpdateTotalUnreads = new Intent(Constants.SBU_actionUpdateTotalUnread);
    Intent intentUpdateDialogs = new Intent(Constants.SBU_actionUpdateDialogs);
    String currentChatDialogId = null;

    public static synchronized QbDialogHolder getInstance() {
        if (instance == null) {
            instance = new QbDialogHolder();
        }
        return instance;
    }

    private QbDialogHolder() {
        dialogsMap = new TreeMap<>();
    }

    public QBChatDialog getChatDialogById(String dialogId) {
        return dialogsMap.get(dialogId);
    }

    public void setCurrentChatDialogId(String dialogId) {
        this.currentChatDialogId = dialogId;
    }

    public String getCurrentChatDialogId() {
        return currentChatDialogId;
    }

    public void addDialog(QBChatDialog dialog) {
        if (dialog != null) {
            dialogsMap.put(dialog.getDialogId(), dialog);
        }
    }

    public boolean dialogPresent(String dialogId) {
        if (dialogsMap.containsKey(dialogId))
            return true;
        return false;
    }

    public void clearDialogs() {
        if (dialogsMap != null)
            dialogsMap.clear();
    }

    public void addDialogs(List<QBChatDialog> dialogs) {
        for (QBChatDialog dialog : dialogs) {
            addDialog(dialog);
        }
        LocalBroadcastManager.getInstance(Globals.getContext()).sendBroadcast(intentUpdateTotalUnreads);
    }

    public Map<String, QBChatDialog> getDialogs() {
        return getSortedMap(dialogsMap);
    }

    public Map<String, QBChatDialog> getSortedMap(Map<String, QBChatDialog> unsortedMap) {
        Map<String, QBChatDialog> sortedMap = new TreeMap(new LastMessageDateSentComparator(unsortedMap));
        sortedMap.putAll(unsortedMap);
        return sortedMap;
    }

    static class LastMessageDateSentComparator implements Comparator<String> {
        Map<String, QBChatDialog> map;

        public LastMessageDateSentComparator(Map<String, QBChatDialog> map) {
            this.map = map;
        }

        public int compare(String keyA, String keyB) {

            long valueA = map.get(keyA).getLastMessageDateSent();
            long valueB = map.get(keyB).getLastMessageDateSent();

            if (valueB < valueA) {
                return -1;
            } else {
                return 1;
            }
        }
    }

    public int getTotalUnreadDialogs() {
        int totalUnreads = 0;
        try {
            ArrayList<QBChatDialog> qbChatDialogs = new ArrayList<>(QbDialogHolder.getInstance().getDialogs().values());
            for (QBChatDialog qbChatDialog : qbChatDialogs) {
                if (qbChatDialog.getUnreadMessageCount() > 0)
                    totalUnreads = totalUnreads + 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return totalUnreads;
    }

    public void updateDialog(QBChatMessage qbChatMessage) {
        QBChatDialog updatedDialog = getChatDialogById(qbChatMessage.getDialogId());
        if (updatedDialog != null) {
            updatedDialog.setLastMessage(qbChatMessage.getBody());
            updatedDialog.setLastMessageDateSent(qbChatMessage.getDateSent());
            if (getCurrentChatDialogId() == null || !getCurrentChatDialogId().equalsIgnoreCase(qbChatMessage.getDialogId())) {

                if (updatedDialog.getUnreadMessageCount() != null && updatedDialog.getUnreadMessageCount() == 0)
                    LocalBroadcastManager.getInstance(Globals.getContext()).sendBroadcast(intentUpdateTotalUnreads);

                updatedDialog.setUnreadMessageCount(updatedDialog.getUnreadMessageCount() != null
                        ? updatedDialog.getUnreadMessageCount() + 1 : 1);
/*
            NotificationUtils.showChatNotification(Globals.getContext(),
                    QBChatActivity.class,
                    String.valueOf(qbChatMessage.getProperty(Constant.IM_username)),
                    qbChatMessage.getBody(),
                    qbChatMessage.getDialogId(),
                    String.valueOf(qbChatMessage.getProperty(Constant.IM_profile_url)));*/
            }
            LocalBroadcastManager.getInstance(Globals.getContext()).sendBroadcast(intentUpdateDialogs);
            dialogsMap.put(updatedDialog.getDialogId(), updatedDialog);
        }
    }

    public void updateUnreadToZero(String dialogId) {
        QBChatDialog updatedDialog = getChatDialogById(dialogId);
        if (updatedDialog != null) {
            updatedDialog.setUnreadMessageCount(0);
            dialogsMap.put(updatedDialog.getDialogId(), updatedDialog);
            LocalBroadcastManager.getInstance(Globals.getContext()).sendBroadcast(intentUpdateTotalUnreads);
        }
    }

    public QBChatDialog getDialogFromOpponentId(Integer opponentID) {
        ArrayList<QBChatDialog> qbChatDialogs = new ArrayList<>(getDialogs().values());
        for (QBChatDialog qbChatDialog : qbChatDialogs) {
            if (qbChatDialog.getRecipientId().equals(opponentID))
                return qbChatDialog;
        }
        return null;
    }
}
