package com.shopbyuber.quickblox.Utils;

/**
 * Created by A on 03-02-2018.
 */

public enum State {
    SENT(0),
    DELIVERED(1),
    READ(2),
    NOSTATE(-1);

    private int code;

    State(int code) {
        this.code = code;
    }
}
