package com.shopbyuber.quickblox.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by A on 05-02-2018.
 */

public class DateUtils {

    public static final long SECOND_IN_MILLIS = 1000;
    public static final long MINUTE_IN_MILLIS = SECOND_IN_MILLIS * 60;
    public static final long HOUR_IN_MILLIS = MINUTE_IN_MILLIS * 60;
    public static final long DAY_IN_MILLIS = HOUR_IN_MILLIS * 24;
    public static final long YEAR_IN_MILLIS = DAY_IN_MILLIS * 365;

    private static final SimpleDateFormat FULL_DATE_FORMAT;
    private static final SimpleDateFormat SHORT_DATE_FORMAT;
    private static final SimpleDateFormat HEADER_TIME_FORMAT;
    private static final SimpleDateFormat SHORT_DATE_WITHOUT_DIVIDERS_FORMAT;
    private static final SimpleDateFormat SIMPLE_TIME_FORMAT;
    private static final SimpleDateFormat DAY_AND_MONTH_AND_YEAR_FULL_FORMAT;
    private static final SimpleDateFormat DAY_AND_MONTH_AND_YEAR_SHORT_FORMAT;
    private static final SimpleDateFormat DAY_AND_SHORT_MONTH_FORMAT;
    private static final SimpleDateFormat SHORT_MONTH_AND_DAY_FORMAT;
    private static SimpleDateFormat PARSE_FORMAT;

    static {
        PARSE_FORMAT = new SimpleDateFormat("h:mm a", Locale.US);
        FULL_DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        SHORT_DATE_FORMAT = new SimpleDateFormat("dd/MM/yy", Locale.getDefault());
        SHORT_DATE_WITHOUT_DIVIDERS_FORMAT = new SimpleDateFormat("ddMMyyyy", Locale.getDefault());
        SIMPLE_TIME_FORMAT = new SimpleDateFormat("HH:mm", Locale.getDefault());
        DAY_AND_MONTH_AND_YEAR_FULL_FORMAT = new SimpleDateFormat("dd MMMM, yyyy", Locale.getDefault());
        DAY_AND_MONTH_AND_YEAR_SHORT_FORMAT = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        DAY_AND_SHORT_MONTH_FORMAT = new SimpleDateFormat("dd MMM", Locale.getDefault());
        SHORT_MONTH_AND_DAY_FORMAT = new SimpleDateFormat("MMM dd", Locale.getDefault());
        HEADER_TIME_FORMAT = new SimpleDateFormat("MMMM d, yyyy", Locale.getDefault());
    }

    public static long nowSeconds() {
        return nowMillis() / SECOND_IN_MILLIS;
    }

    public static long nowMillis() {
        return getCalendar().getTimeInMillis();
    }

    public static Calendar getCalendar() {
        return Calendar.getInstance(TimeZone.getDefault());
    }

    public static Calendar getCalendar(int year, int month, int day) {
        Calendar calendar = getCalendar();
        calendar.set(year, month, day);
        return calendar;
    }

    public static Calendar getCalendar(long seconds) {
        Calendar calendar = getCalendar();
        calendar.setTimeInMillis(seconds * SECOND_IN_MILLIS);
        return calendar;
    }

    public static long roundToDays(long seconds) {
        Calendar c = getCalendar(seconds);
        c.set(Calendar.MILLISECOND, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.HOUR_OF_DAY, 0);
        return c.getTimeInMillis() / SECOND_IN_MILLIS;
    }

    public static long getEndOfADay(long seconds) {
        Calendar calendar = getCalendar(seconds);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTimeInMillis() / SECOND_IN_MILLIS;
    }

    public static boolean isTimePassed(long seconds) {
        return DateUtils.getCalendar(seconds).before(DateUtils.getCalendar());
    }

    /**
     * @return string in "Hours:Minutes" format, i.e. <b>11:23</b>
     */
    public static String formatDateSimpleTime(long seconds) {
        return SIMPLE_TIME_FORMAT.format(new Date(seconds * SECOND_IN_MILLIS));
    }

    public static String formatDateSimpleTimeLocal(long seconds) {
        return PARSE_FORMAT.format(new Date(seconds * SECOND_IN_MILLIS));
    }

    /**
     * @return string in "Day-Month-Year" format, i.e. <b>17-08-1992</b>
     */
    public static String formatDateFull(long seconds) {
        return FULL_DATE_FORMAT.format(new Date(seconds * SECOND_IN_MILLIS));
    }

    public static String formatDateShort(long seconds) {
        return HEADER_TIME_FORMAT.format(new Date(seconds * SECOND_IN_MILLIS));
    }

    public static String formatDateFullShortMonth(long seconds) {
        return DAY_AND_MONTH_AND_YEAR_SHORT_FORMAT.format(new Date(seconds * SECOND_IN_MILLIS));
    }

    /**
     * @param dateString must be in Day-Month-Year format, i.e. <b>17-08-1992</b>
     * @return time in seconds
     */
    public static long parseDateFull(String dateString) {
        try {
            return FULL_DATE_FORMAT.parse(dateString).getTime() / SECOND_IN_MILLIS;
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date().getTime() / SECOND_IN_MILLIS;
        }
    }

    public static int daysBetween(long seconds) {
        long date = roundToDays(seconds);
        long currentDate = roundToDays(nowSeconds());
        long diff = date - currentDate;

        return (int) TimeUnit.DAYS.convert(diff, TimeUnit.SECONDS);
    }

    public static long toShortDateLong(long seconds) {
        Calendar calendar = getCalendar(seconds);
        return Long.parseLong(SHORT_DATE_WITHOUT_DIVIDERS_FORMAT.format(calendar.getTime()));
    }

}
