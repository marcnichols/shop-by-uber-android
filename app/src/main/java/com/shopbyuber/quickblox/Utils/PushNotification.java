package com.shopbyuber.quickblox.Utils;

import android.content.Context;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.utils.Globals;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by differenz026 on 18/4/17.
 */

public class PushNotification {
    private static final String TAG = PushNotification.class.getSimpleName();
    private static PushNotification instance;

    public static synchronized PushNotification getInstance() {
        if (instance == null) {
            instance = new PushNotification();
        }
        return instance;
    }

    public void sendPush(Context context, String userID, String sessionId,
                         String dialogId, String message, String dialogName,
                         String receiverId, Integer opponentQBId, String requestId) {
        if (!Globals.internetCheck(context)) {
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().getPushNotificationParam(
                userID,
                sessionId,
                message, dialogId, dialogName,
                receiverId, opponentQBId, requestId);

        HttpRequestHandler.getInstance().post(context,
                context.getString(R.string.sendQuickBloxNotification), params, new JsonHttpResponseHandler() {

                    @Override
                    public void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Logger.json(response.toString());
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        Logger.json("sendPush error");
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        Logger.json(responseString);
                    }
                });
    }
}
