package com.shopbyuber.quickblox.Utils;

import android.content.Context;
import android.graphics.Bitmap;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.orhanobut.logger.Logger;
import com.shopbyuber.utils.CoreApp;

/**
 * Created by mac on 2/16/18.
 */

public class saveImageFromURL {
    private static saveImageFromURL instance;

    public static synchronized saveImageFromURL getInstance() {
        if (instance == null)
            instance = new saveImageFromURL();
        return instance;
    }

    public void getImageFromURL(final Context context, final String key, String url) {
        if (url != null && !url.isEmpty()) {
            Glide.with(context)
                    .load(url)
                    .asBitmap()
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
                            Logger.e("onBitmapLoaded");
                            SaveImageToSD(key, bitmap, context);
                            if (CoreApp.chatHeadService != null)
                                CoreApp.chatHeadService.updateBadgeCount(key);
                        }
                    });
        }
    }

    private void SaveImageToSD(String key, Bitmap finalBitmap, Context context) {
        new ImageSaver(context).
                setFileName(key).
                save(finalBitmap);
    }
}
