package com.shopbyuber.quickblox;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.quickblox.chat.model.QBChatDialog;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.model.QBNotificationModel;
import com.shopbyuber.quickblox.Utils.QbDialogHolder;
import com.shopbyuber.quickblox.adapters.ChatDialogAdapater;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cc.cloudist.acplibrary.ACProgressFlower;


public class DialogListFragment extends Fragment implements ChatHelper.OnQBChatServicesListener {
    @BindView(R.id.list)
    RecyclerView chatDialogList;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    ArrayList<QBChatDialog> qbChatDialogsArrayList = new ArrayList<>();
    ChatDialogAdapater adapter;
    ChatHelper.OnQBChatServicesListener onQBChatServicesListener;
    Globals globals;
    ACProgressFlower dialog;

    private BroadcastReceiver mQBDialogUpdateReceiver = new BroadcastReceiver() {
        @Override

        public void onReceive(Context context, final Intent intent) {
            if (intent.getAction().equals(Constants.SBU_actionUpdateDialogs)) {
                updateDialogsAdapter();
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dialog_list, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        globals = (Globals) getActivity().getApplicationContext();
        dialog = HttpRequestHandler.getInstance().getProgressBar(getActivity());
        swipeRefreshLayout.setRefreshing(false);
        swipeRefreshLayout.setEnabled(false);
        registerBroadcasts();
        if (adapter != null)
            adapter = null;
        onQBChatServicesListener = this;

        if (dialogExist()) {
            qbChatDialogsArrayList.clear();
            qbChatDialogsArrayList.addAll(new ArrayList<>(QbDialogHolder.getInstance().getDialogs().values()));
            initializeChatDialogAdapter();
        } else {
            showProgressDialog();
            loadDialogs();
        }
    }

    private void showProgressDialog() {
        dialog.show();
    }

    private void hideProgressDialog() {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    boolean dialogExist() {
        return new ArrayList<>(QbDialogHolder.getInstance().getDialogs().values()).size() > 0 ? true : false;
    }

    private void initializeChatDialogAdapter() {
        if (adapter == null) {
            chatDialogList.setLayoutManager(new LinearLayoutManager(getActivity()));
            adapter = new ChatDialogAdapater(getActivity(), qbChatDialogsArrayList);
            chatDialogList.setAdapter(adapter);
        } else {
            updateDialogsAdapter();
        }
        adapter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                intentToChat(qbChatDialogsArrayList.get(position));
            }
        });
    }

    public void registerBroadcasts() {
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mQBDialogUpdateReceiver,
                new IntentFilter(Constants.SBU_actionUpdateDialogs));
    }

    void loadDialogs() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                if (globals.getUserData() != null) {
//                    progress.setVisibility(View.VISIBLE);
                    ChatHelper.getInstance().init(getActivity(), true, null, onQBChatServicesListener);
                }
            }
        }, 300);
    }

    private void intentToChat(QBChatDialog qbChatDialog) {
        Intent intent = new Intent(getActivity(), QBChatActivity.class);
        QBNotificationModel qbNotificationModel = new QBNotificationModel();
        qbNotificationModel.profileImage = qbChatDialog.getPhoto();
        qbNotificationModel.quickBloxId = ChatHelper.getCurrentUser().getId();
        qbNotificationModel.userId = (String) qbChatDialog.getCustomData().getFields().get("data[" + Constants.SBU_UserId + "]");
        qbNotificationModel.userName = qbChatDialog.getName();
        qbNotificationModel.dialogId = qbChatDialog.getDialogId();
        intent.putExtra(Constants.SBU_QBDetails, qbNotificationModel);
        getActivity().startActivity(intent);
    }

    private void updateDialogsAdapter() {
        qbChatDialogsArrayList.clear();
        qbChatDialogsArrayList.addAll(new ArrayList<>(QbDialogHolder.getInstance().getDialogs().values()));

        if (adapter != null)
            adapter.updateList(new ArrayList<>(QbDialogHolder.getInstance().getDialogs().values()));
    }

    @Override
    public void onServiceProcessSuccess(ArrayList<QBChatDialog> qbChatDialogs) {
        hideProgressDialog();
        this.qbChatDialogsArrayList.clear();
        this.qbChatDialogsArrayList.addAll(new ArrayList<>(QbDialogHolder.getInstance().getDialogs().values()));
        initializeChatDialogAdapter();
    }

    @Override
    public void onServiceProcessFailed(String errorMessage) {
        hideProgressDialog();
        Toaster.shortToast(errorMessage);
    }

    @Override
    public void onResume() {
        updateDialogsAdapter();
        super.onResume();
    }
}
