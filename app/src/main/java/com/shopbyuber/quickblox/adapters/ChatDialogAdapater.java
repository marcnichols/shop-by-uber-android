package com.shopbyuber.quickblox.adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.quickblox.chat.model.QBChatDialog;
import com.shopbyuber.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class ChatDialogAdapater extends RecyclerView.Adapter<ChatDialogAdapater.ChatDialogViewHolder> {

    Activity activity;
    public ArrayList<QBChatDialog> qbChatDialogs;
    public ArrayList<QBChatDialog> qbFilterableChatDialogs;
    private AdapterView.OnItemClickListener onItemClickListener;

    public class ChatDialogViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ChatDialogAdapater mAdapter;
        @BindView(R.id.contactImage)
        public CircleImageView contactImage;
        @BindView(R.id.tv_name)
        public TextView tv_name;
        @BindView(R.id.tv_lastMessage)
        public TextView tv_lastMessage;
        @BindView(R.id.tv_date)
        public TextView tv_date;
        @BindView(R.id.tv_unread)
        public TextView tv_unread;

        public ChatDialogViewHolder(View itemView, ChatDialogAdapater mAdapter) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.mAdapter = mAdapter;
            itemView.setOnClickListener(this);
        }

        public void setDialogs(int position) {
            QBChatDialog qbChatDialog = qbChatDialogs.get(position);
            tv_name.setText(qbChatDialog.getName());
            Glide.with(activity)
                    .load(qbChatDialog.getPhoto())
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.drawable.profile_placeholder)
                    .crossFade()
                    .dontAnimate()
                    .into(contactImage);
        }

        @Override
        public void onClick(View v) {
            mAdapter.onItemHolderClick(ChatDialogViewHolder.this);
        }
    }

    public ChatDialogAdapater(Activity activity, ArrayList<QBChatDialog> qbChatDialogs) {
        this.activity = activity;
        this.qbChatDialogs = qbChatDialogs;
        this.qbFilterableChatDialogs = qbChatDialogs;
    }

    @Override
    public ChatDialogViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ChatDialogViewHolder(LayoutInflater.from(activity).inflate(R.layout.item_chat_dialog, parent, false),
                this);
    }

    @Override
    public void onBindViewHolder(ChatDialogViewHolder holder, int position) {
        holder.setDialogs(position);
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void onItemHolderClick(ChatDialogViewHolder holder) {
        if (onItemClickListener != null)
            onItemClickListener.onItemClick(null, holder.itemView, holder.getAdapterPosition(), holder.getItemId());
    }

    @Override
    public int getItemCount() {
        return qbChatDialogs.size();
    }

    public void updateList(ArrayList<QBChatDialog> newData) {
        this.qbChatDialogs = newData;
        this.qbFilterableChatDialogs = newData;
        this.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }
}
