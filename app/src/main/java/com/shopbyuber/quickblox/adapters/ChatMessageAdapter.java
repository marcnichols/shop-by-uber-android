package com.shopbyuber.quickblox.adapters;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.core.helper.CollectionsUtil;
import com.quickblox.users.model.QBUser;
import com.shopbyuber.R;
import com.shopbyuber.quickblox.ChatHelper;
import com.shopbyuber.quickblox.Utils.DateUtils;
import com.shopbyuber.quickblox.Utils.State;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ChatMessageAdapter extends RecyclerView.Adapter<ChatMessageAdapter.ChatMessageViewHolder> {
    private static final String TAG = ChatMessageAdapter.class.getSimpleName();

    private Activity activity;
    private ArrayList<QBChatMessage> messages;
    private QBChatDialog qbChatDialog;
    private static final int TYPE_OWN_TEXT = 1;
    private static final int TYPE_OPPONENT_TEXT = 2;

    public ChatMessageAdapter(Activity activity, QBChatDialog qbChatDialog, ArrayList<QBChatMessage> messages) {
        this.activity = activity;
        this.messages = messages;
        this.qbChatDialog = qbChatDialog;
    }

    @Override
    public ChatMessageViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case TYPE_OWN_TEXT:
                return new ChatMessageViewHolder(LayoutInflater.from(activity).inflate(R.layout.layout_chatmessage_sent, viewGroup, false));
            case TYPE_OPPONENT_TEXT:
                return new ChatMessageViewHolder(LayoutInflater.from(activity).inflate(R.layout.layout_chatmessage_received, viewGroup, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(ChatMessageViewHolder holder, int position) {
        QBChatMessage qbChatMessage = messages.get(position);
        holder.chat_message.setText(qbChatMessage.getBody());
        holder.message_date.setText(DateUtils.formatDateShort(qbChatMessage.getDateSent()));
    }

    @Override
    public int getItemViewType(int position) {
        if (messages.get(position).getSenderId().equals(ChatHelper.getCurrentUser().getId())) {
            return TYPE_OWN_TEXT;
        } else {
            return TYPE_OPPONENT_TEXT;
        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public QBChatMessage getItem(int position) {
        return messages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private State messageState(QBChatMessage qbChatMessage) {
        if (qbChatMessage.getReadIds() != null &&
                qbChatMessage.getReadIds().size() == qbChatDialog.getOccupants().size()) {
            return State.READ;
        } else if (qbChatMessage.getDeliveredIds() != null &&
                qbChatMessage.getDeliveredIds().size() == qbChatDialog.getOccupants().size()) {
            return State.DELIVERED;
        } else if (qbChatMessage.getReadIds() != null &&
                qbChatMessage.getReadIds().contains(ChatHelper.getCurrentUser().getId())) {
            return State.SENT;
        } else {
            return State.NOSTATE;
        }
    }

    private boolean isIncoming(QBChatMessage chatMessage) {
        QBUser currentUser = ChatHelper.getCurrentUser();
        return chatMessage.getSenderId() != null && !chatMessage.getSenderId().equals(currentUser.getId());
    }

    private boolean isRead(QBChatMessage chatMessage) {
        Integer currentUserId = ChatHelper.getCurrentUser().getId();
        return !CollectionsUtil.isEmpty(chatMessage.getReadIds()) && chatMessage.getReadIds().contains(currentUserId);
    }

    private void readMessage(QBChatMessage chatMessage) {
        try {
            qbChatDialog.readMessage(chatMessage);
        } catch (XMPPException | SmackException.NotConnectedException e) {
        }
    }


    public class ChatMessageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.chat_message)
        TextView chat_message;
        @BindView(R.id.message_date)
        TextView message_date;
        @Nullable
        @BindView(R.id.message_status)
        ImageView message_status;

        public ChatMessageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
