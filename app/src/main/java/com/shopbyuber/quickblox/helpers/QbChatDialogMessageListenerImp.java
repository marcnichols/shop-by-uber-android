package com.shopbyuber.quickblox.helpers;

import com.orhanobut.logger.Logger;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBChatDialogMessageListener;
import com.quickblox.chat.model.QBChatMessage;


public class QbChatDialogMessageListenerImp implements QBChatDialogMessageListener {
    public QbChatDialogMessageListenerImp() {
    }

    @Override
    public void processMessage(String s, QBChatMessage qbChatMessage, Integer integer) {
        Logger.d("processMessage" + String.valueOf(qbChatMessage));
    }

    @Override
    public void processError(String s, QBChatException e, QBChatMessage qbChatMessage, Integer integer) {
        Logger.d("processError" + String.valueOf(qbChatMessage));
    }
}
