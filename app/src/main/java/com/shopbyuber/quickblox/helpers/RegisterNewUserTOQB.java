package com.shopbyuber.quickblox.helpers;

import android.app.Activity;
import android.os.Bundle;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.quickblox.auth.session.QBSettings;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.LogLevel;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.model.UserModel;
import com.shopbyuber.quickblox.ChatHelper;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.json.JSONObject;

import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;


public class RegisterNewUserTOQB {
    private static final String TAG = RegisterNewUserTOQB.class.getSimpleName();
    private static RegisterNewUserTOQB instance;
    OnQBRegisterListener onQBRegisterListener;
    Activity activity;
    Globals globals = (Globals) Globals.getContext();
    boolean isUpdatingQBToDB = false;

    public static synchronized RegisterNewUserTOQB getInstance() {
        if (instance == null) {
            QBSettings.getInstance().setLogLevel(LogLevel.DEBUG);
            QBChatService.setDebugEnabled(true);
            QBChatService.setConfigurationBuilder(ChatHelper.buildChatConfigs());
            instance = new RegisterNewUserTOQB();
        }
        return instance;
    }

    public void setUpQBUser(Activity activity, UserModel.Data userDetails, OnQBRegisterListener onQBRegisterListener) {
        this.activity = activity;
        this.onQBRegisterListener = onQBRegisterListener;

        QBUser qbUser = new QBUser();
        qbUser.setFullName(userDetails.firstName + (userDetails.lastName == null ? "" : " " + userDetails.lastName));
        qbUser.setLogin(String.valueOf(userDetails.userId));
        qbUser.setPassword(Constants.SBU_QBPasssword);
        qbUser.setEmail(userDetails.email);
        registerNupdateToDB(activity, qbUser);
    }

    public void registerNupdateToDB(final Activity activity, final QBUser qbUser) {
        if (!isUpdatingQBToDB) {
            isUpdatingQBToDB = true;
            ChatHelper.getInstance().signUpQBUser(qbUser, new QBEntityCallback<QBUser>() {
                @Override
                public void onSuccess(QBUser qbUser, Bundle bundle) {
                    doRequestForUpdateQBId(activity, qbUser.getId());
                }

                @Override
                public void onError(QBResponseException e) {
                    Logger.d(TAG + e.getMessage());
                    if (e.getHttpStatusCode() == Constants.SBU_err_qb_login_taken) {
                        ChatHelper.getInstance().getExistingQBUser(qbUser, new QBEntityCallback<QBUser>() {
                            @Override
                            public void onSuccess(QBUser qbuser, Bundle bundle) {
                                doRequestForUpdateQBId(activity, qbuser.getId());
                            }

                            @Override
                            public void onError(QBResponseException e) {
                                isUpdatingQBToDB = false;
                                Logger.d(TAG + e.getMessage());
                            }
                        });
                    }
                }
            });
        }
    }

    public void doRequestForUpdateQBId(Activity activity, final int quickBloxId) {
        if (!Globals.internetCheck(activity))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getUpdateQuickBloxIdParam(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                quickBloxId);

        HttpRequestHandler.getInstance().post(activity, activity.getString(R.string.updateQuickBloxId), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onFinish() {
                isUpdatingQBToDB = false;
                super.onFinish();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                setupQBId(quickBloxId);
                onQBRegisterListener.onRegisterSuccess(quickBloxId);
                Logger.d(response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void setupQBId(int qbUserId) {
        UserModel userModel = globals.getUserData();
        userModel.Data.quickBloxId = qbUserId;
        globals.setUserData(userModel);
    }

    public interface OnQBRegisterListener {
        public void onRegisterSuccess(Integer qbUserId);

        public void onRegisterFailed(String errorMessage);
    }
}
