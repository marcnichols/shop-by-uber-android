package com.shopbyuber.quickblox.configs;


import com.google.gson.Gson;
import com.shopbyuber.quickblox.SampleConfigs;

import java.io.IOException;


public class ConfigUtils extends CoreConfigUtils {

    public static SampleConfigs getSampleConfigs(String fileName) throws IOException {
        ConfigParser configParser = new ConfigParser();
        Gson gson = new Gson();
        return gson.fromJson(configParser.getConfigsAsJsonString(fileName), SampleConfigs.class);
    }
}
