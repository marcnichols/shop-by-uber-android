package com.shopbyuber.quickblox;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.UserRole;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.main.SplashActivity;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.model.QBNotificationModel;
import com.shopbyuber.model.UserModel;
import com.shopbyuber.quickblox.Utils.PushNotification;
import com.shopbyuber.quickblox.Utils.QbDialogHolder;
import com.shopbyuber.quickblox.adapters.ChatMessageAdapter;
import com.shopbyuber.quickblox.helpers.QbChatDialogMessageListenerImp;
import com.shopbyuber.seller.SellerLandingActivity;
import com.shopbyuber.user.UserLandingActivity;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.CoreApp;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.jivesoftware.smack.SmackException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;

public class QBChatActivity extends BaseActivity implements ChatHelper.OnQBChatServicesListener,
        ChatHelper.OnQBChatMeesageListener, SwipeRefreshLayout.OnRefreshListener {

    private static final String PROPERTY_SAVE_TO_HISTORY = "save_to_history";

    @BindView(R.id.img_Logo)
    ImageView imgLogo;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.chat_message_list)
    RecyclerView chatMessageList;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.et_message)
    EditText etMessage;
    @BindView(R.id.img_send_message)
    ImageView img_send_message;

    @BindView(R.id.send_message_footer)
    LinearLayout sendMessageFooter;
    @BindView(R.id.progress)
    ProgressBar progress;

    QBChatDialog qbChatDialog;
    ChatHelper.OnQBChatServicesListener onQBChatServicesListener;
    ChatHelper.OnQBChatMeesageListener onQBChatMeesageListener;

    ChatMessageAdapter chatMessageAdapter;

    private ChatQBMessageListener chatMessageListener;
    ArrayList<QBChatMessage> qbChatMessagesArrayList = new ArrayList<>();
    ACProgressFlower dialog;
    Globals globals;
    QBNotificationModel qbNotificationModel;
    UserModel userModel;

    private boolean isLoaded = false;
    private int serviceCompleted = 5;

    private BroadcastReceiver showServiceCompletePopupReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            if (intent.getAction().equals(Constants.SBU_ServiceComplete)) {
                Bundle bundle = intent.getExtras();
                String requstId = bundle.getString(Constants.SBU_RequestId);
                if (qbNotificationModel.requestId != null &&
                        !qbNotificationModel.requestId.isEmpty()) {
                    if (requstId.equalsIgnoreCase(qbNotificationModel.requestId))
                        doGetRequestStatus(false);
                }
            }
        }
    };

    public class ChatQBMessageListener extends QbChatDialogMessageListenerImp {
        @Override
        public void processMessage(String s, QBChatMessage qbChatMessage, Integer integer) {
            if (qbChatDialog.getDialogId().equalsIgnoreCase(qbChatMessage.getDialogId()))
                qbChatMessagesArrayList.add(qbChatMessage);
            inititalizeAdapter();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qbchat);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            imgLogo.setVisibility(View.GONE);
        }
        globals = (Globals) getApplicationContext();
        userModel = globals.getUserData();

        etMessage.addTextChangedListener(textWatcher);
        etMessage.setText("");

        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorToolbarTitle);
        swipeRefreshLayout.setOnRefreshListener(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            qbNotificationModel = (QBNotificationModel) bundle.getSerializable(Constants.SBU_QBDetails);

            resetBadge();
        }

        if (qbNotificationModel != null) {
            if (getSupportActionBar() != null)
                getSupportActionBar().setTitle(qbNotificationModel.userName);
        }

        dialog = HttpRequestHandler.getInstance().getProgressBar(QBChatActivity.this);
        onQBChatServicesListener = this;
        onQBChatMeesageListener = this;
        chatMessageListener = new ChatQBMessageListener();

        doGetRequestStatus(true);
    }

    public void registerBroadcast() {
        LocalBroadcastManager.getInstance(this).registerReceiver(showServiceCompletePopupReceiver,
                new IntentFilter(Constants.SBU_ServiceComplete));
    }

    private void unregisterBroadcast() {
        if (showServiceCompletePopupReceiver != null)
            LocalBroadcastManager.getInstance(this).unregisterReceiver(showServiceCompletePopupReceiver);
    }

    private void resetBadge() {
        if (CoreApp.chatHeadService != null && qbNotificationModel != null)
            CoreApp.chatHeadService.resetBadge(qbNotificationModel.userId);
    }

    private void removeBubble() {
        if (CoreApp.chatHeadService != null && qbNotificationModel != null)
            CoreApp.chatHeadService.removeChatHead(qbNotificationModel.userId);
    }

    private QBChatDialog getDialog(String dialogId) {
        QBChatDialog qbChatDialog = QbDialogHolder.getInstance().getChatDialogById(dialogId);
        if (qbChatDialog != null)
            return qbChatDialog;
        return null;
    }

    private void initQBProcess() {
        showDialog();
        if (qbNotificationModel.dialogId == null ||
                qbNotificationModel.dialogId.equals(""))
            checkNmakeDialog(qbNotificationModel.quickBloxId);
        else {
            qbChatDialog = getDialog(qbNotificationModel.dialogId);
            if (qbChatDialog != null) {
                retriveMessages();
            } else
                ChatHelper.getInstance().init(this, false, qbNotificationModel.dialogId, onQBChatServicesListener);
        }
    }

    //TextWatcher for check empty textchat
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            checkFieldsForEmptyValues();
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private void checkFieldsForEmptyValues() {
        String s1 = etMessage.getText().toString();
        if (globals.isWhitespace(s1)) {
            img_send_message.setEnabled(false);
            img_send_message.setClickable(false);
            img_send_message.setFocusable(false);
            img_send_message.setColorFilter(ContextCompat.getColor(this, R.color.gray),
                    android.graphics.PorterDuff.Mode.MULTIPLY);
        } else {
            if (s1.equals("")) {
                img_send_message.setEnabled(false);
                img_send_message.setClickable(false);
                img_send_message.setFocusable(false);
                img_send_message.setColorFilter(ContextCompat.getColor(this, R.color.gray),
                        android.graphics.PorterDuff.Mode.MULTIPLY);
            } else {
                img_send_message.setEnabled(true);
                img_send_message.setClickable(true);
                img_send_message.setFocusable(true);
                img_send_message.setColorFilter(ContextCompat.getColor(this, R.color.colorAccent),
                        android.graphics.PorterDuff.Mode.MULTIPLY);
            }
        }
    }

    void inititalizeAdapter() {
        if (chatMessageAdapter == null) {

            chatMessageList.setLayoutManager(new LinearLayoutManager(this));
            chatMessageList.setItemAnimator(new DefaultItemAnimator());
            chatMessageAdapter = new ChatMessageAdapter(this, qbChatDialog, this.qbChatMessagesArrayList);
            chatMessageList.setAdapter(chatMessageAdapter);
        } else {
            stopRefreshing();
            chatMessageAdapter.notifyDataSetChanged();
        }

        scrollMessageListDown();
    }

    private void showDialog() {
        if (!dialog.isShowing() && !isFinishing())
            dialog.show();
    }

    private void hideDialog() {
        if (dialog != null && dialog.isShowing() && !isFinishing())
            dialog.dismiss();
    }

    private void checkNmakeDialog(int quickboxID) {
        qbChatDialog = QbDialogHolder.getInstance().getDialogFromOpponentId(quickboxID);
        if (qbChatDialog != null) {
            qbNotificationModel.dialogId = qbChatDialog.getDialogId();
            loadMessages();
        } else
            createDialog(quickboxID);
    }

    void createDialog(int quickboxID) {
        ChatHelper.getInstance().createDialogWithSelectedUsers(this, quickboxID, onQBChatServicesListener);
    }

    private void loadMessages() {
        initDialogForChat();
        ChatHelper.getInstance().getDialogMessage(qbChatDialog, false, 0, onQBChatMeesageListener);
    }

    private void initDialogForChat() {
        QbDialogHolder.getInstance().addDialog(qbChatDialog);
        ChatHelper.getInstance().setActiveChatDialog(qbChatDialog);
        qbChatDialog.initForChat(QBChatService.getInstance());
        qbChatDialog.addMessageListener(chatMessageListener);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onServiceProcessSuccess(ArrayList<QBChatDialog> qbChatDialogs) {
        stopRefreshing();
        qbChatDialog = qbChatDialogs.get(0);
        qbNotificationModel.dialogId = qbChatDialogs.get(0).getDialogId();
        retriveMessages();
    }

    private void retriveMessages() {
        initDialogForChat();
        loadMessages();
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(qbChatDialog.getName());
    }

    @Override
    public void onServiceProcessFailed(String errorMessage) {
        hideDialog();
        stopRefreshing();
        Toaster.shortToast(errorMessage);
    }

    @Override
    public void messageRetriveSuccess(ArrayList<QBChatMessage> qbChatMessages) {
        hideDialog();

        if (qbChatMessages.isEmpty())
            isLoaded = true;

        Collections.reverse(qbChatMessages);

        ArrayList<QBChatMessage> messagesTemp = new ArrayList<QBChatMessage>();
        messagesTemp.addAll(qbChatMessages);
        if (!qbChatMessages.isEmpty()) {
            messagesTemp.addAll(qbChatMessagesArrayList);
            this.qbChatMessagesArrayList.clear();
            this.qbChatMessagesArrayList.addAll(messagesTemp);
            messagesTemp.clear();
        }

        if (!swipeRefreshLayout.isRefreshing())
            if (chatMessageAdapter != null) {
                chatMessageAdapter.notifyDataSetChanged();
            } else
                inititalizeAdapter();
        else {
            if (chatMessageAdapter != null) {
                try {
                    chatMessageList.scrollToPosition(qbChatMessages.size());
                } catch (Exception e) {
                    Logger.e(e.getMessage());
                }
            }
        }
        stopRefreshing();
    }

    @Override
    public void messageRetriveFailed(String message) {
        hideDialog();
        stopRefreshing();
        isLoaded = true;
        Toaster.shortToast(message);
    }

    private void scrollMessageListDown() {
        try {
            chatMessageList.scrollToPosition(chatMessageAdapter.getItemCount() - 1);
        } catch (Exception e) {
            Logger.e(e.getMessage());
        }
    }

    @OnClick(R.id.img_send_message)
    public void sendMessage() {
        if (qbChatDialog != null) {
            QBChatMessage qbChatMessage = new QBChatMessage();
            qbChatMessage.setBody(etMessage.getText().toString().trim());
            qbChatMessage.setSenderId(ChatHelper.getCurrentUser().getId());
            qbChatMessage.setProperty(PROPERTY_SAVE_TO_HISTORY, "1");
            qbChatMessage.setDateSent(System.currentTimeMillis() / 1000);
            qbChatMessage.setMarkable(true);

            try {
                qbChatDialog.sendMessage(qbChatMessage);
                this.qbChatMessagesArrayList.add(qbChatMessage);
                QbDialogHolder.getInstance().updateDialog(qbChatMessage);
                doSendPushNotification();
                inititalizeAdapter();
                etMessage.setText("");
            } catch (SmackException.NotConnectedException e) {
                Logger.d(e.getMessage());
            }
        }
    }

    public void doSendPushNotification() {
        PushNotification.getInstance().sendPush(this, globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId, qbChatDialog.getDialogId(),
                etMessage.getText().toString().trim(), qbChatDialog.getName(),
                qbNotificationModel.userId, globals.getUserData().Data.quickBloxId, qbNotificationModel.requestId);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Bundle bundle = intent.getExtras();
        if (bundle != null && !dialog.isShowing()) {
            QBNotificationModel qbNotification = (QBNotificationModel) bundle.getSerializable(Constants.SBU_QBDetails);
            resetBadge();
            if (qbNotification.dialogId != null && !qbNotification.dialogId.equals("")) {
                if (!qbNotification.dialogId.equalsIgnoreCase(qbNotificationModel.dialogId)) {
                    qbNotificationModel = qbNotification;
                    updateChat();
                } else
                    Logger.e("no change required here");
            } else if (qbNotification.quickBloxId != qbNotificationModel.quickBloxId) {
                qbNotificationModel = qbNotification;
                updateChat();
            }
        }
    }

    void updateChat() {
        QBChatDialog qbChatDialog = QbDialogHolder.getInstance().getDialogFromOpponentId(qbNotificationModel.quickBloxId);
        if (qbChatDialog != null) {
            qbNotificationModel.dialogId = qbChatDialog.getDialogId();
        }
        qbChatMessagesArrayList.clear();
        if (chatMessageAdapter != null)
            chatMessageAdapter.notifyDataSetChanged();

        doGetRequestStatus(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ChatHelper.getInstance().setActiveChatDialog(qbChatDialog);
        resetBadge();
        registerBroadcast();
    }

    @Override
    protected void onPause() {
        super.onPause();
        ChatHelper.getInstance().resetActiveChatDialog();
        unregisterBroadcast();
    }

    @Override
    public void onBackPressed() {
        if (isTaskRoot()) {
            if (globals.getUserData().Data.userRole == UserRole.BUYER.getRole())
                startActivity(new Intent(this, UserLandingActivity.class));
            else
                startActivity(new Intent(this, SellerLandingActivity.class));
        }

        ChatHelper.getInstance().resetActiveChatDialog();
        unregisterBroadcast();
        finish();
    }


    @Override
    public void onRefresh() {
        getMessage(qbChatDialog, true);
    }

    void getMessage(QBChatDialog qbChatDialog, boolean forReferesh) {
        long dateSent = 0;
        if (qbChatMessagesArrayList.size() > 0)
            dateSent = qbChatMessagesArrayList.get(0).getDateSent();
        else
            forReferesh = false;

        ChatHelper.getInstance().getDialogMessage(qbChatDialog, forReferesh, dateSent, onQBChatMeesageListener);
    }

    private void stopRefreshing() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void doGetRequestStatus(final boolean initProcess) {
        if (!Globals.internetCheck(QBChatActivity.this) &&
                qbNotificationModel.requestId == null &&
                qbNotificationModel.requestId.isEmpty()) {
            Toaster.shortToast(R.string.error_went_wrong);
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().getServiceStatusParams(
                userModel.Data.userId, userModel.Data.sessionId,
                qbNotificationModel.requestId);

        HttpRequestHandler.getInstance().post(QBChatActivity.this,
                getString(R.string.getRequestStatus), params, new JsonHttpResponseHandler() {

                    @Override
                    public void onStart() {
                        super.onStart();
                        showDialog();
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Logger.json(response.toString());
                        DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                        if (dataModel.Result) {
                            if (dataModel.Data.status == serviceCompleted) {
                                hideDialog();
                                showCompletionDialog(dataModel.Message);
                            } else if (initProcess) {
                                initQBProcess();
                            }
                        } else {
                            hideDialog();
                            Toaster.shortToast(R.string.error_went_wrong);
                            onBackPressed();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        Toaster.shortToast(R.string.error_went_wrong);
                        onBackPressed();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        Toaster.shortToast(R.string.error_went_wrong);
                        onBackPressed();
                    }
                });
    }

    private void showCompletionDialog(String message) {
        new MaterialDialog.Builder(this)
                .content(message)
                .positiveText(getString(R.string.ok))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        removeBubble();
                        onBackPressed();
                    }
                })
                .cancelable(false)
                .show();
    }
}
