package com.shopbyuber.quickblox.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class Users implements Serializable {

    @SerializedName("Data")
    public List<Data> Data;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Result")
    public boolean Result;

    public static class Data {
        @SerializedName("profileImage")
        public String profileImage;
        @SerializedName("_id")
        public String _id;
        @SerializedName("quickBloxId")
        public int quickBloxId;
        @SerializedName("name")
        public String name;
    }
}
