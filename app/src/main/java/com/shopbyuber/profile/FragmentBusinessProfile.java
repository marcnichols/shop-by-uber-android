package com.shopbyuber.profile;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.ImageType;
import com.shopbyuber.enums.PaymentEnums;
import com.shopbyuber.model.BusinessProfileModel;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.model.StateListModel;
import com.shopbyuber.model.UserModel;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.CoreApp;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class FragmentBusinessProfile extends Fragment implements PermissionListener {
    private final String TAG = FragmentBusinessProfile.class.getName();

    Globals globals;

    PermissionListener permissionListener;

    File selectedFile = null;

    @BindView(R.id.img_profile)
    ImageView img_profile;

    @BindView(R.id.edt_owner)
    EditText edt_owner;

    @BindView(R.id.tv_location)
    TextView tv_location;

    @BindView(R.id.edt_phone)
    EditText edt_phone;

    @BindView(R.id.edt_description)
    EditText edt_description;

    @BindView(R.id.ratingBar)
    RatingBar ratingBar;

    @BindView(R.id.layout_list_review)
    LinearLayout layout_list_review;

    @BindView(R.id.tv_country_code)
    TextView tv_country_code;

    @BindView(R.id.txt_email)
    TextView txt_email;

    @BindView(R.id.edt_password)
    EditText edt_password;

    @BindView(R.id.edt_screen_name)
    EditText edt_screen_name;


    @BindView(R.id.edt_conf_password)
    EditText edt_conf_password;

    @BindView(R.id.edt_first_name)
    EditText edt_first_name;

    @BindView(R.id.edt_last_name)
    EditText edt_last_name;


    @BindView(R.id.edt_paypal_email)
    EditText edt_paypal_email;

    @BindView(R.id.ly_extra_field)
    LinearLayout lyExtraFeild;

    @BindView(R.id.ly_bank)
    LinearLayout lyBank;

    @BindView(R.id.ly_main)
    LinearLayout lyMain;

    @BindView(R.id.expand_payment_detail)
    ExpandableLayout expandPaymentDetail;


    @BindView(R.id.spn_payment_type)
    Spinner spnPaymentType;

    @BindView(R.id.cardview_paypal_email)
    CardView cardview_paypal_email;


    @BindView(R.id.edt_payment_fname)
    EditText edtPaymentFname;

    @BindView(R.id.edt_payment_lname)
    EditText edtPaymentLname;

    @BindView(R.id.tv_street_address)
    EditText tvStreetAddress;

    @BindView(R.id.spn_city)
    Spinner spnCity;

    @BindView(R.id.spn_state)
    Spinner spnState;

    @BindView(R.id.edt_zip_code)
    EditText edtZipCode;

    @BindView(R.id.edt_routing_number)
    EditText edtRoutingNumber;

    @BindView(R.id.edt_account_number)
    EditText edtAccountNumber;

    @BindView(R.id.edt_promo_code)
    EditText edtPromoCode;

    @BindView(R.id.tv_note)
    TextView tv_note;

    LatLng latLng;

    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    String img_url = "";
    BusinessProfileModel businessProfileModel;

    private StateListModel stateListModel;

    ArrayList<StateListModel.States> statesArrayList;

    private StateListModel cities;


    private ArrayAdapter<StateListModel.States> statesArrayAdapter;

    private String selectedState = "", selectedCity;
    private boolean isFirstTime = true, isFirstLoad = false;
    private int paymentMethod;

    public FragmentBusinessProfile() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_business_profile, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    private void init() {
        globals = (Globals) getActivity().getApplicationContext();
        permissionListener = this;
        statesArrayList = new ArrayList<>();

        edt_phone.addTextChangedListener(new TextWatcher() {
            private boolean mFormatting; // this is a flag which prevents the  stack overflow.
            private int mAfter;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                mAfter = after;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!mFormatting) {
                    mFormatting = true;
                    // using US formatting...
                    if (mAfter != 0) // in case back space ain't clicked...
                        PhoneNumberUtils.formatNumber(s, PhoneNumberUtils.getFormatTypeForLocale(Locale.US));
                    mFormatting = false;
                }
            }
        });

        doGetBusinessProfile();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            tv_note.setText(Html.fromHtml(getString(R.string.paypal_email_note), Html.FROM_HTML_MODE_COMPACT));
        else
            tv_note.setText(Html.fromHtml(getString(R.string.paypal_email_note)));

        spnState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedState = stateListModel.Data.States.get(position)._id;
                doGetCityList(stateListModel.Data.States.get(position)._id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCity = cities.Data.Cities.get(position)._id;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnPaymentType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (isFirstLoad) {
                    if (PaymentEnums.fromInt(position).getId() == PaymentEnums.SELECT_PAYMENT_TYPE.getId()) {

                        showConfirmChangePayment(position, true);

                    } else
                        showConfirmChangePayment(position, false);
                } else {

                    if (businessProfileModel.Data.paymentMethod == PaymentEnums.SELECT_PAYMENT_TYPE.getId()) {
                        doDisplaySelectedPaymentOption(position);
                        isFirstLoad = false;
                    } else {
                        doDisplaySelectedPaymentOption(position);
                        isFirstLoad = true;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void doGetBusinessProfile() {
        if (!Globals.internetCheck(getActivity()))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getBusinessProfileParams(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(getActivity());
        HttpRequestHandler.getInstance().post(getActivity(), getString(R.string.getBusinessProfile), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing() && !getActivity().isFinishing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                businessProfileModel = new Gson().fromJson(response.toString(), BusinessProfileModel.class);
                if (businessProfileModel.Result) {
                    setSpinnerAdapter();
                    if (paymentMethod == PaymentEnums.BANK_ACCOUNT.getId() || paymentMethod == PaymentEnums.PAY_BY_CHEQUE.getId())
                        doGetStateList();
                    setData(businessProfileModel.Data);
                    addReviewLayout(businessProfileModel.Data.serviceReviews);
                } else {
                    Toaster.shortToast(businessProfileModel.Message);
                    if (businessProfileModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(getActivity());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void setData(BusinessProfileModel.Data data) {
        latLng = new LatLng(data.latitude, data.longitude);
        img_url = data.companyProfileImage != null ? data.companyProfileImage : "";

        Glide.with(this)
                .load(img_url)
                .centerCrop()
                .placeholder(R.drawable.image_place_holder)
                .dontAnimate()
                .into(img_profile);

        edt_owner.setText(data.ownerName);
        tv_location.setText(data.companyAddress);

        ratingBar.setRating(data.overAllRating);
        tv_country_code.setText(data.countryCode);
        edt_phone.setText(data.companyPhoneNumber);

        txt_email.setText(data.vendorData.email);
        edt_screen_name.setText(data.comapnyScreenName);
        edt_first_name.setText(data.firstName == null ? "" : data.firstName.trim());
        edt_last_name.setText(data.lastName == null ? "" : data.lastName.trim());

        edtPromoCode.setText(data.promoCode == null ? "" : data.promoCode.trim());

        edt_paypal_email.setText(data.paypalEmail == null ? "" : data.paypalEmail.trim());
        setPaymentData(data.paymentDetail);
    }

    private void setPaymentData(BusinessProfileModel.PaymentDetail paymentDetail) {
        edtPaymentFname.setText(paymentDetail.firstName == null ? "" : paymentDetail.firstName.trim());
        edtPaymentLname.setText(paymentDetail.lastName == null ? "" : paymentDetail.lastName.trim());
        tvStreetAddress.setText(paymentDetail.streetAddress == null ? "" : paymentDetail.streetAddress.trim());
        edtZipCode.setText(paymentDetail.zipcode == null ? "" : paymentDetail.zipcode.trim());
        edtRoutingNumber.setText(paymentDetail.routingNumber == null ? "" : paymentDetail.routingNumber.trim());
        edtAccountNumber.setText(paymentDetail.accountNumber == null ? "" : paymentDetail.accountNumber.trim());
    }

    private void addReviewLayout(List<BusinessProfileModel.ServiceReviews> serviceReviews) {
        for (int i = 0; i < serviceReviews.size(); i++) {
            View layout2 = LayoutInflater.from(getActivity()).inflate(R.layout.review_list_item, layout_list_review, false);

            TextView tv_date = layout2.findViewById(R.id.tv_date);
            TextView tv_review = layout2.findViewById(R.id.tv_review);
            TextView tv_given_user = layout2.findViewById(R.id.tv_given_user);

            tv_date.setText(serviceReviews.get(i).feedbackDateTime != null ? serviceReviews.get(i).feedbackDateTime : "");
            tv_review.setText(serviceReviews.get(i).Feedback);
            tv_given_user.setText(serviceReviews.get(i).givenByUser != null && !serviceReviews.get(i).givenByUser.isEmpty() ? serviceReviews.get(i).givenByUser : "");
            layout_list_review.addView(layout2);
        }
    }

    @OnClick(R.id.img_profile)
    public void doClickProfile() {
        new TedPermission(getActivity())
                .setPermissionListener(permissionListener)
                .setRationaleMessage(R.string.camera_message)
                .setDeniedMessage(R.string.denied_message)
                .setGotoSettingButtonText(R.string.ok)
                .setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();
    }

    @Override
    public void onPermissionGranted() {
        EasyImage.openChooserWithGallery(this, getString(R.string.action_Pick_source), 0);
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                selectedFile = new File(result.getUri().getPath());
                Bitmap myBitmap = BitmapFactory.decodeFile(result.getUri().getPath());
                img_profile.setImageBitmap(myBitmap);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Logger.e(error.getMessage());
            }
        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(getActivity(), data);
                tvStreetAddress.setText(place.getAddress());
                latLng = place.getLatLng();
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                Logger.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        } else
            EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
                @Override
                public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                    selectedFile = null;
                    img_profile.setVisibility(View.GONE);
                }

                @Override
                public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                    if (imageFile.exists()) {
                        if (!imageFile.getAbsolutePath().substring(imageFile.getAbsolutePath().lastIndexOf(".")).equalsIgnoreCase(".gif")) {
                            startCropper(imageFile.getPath());
                        } else {
                            Toaster.shortToast(R.string.file_type_not_supported);
                        }
                    }
                }

                @Override
                public void onCanceled(EasyImage.ImageSource source, int type) {
                    //Cancel handling, you might wanna remove taken photo if it was canceled
                    if (source == EasyImage.ImageSource.CAMERA) {
                        File photoFile = EasyImage.lastlyTakenButCanceledPhoto(getActivity());
                        if (photoFile != null) photoFile.delete();
                    }
                }
            });
    }

    private void startCropper(String path) {
        CropImage.activity(Uri.fromFile(new File(path)))
                .setAspectRatio(4, 3)
                .setAllowFlipping(false)
                .setAllowRotation(false)
                .setOutputCompressQuality(60)
                .setOutputCompressFormat(Bitmap.CompressFormat.JPEG)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(getContext(), this);
    }

    @OnClick(R.id.btn_edit_profile)
    public void doEditProfile() {
        if (validate()) {
            if (selectedFile != null) {
                doRequestForUploadImage();
            } else {
                doUpdateBusinessProfile("");
            }
        }
    }

    public boolean validate() {
        if (selectedFile == null && img_url.isEmpty()) {
            Toaster.shortToast(R.string.err_add_profile_image);
            return false;
        }

        if (edt_first_name.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_first_name);
            return false;
        }
        if (edt_last_name.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_last_name);
            return false;
        }
        if (edt_screen_name.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_ez_list_screen_name);
            return false;
        }
        if (spnPaymentType.getSelectedItemPosition() == PaymentEnums.PAYPAL.getId()) {
            if (edt_paypal_email.getText().toString().trim().isEmpty()) {
                Toaster.shortToast(R.string.err_enter_paypal_email);
                return false;
            }
            if (!Globals.validateEmailAddress(edt_paypal_email.getText().toString().trim())) {
                Toaster.shortToast(R.string.err_enter_valid_paypal_email);
                return false;
            }
        } else if (spnPaymentType.getSelectedItemPosition() == PaymentEnums.BANK_ACCOUNT.getId()) {

            boolean isValidBank = validPaymentDetails(true);
            if (isValidBank)
                return validatePhoneNPassword();
            else return isValidBank;

        } else if (spnPaymentType.getSelectedItemPosition() == PaymentEnums.PAY_BY_CHEQUE.getId()) {

            boolean isValidBank = validPaymentDetails(false);
            if (isValidBank)
                return validatePhoneNPassword();
            else return isValidBank;
        }
        return true;
    }

    private boolean validatePhoneNPassword() {
        if (edt_phone.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.please_enter_phone_number);
            return false;
        }
        if (edt_phone.getText().toString().trim().length() < 10) {
            Toaster.shortToast(R.string.please_enter_valid_phone_number);
            return false;
        }
        if (!edt_password.getText().toString().trim().isEmpty()) {
            if (edt_password.getText().toString().trim().isEmpty()) {
                Toaster.shortToast(R.string.err_enter_password);
                return false;
            }
            if (edt_conf_password.getText().toString().trim().isEmpty()) {
                Toaster.shortToast(R.string.err_enter_confirm_password);
                return false;
            }
            if (!edt_password.getText().toString().trim().equalsIgnoreCase(edt_conf_password.getText().toString().trim())) {
                Toaster.shortToast(R.string.err_confirm_password_should_be_same_as_password);
                return false;
            }
        }
        if (edt_password.getText().toString().trim().isEmpty() && !edt_conf_password.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_password);
            return false;
        }
        return true;
    }

    private boolean validPaymentDetails(boolean isFromBank) {
        if (edtPaymentFname.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_first_name);
            return false;
        }
        if (edtPaymentLname.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_last_name);
            return false;
        }

        if (tvStreetAddress.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_street_addrss);
            return false;
        }
        if (edtZipCode.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_zip_code);
            return false;
        }
        if (edtZipCode.getText().toString().trim().length() < 5) {
            Toaster.shortToast(R.string.err_zip_code_should_5_digit);
            return false;
        }
        if (isFromBank) {
            if (edtRoutingNumber.getText().toString().trim().isEmpty()) {
                Toaster.shortToast(R.string.err_enter_routing_number);
                return false;
            }
            if (edtAccountNumber.getText().toString().trim().isEmpty()) {
                Toaster.shortToast(R.string.err_enter_account_number);
                return false;
            }
        }
        return true;
    }

    public void doRequestForUploadImage() {
        if (!Globals.internetCheck(getActivity()))
            return;

        RequestParams params = HttpRequestHandler.getInstance().getUploadCompanyProfileImageParams(selectedFile, ImageType.COMPANY_PROFILE.getType());

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(getActivity());
        HttpRequestHandler.getInstance().postFile(getActivity(), getString(R.string.uploadProfileImage), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel.Result) {
                    doUpdateBusinessProfile(dataModel.Data.fileName);
                } else {
                    Toaster.shortToast(dataModel.Message);
                    if (dataModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(getActivity());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    public void doUpdateBusinessProfile(String fileName) {
        if (!Globals.internetCheck(getActivity()))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getUpdateBusinessProfileParams(
                globals.getUserData().Data.sessionId,
                globals.getUserData().Data.userId, // vendorId = UserId
                businessProfileModel.Data.companyId,
                "",
                edt_screen_name.getText().toString().trim(),
                edt_first_name.getText().toString().trim() + " " + edt_last_name.getText().toString().trim(),
                tv_location.getText().toString().trim(),
                globals.getPostal(latLng),
                businessProfileModel.Data.latitude, //21.187392,
                businessProfileModel.Data.longitude, // 72.810596 ,
                tv_country_code.getText().toString(),
                edt_phone.getText().toString().trim().replace(getString(R.string.service_details_null), ""),
                edt_phone.getText().toString().trim(),
                "",
                "",
                fileName, setVendorData(),
                paymentMethod != PaymentEnums.PAYPAL.getId() ? "" : edt_paypal_email.getText().toString().trim(),
                edt_first_name.getText().toString().trim(),
                edt_last_name.getText().toString().trim(), paymentMethod, setPaymentDetail(), edtPromoCode.getText().toString().trim());

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(getActivity());
        HttpRequestHandler.getInstance().post(getActivity(), getString(R.string.updateBusinessProfile), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                BusinessProfileModel businessProfileModel = new Gson().fromJson(response.toString(), BusinessProfileModel.class);
                Toaster.shortToast(businessProfileModel.Message);
                if (!businessProfileModel.Result) {
                    if (businessProfileModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(getActivity());
                } else {
                    UserModel userModel = globals.getUserData();
                    userModel.Data.promoCode = edtPromoCode.getText().toString().trim();
                    globals.setUserData(userModel);

                    CoreApp.getInstance().initializeBubblesManager();
                    edt_password.setText("");
                    edt_conf_password.setText("");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private JSONObject setPaymentDetail() {
        JSONObject jsnPaymentDetail = new JSONObject();
        try {
            jsnPaymentDetail.put(Constants.SBU_FirstName, edtPaymentFname.getText().toString().trim());
            jsnPaymentDetail.put(Constants.SBU_LastName, edtPaymentLname.getText().toString().trim());
            jsnPaymentDetail.put(Constants.SBU_StreetAddress, tvStreetAddress.getText().toString().trim());
            jsnPaymentDetail.put(Constants.SBU_City, selectedCity);
            jsnPaymentDetail.put(Constants.SBU_State, selectedState);
            jsnPaymentDetail.put(Constants.SBU_PaymentZipCode, edtZipCode.getText().toString().trim());
            if (paymentMethod == PaymentEnums.BANK_ACCOUNT.getId()) {
                jsnPaymentDetail.put(Constants.SBU_RoutingNumber, edtRoutingNumber.getText().toString().trim());
                jsnPaymentDetail.put(Constants.SBU_AccountNumber, edtAccountNumber.getText().toString());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsnPaymentDetail;
    }

    private JSONObject setVendorData() {
        JSONObject jsnVendorData = new JSONObject();
        try {
            jsnVendorData.put(Constants.SBU_Email, businessProfileModel.Data.vendorData.email);
            jsnVendorData.put(Constants.SBU_UserRole, businessProfileModel.Data.vendorData.userRole);
            jsnVendorData.put(Constants.SBU_TimeZone, TimeZone.getDefault().getID());
            jsnVendorData.put(Constants.SBU_Description, /*edt_experience.getText().toString().trim()*/"");
            jsnVendorData.put(Constants.SBU_Speciality,/* edt_speciality.getText().toString().trim()*/"");
            jsnVendorData.put(Constants.SBU_Password, edt_password.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsnVendorData;
    }


    private void setSpinnerAdapter() {
        spnPaymentType.setAdapter(new ArrayAdapter<PaymentEnums>(getActivity(), R.layout.layout_spinner_row_payment, R.id.tv_payment_selection, PaymentEnums.values()));
        for (int i = 0; i < PaymentEnums.values().length; i++) {
            if (businessProfileModel.Data.paymentMethod == PaymentEnums.fromInt(i).getId()) {
                spnPaymentType.setSelection(i);
            }
        }
    }

    private void setExpandLayoutPaymentDetails(boolean isExpand) {
        expandPaymentDetail.setExpanded(isExpand);
    }

    public void doGetStateList() {
        if (!Globals.internetCheck(getActivity()))
            return;


        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(getActivity());
        HttpRequestHandler.getInstance().get(getActivity(), getString(R.string.getStatesList), new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                stateListModel = new Gson().fromJson(response.toString(), StateListModel.class);
                if (stateListModel.Result) {
                    statesArrayList.addAll(stateListModel.Data.States);
                    doDisplayStateList(statesArrayList);
                } else {
                    Toaster.shortToast(stateListModel.Message);
                    if (stateListModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(getActivity());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void doDisplayStateList(ArrayList<StateListModel.States> stateListModelArrayList) {
        statesArrayAdapter = new ArrayAdapter<>(getActivity(), R.layout.layout_spinner_row_payment, R.id.tv_payment_selection, stateListModelArrayList);
        spnState.setAdapter(statesArrayAdapter);

        if (businessProfileModel.Data.paymentDetail.state != null && !stateListModelArrayList.isEmpty()) {
            for (int i = 0; i < stateListModelArrayList.size(); i++) {
                if (stateListModelArrayList.get(i)._id.equals(businessProfileModel.Data.paymentDetail.state)) {
                    spnState.setSelection(i);
                }
            }
        }
    }

    private void doGetCityList(String _id) {
        if (!Globals.internetCheck(getActivity()))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getCityListParam(_id);
        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(getActivity());
        HttpRequestHandler.getInstance().post(getActivity(), getString(R.string.getCitiesList), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                cities = new Gson().fromJson(response.toString(), StateListModel.class);
                if (cities.Result) {
                    doDisplayCityList(cities);
                } else {
                    Toaster.shortToast(cities.Message);
                    if (cities.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(getActivity());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void doDisplayCityList(StateListModel citiesModel) {
        if (citiesModel.Data.Cities != null) {
            ArrayAdapter<StateListModel.Cities> dataAdapter = new ArrayAdapter<StateListModel.Cities>(getActivity(), R.layout.layout_spinner_row_payment, R.id.tv_payment_selection, citiesModel.Data.Cities);
            spnCity.setAdapter(dataAdapter);

            if (isFirstTime) {
                if (businessProfileModel.Data.paymentDetail.city != null && citiesModel.Data != null) {
                    for (int i = 0; i < citiesModel.Data.Cities.size(); i++) {
                        if (citiesModel.Data.Cities.get(i)._id.equals(businessProfileModel.Data.paymentDetail.city)) {
                            spnCity.setSelection(i);
                            isFirstTime = false;
                        }
                    }
                }
            }
        }
    }

    private void showConfirmChangePayment(final int position, boolean isFromRemove) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle(getActivity().getString(R.string.sure));
        alertDialog.setMessage(getActivity().getString(isFromRemove ? R.string.you_want_to_remove_your_payment_option : R.string.you_want_to_change_your_payment_option));
        alertDialog.setCancelable(false);
        // On pressing Settings button
        alertDialog.setPositiveButton(getActivity().getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                doDisplaySelectedPaymentOption(position);
            }
        });

        alertDialog.setNegativeButton(getActivity().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                isFirstLoad = false;
                spnPaymentType.setSelection(paymentMethod);
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    // display payment layout as per selected payment type
    private void doDisplaySelectedPaymentOption(int position) {
        PaymentEnums paymentEnums = PaymentEnums.fromInt(position);
        switch (paymentEnums) {
            case SELECT_PAYMENT_TYPE:
                paymentMethod = paymentEnums.getId();
                setExpandLayoutPaymentDetails(false);
                break;
            case PAYPAL:
                paymentMethod = paymentEnums.getId();
                setExpandLayoutPaymentDetails(true);
                cardview_paypal_email.setVisibility(View.VISIBLE);
                lyBank.setVisibility(View.GONE);
                tv_note.setVisibility(View.VISIBLE);
                break;
            case BANK_ACCOUNT:
                paymentMethod = paymentEnums.getId();
                if (statesArrayList.isEmpty())
                    doGetStateList();
                setExpandLayoutPaymentDetails(true);
                cardview_paypal_email.setVisibility(View.GONE);
                lyBank.setVisibility(View.VISIBLE);
                lyExtraFeild.setVisibility(View.VISIBLE);
                tv_note.setVisibility(View.GONE);
                break;
            case PAY_BY_CHEQUE:
                paymentMethod = paymentEnums.getId();
                if (statesArrayList.isEmpty())
                    doGetStateList();
                setExpandLayoutPaymentDetails(true);
                cardview_paypal_email.setVisibility(View.GONE);
                lyBank.setVisibility(View.VISIBLE);
                lyExtraFeild.setVisibility(View.GONE);
                tv_note.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

}
