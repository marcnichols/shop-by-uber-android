package com.shopbyuber.profile;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.UserRole;
import com.shopbyuber.model.FavouritesOfUserModel;
import com.shopbyuber.model.FavouritesOfUserModel.Favourites;
import com.shopbyuber.model.SalesListModel;
import com.shopbyuber.model.SalesListModel.Sales;
import com.shopbyuber.model.UserModel;
import com.shopbyuber.seller.AdapterFavouriteOfUser;
import com.shopbyuber.seller.AdapterMySales;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

public class FragmentMyProfile extends Fragment implements AdapterView.OnItemClickListener {
    @BindView(R.id.profile_img)
    CircleImageView profile_img;

    @BindView(R.id.profile_name)
    TextView profile_name;

    @BindView(R.id.description)
    TextView description;

    @BindView(R.id.list)
    RecyclerView recyclerView;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tv_no_record_found)
    TextView tv_no_record_found;

    @BindView(R.id.tv_recent_service)
    TextView tvRecentService;

    SalesListModel salesListModel;
    ArrayList<Sales> arrSales;
    AdapterMySales adapterMySales;

    FavouritesOfUserModel favouritesOfUserModel;
    ArrayList<Favourites> arrFavourite;
    AdapterFavouriteOfUser adapterFavourite;

    Globals globals;
    UserModel userModel;

    public static FragmentMyProfile mContext;

    public FragmentMyProfile() {
        // Required empty public constructor
    }

    public static FragmentMyProfile getInstance() {
        return mContext;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_profile, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        if (globals == null)
            globals = (Globals) getActivity().getApplicationContext();
        userModel = globals.getUserData();
        arrSales = new ArrayList<>();
        arrFavourite = new ArrayList<>();
        getProfileDataRequest();
    }

    @Override
    public void onResume() {
        super.onResume();
        showData();
    }

    private void showData() {
        mContext = this;
        if (globals == null)
            globals = (Globals) getActivity().getApplicationContext();
        userModel = globals.getUserData();
        Glide.with(getActivity())
                .load(userModel.Data.profileImage == null ? "" : userModel.Data.profileImage)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .centerCrop()
                .placeholder(R.drawable.profile_placeholder)
                .crossFade()
                .dontAnimate()
                .into(profile_img);
        profile_name.setText(userModel.Data.name == null ? "" : userModel.Data.name);
        if (userModel.Data.description == null ||
                userModel.Data.description.equalsIgnoreCase(""))
            description.setVisibility(View.GONE);
        else {
            description.setText(userModel.Data.description);
            description.setVisibility(View.VISIBLE);
        }
    }

    private void setUpData() {
        checkUserForRequestService();
    }

    public void getProfileDataRequest() {
        if (!Globals.internetCheck(getActivity()))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getServiceCatagoryParams(
                userModel.Data.userId,
                userModel.Data.sessionId);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(getActivity());

        HttpRequestHandler.getInstance().post(getActivity(), getString(R.string.getEditUserData), params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                UserModel userModel = new Gson().fromJson(response.toString(), UserModel.class);
                if (userModel.Result) {
                    globals.setUserData(userModel);
                    showData();
                    setUpData();
                } else {
                    showNoRecordFound();
                    Toaster.shortToast(userModel.Message);
                    if (userModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(getActivity());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    public void checkUserForRequestService() {
        tvRecentService.setVisibility(View.VISIBLE);
        if (userModel.Data.userRole == UserRole.SELLER.getRole()) {
            doGetRecentServiceRequest(true);
        } else {
            tvRecentService.setText(R.string.recent_favorites);
            doGetRecentServiceRequest(false);
        }
    }

    public void doGetRecentServiceRequest(final boolean isSeller) {
        if (!Globals.internetCheck(getActivity())) {
            showNoRecordFound();
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().getRecentServiceRequestParam(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId);

        HttpRequestHandler.getInstance().post(getActivity(), isSeller ? getString(R.string.getRecentServices) : getString(R.string.getFavouritesOfUser), params,
                new JsonHttpResponseHandler() {

                    @Override
                    public void onStart() {
                        super.onStart();
                        progressBar.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        if (progressBar.getVisibility() == View.VISIBLE)
                            progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Logger.json(response.toString());
                        if (isSeller) {
                            salesListModel = new Gson().fromJson(response.toString(), SalesListModel.class);
                            if (salesListModel.Result) {
                                setupMySalesList(salesListModel.Data.recentServices);
                            } else {
                                showNoRecordFound();
                                if (salesListModel.Message.contains(getString(R.string.invalid_session)))
                                    globals.doLogout(getActivity());
                            }
                        } else {
                            favouritesOfUserModel = new Gson().fromJson(response.toString(), FavouritesOfUserModel.class);
                            if (favouritesOfUserModel.Result) {
                                setupFavouriteList(favouritesOfUserModel.Data.favourites);
                            } else {
                                showNoRecordFound();
                                if (favouritesOfUserModel.Message.contains(getString(R.string.invalid_session)))
                                    globals.doLogout(getActivity());
                            }
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        showNoRecordFound();
                        Toaster.shortToast(R.string.error_went_wrong);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        showNoRecordFound();
                        Toaster.shortToast(R.string.error_went_wrong);
                    }
                });
    }

    @OnClick(R.id.edit_prfole)
    public void editProfile() {
        Intent intent;
        if (userModel.Data.userRole == UserRole.BUYER.getRole())
            intent = new Intent(getActivity(), EditBuyerProfile.class);
        else
            intent = new Intent(getActivity(), EditSellerProfile.class);
        startActivity(intent);
    }

    private void showNoRecordFound() {
        if (userModel.Data.userRole == UserRole.SELLER.getRole()) {
            if (arrSales.isEmpty()) {
                if (tv_no_record_found.getVisibility() == View.GONE)
                    tv_no_record_found.setText(getString(R.string.you_do_not_have_any_sales));
                tv_no_record_found.setVisibility(View.VISIBLE);
            }
        } else {
            if (arrFavourite.isEmpty()) {
                if (tv_no_record_found.getVisibility() == View.GONE)
                    tv_no_record_found.setText(getString(R.string.you_do_not_have_any_favorites));
                tv_no_record_found.setVisibility(View.VISIBLE);
            }
        }
    }

    private void hideNoRecordFound() {
        if (tv_no_record_found.getVisibility() == View.VISIBLE)
            tv_no_record_found.setVisibility(View.GONE);
    }

    //sales list if seller login
    private void setupMySalesList(List<Sales> servicesArrayList) {
        if (servicesArrayList != null && !servicesArrayList.isEmpty()) {
            arrSales.addAll(servicesArrayList);
            setMySalesAdapter();
        } else
            showNoRecordFound();
    }

    private void setMySalesAdapter() {
        hideNoRecordFound();
        if (adapterMySales == null) {
            adapterMySales = new AdapterMySales(getActivity(), true);
            adapterMySales.setOnItemClickListener(this);
        }
        adapterMySales.doRefresh(arrSales);

        if (recyclerView.getAdapter() == null) {
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapterMySales);
        }
    }

    //favorite list if buyer login
    private void setupFavouriteList(List<Favourites> favouritesArrayList) {
        if (favouritesArrayList != null && !favouritesArrayList.isEmpty()) {
            arrFavourite.addAll(favouritesArrayList);
            setFavouriteAdapter();
        } else
            showNoRecordFound();
    }

    private void setFavouriteAdapter() {
        hideNoRecordFound();
        if (adapterFavourite == null) {
            adapterFavourite = new AdapterFavouriteOfUser(getActivity());
            adapterFavourite.setOnItemClickListener(this);
        }
        adapterFavourite.doRefresh(arrFavourite);

        if (recyclerView.getAdapter() == null) {
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapterFavourite);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}
