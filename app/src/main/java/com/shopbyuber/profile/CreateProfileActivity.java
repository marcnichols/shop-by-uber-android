package com.shopbyuber.profile;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.AddServiceFrom;
import com.shopbyuber.enums.ImageType;
import com.shopbyuber.enums.PaymentEnums;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.model.StateListModel;
import com.shopbyuber.model.UserModel;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;


public class CreateProfileActivity extends BaseActivity implements PermissionListener {

    private final String TAG = CreateProfileActivity.class.getName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_title)
    TextView toolbar_title;

    @BindView(R.id.profile_img)
    ImageView profile_img;

    @BindView(R.id.txt_pick_img)
    TextView txt_pick_img;

    @BindView(R.id.rel_img)
    RelativeLayout rel_img;

    @BindView(R.id.lnr_pick_img)
    LinearLayout lnr_pick_img;

    @BindView(R.id.edt_company_owners_name)
    EditText edt_company_owners_name;

    @BindView(R.id.edt_screen_name)
    EditText edt_screen_name;

    @BindView(R.id.tv_company_address)
    TextView tv_company_address;

    @BindView(R.id.edt_phone_number)
    EditText edt_phone_number;

    @BindView(R.id.edt_description_of_services)
    EditText edt_description_of_services;

    @BindView(R.id.tv_service_provider)
    TextView tv_service_provider;

    @BindView(R.id.img_provider)
    ImageView img_provider;

    @BindView(R.id.tv_country_code)
    TextView tv_country_code;

    @BindView(R.id.edt_first_name)
    EditText edt_first_name;

    @BindView(R.id.edt_last_name)
    EditText edt_last_name;

    @BindView(R.id.edt_paypal_email)
    EditText edt_paypal_email;

    @BindView(R.id.ly_extra_field)
    LinearLayout lyExtraFeild;

    @BindView(R.id.ly_bank)
    LinearLayout lyBank;

    @BindView(R.id.ly_main)
    LinearLayout lyMain;

    @BindView(R.id.expand_payment_detail)
    ExpandableLayout expandPaymentDetail;

    @BindView(R.id.spn_payment_type)
    Spinner spnPaymentType;

    @BindView(R.id.cardview_paypal_email)
    CardView cardview_paypal_email;

    @BindView(R.id.edt_payment_fname)
    EditText edtPaymentFname;

    @BindView(R.id.edt_payment_lname)
    EditText edtPaymentLname;

    @BindView(R.id.tv_street_address)
    EditText tvStreetAddress;

    @BindView(R.id.spn_city)
    Spinner spnCity;

    @BindView(R.id.spn_state)
    Spinner spnState;

    @BindView(R.id.edt_zip_code)
    EditText edtZipCode;

    @BindView(R.id.edt_routing_number)
    EditText edtRoutingNumber;

    @BindView(R.id.edt_city)
    AutoCompleteTextView auto_tv_city;

    @BindView(R.id.edt_state)
    AutoCompleteTextView auto_tv_state;

    @BindView(R.id.edt_account_number)
    EditText edtAccountNumber;

    @BindView(R.id.tv_note)
    TextView tv_note;

    @BindView(R.id.cardview_state)
    CardView cardview_state;

    @BindView(R.id.cardview_city)
    CardView cardview_city;

    private StateListModel stateListModel;
    ArrayList<StateListModel.States> statesArrayList;
    ArrayList<StateListModel.Cities> cityArraylist;

    private StateListModel cities;

    private ArrayAdapter<StateListModel.States> statesArrayAdapter;

    private String selectedState = "", selectedCity;
    private boolean isFirstGetCity = true;
    private int paymentMethod;

    PermissionListener permissionListener;
    File selectedFile = null;
    Globals globals;

    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    LatLng latLng;
    private String ownerName = "", companyName = "", phoneNumber = "", countryCode = "", ezListScreenName = "", address = "", profileImage, state = "", city = "";
    double latitude, longitude;
    UserModel userModel;

    EditText edt_promo_code;
    CheckBox chk_tnc;
    String promoCode = "";
    Dialog dialogPromoCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_profile);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(false);   //disable back button
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            toolbar_title.setText(R.string.create_service_profile);
        }
        globals = (Globals) getApplicationContext();

        permissionListener = this;
        statesArrayList = new ArrayList<>();
        cityArraylist = new ArrayList<>();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            ownerName = bundle.getString(Constants.SBU_OwnerName);
            companyName = bundle.getString(Constants.SBU_CompanyName);
            phoneNumber = bundle.getString(Constants.SBU_MobilePhone);
            countryCode = bundle.getString(Constants.SBU_CountryCode);
        }

        userModel = globals.getUserData();
        if (userModel != null && userModel.Data != null) {
            ezListScreenName = userModel.Data.ezListScreenName;
            address = userModel.Data.address;
            latitude = userModel.Data.latitude;
            longitude = userModel.Data.longitude;
            profileImage = userModel.Data.profileImage;
            promoCode = userModel.Data.isSalesperson ? userModel.Data.promoCode : "";
            state = userModel.Data.state;
            city = userModel.Data.city;
        }


        edt_phone_number.addTextChangedListener(new TextWatcher() {
            private boolean mFormatting; // this is a flag which prevents the  stack overflow.
            private int mAfter;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                mAfter = after;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!mFormatting) {
                    mFormatting = true;
                    // using US formatting...
                    if (mAfter != 0) // in case back space ain't clicked...
                        PhoneNumberUtils.formatNumber(s, PhoneNumberUtils.getFormatTypeForLocale(Locale.US));
                    mFormatting = false;
                }
            }
        });

        //prepopulate screen based on register info
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            tv_note.setText(Html.fromHtml(getString(R.string.paypal_email_note), Html.FROM_HTML_MODE_COMPACT));
        else
            tv_note.setText(Html.fromHtml(getString(R.string.paypal_email_note)));

        edt_company_owners_name.setText(ownerName);
        edt_phone_number.setText(phoneNumber);
        tv_country_code.setText(countryCode);

        setSpinnerAdapter();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            tv_note.setText(Html.fromHtml(getString(R.string.paypal_email_note), Html.FROM_HTML_MODE_COMPACT));
        else
            tv_note.setText(Html.fromHtml(getString(R.string.paypal_email_note)));

        spnState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedState = stateListModel.Data.States.get(position)._id;
                doGetCityList(stateListModel.Data.States.get(position)._id, false, isFirstGetCity);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCity = cities.Data.Cities.get(position)._id;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnPaymentType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                doDisplaySelectedPaymentOption(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        edt_screen_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                tv_service_provider.setText(edt_screen_name.getText().toString().trim());
            }
        });

        edt_screen_name.setText(ezListScreenName == null ? "" : ezListScreenName.trim());
        edt_first_name.setText(userModel.Data.firstName == null ? "" : userModel.Data.firstName.trim());
        edt_last_name.setText(userModel.Data.lastName == null ? "" : userModel.Data.lastName.trim());
        loadProfileImage(profileImage.trim());

        //display promo code dialog after api call
        if (state == null || state.isEmpty() || city == null || city.isEmpty()) {
            cardview_state.setVisibility(View.VISIBLE);
            cardview_city.setVisibility(View.VISIBLE);
            doGetStateList(true);
        } else showPromoCode();

        auto_tv_state.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                StateListModel.States stateListModel = (StateListModel.States) parent.getAdapter().getItem(position);
                doGetCityList(stateListModel._id, true, false);
            }
        });

    }

    private void showPromoCode() {
        if (!userModel.Data.isSalesperson)
            showPromoConfirmDialog();
    }

    @Override
    public void onBackPressed() {
        Toaster.shortToast(R.string.must_create_profile);
    }

    @OnClick(R.id.btn_submit_profile)
    public void onSubmitProfile() {
        if (validation())
            doRequestForUploadImage();
    }

    public boolean validation() {
        if (selectedFile == null) {
            Toaster.shortToast(R.string.err_add_profile_image);
            return false;
        }
        if (edt_first_name.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_first_name);
            return false;
        }
        if (edt_last_name.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_last_name);
            return false;
        }
        if (edt_screen_name.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_ez_list_screen_name);
            return false;
        }
        if (edt_phone_number.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_company_phone_number);
            return false;
        }
        if (edt_phone_number.getText().toString().trim().length() < 10) {
            Toaster.shortToast(R.string.err_enter_valid_company_phone_number);
            return false;
        }
        if ((state == null || state.isEmpty()) && auto_tv_state.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_please_enter_state_name);
            return false;
        }
        if ((state == null || state.isEmpty()) && auto_tv_city.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_please_enter_city_name);
            return false;
        }
        if (spnPaymentType.getSelectedItemPosition() == PaymentEnums.PAYPAL.getId()) {
            if (edt_paypal_email.getText().toString().trim().isEmpty()) {
                Toaster.shortToast(R.string.err_enter_paypal_email);
                return false;
            }
            if (!Globals.validateEmailAddress(edt_paypal_email.getText().toString().trim())) {
                Toaster.shortToast(R.string.err_enter_valid_paypal_email);
                return false;
            }
        } else if (spnPaymentType.getSelectedItemPosition() == PaymentEnums.BANK_ACCOUNT.getId()) {

            return validPaymentDetails(true);

        } else if (spnPaymentType.getSelectedItemPosition() == PaymentEnums.PAY_BY_CHEQUE.getId()) {

            return validPaymentDetails(false);
        }
        return true;
    }

    private boolean validPaymentDetails(boolean isFromBank) {
        if (edtPaymentFname.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_first_name);
            return false;
        }
        if (edtPaymentLname.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_last_name);
            return false;
        }

        if (tvStreetAddress.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_street_addrss);
            return false;
        }
        if (edtZipCode.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_zip_code);
            return false;
        }
        if (edtZipCode.getText().toString().trim().length() < 5) {
            Toaster.shortToast(R.string.err_zip_code_should_5_digit);
            return false;
        }
        if (isFromBank) {
            if (edtRoutingNumber.getText().toString().trim().isEmpty()) {
                Toaster.shortToast(R.string.err_enter_routing_number);
                return false;
            }
            if (edtAccountNumber.getText().toString().trim().isEmpty()) {
                Toaster.shortToast(R.string.err_enter_account_number);
                return false;
            }
        }
        return true;
    }

    @OnClick({R.id.lnr_pick_img, R.id.txt_pick_img})
    public void onAddProfile() {
        new TedPermission(this)
                .setPermissionListener(permissionListener)
                .setRationaleMessage(R.string.camera_message)
                .setDeniedMessage(R.string.denied_message)
                .setGotoSettingButtonText(R.string.ok)
                .setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();
    }

    @Override
    public void onPermissionGranted() {
        EasyImage.openChooserWithGallery(this, getString(R.string.action_Pick_source), 0);
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                tvStreetAddress.setText(place.getAddress());
                latLng = place.getLatLng();
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Logger.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                selectedFile = new File(result.getUri().getPath());
                lnr_pick_img.setVisibility(View.GONE);
                txt_pick_img.setVisibility(View.VISIBLE);
                rel_img.setVisibility(View.VISIBLE);
                loadProfileImage(result.getUri().getPath());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Logger.e(error.getMessage());
            }
        } else
            EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
                @Override
                public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {

                }

                @Override
                public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                    if (imageFile.exists()) {
                        if (!imageFile.getAbsolutePath().substring(imageFile.getAbsolutePath().lastIndexOf(".")).equalsIgnoreCase(".gif")) {
                            startCropper(imageFile.getPath());
                        } else {
                            Toaster.shortToast(R.string.file_type_not_supported);
                        }
                    }
                }

                @Override
                public void onCanceled(EasyImage.ImageSource source, int type) {
                    //Cancel handling, you might wanna remove taken photo if it was canceled
                    if (source == EasyImage.ImageSource.CAMERA) {
                        File photoFile = EasyImage.lastlyTakenButCanceledPhoto(CreateProfileActivity.this);
                        if (photoFile != null) photoFile.delete();
                    }
                }
            });
    }

    private void startCropper(String path) {
        CropImage.activity(Uri.fromFile(new File(path)))
                .setAspectRatio(4, 3)
                .setAllowFlipping(false)
                .setAllowRotation(false)
                .setOutputCompressQuality(60)
                .setOutputCompressFormat(Bitmap.CompressFormat.JPEG)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }

    private void loadProfileImage(String path) {
        Glide.with(this)
                .load(path)
                .centerCrop()
                .placeholder(R.drawable.image_place_holder)
                .dontAnimate()
                .into(profile_img);
        Glide.with(this)
                .load(path)
                .centerCrop()
                .placeholder(R.drawable.image_place_holder)
                .dontAnimate()
                .into(img_provider);
    }

    public void doRequestForUploadImage() {
        if (!Globals.internetCheck(CreateProfileActivity.this))
            return;

        RequestParams params = HttpRequestHandler.getInstance().getUploadCompanyProfileImageParams(selectedFile,
                ImageType.COMPANY_PROFILE.getType());

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(CreateProfileActivity.this);
        HttpRequestHandler.getInstance().postFile(CreateProfileActivity.this, getString(R.string.uploadProfileImage), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel.Result) {
                    doAddCompanyProfileRequest(dataModel.Data.fileName);
                } else {
                    Toaster.shortToast(dataModel.Message);
                    if (dataModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(CreateProfileActivity.this);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    public void doAddCompanyProfileRequest(String fileName) {
        if (!Globals.internetCheck(CreateProfileActivity.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getAddCompanyProfileParams(
                globals.getUserData().Data.sessionId,
                globals.getUserData().Data.userId, // vendorId = UserId
                "",
                edt_screen_name.getText().toString().trim(),
                edt_first_name.getText().toString().trim() + " " + edt_last_name.getText().toString().trim(),
                address,
                globals.getPostal(new LatLng(latitude, longitude)),
                latitude, //21.187392,
                longitude, // 72.810596 ,
                tv_country_code.getText().toString(),
                edt_phone_number.getText().toString().trim().replace(getString(R.string.service_details_null), ""),
                "",
                "",
                fileName,
                paymentMethod != PaymentEnums.PAYPAL.getId() ? "" : edt_paypal_email.getText().toString().trim(),
                edt_first_name.getText().toString().trim(),
                edt_last_name.getText().toString().trim(),
                paymentMethod,
                setPaymentDetail(), promoCode,
                state == null || state.isEmpty() ? auto_tv_state.getText().toString().trim() : state,
                state == null || state.isEmpty() ? auto_tv_city.getText().toString().trim() : city);
        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(CreateProfileActivity.this);

        HttpRequestHandler.getInstance().post(CreateProfileActivity.this, getString(R.string.addCompanyProfile), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel.Result) {
                    UserModel userModel = globals.getUserData();
                    userModel.Data.promoCode = promoCode;
                    globals.setUserData(userModel);
                    gotoAddNewServie();
                } else {
                    Toaster.shortToast(dataModel.Message);
                    if (dataModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(CreateProfileActivity.this);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void gotoAddNewServie() {
        UserModel userModel = globals.getUserData();
        userModel.Data.isCompanyProfileCreated = true;
        globals.setUserData(userModel);

        startActivity(new Intent(this, AddNewService.class)
                .putExtra(Constants.SBU_Add_service_from, AddServiceFrom.FROM_MY_SERVICE));
        finishAffinity();
    }

    private void setSpinnerAdapter() {
        spnPaymentType.setAdapter(new ArrayAdapter<PaymentEnums>(this, R.layout.layout_spinner_row_payment, R.id.tv_payment_selection, PaymentEnums.values()));
    }

    private void setExpandLayoutPaymentDetails(boolean isExpand) {
        expandPaymentDetail.setExpanded(isExpand);
    }

    public void doGetStateList(final boolean isFromState) {
        if (!Globals.internetCheck(this))
            return;


        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(this);
        HttpRequestHandler.getInstance().get(this, getString(R.string.getStatesList), new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                stateListModel = new Gson().fromJson(response.toString(), StateListModel.class);
                if (stateListModel.Result) {
                    statesArrayList.addAll(stateListModel.Data.States);
                    if (isFromState) {
                        ArrayAdapter<StateListModel.States> arrayAdapter = new ArrayAdapter<StateListModel.States>(
                                CreateProfileActivity.this, android.R.layout.simple_dropdown_item_1line, statesArrayList);
                        auto_tv_state.setAdapter(arrayAdapter);
                    }
                    doDisplayStateList(statesArrayList);
                } else {
                    Toaster.shortToast(stateListModel.Message);
                    if (stateListModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(CreateProfileActivity.this);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void doDisplayStateList(ArrayList<StateListModel.States> stateListModelArrayList) {
        statesArrayAdapter = new ArrayAdapter<StateListModel.States>(this, R.layout.layout_spinner_row_payment, R.id.tv_payment_selection, stateListModelArrayList);
        spnState.setAdapter(statesArrayAdapter);
    }

    private void doGetCityList(String _id, final boolean isStateSelected, final boolean isPromo) {
        if (!Globals.internetCheck(this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getCityListParam(_id);
        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(this);
        HttpRequestHandler.getInstance().post(this, getString(R.string.getCitiesList), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                cities = new Gson().fromJson(response.toString(), StateListModel.class);
                if (cities.Result) {
                    cityArraylist.addAll(cities.Data.Cities);
                    if (isStateSelected) {
                        ArrayAdapter<StateListModel.Cities> arrayAdapter = new ArrayAdapter<StateListModel.Cities>(
                                CreateProfileActivity.this, android.R.layout.simple_dropdown_item_1line, cityArraylist);
                        auto_tv_city.setAdapter(arrayAdapter);
                    }
                    if (!isStateSelected && isPromo) {
                        isFirstGetCity = false;
                        showPromoCode();
                    }
                    doDisplayCityList(cities);
                } else {
                    Toaster.shortToast(cities.Message);
                    if (cities.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(CreateProfileActivity.this);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (!isStateSelected && isPromo) {
                    showPromoCode();
                }
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                if (!isStateSelected && isPromo) {
                    showPromoCode();
                }
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void doDisplayCityList(StateListModel citiesModel) {
        if (citiesModel.Data.Cities != null) {
            ArrayAdapter<StateListModel.Cities> dataAdapter = new ArrayAdapter<StateListModel.Cities>(this, R.layout.layout_spinner_row_payment, R.id.tv_payment_selection, citiesModel.Data.Cities);
            spnCity.setAdapter(dataAdapter);
        }
    }

    private void showConfirmChangePayment(final int position, boolean isFromRemove) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle(this.getString(R.string.sure));
        alertDialog.setMessage(this.getString(isFromRemove ? R.string.you_want_to_remove_your_payment_option : R.string.you_want_to_change_your_payment_option));
        alertDialog.setCancelable(false);
        // On pressing Settings button
        alertDialog.setPositiveButton(this.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                doDisplaySelectedPaymentOption(position);
            }
        });

        alertDialog.setNegativeButton(this.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                spnPaymentType.setSelection(paymentMethod);
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    private void doDisplaySelectedPaymentOption(int position) {
        //display payment layout based on selection
        PaymentEnums paymentEnums = PaymentEnums.fromInt(position);
        switch (paymentEnums) {
            case SELECT_PAYMENT_TYPE:
                paymentMethod = paymentEnums.getId();
                setExpandLayoutPaymentDetails(false);
                break;
            case PAYPAL:
                paymentMethod = paymentEnums.getId();
                setExpandLayoutPaymentDetails(true);
                cardview_paypal_email.setVisibility(View.VISIBLE);
                lyBank.setVisibility(View.GONE);
                tv_note.setVisibility(View.VISIBLE);
                break;
            case BANK_ACCOUNT:
                paymentMethod = paymentEnums.getId();
                if (statesArrayList.isEmpty())
                    doGetStateList(false);
                setExpandLayoutPaymentDetails(true);
                cardview_paypal_email.setVisibility(View.GONE);
                lyBank.setVisibility(View.VISIBLE);
                lyExtraFeild.setVisibility(View.VISIBLE);
                tv_note.setVisibility(View.GONE);
                break;
            case PAY_BY_CHEQUE:
                paymentMethod = paymentEnums.getId();
                if (statesArrayList.isEmpty())
                    doGetStateList(false);
                setExpandLayoutPaymentDetails(true);
                cardview_paypal_email.setVisibility(View.GONE);
                lyBank.setVisibility(View.VISIBLE);
                lyExtraFeild.setVisibility(View.GONE);
                tv_note.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

    private JSONObject setPaymentDetail() {
        JSONObject jsnPaymentDetail = new JSONObject();
        try {
            jsnPaymentDetail.put(Constants.SBU_FirstName, edtPaymentFname.getText().toString().trim());
            jsnPaymentDetail.put(Constants.SBU_LastName, edtPaymentLname.getText().toString().trim());
            jsnPaymentDetail.put(Constants.SBU_StreetAddress, tvStreetAddress.getText().toString().trim());
            jsnPaymentDetail.put(Constants.SBU_City, selectedCity);
            jsnPaymentDetail.put(Constants.SBU_State, selectedState);
            jsnPaymentDetail.put(Constants.SBU_PaymentZipCode, edtZipCode.getText().toString().trim());
            if (paymentMethod == PaymentEnums.BANK_ACCOUNT.getId()) {
                jsnPaymentDetail.put(Constants.SBU_RoutingNumber, edtRoutingNumber.getText().toString().trim());
                jsnPaymentDetail.put(Constants.SBU_AccountNumber, edtAccountNumber.getText().toString());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsnPaymentDetail;
    }

    private void showPromoConfirmDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Title
        // Setting Dialog Message
        alertDialog.setTitle("");
        alertDialog.setMessage(getString(R.string.do_you_have_promo_code));
        alertDialog.setCancelable(false);
        // On pressing Settings button
        alertDialog.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                doAskPromoCodeDialog();
            }
        });

        alertDialog.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
        isFirstGetCity = false;
    }

    private void doAskPromoCodeDialog() {
        dialogPromoCode = new Dialog(this, R.style.CustomDialogTermsNConditionTheme);
        dialogPromoCode.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogPromoCode.setContentView(R.layout.layout_promo_code_dialog);
        dialogPromoCode.setCancelable(false);

        edt_promo_code = dialogPromoCode.findViewById(R.id.edt_promo_code);
        chk_tnc = dialogPromoCode.findViewById(R.id.chk_tnc);
        TextView tv_submit = dialogPromoCode.findViewById(R.id.tv_submit);
        ImageView img_cancel = dialogPromoCode.findViewById(R.id.img_cancel);


        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promoCode = "";
                dialogPromoCode.dismiss();

            }
        });
        chk_tnc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chk_tnc.isChecked())
                    showTermsNConditionDialog();
            }
        });
        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (doPromoCodeValidation()) {
                    doVerifyPromoCode();
                }
            }
        });
        dialogPromoCode.show();
    }


    public boolean doPromoCodeValidation() {
        if (edt_promo_code.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(getString(R.string.please_enter_promo_code));
            return false;
        }
        if (!chk_tnc.isChecked()) {
            Toaster.shortToast(getString(R.string.please_accept_tnc));
            return false;
        }
        return true;
    }

    private void showTermsNConditionDialog() {
        final Dialog dialog = new Dialog(this, R.style.CustomDialogTermsNConditionTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_promo_code_dialog);
        dialog.setCancelable(false);


        LinearLayout ly_promo = dialog.findViewById(R.id.ly_promo);
        LinearLayout ly_checkbox = dialog.findViewById(R.id.ly_checkbox);
        TextView tv_ok = dialog.findViewById(R.id.tv_ok);
        LinearLayout ly_ok = dialog.findViewById(R.id.ly_ok);
        WebView webview = dialog.findViewById(R.id.webview);

        //promo layout to gone
        ly_promo.setVisibility(View.GONE);
        ly_checkbox.setVisibility(View.GONE);

        //terms and condition layout to display
        webview.setVisibility(View.VISIBLE);
        ly_ok.setVisibility(View.VISIBLE);

        webview.loadUrl(getString(R.string.base_server_url) + getString(R.string.promo_terms_n_condition_url));

        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void doVerifyPromoCode() {
        if (!Globals.internetCheck(CreateProfileActivity.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().doVerifyPromoCode(globals.getUserData().Data.sessionId,
                globals.getUserData().Data.userId, edt_promo_code.getText().toString().trim());

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(CreateProfileActivity.this);
        HttpRequestHandler.getInstance().post(CreateProfileActivity.this, getString(R.string.verifyPromoCode), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel.Result) {
                    promoCode = edt_promo_code.getText().toString().trim();
                    if (dialogPromoCode != null)
                        dialogPromoCode.dismiss();
                } else {
                    Toaster.shortToast(dataModel.Message);
                    if (dataModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(CreateProfileActivity.this);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

}
