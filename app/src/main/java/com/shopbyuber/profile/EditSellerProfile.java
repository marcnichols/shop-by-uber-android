package com.shopbyuber.profile;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.ImageType;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.model.UserModel;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class EditSellerProfile extends BaseActivity implements PermissionListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.profile_img)
    CircleImageView profile_img;

    @BindView(R.id.edt_name)
    EditText edt_name;

    @BindView(R.id.edt_speciality)
    EditText edt_speciality;

    @BindView(R.id.edt_description)
    EditText edt_description;

    @BindView(R.id.txt_email)
    TextView txt_email;

    @BindView(R.id.edt_password)
    EditText edt_password;

    @BindView(R.id.edt_conf_password)
    EditText edt_conf_password;

    @BindView(R.id.pick_img)
    TextView pick_img;

    @BindView(R.id.pick_camera)
    ImageView pick_camera;

    Globals globals;
    UserModel userModel;
    File selectedImageFile = null;

    PermissionListener permissionListener;
    private long lastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile_seller);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        permissionListener = this;
        globals = (Globals) getApplicationContext();
        userModel = globals.getUserData();

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.title_activity_edit_profile);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        getProfileDataRequest();
    }

    public void getProfileDataRequest() {
        if (!Globals.internetCheck(EditSellerProfile.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getServiceCatagoryParams(
                userModel.Data.userId,
                userModel.Data.sessionId);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(EditSellerProfile.this);

        HttpRequestHandler.getInstance().post(EditSellerProfile.this,
                getString(R.string.getEditUserData), params, new JsonHttpResponseHandler() {
                    @Override
                    public void onStart() {
                        super.onStart();
                        dialog.show();
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        if (dialog != null && dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Logger.json(response.toString());
                        UserModel userModel = new Gson().fromJson(response.toString(), UserModel.class);
                        if (userModel.Result) {
                            globals.setUserData(userModel);
                            setupData();
                        } else {
                            Toaster.shortToast(userModel.Message);
                            if (userModel.Message.contains(getString(R.string.invalid_session)))
                                globals.doLogout(EditSellerProfile.this);
                            else
                                finish();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        Toaster.shortToast(R.string.error_went_wrong);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        Toaster.shortToast(R.string.error_went_wrong);
                    }
                });
    }

    private void setupData() {
        userModel = globals.getUserData();
        if (userModel.Data.profileImage != null && !userModel.Data.profileImage.isEmpty()) {
            pick_img.setVisibility(View.GONE);
            pick_camera.setVisibility(View.VISIBLE);
            loadProfileImage(userModel.Data.profileImage);
        } else {
            pick_camera.setVisibility(View.GONE);
        }

        edt_name.setText(userModel.Data.name == null ? "" : userModel.Data.name);
        edt_description.setText(userModel.Data.description == null ? "" : userModel.Data.description);
        edt_speciality.setText(userModel.Data.speciality == null ? "" : userModel.Data.speciality);
        txt_email.setText(userModel.Data.email);
    }

    private void loadProfileImage(String path) {
        Glide.with(this)
                .load(path)
                .centerCrop()
                .placeholder(R.drawable.profile_placeholder)
                .dontAnimate()
                .into(profile_img);
    }

    @OnClick(R.id.btn_submit_profile)
    public void updateProfile() {
        if (doValidation()) {
            if (selectedImageFile != null)
                doRequestForUploadImage();
            else
                doUpdateProfileRequest("");
        }
    }

    @OnClick(R.id.frm_pick_img)
    public void onAddProfile() {
        if (pick_img.getVisibility() == View.GONE)
            return;
        onAddProfilePic();
    }

    @OnClick(R.id.pick_camera)
    public void onAddProfileCamera() {
        if (pick_camera.getVisibility() == View.GONE)
            return;

        onAddProfilePic();
    }

    public void onAddProfilePic() {
        if (SystemClock.elapsedRealtime() - lastClickTime < 500) {
            return;
        }

        lastClickTime = SystemClock.elapsedRealtime();
        new TedPermission(EditSellerProfile.this)
                .setPermissionListener(permissionListener)
                .setRationaleMessage(R.string.camera_message)
                .setDeniedMessage(R.string.denied_message)
                .setGotoSettingButtonText(R.string.ok)
                .setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();
    }

    @Override
    public void onPermissionGranted() {
        EasyImage.openChooserWithGallery(this, getString(R.string.action_Pick_source), 0);
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                selectedImageFile = new File(result.getUri().getPath());
                pick_img.setVisibility(View.GONE);
                pick_camera.setVisibility(View.VISIBLE);
                loadProfileImage(result.getUri().getPath());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Logger.e(error.getMessage());
            }
        } else {
            EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
                @Override
                public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                    selectedImageFile = null;
                }

                @Override
                public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                    if (imageFile.exists()) {
                        if (!imageFile.getAbsolutePath().substring(imageFile.getAbsolutePath().lastIndexOf(".")).equalsIgnoreCase(".gif")) {
                            startCropper(imageFile.getPath());
                        } else {
                            Toaster.shortToast(R.string.file_type_not_supported);
                        }
                    }
                }

                @Override
                public void onCanceled(EasyImage.ImageSource source, int type) {
                    //Cancel handling, you might wanna remove taken photo if it was canceled
                    if (source == EasyImage.ImageSource.CAMERA) {
                        File photoFile = EasyImage.lastlyTakenButCanceledPhoto(EditSellerProfile.this);
                        if (photoFile != null) photoFile.delete();
                    }
                }
            });
        }
    }

    private void startCropper(String path) {
        CropImage.activity(Uri.fromFile(new File(path)))
                .setAspectRatio(4, 3)
                .setAllowFlipping(false)
                .setAllowRotation(false)
                .setOutputCompressQuality(60)
                .setOutputCompressFormat(Bitmap.CompressFormat.JPEG)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }

    public void doRequestForUploadImage() {
        if (!Globals.internetCheck(EditSellerProfile.this))
            return;

        RequestParams params = HttpRequestHandler.getInstance().getUploadCompanyProfileImageParams(selectedImageFile,
                ImageType.COMPANY_PROFILE.getType());

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(EditSellerProfile.this);
        HttpRequestHandler.getInstance().postFile(EditSellerProfile.this, getString(R.string.uploadProfileImage), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel.Result) {
                    doUpdateProfileRequest(dataModel.Data.fileName);
                } else {
                    Toaster.shortToast(dataModel.Message);
                    if (dataModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(EditSellerProfile.this);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    public void doUpdateProfileRequest(String fileName) {
        if (!Globals.internetCheck(EditSellerProfile.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getEditProfileParams(
                userModel.Data.userId,
                userModel.Data.sessionId,
                edt_name.getText().toString().trim(), "",
                "", "",
                edt_password.getText().toString().trim(),
                fileName,
                edt_description.getText().toString().trim(),
                edt_speciality.getText().toString().trim(),"","","");

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(EditSellerProfile.this);

        HttpRequestHandler.getInstance().post(EditSellerProfile.this, getString(R.string.updateMyProfile), params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                UserModel userModel = new Gson().fromJson(response.toString(), UserModel.class);
                if (userModel.Result) {
                    globals.setUserData(userModel);
                    finish();
                } else {
                    Toaster.shortToast(userModel.Message);
                    if (userModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(EditSellerProfile.this);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    public boolean doValidation() {
        if (edt_name.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_name);
            return false;
        }

        if (!edt_password.getText().toString().trim().isEmpty()) {
            if (edt_password.getText().toString().trim().isEmpty()) {
                Toaster.shortToast(R.string.err_enter_password);
                return false;
            }
            if (edt_conf_password.getText().toString().trim().isEmpty()) {
                Toaster.shortToast(R.string.err_enter_confirm_password);
                return false;
            }
            if (!edt_password.getText().toString().trim().equalsIgnoreCase(edt_conf_password.getText().toString().trim())) {
                Toaster.shortToast(R.string.err_confirm_password_should_be_same_as_password);
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
