package com.shopbyuber.profile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.orhanobut.logger.Logger;
import com.sch.calendar.CalendarView;
import com.sch.calendar.adapter.SampleVagueAdapter;
import com.sch.calendar.annotation.DayOfMonth;
import com.sch.calendar.annotation.Month;
import com.sch.calendar.entity.Date;
import com.sch.calendar.listener.OnDateClickedListener;
import com.sch.calendar.listener.OnMonthChangedListener;
import com.sch.calendar.util.DateUtil;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.AddServiceFrom;
import com.shopbyuber.enums.Days;
import com.shopbyuber.enums.ImageType;
import com.shopbyuber.enums.PaymentEnums;
import com.shopbyuber.enums.ServiceSpinnerType;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.model.AppointmentOfSpecificDateModel;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.model.MyServicesModel;
import com.shopbyuber.model.ServiceCatagoryModel;
import com.shopbyuber.model.ServiceCatagoryModel.Categories;
import com.shopbyuber.model.ServiceOccupiedDataModel;
import com.shopbyuber.model.ServiceOccupiedDataModel.allocatedDates;
import com.shopbyuber.model.ServiceOccupiedDataModel.availableDays;
import com.shopbyuber.model.ServiceOccupiedDataModel.availableSchedule;
import com.shopbyuber.model.ServiceOccupiedDataModel.preAssignedDay;
import com.shopbyuber.model.ServiceOccupiedDataModel.slotes;
import com.shopbyuber.model.ServiceOccupiedDataModel.timeslots;
import com.shopbyuber.model.StateListModel;
import com.shopbyuber.model.UserModel;
import com.shopbyuber.model.WantedDetailsModel;
import com.shopbyuber.seller.AppointmentSpecificDateAdapter;
import com.shopbyuber.seller.CustomDialogAdapter;
import com.shopbyuber.seller.DaySelectionDialogAdapter;
import com.shopbyuber.seller.SellerLandingActivity;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class AddNewService extends BaseActivity implements PermissionListener, AppointmentSpecificDateAdapter.DoCancelRequestListner {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.toolbar_title)
    TextView toolbar_title;

    @BindView(R.id.calendar_view)
    CalendarView calendarView;

    @BindView(R.id.rel_img)
    RelativeLayout rel_img;

    @BindView(R.id.lnr_pick_img)
    LinearLayout lnr_pick_img;

    @BindView(R.id.txt_pick_img)
    TextView txt_pick_img;

    @BindView(R.id.sp_service_catagory)
    TextView sp_service_catagory;

    @BindView(R.id.sp_service_sub_catagory)
    TextView sp_service_sub_catagory;

    @BindView(R.id.sp_available_days)
    TextView sp_available_days;

    @BindView(R.id.sp_available_times)
    TextView sp_available_times;

    @BindView(R.id.sp_service_appointment)
    TextView sp_service_appointment;

    @BindView(R.id.edt_screen_name)
    TextView edt_screen_name;

    @BindView(R.id.edt_flat_fee)
    TextView edt_flat_fee;

    @BindView(R.id.edt_offer_details)
    TextView edt_offer_details;

    @BindView(R.id.profile_img)
    ImageView profile_img;

    @BindView(R.id.tv_delete_service)
    TextView tv_delete_service;

    @BindView(R.id.btn_submit_service)
    Button btn_submit_service;

    @BindView(R.id.ly_extra_field)
    LinearLayout lyExtraFeild;

    @BindView(R.id.ly_bank)
    LinearLayout lyBank;

    @BindView(R.id.ly_main)
    LinearLayout lyMain;

    @BindView(R.id.edt_paypal_email)
    EditText edt_paypal_email;

    @BindView(R.id.expand_payment_detail)
    ExpandableLayout expandPaymentDetail;

    @BindView(R.id.spn_payment_type)
    Spinner spnPaymentType;

    @BindView(R.id.cardview_paypal_email)
    CardView cardview_paypal_email;

    @BindView(R.id.edt_payment_fname)
    EditText edtPaymentFname;

    @BindView(R.id.edt_payment_lname)
    EditText edtPaymentLname;

    @BindView(R.id.tv_street_address)
    EditText tvStreetAddress;

    @BindView(R.id.spn_city)
    Spinner spnCity;

    @BindView(R.id.spn_state)
    Spinner spnState;

    @BindView(R.id.edt_zip_code)
    EditText edtZipCode;

    @BindView(R.id.edt_routing_number)
    EditText edtRoutingNumber;

    @BindView(R.id.edt_account_number)
    EditText edtAccountNumber;

    @BindView(R.id.edt_search_keyword)
    EditText edtSearchKeyword;

    @BindView(R.id.tv_note)
    TextView tv_note;

    TextView tvNoRecordAppointment;
    ListView listViewAppointment;

    PermissionListener permissionListener;
    Globals globals;
    UserModel userModel;
    ServiceCatagoryModel serviceCatagoryModel;
    ServiceCatagoryModel serviceSubCatagoryModel;

    ArrayList<ServiceOccupiedDataModel> serviceOccupiedDataModelArrayList;
    ArrayList<availableSchedule> availableSchedules;
    ArrayList<allocatedDates> dayScheduling;
    ArrayList<preAssignedDay> preAssignedDays;

    MyServicesModel.services serviceModel;
    AddServiceFrom serviceFrom;
    int currMonth;
    int currYear;

    File selectedImageFile = null;
    String categoryId = "", subCategoryId = "", selectedCategory = "", requestId = "";
    private boolean editAble = true;

    ServiceOccupiedDataModel occupiedDataModel;

    private StateListModel stateListModel;
    ArrayList<StateListModel.States> statesArrayList;

    private StateListModel cities;

    private ArrayAdapter<StateListModel.States> statesArrayAdapter;
    AppointmentOfSpecificDateModel appointmentOfSpecificDateModel;
    AppointmentSpecificDateAdapter appointmentSpecificDateAdapter;

    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    private String selectedState = "", selectedCity;
    private boolean isFirstTime = true, isFirstLoad = false, getAll = false;
    private int paymentMethod;
    boolean isFromFirstCall;

    private WantedDetailsModel wantedDetailsModel;
    private String wantedImage = "";

    @SuppressLint("StaticFieldLeak")
    public static AddNewService mContext;

    public static AddNewService getInstance() {
        return mContext;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_service);
        ButterKnife.bind(this);
        init();
    }

    private enum SwitchMonth {
        PREVIOUS,
        NEXT
    }

    private void init() {
        mContext = this;
        serviceOccupiedDataModelArrayList = new ArrayList<>();
        availableSchedules = new ArrayList<>();
        dayScheduling = new ArrayList<>();
        preAssignedDays = new ArrayList<>();

        permissionListener = this;
        globals = (Globals) getApplicationContext();
        userModel = globals.getUserData();
        statesArrayList = new ArrayList<>();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            serviceFrom = (AddServiceFrom) bundle.getSerializable(Constants.SBU_Add_service_from);
            if (bundle.containsKey(Constants.SBU_RequestId))
                requestId = bundle.getString(Constants.SBU_RequestId);
            if (serviceFrom == AddServiceFrom.FROM_EDIT_SERVICE) {
                serviceModel = (MyServicesModel.services) bundle.getSerializable(Constants.SBU_Service);
                editAble = serviceModel.editAble;
            }
            if (bundle.containsKey(Constants.SBU_GetAll))
                getAll = bundle.getBoolean(Constants.SBU_GetAll);
        }

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            if (serviceFrom == AddServiceFrom.FROM_REGISTER)
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            else
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            // set layout based on last screen
            switch (serviceFrom) {
                case FROM_REGISTER:
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                    toolbar_title.setText(R.string.add_service);
                    btn_submit_service.setText(R.string.submit_new_service);
                    break;
                case FROM_EDIT_SERVICE:
                    getSupportActionBar().setTitle(serviceModel.screenName);
                    tv_delete_service.setVisibility(View.VISIBLE);
                    btn_submit_service.setText(R.string.edit_service);
                    break;
                default:
                    getSupportActionBar().setTitle(R.string.add_service);
                    btn_submit_service.setText(R.string.submit_new_service);
                    break;
            }
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        Calendar cal = Calendar.getInstance();
        currMonth = cal.get(Calendar.MONTH);
        currYear = cal.get(Calendar.YEAR);

        initCalendarView(new HashMap<String, Integer>());

        //from wanted service display alert otherwise request for category list
        if (getAll)
            displayConfirmationDialog();
        else
            doGetCategoryListRequest();

        if (!editAble) tv_delete_service.setVisibility(View.GONE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            tv_note.setText(Html.fromHtml(getString(R.string.paypal_email_note), Html.FROM_HTML_MODE_COMPACT));
        else
            tv_note.setText(Html.fromHtml(getString(R.string.paypal_email_note)));

        spnState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //display city for selected state
                selectedState = stateListModel.Data.States.get(position)._id;
                doGetCityList(stateListModel.Data.States.get(position)._id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCity = cities.Data.Cities.get(position)._id;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnPaymentType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //manage payment selection
                if (isFirstLoad) {
                    if (PaymentEnums.fromInt(position).getId() == PaymentEnums.SELECT_PAYMENT_TYPE.getId())
                        showConfirmChangePayment(position, true);
                    else
                        showConfirmChangePayment(position, false);
                } else {
                    if (occupiedDataModel.Data.paymentMethod == PaymentEnums.SELECT_PAYMENT_TYPE.getId()) {
                        doDisplaySelectedPaymentOption(position);
                        isFirstLoad = false;
                    } else {
                        doDisplaySelectedPaymentOption(position);
                        isFirstLoad = true;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void displayConfirmationDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        alertDialog.setMessage(mContext.getString(R.string.are_you_sure_you_want_to_create_a_service_to_accommodate_this_buyers_request));
        alertDialog.setCancelable(false);
        // On pressing Settings button
        alertDialog.setPositiveButton(mContext.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                displayAutoPopulateDialog();
                dialog.cancel();
            }
        });

        alertDialog.setNegativeButton(mContext.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                onBackPressed();
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    private void displayAutoPopulateDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        alertDialog.setMessage(mContext.getString(R.string.please_populate_all_fields_on_this_screen_to_create_a_service_for_this_buyer));
        alertDialog.setCancelable(false);
        // On pressing Settings button
        alertDialog.setPositiveButton(mContext.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                doGetCategoryListRequest();
                dialog.cancel();
            }
        });

        alertDialog.setNegativeButton(mContext.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                onBackPressed();
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }


    private void doGetDetailRequest() {
        if (!Globals.internetCheck(AddNewService.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getWantedDetailsParams(
                userModel.Data.userId, userModel.Data.sessionId, requestId);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(AddNewService.this);

        HttpRequestHandler.getInstance().post(AddNewService.this, getString(R.string.getDetail), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing() && !isFinishing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                wantedDetailsModel = new Gson().fromJson(response.toString(), WantedDetailsModel.class);
                if (wantedDetailsModel.Result) {
                    showSelectedWantedCategory();
                    setWantedServiceData();
                    categoryId = wantedDetailsModel.Data.categoryId;
                    doGetSubCategory(true);
                } else {
                    Toaster.shortToast(wantedDetailsModel.Message);
                    if (wantedDetailsModel.Message.contains(getString(R.string.invalid_session))) {
                        globals.doLogout(AddNewService.this);
                    } else
                        finish();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void showSelectedWantedCategory() {
        // show selected category
        for (Categories category : serviceCatagoryModel.Data.categories) {
            if (category._id.equalsIgnoreCase(wantedDetailsModel.Data.categoryId)) {
                sp_service_catagory.setText(category.catName);
                categoryId = category._id;
                category.isAllocate = true;
            }
        }
    }

    //use for prepopulate data
    private void setWantedServiceData() {

        loadProfileImage(wantedDetailsModel.Data.cateImage);
        edt_screen_name.setText(wantedDetailsModel.Data.serviceName.isEmpty() ? "" : wantedDetailsModel.Data.serviceName);
        edt_offer_details.setText(wantedDetailsModel.Data.serviceDescription.isEmpty() ? "" : wantedDetailsModel.Data.serviceDescription);

        String[] imgSplit = wantedDetailsModel.Data.cateImage.split("/");
        wantedImage = imgSplit[imgSplit.length - 1];

        // field which is not editable by user
        edt_screen_name.setFocusable(false);
        edt_offer_details.setFocusable(false);
        sp_service_catagory.setClickable(false);
        sp_service_sub_catagory.setClickable(false);
        txt_pick_img.setClickable(false);

    }

    private void setSubCategoryWantedService() {
        // show selected sub category
        for (ServiceCatagoryModel.SubCategories subCategories : serviceSubCatagoryModel.Data.subCategories) {
            if (subCategories._id.equalsIgnoreCase(wantedDetailsModel.Data.subCategoryId)) {
                sp_service_sub_catagory.setText(subCategories.subcatName);
                subCategoryId = subCategories._id;
                subCategories.isAllocate = true;
            }
        }
    }

    private void showDisabledFieldPopup() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.err_not_allow_to_edit)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .show();
    }

    @Override
    public void onBackPressed() {
        if (isTaskRoot()) {
            startActivity(new Intent(AddNewService.this, SellerLandingActivity.class));
            finish();
        } else
            finish();

    }

    private void initCalendarView(HashMap<String, Integer> map) {
        if (calendarView.getVagueAdapter() == null) {
            calendarView.setCanDrag(false);
            calendarView.setScaleEnable(false);
            calendarView.setShowOverflowDate(true);
            calendarView.setCanFling(false);
            String strTitleFormat = "MMMM yyyy";
            calendarView.setTitleFormat(strTitleFormat, Locale.US);
            calendarView.ibtnNextMonth.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isFirstLoad = false;
                    availableSchedule schedule = getAvailableSchedule();
                    if (schedule != null)
                        prepareTimings(schedule, true);
                    if (schedule != null && schedule.timings.isEmpty())
                        showConfirmationDialog(SwitchMonth.NEXT);
                    else
                        calendarView.goToNext();
                }
            });
            calendarView.ibtnLastMonth.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isFirstLoad = false;
                    availableSchedule schedule = getAvailableSchedule();
                    if (schedule != null)
                        prepareTimings(schedule, true);
                    if (schedule != null && schedule.timings.isEmpty())
                        showConfirmationDialog(SwitchMonth.PREVIOUS);
                    else
                        calendarView.goToPrevious();
                }
            });
            calendarView.setOnMonthChangedListener(new OnMonthChangedListener() {
                @Override
                public void onMonthChanged(Date date) {
                    isFirstLoad = false;
                    currMonth = date.getMonth();
                    currYear = date.getYear();
                    setUpSpinners();
                    getNextMonthData();
                }
            });
            calendarView.setOnDateClickedListener(new OnDateClickedListener() {
                @Override
                public void onDateClicked(View itemView, int year, @Month int month, @DayOfMonth int dayOfMonth) {
                    if (DateUtil.allowSelection(month, year, dayOfMonth))
                        if (serviceFrom == AddServiceFrom.FROM_EDIT_SERVICE) {
                            doGetAppointmentOfSpecificDate(DateUtil.getDate(year, month, dayOfMonth));
                        } else
                            showDaySelection(DateUtil.getDate(year, month, dayOfMonth));
                }
            });
            calendarView.setVagueAdapter(new SampleVagueAdapter(getMap()));
        } else
            calendarView.notifiDataSetChanged(map);
    }

    private void getNextMonthData() {
        if (!monthDataAvailable(currMonth, currYear)) {
            if (serviceFrom == AddServiceFrom.FROM_EDIT_SERVICE)
                doGetScreenOccupiedEditData(currMonth, currYear);
            else
                doGetScreenOccupiedData(currMonth, currYear);
        }
    }

    private void showConfirmationDialog(final SwitchMonth type) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.err_insufficient_data))
                .setMessage(R.string.err_insufficient_data_message)
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // continue with delete
                        removeScheduleIfNoSlotes();
                        unAssignDay(true);
                        switch (type) {
                            case NEXT:
                                calendarView.goToNext();
                                break;
                            case PREVIOUS:
                                calendarView.goToPrevious();
                                break;
                            default:
                                break;
                        }
                        getNextMonthData();
                    }
                })
                .setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // do nothing
                    }
                })
                .show();
    }

    private void setUpSpinners() {
        availableSchedule schedule = getAvailableSchedule();

        if (schedule != null) {
            prepareTimings(schedule, false);

            setAppointmentText(schedule.duration);
            setDayToSpinner(schedule);
            if (!schedule.timings.isEmpty())
                sp_available_times.setText(schedule.timings.get(0));
            else
                sp_available_times.setText("");
        } else {
            sp_service_appointment.setText("");
            sp_available_days.setText("");
            sp_available_times.setText("");
        }
    }

    private void prepareTimings(availableSchedule schedule, boolean deAllocate) {
        ArrayList<slotes> slotes = getOccupiedDayTimeSlots(schedule.month, schedule.Year);
        if (slotes != null) {
            schedule.timings.clear();
            for (slotes slote : slotes) {
                if (slote.isAllocate && ifSloteIsAvailableInSchedule(slote))
                    schedule.timings.add(slote.time);
                else if (deAllocate)
                    slote.isAllocate = false;
            }
        }
    }

    private boolean ifSloteIsAvailableInSchedule(slotes slote) {
        for (allocatedDates dates : dayScheduling) {
            for (slotes slotes : dates.slotes) {
                if (slotes.time.equals(slote.time))
                    return true;
            }
        }
        return false;
    }

    /**
     * Check if selected month data is already in list or not
     */
    private boolean monthDataAvailable(int currMonth, int currYear) {
        for (ServiceOccupiedDataModel dataModel : serviceOccupiedDataModelArrayList) {
            if (dataModel.month == currMonth && dataModel.year == currYear)
                return true;
        }
        return false;
    }

    public void doGetCategoryListRequest() {
        if (!Globals.internetCheck(AddNewService.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getServiceCatagoryParams(
                userModel.Data.userId, userModel.Data.sessionId);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(AddNewService.this);

        HttpRequestHandler.getInstance().post(AddNewService.this, getString(R.string.getCategoryList), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing() && !isFinishing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                serviceCatagoryModel = new Gson().fromJson(response.toString(), ServiceCatagoryModel.class);
                if (serviceCatagoryModel.Result) {

                    if (serviceFrom == AddServiceFrom.FROM_EDIT_SERVICE)
                        doGetScreenOccupiedEditData(currMonth, currYear);
                    else
                        doGetScreenOccupiedData(currMonth, currYear);
                } else {
                    Toaster.shortToast(serviceCatagoryModel.Message);
                    if (serviceCatagoryModel.Message.contains(getString(R.string.invalid_session))) {
                        globals.doLogout(AddNewService.this);
                    } else
                        finish();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void displayCreateAServiceDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        // Setting Dialog Message
        alertDialog.setMessage(mContext.getString(R.string.please_create_a_service_to_offer_to_our_buyer));
        alertDialog.setCancelable(false);
        // On pressing Settings button
        alertDialog.setPositiveButton(mContext.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void doGetScreenOccupiedData(final int month, final int year) {
        if (!Globals.internetCheck(AddNewService.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getServiceOccupiedParams(
                userModel.Data.userId, userModel.Data.sessionId,
                month, year);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(AddNewService.this);

        HttpRequestHandler.getInstance().post(AddNewService.this, getString(R.string.getScreenOccupiedData), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing() && !isFinishing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                occupiedDataModel = new Gson().fromJson(response.toString(), ServiceOccupiedDataModel.class);
                if (occupiedDataModel.Result) {
                    if (!serviceCatagoryModel.Data.isServiceCreated) {
                        displayCreateAServiceDialog();
                    }
                    setSpinnerAdapter();
                    if (paymentMethod == PaymentEnums.BANK_ACCOUNT.getId() || paymentMethod == PaymentEnums.PAY_BY_CHEQUE.getId())
                        doGetStateList();
                    occupiedDataModel.month = month;
                    occupiedDataModel.year = year;
                    serviceOccupiedDataModelArrayList.add(occupiedDataModel);

                    setupPreAssignedDays(month, year, occupiedDataModel);
                    setPaymentData(occupiedDataModel.Data);
                    if (getAll) {
                        doGetDetailRequest();
                    }
                } else if (occupiedDataModel.Message.contains(getString(R.string.invalid_session)))
                    globals.doLogout(AddNewService.this);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void setPaymentData(ServiceOccupiedDataModel.Data data) {
        edt_paypal_email.setText(data.paypalEmail == null ? "" : data.paypalEmail.trim());
        edtPaymentFname.setText(data.paymentDetail.firstName == null ? "" : data.paymentDetail.firstName.trim());
        edtPaymentLname.setText(data.paymentDetail.lastName == null ? "" : data.paymentDetail.lastName.trim());
        tvStreetAddress.setText(data.paymentDetail.streetAddress == null ? "" : data.paymentDetail.streetAddress.trim());
        edtZipCode.setText(data.paymentDetail.zipcode == null ? "" : data.paymentDetail.zipcode.trim());
        edtRoutingNumber.setText(data.paymentDetail.routingNumber == null ? "" : data.paymentDetail.routingNumber.trim());
        edtAccountNumber.setText(data.paymentDetail.accountNumber == null ? "" : data.paymentDetail.accountNumber.trim());
    }

    private void setupPreAssignedDays(int month, int year, ServiceOccupiedDataModel serviceOccupiedDataModel) {
        preAssignedDay preAssignedDay = new preAssignedDay();
        preAssignedDay.month = month;
        preAssignedDay.Year = year;
        for (availableDays days : serviceOccupiedDataModel.Data.availableDays) {
            if (days.isAllocate)
                preAssignedDay.day.add(days.day);
        }
        preAssignedDays.add(preAssignedDay);
    }

    public void doGetScreenOccupiedEditData(final int month, final int year) {
        if (!Globals.internetCheck(AddNewService.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getServiceOccupiedEditParams(
                userModel.Data.userId, userModel.Data.sessionId, serviceModel.serviceId, currMonth, currYear);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(AddNewService.this);

        HttpRequestHandler.getInstance().post(AddNewService.this, getString(R.string.getDataEditService), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing() && !isFinishing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                occupiedDataModel = new Gson().fromJson(response.toString(), ServiceOccupiedDataModel.class);
                if (occupiedDataModel.Result) {
                    occupiedDataModel.month = month;
                    occupiedDataModel.year = year;
                    serviceOccupiedDataModelArrayList.add(occupiedDataModel);

                    setSpinnerAdapter();
                    categoryId = occupiedDataModel.Data.serviceDetail.categoryId;
                    doGetSubCategory(true);
                    if (paymentMethod == PaymentEnums.BANK_ACCOUNT.getId() || paymentMethod == PaymentEnums.PAY_BY_CHEQUE.getId())
                        doGetStateList();
                    setPaymentData(occupiedDataModel.Data);
                    setupPreAssignedDays(month, year, occupiedDataModel);
                    setDataForEdit();

                } else if (occupiedDataModel.Message.contains(getString(R.string.invalid_session)))
                    globals.doLogout(AddNewService.this);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
                finish();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
                finish();
            }
        });
    }

    private void setDataForEdit() {
        ServiceOccupiedDataModel serviceOccupiedDataModel = getModel(currMonth, currYear);
        ServiceOccupiedDataModel.serviceDetail serviceDetails = serviceOccupiedDataModel.Data.serviceDetail;
        loadProfileImage(serviceDetails.serviceImage);
        edt_screen_name.setText(serviceDetails.screenName);
        edt_flat_fee.setText(serviceDetails.flatFee);
        edtSearchKeyword.setText(serviceDetails.keywords != null ? serviceDetails.keywords : "");
        edt_offer_details.setText(serviceDetails.serviceDetail);
        dayScheduling.addAll(serviceDetails.dayScheduling);
        serviceOccupiedDataModel.Data.allocatedDates.addAll(serviceDetails.dayScheduling);

        // setupAllocated Dates
        for (ServiceOccupiedDataModel.allocatedDates dates : serviceOccupiedDataModel.Data.allocatedDates) {
            dates.month = currMonth;
            dates.year = currYear;
            dates.intdate = DateUtil.getDayFromDate(dates.date);
        }

        // show selected category
        for (Categories category : serviceCatagoryModel.Data.categories) {
            if (category._id.equalsIgnoreCase(serviceDetails.categoryId)) {
                sp_service_catagory.setText(category.catName);
                categoryId = category._id;
                category.isAllocate = true;
            }
        }


        if (!serviceDetails.availableTimes.isEmpty()) {
            availableSchedule availableSchedule = new availableSchedule();
            availableSchedule.month = currMonth;
            availableSchedule.Year = currYear;
            availableSchedule.duration = serviceDetails.appointmentDuration;
            availableSchedule.day.addAll(serviceDetails.availableDay);
            availableSchedule.timings.addAll(serviceDetails.availableTimes);
            availableSchedules.add(availableSchedule);
        }
        setUpSpinners();

        // mark day as occupied
        for (availableDays days : serviceOccupiedDataModel.Data.availableDays) {
            if (serviceDetails.availableDay.contains(days.day)) {
                days.isAllocate = true;
            }
        }

        // mark occupied slotes
        for (timeslots timeslots : serviceOccupiedDataModel.Data.allocate) {
            for (timeslots timeslot : serviceOccupiedDataModel.Data.timeslots) {
                if (timeslots.duration == timeslot.duration) {
                    timeslot.isSelected = true;
                    timeslot.slotes = timeslots.slotes;
                }
            }
        }

        initCalendarView(getMap());
    }

    private ServiceOccupiedDataModel getModel(int month, int year) {
        for (ServiceOccupiedDataModel serviceOccupiedDataModel1 : serviceOccupiedDataModelArrayList) {
            if (serviceOccupiedDataModel1.month == month &&
                    serviceOccupiedDataModel1.year == year)
                return serviceOccupiedDataModel1;
        }
        return null;
    }

    private boolean validateAvailableTimeDialog() {
        if (sp_service_appointment.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_select_service_appointment_duration);
            return false;
        }
        if (sp_available_days.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_select_available_days);
            return false;
        }
        return true;
    }

    @OnClick(R.id.btn_submit_service)
    public void addNewService() {
        for (ServiceOccupiedDataModel.availableSchedule schedule : availableSchedules) {
            prepareTimings(schedule, false);
        }
        removeScheduleIfNoSlotes();

        if (doValidate()) {
            if (serviceFrom == AddServiceFrom.FROM_EDIT_SERVICE) {
                if (selectedImageFile == null)
                    doAddServiceRequest("", false);
                else
                    doRequestForUploadImage(false);
            } else {
                /*if (getAll) doAddServiceRequest(wantedImage, true);
                else*/
                doRequestForUploadImage(true);
            }
        }
    }

    private void removeScheduleIfNoSlotes() {
        Iterator<availableSchedule> i = availableSchedules.iterator();
        while (i.hasNext()) {
            availableSchedule schedule = i.next();
            if (schedule.timings.isEmpty())
                i.remove();
        }
    }

    private void getImageBitmap() {
        Bitmap bitmap = ((GlideBitmapDrawable) profile_img.getDrawable().getCurrent()).getBitmap();

        //  Get path to new gallery image
        Uri path = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new ContentValues());
        try {
            OutputStream stream = getContentResolver().openOutputStream(path);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
        } catch (FileNotFoundException e) {
            Logger.e("ADDNewService" + e.getMessage());
        }

        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(path, filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();

        selectedImageFile = new File(picturePath);
    }

    public void doRequestForUploadImage(final boolean forAdd) {
        if (!Globals.internetCheck(AddNewService.this))
            return;

        if (getAll)
            getImageBitmap();

        RequestParams params = HttpRequestHandler.getInstance().getUploadCompanyProfileImageParams(selectedImageFile,
                ImageType.SERVICE_IMAGE.getType());

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(AddNewService.this);
        HttpRequestHandler.getInstance().postFile(AddNewService.this, getString(R.string.uploadProfileImage), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing() && !isFinishing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel.Result) {
                    doAddServiceRequest(dataModel.Data.fileName, forAdd);
                } else {
                    Toaster.shortToast(dataModel.Message);
                    if (dataModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(AddNewService.this);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void doAddServiceRequest(String fileName, boolean forAdd) {
        if (!Globals.internetCheck(AddNewService.this))
            return;

        String serviceId = "";
        if (serviceFrom == AddServiceFrom.FROM_EDIT_SERVICE)
            serviceId = serviceModel.serviceId;


        JSONObject params = HttpRequestHandler.getInstance().getAddNewServiceParams1(
                userModel.Data.userId, userModel.Data.sessionId, categoryId,
                edt_screen_name.getText().toString().trim(),
                edt_flat_fee.getText().toString().trim(),
                edt_offer_details.getText().toString().trim(),
                availableSchedules, dayScheduling,
                fileName, serviceId, forAdd,
                editAble,
                paymentMethod != PaymentEnums.PAYPAL.getId() ? "" : edt_paypal_email.getText().toString().trim(),
                paymentMethod, setPaymentDetail(), subCategoryId, edtSearchKeyword.getText().toString().trim(), requestId);

        String api = "";
        if (forAdd)
            api = getString(R.string.addService);
        else
            api = getString(R.string.updateService);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(AddNewService.this);
        HttpRequestHandler.getInstance().post(AddNewService.this, api, params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing() && !isFinishing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                Toaster.shortToast(dataModel.Message);
                if (dataModel.Result)
                    doFinish();
                else if (dataModel.Message.contains(getString(R.string.invalid_session)))
                    globals.doLogout(AddNewService.this);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void doFinish() {
        if (isTaskRoot())
            gotoLandingPage();
        else {
            Intent returnIntent = new Intent();
            setResult(RESULT_OK, returnIntent);
            finish();
        }
    }

    private void gotoLandingPage() {
        Intent intent = new Intent(AddNewService.this, SellerLandingActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    // retrive dates of selected day i.e monday, tuesday
    private ArrayList<Integer> getDays(int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, currYear);
        cal.set(Calendar.MONTH, currMonth);
        cal.set(Calendar.DAY_OF_WEEK, day);
        cal.set(Calendar.DAY_OF_WEEK_IN_MONTH, 1);
        int month = cal.get(Calendar.MONTH);
        ArrayList<Integer> output = new ArrayList<>();

        while (cal.get(Calendar.MONTH) == month) {
            output.add(cal.get(Calendar.DAY_OF_MONTH));
            cal.add(Calendar.DAY_OF_MONTH, 7);
        }
        return output;
    }

    @OnClick({R.id.lnr_pick_img, R.id.txt_pick_img})
    public void pickImage() {
        new TedPermission(this)
                .setPermissionListener(permissionListener)
                .setRationaleMessage(R.string.camera_message)
                .setDeniedMessage(R.string.denied_message)
                .setGotoSettingButtonText(R.string.ok)
                .setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();
    }

    @Override
    public void onPermissionGranted() {
        EasyImage.openChooserWithGallery(this, getString(R.string.action_Pick_source), 0);
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

    }

    private void startCropper(String path) {
        CropImage.activity(Uri.fromFile(new File(path)))
                .setAspectRatio(4, 3)
                .setAllowFlipping(false)
                .setAllowRotation(false)
                .setOutputCompressQuality(60)
                .setOutputCompressFormat(Bitmap.CompressFormat.JPEG)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }

    private void loadProfileImage(String path) {
        if (path != null && !path.equalsIgnoreCase("")) {
            lnr_pick_img.setVisibility(View.GONE);
            txt_pick_img.setVisibility(View.VISIBLE);
            rel_img.setVisibility(View.VISIBLE);

            Glide.with(this)
                    .load(path)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .centerCrop()
                    .placeholder(R.drawable.image_place_holder)
                    .dontAnimate()
                    .into(profile_img);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                selectedImageFile = new File(result.getUri().getPath());
                loadProfileImage(result.getUri().getPath());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Logger.e(error.getMessage());
            }
        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                tvStreetAddress.setText(place.getAddress());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        } else {
            EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
                @Override
                public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {

                }

                @Override
                public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                    if (imageFile.exists()) {
                        if (!imageFile.getAbsolutePath().substring(imageFile.getAbsolutePath().lastIndexOf(".")).equalsIgnoreCase(".gif")) {
                            startCropper(imageFile.getPath());
                        } else {
                            Toaster.shortToast(R.string.file_type_not_supported);
                        }
                    }
                }

                @Override
                public void onCanceled(EasyImage.ImageSource source, int type) {
                    //Cancel handling, you might wanna remove taken photo if it was canceled
                    if (source == EasyImage.ImageSource.CAMERA) {
                        File photoFile = EasyImage.lastlyTakenButCanceledPhoto(AddNewService.this);
                        if (photoFile != null) photoFile.delete();
                    }
                }
            });
        }
    }

    // ----------------------------------------- showCatagoryDialog -----------------------------------------

    @OnClick(R.id.sp_service_catagory)
    public void showCatagoryDialog() {
        final Dialog dialog = new Dialog(AddNewService.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog_layout);
        dialog.setCancelable(false);

        TextView tv_dg_main_title = dialog.findViewById(R.id.tv_dg_main_title);
        tv_dg_main_title.setText(getString(R.string.select_a_category));
        final ListView listview = dialog.findViewById(R.id.listview);

        final CustomDialogAdapter customDialogAdapter = new CustomDialogAdapter(this, serviceCatagoryModel, ServiceSpinnerType.CATEGORY.getType());
        listview.setAdapter(customDialogAdapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                for (int i = 0; i < serviceCatagoryModel.Data.categories.size(); i++) {
                    Categories categories = serviceCatagoryModel.Data.categories.get(i);
                    categories.isAllocate = i == position;

                }

                customDialogAdapter.notifyDataSetChanged();
            }
        });

        dialog.show();

        Button btn_submit = dialog.findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedCategory = categoryId;
                setCatagory();
                if (!categoryId.equalsIgnoreCase(selectedCategory)) {
                    sp_service_sub_catagory.setText("");
                    doGetSubCategory(false);
                }
                dialog.dismiss();
            }
        });
    }

    private void setCatagory() {
        for (Categories categories : serviceCatagoryModel.Data.categories) {
            if (categories.isAllocate) {
                categoryId = categories._id;
                sp_service_catagory.setText(categories.catName);
            }
        }
    }


    // ----------------------------------------------------------------------------------------------------------


    // ----------------------------------------- showAvailableDurationDialog -----------------------------------------

    @OnClick(R.id.sp_service_appointment)
    public void showAvailableDurationDialog() {
        if (editAble) {
            final ServiceOccupiedDataModel serviceOccupiedDataModel = getModel(currMonth, currYear);

            final Dialog dialog = new Dialog(AddNewService.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_dialog_layout);
            dialog.setCancelable(false);

            TextView tv_dg_main_title = dialog.findViewById(R.id.tv_dg_main_title);
            tv_dg_main_title.setText(getString(R.string.service_appointment_duration));
            ListView listview = dialog.findViewById(R.id.listview);

            final CustomDialogAdapter customDialogAdapter = new CustomDialogAdapter(this, serviceOccupiedDataModel, ServiceSpinnerType.DURATION.getType());
            listview.setAdapter(customDialogAdapter);
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    for (int i = 0; i < serviceOccupiedDataModel.Data.timeslots.size(); i++) {
                        timeslots timeslot = serviceOccupiedDataModel.Data.timeslots.get(i);
                        timeslot.isSelected = i == position;
                    }
                    customDialogAdapter.notifyDataSetChanged();
                }
            });

            dialog.show();

            Button btn_submit = dialog.findViewById(R.id.btn_submit);
            btn_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    for (timeslots timeslot : serviceOccupiedDataModel.Data.timeslots) {
                        if (timeslot.isSelected) {
                            availableSchedule schedule = getAvailableSchedule();
                            if (schedule != null && schedule.duration != timeslot.duration) {
                                if (serviceFrom == AddServiceFrom.FROM_EDIT_SERVICE)
                                    showAppointmentConfirmationDialog(schedule, timeslot.duration);
                                else {
                                    unAssignDaynTimes();
                                    availableSchedules.remove(schedule);
                                    setAppoinmentDuration(timeslot.duration);
                                }
                            } else
                                setAppoinmentDuration(timeslot.duration);
                            break;
                        }
                    }
                }
            });
        } else
            showDisabledFieldPopup();
    }

    private void showAppointmentConfirmationDialog(final availableSchedule schedule, final int duration) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.err_appointment_change))
                .setMessage(R.string.err_appointment_change_message)
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // continue with delete
                        unAssignDaynTimes();
                        availableSchedules.remove(schedule);
                        setAppoinmentDuration(duration);
                    }
                })
                .setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // do nothing
                    }
                })
                .show();
    }

    private void unAssignDay(boolean clearHours) {
        ServiceOccupiedDataModel serviceOccupiedDataModel = getModel(currMonth, currYear);
        ArrayList<Integer> selectedDays = getPreAssidned();
        for (availableDays availableDay : serviceOccupiedDataModel.Data.availableDays) {
            if (!selectedDays.contains(availableDay.day))
                availableDay.isAllocate = false;
        }
        if (clearHours) {
            for (ServiceOccupiedDataModel.timeslots timeslot : serviceOccupiedDataModel.Data.timeslots)
                timeslot.isSelected = false;
        }
    }

    private void unAssignDaynTimes() {
        ArrayList<slotes> slotes = getOccupiedDayTimeSlots(currMonth, currYear);
        if (slotes != null) {
            for (slotes slote : slotes) {
                slote.isAllocate = false;
            }
            unAssignDay(false);
            sp_available_times.setText("");
            sp_available_days.setText("");

            clearDayScheduling();
        }
    }

    private void clearDayScheduling() {
        Iterator<allocatedDates> i = dayScheduling.iterator();
        while (i.hasNext()) {
            allocatedDates allocatedDate = i.next(); // must be called before you can call i.remove()
            // Do something
            if (allocatedDate.month == currMonth && allocatedDate.year == currYear) {
                i.remove();
            }
        }
        initCalendarView(getMap());
    }

    private void setAppoinmentDuration(int appointmentDuration) {
        availableSchedule schedule = getAvailableSchedule();
        if (schedule == null) {
            availableSchedule availableSchedule = new availableSchedule();
            availableSchedule.month = currMonth;
            availableSchedule.Year = currYear;
            availableSchedule.duration = appointmentDuration;
            availableSchedules.add(availableSchedule);
        } else {
            schedule.duration = appointmentDuration;
        }
        setAppointmentText(appointmentDuration);
    }

    private void setAppointmentText(int appointmentDuration) {
        if (appointmentDuration != 0)
            sp_service_appointment.setText(appointmentDuration > 1
                    ? appointmentDuration + Constants.SBU_Hours
                    : appointmentDuration + Constants.SBU_Hour);
    }

    private availableSchedule getAvailableSchedule() {
        for (availableSchedule schedule : availableSchedules) {
            if (schedule.month == currMonth && schedule.Year == currYear)
                return schedule;
        }
        return null;
    }

    private boolean validateAvailableDayDialog() {
        if (sp_service_appointment.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_select_service_appointment_duration);
            return false;
        }
        return true;
    }

    // ----------------------------------------------------------------------------------------------------------


    // ----------------------------------------- showAvailableDaysDialog -----------------------------------------

    private ArrayList<Integer> getPreAssidned() {
        for (preAssignedDay day : preAssignedDays) {
            if (day.month == currMonth && day.Year == currYear)
                return day.day;
        }
        return new ArrayList<>();
    }

    @OnClick(R.id.sp_available_days)
    public void showAvailableDaysDialog() {
        if (editAble) {
            if (!validateAvailableDayDialog())
                return;

            final ServiceOccupiedDataModel serviceOccupiedDataModel = getModel(currMonth, currYear);

            final Dialog dialog = new Dialog(AddNewService.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_dialog_layout);
            dialog.setCancelable(false);

            TextView tv_dg_main_title = dialog.findViewById(R.id.tv_dg_main_title);
            tv_dg_main_title.setText(getString(R.string.select_you_available_days));
            ListView listview = dialog.findViewById(R.id.listview);

            final CustomDialogAdapter customDialogAdapter = new CustomDialogAdapter(this, serviceOccupiedDataModel, ServiceSpinnerType.DAYS.getType());
            listview.setAdapter(customDialogAdapter);
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    availableDays availableDay = serviceOccupiedDataModel.Data.availableDays.get(position);
                    if (availableDay != null && !getPreAssidned().contains(availableDay.day)) {
                        addOrRemoveDay(!availableDay.isAllocate, availableDay.day);

                        availableDay.isAllocate = !availableDay.isAllocate;
                        customDialogAdapter.notifyDataSetChanged();
                    } else
                        Toaster.shortToast(R.string.err_service_already_created_for_day);
                }
            });

            dialog.show();

            Button btn_submit = dialog.findViewById(R.id.btn_submit);
            btn_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    availableSchedule schedule = getAvailableSchedule();
                    if (schedule == null) {
                        for (ServiceOccupiedDataModel.timeslots timeslot : serviceOccupiedDataModel.Data.timeslots) {
                            if (timeslot.isSelected) {
                                setAppoinmentDuration(timeslot.duration);
                                break;
                            }
                        }
                        schedule = getAvailableSchedule();
                    } else
                        schedule.day.clear();

                    for (availableDays availableDay : serviceOccupiedDataModel.Data.availableDays) {
                        if (availableDay.isAllocate && !getPreAssidned().contains(availableDay.day))
                            schedule.day.add(availableDay.day);
                    }
                    setDayToSpinner(schedule);
                    setupDaySchedulings();
                }
            });
        } else
            showDisabledFieldPopup();
    }

    private void addOrRemoveDay(boolean forAdd, int day) {
        if (forAdd)
            addDay(day);
        else
            removeDay(day);
    }

    // add day when select new day
    private void addDay(int day) {
        if (!dayScheduling.isEmpty()) {
            ArrayList<slotes> slotes = new ArrayList<>();
            for (slotes s : getOccupiedDayTimeSlots(currMonth, currYear)) {
                if (s.isAllocate)
                    slotes.add(s);
            }

            ArrayList<Integer> integers = getDays(Days.getCalendarDay(day));
            for (Integer integer : integers) {
                allocatedDates dates = new allocatedDates();
                dates.slotes = new ArrayList<>();
                dates.intdate = integer;

                dates.month = currMonth;
                dates.year = currYear;

                dates.date = DateUtil.getDate(currYear, currMonth, integer);
                dates.slotes.addAll(slotes);
                dayScheduling.add(dates);
            }
        }
    }

    // remove selected day when unselect
    private void removeDay(int day) {
        if (!dayScheduling.isEmpty()) {
            ArrayList<Integer> integers = getDays(Days.getCalendarDay(day));
            for (Integer integer : integers) {
                remove(integer);
            }
        }
    }

    private void remove(Integer integer) {
        for (allocatedDates date : dayScheduling) {
            if (date.intdate == integer) {
                dayScheduling.remove(date);
                break;
            }
        }
    }

    private void setDayToSpinner(availableSchedule schedule) {
        if (!schedule.day.isEmpty()) {
            String days = "";
            if (schedule.day.size() == 1) {
                days = days + Days.getDay(schedule.day.get(0));
            } else {
                for (Integer day : schedule.day) {
                    days = days + Days.getShortDay(day) + ", ";
                }
            }
            days = days.replaceAll(", $", "");
            sp_available_days.setText(days);
        } else {
            sp_available_days.setText("");
        }
    }

    // ----------------------------------------------------------------------------------------------------------


    // ----------------------------------------- showAvailableTimeDialog -----------------------------------------

    @OnClick(R.id.sp_available_times)
    public void showAvailableTimeDialog() {
        if (editAble) {
            if (!validateAvailableTimeDialog())
                return;

            ServiceOccupiedDataModel serviceOccupiedDataModel = getModel(currMonth, currYear);

            final Dialog dialog = new Dialog(AddNewService.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_dialog_layout);
            dialog.setCancelable(false);

            TextView tv_dg_main_title = dialog.findViewById(R.id.tv_dg_main_title);
            tv_dg_main_title.setText(getString(R.string.select_you_available_times));
            ListView listview = dialog.findViewById(R.id.listview);

            final CustomDialogAdapter customDialogAdapter = new CustomDialogAdapter(this, serviceOccupiedDataModel, ServiceSpinnerType.TIMES.getType());
            listview.setAdapter(customDialogAdapter);
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    ArrayList<slotes> slotes = getOccupiedDayTimeSlots(currMonth, currYear);
                    if (slotes != null) {
                        addOrRemoveSlote(slotes.get(position), !slotes.get(position).isAllocate);
                        slotes.get(position).isAllocate = !slotes.get(position).isAllocate;
                        customDialogAdapter.notifyDataSetChanged();
                    }
                }
            });

            dialog.show();

            Button btn_submit = dialog.findViewById(R.id.btn_submit);
            btn_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    sp_available_times.setText(getSelectedTime());
                    setupDaySchedulings();
                }
            });
        } else
            showDisabledFieldPopup();
    }

    // add or remove slote to or from particular date
    private void addOrRemoveSlote(slotes slotes, boolean forAdd) {
        if (!dayScheduling.isEmpty()) {
            for (allocatedDates dates : dayScheduling) {
                if (dates.month == currMonth && dates.year == currYear) {
                    if (forAdd) {
                        dates.slotes.add(slotes);
                    } else {
                        if (serviceFrom == AddServiceFrom.FROM_EDIT_SERVICE) {
                            for (slotes slote : dates.slotes) {
                                if (slote.time.equals(slotes.time)) {
                                    dates.slotes.remove(slote);
                                    break;
                                }
                            }
                        } else
                            dates.slotes.remove(slotes);
                    }
                }
            }
        }
    }

    // retrive selected time slotes from selected day
    public String getSelectedTime() {
        ArrayList<slotes> slotes = getOccupiedDayTimeSlots(currMonth, currYear);
        if (slotes != null)
            for (slotes slote : slotes)
                if (slote.isAllocate) {
                    return slote.time;
                }
        return "";
    }

    // assign time slotes to dates of selected days
    private void setupDaySchedulings() {
        if (dayScheduling.isEmpty()) {
            ArrayList<slotes> slotes = new ArrayList<>();
            for (slotes s : getOccupiedDayTimeSlots(currMonth, currYear)) {
                if (s.isAllocate)
                    slotes.add(s);
            }
            availableSchedule schedule = getAvailableSchedule();
            for (Integer day : schedule.day) {
                for (Integer integer : getDays(Days.getCalendarDay(day))) {
                    allocatedDates dates = new allocatedDates();
                    dates.slotes = new ArrayList<>();
                    dates.intdate = integer;

                    dates.month = currMonth;
                    dates.year = currYear;

                    dates.date = DateUtil.getDate(currYear, currMonth, integer);
                    dates.slotes.addAll(slotes);
                    dayScheduling.add(dates);
                }
            }
        }
        initCalendarView(getMap());
    }

    // prepare map for show dots in calendar i.e two time slotes occupied for a date
    private HashMap<String, Integer> getMap() {
        HashMap<String, Integer> map = new HashMap<>();
        for (allocatedDates dates : dayScheduling) {
            map.put(dates.date, dates.slotes.size());
        }
        return map;
    }

    // retrive time slotes of selected day
    private ArrayList<slotes> getOccupiedDayTimeSlots(int month, int year) {
        ServiceOccupiedDataModel serviceOccupiedDataModel = getModel(month, year);
        for (timeslots timeslot : serviceOccupiedDataModel.Data.timeslots) {
            if (timeslot.isSelected) {
                return timeslot.slotes;
            }
        }
        return null;
    }

    // ---------------------------------------------------------------------------------------------------------


    // ----------------------------------------- showAvailableTimeDialog -----------------------------------------

    private void showDaySelection(final String date) {
        ArrayList<slotes> dataSet = getSlotes(date);
        if (!dataSet.isEmpty()) {
            /* if (editAble) {*/
            final Dialog dialog = new Dialog(AddNewService.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_dialog_layout);
            dialog.setCancelable(false);

            TextView tv_dg_main_title = dialog.findViewById(R.id.tv_dg_main_title);
            final TextView tv_no_record_found = dialog.findViewById(R.id.tv_no_record_found);
            tv_dg_main_title.setText(getString(R.string.your_available));
            ListView listview = dialog.findViewById(R.id.listview);

            final DaySelectionDialogAdapter daySelectionDialogAdapter = new DaySelectionDialogAdapter(this, dataSet);
            listview.setAdapter(daySelectionDialogAdapter);
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    if (editAble) {
                        removeSlote(date, position);
                        daySelectionDialogAdapter.notifyDataSetChanged();
                        if (getSlotes(date).isEmpty())
                            tv_no_record_found.setVisibility(View.VISIBLE);
                        else
                            tv_no_record_found.setVisibility(View.GONE);
                    } else
                        showDisabledFieldPopup();
                }
            });

            dialog.show();

            Button btn_close = dialog.findViewById(R.id.btn_submit);
            btn_close.setText(R.string.close);
            btn_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    sp_available_times.setText(getSelectedTime());
                    initCalendarView(getMap());
                }
            });
        }
    }

    // remove particular time slot of particular date
    private void removeSlote(String date, int pos) {
        for (allocatedDates dates : dayScheduling) {
            if (dates.date.equals(date)) {
                dates.slotes.remove(pos);
                break;
            }
        }
    }

    // retrive allocated time slotes of particulat date
    private ArrayList<slotes> getSlotes(String date) {
        ArrayList<slotes> dataSet = new ArrayList<>();
        for (allocatedDates dates : dayScheduling) {
            if (dates.date.equals(date)) {
                dataSet = dates.slotes;
            }
        }
        return dataSet;
    }

    // ---------------------------------------------------------------------------------------------------------

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean doValidate() {
        if (!getAll && serviceFrom != AddServiceFrom.FROM_EDIT_SERVICE && selectedImageFile == null) {
            Toaster.shortToast(R.string.err_add_provider_image);
            return false;
        }
        if (sp_service_catagory.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_select_a_category_for_your_service);
            return false;
        }

        if (sp_service_sub_catagory.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_select_a_sub_category_for_your_service);
            return false;
        }
        if (edt_screen_name.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_your_screen_name);
            return false;
        }
        if (availableSchedules.isEmpty()) {
            if (sp_service_appointment.getText().toString().trim().isEmpty()) {
                Toaster.shortToast(R.string.err_select_service_appointment_duration);
                return false;
            }
            if (sp_available_days.getText().toString().trim().isEmpty()) {
                Toaster.shortToast(R.string.err_select_available_days);
                return false;
            }
            if (sp_available_times.getText().toString().trim().isEmpty()) {
                Toaster.shortToast(R.string.err_select_available_times);
                return false;
            }
        }
        if (edt_flat_fee.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_flat_fee);
            return false;
        }
        if (edt_offer_details.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_your_offer_details_of_service);
            return false;
        }
        if (edtSearchKeyword.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_please_add_keywords_to_your_service_so_our_buyer_can_search_and_find_you);
            return false;
        }
        //validate selected payment type
        if (spnPaymentType.getSelectedItemPosition() == PaymentEnums.PAYPAL.getId()) {
            if (edt_paypal_email.getText().toString().trim().isEmpty()) {
                Toaster.shortToast(R.string.err_enter_paypal_email);
                return false;
            }
            if (!Globals.validateEmailAddress(edt_paypal_email.getText().toString().trim())) {
                Toaster.shortToast(R.string.err_enter_valid_paypal_email);
                return false;
            }
        } else if (spnPaymentType.getSelectedItemPosition() == PaymentEnums.BANK_ACCOUNT.getId()) {

            return validPaymentDetails(true);

        } else if (spnPaymentType.getSelectedItemPosition() == PaymentEnums.PAY_BY_CHEQUE.getId()) {
            return validPaymentDetails(false);
        }

        return true;
    }

    private boolean validPaymentDetails(boolean isFromBank) {
        if (edtPaymentFname.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_first_name);
            return false;
        }
        if (edtPaymentLname.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_last_name);
            return false;
        }

        if (tvStreetAddress.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_street_addrss);
            return false;
        }
        if (edtZipCode.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_zip_code);
            return false;
        }
        if (edtZipCode.getText().toString().trim().length() < 5) {
            Toaster.shortToast(R.string.err_zip_code_should_5_digit);
            return false;
        }
        if (isFromBank) {
            if (edtRoutingNumber.getText().toString().trim().isEmpty()) {
                Toaster.shortToast(R.string.err_enter_routing_number);
                return false;
            }
            if (edtAccountNumber.getText().toString().trim().isEmpty()) {
                Toaster.shortToast(R.string.err_enter_account_number);
                return false;
            }
        }
        return true;
    }

    @OnClick(R.id.tv_delete_service)
    public void onDeleteServiceClicks() {
        showConfirmationDelete();
    }

    public void showConfirmationDelete() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setTitle(mContext.getString(R.string.sure));
        alertDialog.setMessage(mContext.getString(R.string.delete_service_confirmation));
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(mContext.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                doRemoveService();
            }
        });

        alertDialog.setNegativeButton(mContext.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    private void doRemoveService() {
        if (!Globals.internetCheck(AddNewService.this))
            return;

        String serviceId = "";
        if (serviceFrom == AddServiceFrom.FROM_EDIT_SERVICE)
            serviceId = serviceModel.serviceId;

        JSONObject params = HttpRequestHandler.getInstance().getRemoveServiceeParams(
                userModel.Data.userId, userModel.Data.sessionId, serviceId);


        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(AddNewService.this);
        HttpRequestHandler.getInstance().post(AddNewService.this, getString(R.string.removeService), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing() && !isFinishing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                Toaster.shortToast(dataModel.Message);
                if (dataModel.Result)
                    doFinish();
                else if (dataModel.Message.contains(getString(R.string.invalid_session)))
                    globals.doLogout(AddNewService.this);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }


    private void setSpinnerAdapter() {
        spnPaymentType.setAdapter(new ArrayAdapter<PaymentEnums>(this, R.layout.layout_spinner_row_payment, R.id.tv_payment_selection, PaymentEnums.values()));
        for (int i = 0; i < PaymentEnums.values().length; i++) {
            if (occupiedDataModel.Data.paymentMethod == PaymentEnums.fromInt(i).getId()) {
                spnPaymentType.setSelection(i);
            }
        }
    }

    private void setExpandLayoutPaymentDetails(boolean isExpand) {
        expandPaymentDetail.setExpanded(isExpand);
    }

    public void doGetStateList() {
        if (!Globals.internetCheck(this))
            return;


        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(this);
        HttpRequestHandler.getInstance().get(this, getString(R.string.getStatesList), new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing() && !isFinishing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                stateListModel = new Gson().fromJson(response.toString(), StateListModel.class);
                if (stateListModel.Result) {
                    statesArrayList.clear();
                    statesArrayList.addAll(stateListModel.Data.States);
                    doDisplayStateList(occupiedDataModel, statesArrayList);
                } else {
                    Toaster.shortToast(stateListModel.Message);
                    if (stateListModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(AddNewService.this);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void doDisplayStateList(ServiceOccupiedDataModel serviceOccupiedDataModel, ArrayList<StateListModel.States> stateListModelArrayList) {
        statesArrayAdapter = new ArrayAdapter<StateListModel.States>(this, R.layout.layout_spinner_row_payment, R.id.tv_payment_selection, stateListModelArrayList);
        spnState.setAdapter(statesArrayAdapter);

        if (serviceOccupiedDataModel.Data.paymentDetail.state != null && !stateListModelArrayList.isEmpty()) {
            for (int i = 0; i < stateListModelArrayList.size(); i++) {
                if (stateListModelArrayList.get(i)._id.equals(serviceOccupiedDataModel.Data.paymentDetail.state)) {
                    spnState.setSelection(i);
                }
            }
        }
    }

    private void doGetCityList(String _id) {
        if (!Globals.internetCheck(this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getCityListParam(_id);
        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(this);
        HttpRequestHandler.getInstance().post(this, getString(R.string.getCitiesList), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing() && !isFinishing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                cities = new Gson().fromJson(response.toString(), StateListModel.class);
                if (cities.Result) {
                    doDisplayCityList(cities);
                } else {
                    Toaster.shortToast(cities.Message);
                    if (cities.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(AddNewService.this);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void doDisplayCityList(StateListModel citiesModel) {
        if (citiesModel.Data.Cities != null) {
            ArrayAdapter<StateListModel.Cities> dataAdapter = new ArrayAdapter<StateListModel.Cities>(this, R.layout.layout_spinner_row_payment, R.id.tv_payment_selection, citiesModel.Data.Cities);
            spnCity.setAdapter(dataAdapter);
            if (isFirstTime) {
                if (occupiedDataModel.Data.paymentDetail.city != null && citiesModel.Data != null) {
                    for (int i = 0; i < citiesModel.Data.Cities.size(); i++) {
                        if (citiesModel.Data.Cities.get(i)._id.equals(occupiedDataModel.Data.paymentDetail.city)) {
                            spnCity.setSelection(i);
                            isFirstTime = false;
                        }
                    }
                }
            }
        }
    }

    private void showConfirmChangePayment(final int position, boolean isFromRemove) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(this.getString(R.string.sure));
        alertDialog.setMessage(this.getString(isFromRemove ? R.string.you_want_to_remove_your_payment_option : R.string.you_want_to_change_your_payment_option));
        alertDialog.setCancelable(false);
        // On pressing Settings button
        alertDialog.setPositiveButton(this.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                doDisplaySelectedPaymentOption(position);
            }
        });

        alertDialog.setNegativeButton(this.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                isFirstLoad = false;
                spnPaymentType.setSelection(paymentMethod);
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    private void doDisplaySelectedPaymentOption(int position) {
        //display layout based on selection
        PaymentEnums paymentEnums = PaymentEnums.fromInt(position);
        switch (paymentEnums) {
            case SELECT_PAYMENT_TYPE:
                paymentMethod = paymentEnums.getId();
                setExpandLayoutPaymentDetails(false);
                break;
            case PAYPAL:
                paymentMethod = paymentEnums.getId();
                setExpandLayoutPaymentDetails(true);
                cardview_paypal_email.setVisibility(View.VISIBLE);
                lyBank.setVisibility(View.GONE);
                tv_note.setVisibility(View.VISIBLE);
                break;
            case BANK_ACCOUNT:
                paymentMethod = paymentEnums.getId();
                if (statesArrayList.isEmpty())
                    doGetStateList();
                setExpandLayoutPaymentDetails(true);
                cardview_paypal_email.setVisibility(View.GONE);
                lyBank.setVisibility(View.VISIBLE);
                lyExtraFeild.setVisibility(View.VISIBLE);
                tv_note.setVisibility(View.GONE);
                break;
            case PAY_BY_CHEQUE:
                paymentMethod = paymentEnums.getId();
                if (statesArrayList.isEmpty())
                    doGetStateList();
                setExpandLayoutPaymentDetails(true);
                cardview_paypal_email.setVisibility(View.GONE);
                lyBank.setVisibility(View.VISIBLE);
                lyExtraFeild.setVisibility(View.GONE);
                tv_note.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

    //Json for httpsRequest
    private JSONObject setPaymentDetail() {
        JSONObject jsnPaymentDetail = new JSONObject();
        try {
            jsnPaymentDetail.put(Constants.SBU_FirstName, edtPaymentFname.getText().toString().trim());
            jsnPaymentDetail.put(Constants.SBU_LastName, edtPaymentLname.getText().toString().trim());
            jsnPaymentDetail.put(Constants.SBU_StreetAddress, tvStreetAddress.getText().toString().trim());
            jsnPaymentDetail.put(Constants.SBU_City, selectedCity);
            jsnPaymentDetail.put(Constants.SBU_State, selectedState);
            jsnPaymentDetail.put(Constants.SBU_PaymentZipCode, edtZipCode.getText().toString().trim());
            if (paymentMethod == PaymentEnums.BANK_ACCOUNT.getId()) {
                jsnPaymentDetail.put(Constants.SBU_RoutingNumber, edtRoutingNumber.getText().toString().trim());
                jsnPaymentDetail.put(Constants.SBU_AccountNumber, edtAccountNumber.getText().toString());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsnPaymentDetail;
    }


    private void doGetAppointmentOfSpecificDate(String date) {

        if (!Globals.internetCheck(this))
            return;

        String serviceId = "";
        if (serviceFrom == AddServiceFrom.FROM_EDIT_SERVICE)
            serviceId = serviceModel.serviceId;

        JSONObject params = HttpRequestHandler.getInstance().getAppointmentOfSpecificDateParam(userModel.Data.userId, userModel.Data.sessionId, serviceId, date);
        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(this);
        HttpRequestHandler.getInstance().post(this, getString(R.string.getAppointmentOfSpecificDate), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing() && !isFinishing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                appointmentOfSpecificDateModel = new Gson().fromJson(response.toString(), AppointmentOfSpecificDateModel.class);
                if (appointmentOfSpecificDateModel.Result) {
                    displayAppointmentOfSpecificDialog(appointmentOfSpecificDateModel);
                } else {
                    if (appointmentOfSpecificDateModel.Message.contains(getString(R.string.invalid_session))) {
                        globals.doLogout(AddNewService.this);
                        Toaster.shortToast(appointmentOfSpecificDateModel.Message);
                    } else
                        displayAppointmentOfSpecificDialog(appointmentOfSpecificDateModel);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void displayAppointmentOfSpecificDialog(AppointmentOfSpecificDateModel appointmentOfSpecificDateModel) {
        final Dialog dialogForAppointment = new Dialog(AddNewService.this, R.style.CustomDialogAppointmentTheme);
        dialogForAppointment.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogForAppointment.setContentView(R.layout.custom_edit_dialog_layout);
        dialogForAppointment.setCancelable(false);

        TextView tv_dg_main_title = dialogForAppointment.findViewById(R.id.tv_dg_main_title);
        tvNoRecordAppointment = dialogForAppointment.findViewById(R.id.tv_no_record_found);
        tv_dg_main_title.setText(getString(R.string.your_appointment));
        listViewAppointment = dialogForAppointment.findViewById(R.id.listview);

        if (appointmentOfSpecificDateModel.Data == null) {
            listViewAppointment.setVisibility(View.GONE);
            tvNoRecordAppointment.setVisibility(View.VISIBLE);
            tvNoRecordAppointment.setText(appointmentOfSpecificDateModel.Message);
        } else {
            listViewAppointment.setVisibility(View.VISIBLE);
            tvNoRecordAppointment.setVisibility(View.GONE);
            appointmentSpecificDateAdapter = new AppointmentSpecificDateAdapter(this, appointmentOfSpecificDateModel.Data);
            listViewAppointment.setAdapter(appointmentSpecificDateAdapter);
            appointmentSpecificDateAdapter.setDoCancelRequestListner(this);
        }
        dialogForAppointment.show();

        Button btn_close = dialogForAppointment.findViewById(R.id.btn_close);
        btn_close.setText(R.string.close);
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogForAppointment.dismiss();
                closeDialog();
            }
        });
        ImageView imgClose = dialogForAppointment.findViewById(R.id.img_remove);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogForAppointment.dismiss();
                closeDialog();
            }
        });
    }

    private void closeDialog() {

        sp_available_times.setText(getSelectedTime());
        initCalendarView(getMap());
    }

    @Override
    public void doCancelRequest(int position) {
        showDoCancelConfirm(position);
    }

    private void showDoCancelConfirm(final int position) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        alertDialog.setTitle(mContext.getString(R.string.sure));
        alertDialog.setMessage(mContext.getString(R.string.you_want_to_cancel_an_appointment, appointmentOfSpecificDateModel.Data.get(position).duration));
        alertDialog.setCancelable(false);
        // On pressing Settings button
        alertDialog.setPositiveButton(mContext.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                doAskReasonToCancel(position);
            }
        });

        alertDialog.setNegativeButton(mContext.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    private void doAskReasonToCancel(final int position) {
        final Dialog dialog = new Dialog(AddNewService.this, R.style.CustomDialogReasonTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_edit_dialog_layout);
        dialog.setCancelable(false);

        TextView tv_dg_main_title = dialog.findViewById(R.id.tv_dg_main_title);
        RelativeLayout rlListView = dialog.findViewById(R.id.rl_list_view);
        LinearLayout lyReasonToCancel = dialog.findViewById(R.id.ly_reason);
        Button btn_close = dialog.findViewById(R.id.btn_close);
        ImageView imgRemove = dialog.findViewById(R.id.img_remove);
        final EditText edtReasonToCancel = dialog.findViewById(R.id.edt_reason_to_cancel);


        rlListView.setVisibility(View.GONE);
        lyReasonToCancel.setVisibility(View.VISIBLE);
        tv_dg_main_title.setText(mContext.getString(R.string.reasonToCancel));
        btn_close.setText(mContext.getString(R.string.cancel_this_request));


        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtReasonToCancel.getText().toString().trim().isEmpty()) {
                    Toaster.shortToast(R.string.please_enter_reason_to_cancel);
                    return;
                }
                doCancelAppointment(position, edtReasonToCancel.getText().toString().trim());
                dialog.cancel();
            }
        });
        imgRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();

    }

    public void doCancelAppointment(final int position, String reasonTocancel) {
        if (!Globals.internetCheck(AddNewService.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().doCancelRequest(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                appointmentOfSpecificDateModel.Data.get(position).requestId,
                appointmentOfSpecificDateModel.Data.get(position).serviceId,
                appointmentOfSpecificDateModel.Data.get(position).timeSlotId,
                reasonTocancel,
                appointmentOfSpecificDateModel.Data.get(position).flatFee);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(AddNewService.this);

        HttpRequestHandler.getInstance().post(AddNewService.this, getString(R.string.doCancelRequest), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing() && !isFinishing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel.Result) {
                    appointmentOfSpecificDateModel.Data.remove(position);
                    appointmentSpecificDateAdapter.notifyDataSetChanged();
                    if (appointmentOfSpecificDateModel.Data.isEmpty()) {
                        listViewAppointment.setVisibility(View.GONE);
                        tvNoRecordAppointment.setVisibility(View.VISIBLE);
                        tvNoRecordAppointment.setText(getString(R.string.you_do_not_have_any_appointment));
                    }
                } else {

                    if (dataModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(AddNewService.this);
                }
                Toaster.shortToast(dataModel.Message);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }


    public void doGetSubCategory(final boolean setFirstForm) {
        if (!Globals.internetCheck(AddNewService.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().doSubCategoryParam(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                categoryId, false);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(AddNewService.this);

        HttpRequestHandler.getInstance().post(AddNewService.this, getString(R.string.getSubSubcategories), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing() && !isFinishing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                serviceSubCatagoryModel = new Gson().fromJson(response.toString(), ServiceCatagoryModel.class);
                isFromFirstCall = setFirstForm;
                if (serviceSubCatagoryModel.Result) {
                    if (serviceSubCatagoryModel.Data.subCategories != null && !serviceSubCatagoryModel.Data.subCategories.isEmpty())
                        serviceSubCatagoryModel.Data.subCategories.get(0).isAllocate = true;
                    if (getAll) {
                        setSubCategoryWantedService();
                    } else if (serviceFrom == AddServiceFrom.FROM_EDIT_SERVICE) {
                        serviceSubCatagoryModel.Data.subCategories.get(0).isAllocate = false;
                        if (isFromFirstCall) {
                            setSubCategoryForEdit();
                            isFromFirstCall = false;

                        }
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void doDisplaySubCategory(final boolean isSubCategory) {

        final Dialog dialog = new Dialog(AddNewService.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog_layout);
        dialog.setCancelable(false);

        TextView tv_dg_main_title = dialog.findViewById(R.id.tv_dg_main_title);
        TextView tv_no_record_found = dialog.findViewById(R.id.tv_no_record_found);
        tv_dg_main_title.setText(getString(R.string.select_a_sub_category));
        final ListView listview = dialog.findViewById(R.id.listview);

        if (isSubCategory) {
            final CustomDialogAdapter customDialogAdapter = new CustomDialogAdapter(this, serviceSubCatagoryModel, ServiceSpinnerType.SUB_CATEGORY.getType());
            listview.setAdapter(customDialogAdapter);
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    for (int i = 0; i < serviceSubCatagoryModel.Data.subCategories.size(); i++) {
                        ServiceCatagoryModel.SubCategories subCategories = serviceSubCatagoryModel.Data.subCategories.get(i);
                        subCategories.isAllocate = i == position;
                    }
                    customDialogAdapter.notifyDataSetChanged();
                }
            });
        } else {
            tv_no_record_found.setVisibility(View.VISIBLE);
            tv_no_record_found.setText(serviceSubCatagoryModel.Message);
        }


        dialog.show();

        Button btn_submit = dialog.findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isSubCategory)
                    setSubCatagory();

                dialog.dismiss();
            }
        });
    }


    private void setSubCategoryForEdit() {
        // show selected sub category
        for (ServiceCatagoryModel.SubCategories subCategories : serviceSubCatagoryModel.Data.subCategories) {
            if (subCategories._id.equalsIgnoreCase(occupiedDataModel.Data.serviceDetail.subCategoryId)) {
                sp_service_sub_catagory.setText(subCategories.subcatName);
                subCategoryId = subCategories._id;
                subCategories.isAllocate = true;
            }
        }


    }


    // ----------------------------------------- showSubCatagoryDialog -----------------------------------------

    @OnClick(R.id.sp_service_sub_catagory)
    public void showSubCategoryDialog() {

        if (categoryId.isEmpty()) {
            Toaster.shortToast(R.string.err_select_a_category_for_your_service);
            return;
        }

        if (serviceSubCatagoryModel.Result) {
            if (serviceSubCatagoryModel.Data.subCategories != null && !serviceSubCatagoryModel.Data.subCategories.isEmpty()) {
                if (!isFromFirstCall) {
                    if (serviceFrom == AddServiceFrom.FROM_EDIT_SERVICE) {
                        for (ServiceCatagoryModel.SubCategories subCategories : serviceSubCatagoryModel.Data.subCategories) {
                            if (subCategories._id.equalsIgnoreCase(subCategoryId)) {
                                subCategories.isAllocate = true;
                            }
                        }
                    }
                    doDisplaySubCategory(true);
                }
            }
        } else {
            doDisplaySubCategory(false);
            if (serviceSubCatagoryModel.Message.contains(getString(R.string.invalid_session))) {
                globals.doLogout(AddNewService.this);
                Toaster.shortToast(serviceSubCatagoryModel.Message);
            }
        }

    }

    private void setSubCatagory() {
        for (ServiceCatagoryModel.SubCategories subCategories : serviceSubCatagoryModel.Data.subCategories) {
            if (subCategories.isAllocate) {
                subCategoryId = subCategories._id;
                sp_service_sub_catagory.setText(subCategories.subcatName);
            }
        }
    }
}
