package com.shopbyuber.seller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.shopbyuber.R;
import com.shopbyuber.model.AppointmentOfSpecificDateModel;

import java.util.ArrayList;


public class AppointmentSpecificDateAdapter extends BaseAdapter implements AdapterView.OnItemClickListener {
    private Context mContext;
    private ArrayList<AppointmentOfSpecificDateModel.Data> dataArrayList;
    private DoCancelRequestListner doCancelRequestListner;

    public AppointmentSpecificDateAdapter(Context mContext, ArrayList<AppointmentOfSpecificDateModel.Data> dataArrayList) {
        this.mContext = mContext;
        this.dataArrayList = dataArrayList;
    }

    @Override
    public int getCount() {
        return dataArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final AppointmentSpecificDateAdapter.ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new AppointmentSpecificDateAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.custom_dialog_edit_item, parent, false);

            setView(viewHolder, convertView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (AppointmentSpecificDateAdapter.ViewHolder) convertView.getTag();
        }

        setData(position, viewHolder);

        viewHolder.tvCancelRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doCancelRequestListner.doCancelRequest(position);
            }
        });

        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    private void setView(ViewHolder viewHolder, View convertView) {
        viewHolder.imgUser = convertView.findViewById(R.id.img_user);
        viewHolder.tvUserName = convertView.findViewById(R.id.tv_user_name);
        viewHolder.tvTimeSlot = convertView.findViewById(R.id.tv_time_slot);
        viewHolder.tvServiceDescription = convertView.findViewById(R.id.tv_service_description);
        viewHolder.tvCancelRequest = convertView.findViewById(R.id.tv_cancel_request);
        viewHolder.tvAddress = convertView.findViewById(R.id.tv_address);
    }

    private void setData(int position, ViewHolder viewHolder) {
        AppointmentOfSpecificDateModel.Data data = dataArrayList.get(position);

        Glide.with(mContext)
                .load(data.userImage)
                .centerCrop()
                .placeholder(R.drawable.image_place_holder_payment)
                .crossFade()
                .dontAnimate()
                .into(viewHolder.imgUser);
        viewHolder.tvUserName.setText(data.userName == null ? "" : data.userName);
        viewHolder.tvTimeSlot.setText(data.duration == null ? "" : data.duration.substring(0, 1).toUpperCase() + (data.duration.substring(1)));
        viewHolder.tvServiceDescription.setText(data.serviceDetail == null ? "" : data.serviceDetail);
        viewHolder.tvAddress.setText(data.address == null ? "" : data.address);
    }

    private static class ViewHolder {
        ImageView imgUser;
        TextView tvUserName, tvTimeSlot, tvServiceDescription, tvCancelRequest, tvAddress;
    }

    public interface DoCancelRequestListner {
        void doCancelRequest(int position);
    }

    public void setDoCancelRequestListner(DoCancelRequestListner onItemClickListener) {
        this.doCancelRequestListner = onItemClickListener;
    }
}
