package com.shopbyuber.seller;

import android.Manifest;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.quickblox.users.model.QBUser;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.UserRole;
import com.shopbyuber.login.RegisterActivity;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.main.FragmentSettings;
import com.shopbyuber.main.FragmentTermsAndCondition;
import com.shopbyuber.main.VideoCommercialActivity;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.model.UserModel;
import com.shopbyuber.profile.FragmentBusinessProfile;
import com.shopbyuber.profile.FragmentMyProfile;
import com.shopbyuber.quickblox.DialogListFragment;
import com.shopbyuber.quickblox.helpers.RegisterNewUserTOQB;
import com.shopbyuber.user.ContactUsActivity;
import com.shopbyuber.user.NotificationActivity;
import com.shopbyuber.user.UserLandingActivity;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.CoreApp;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.NotificationCountHolder;
import com.shopbyuber.utils.Toaster;
import com.shopbyuber.wantedservice.WantedServiceTabFragment;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;

public class SellerLandingActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener,
        RegisterNewUserTOQB.OnQBRegisterListener, PermissionListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.img_Logo)
    ImageView img_Logo;

    Globals globals;
    boolean doubleBackToExitPressedOnce = false;
    TextView unread_notification;
    DataModel dataModel;
    RegisterNewUserTOQB.OnQBRegisterListener onQBRegisterListener;

    PermissionListener permissionListener;

    public static SellerLandingActivity mContext;

    public static SellerLandingActivity getInstance() {
        return mContext;
    }

    private BroadcastReceiver mCountsUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            if (intent.getAction().equals(Constants.SBU_ActionUpdateCounts)) {
                setUnreads();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_landing);
        ButterKnife.bind(this);
        checkPermission();
    }

    private void checkPermission() {
        // Check if location permission is granted??
        permissionListener = this;
        new TedPermission(this)
                .setPermissionListener(permissionListener)
                .setRationaleMessage(R.string.alert_rationale_message)
                .setDeniedMessage(R.string.denied_message)
                .setGotoSettingButtonText(R.string.ok)
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.SYSTEM_ALERT_WINDOW)
                .check();
    }

    private void init() {
        mContext = this;
        globals = (Globals) getApplicationContext();
        onQBRegisterListener = this;

        setSupportActionBar(toolbar);
        setToolbarTitle(R.string.toolbar_title_my_services);

        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        TextView tvContactUs = navigationView.findViewById(R.id.tv_contact_us);
        TextView tv_privacy_policy = navigationView.findViewById(R.id.tv_privacy_policy);
        tvContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SellerLandingActivity.this, ContactUsActivity.class));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        tv_privacy_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFragment(new FragmentTermsAndCondition().newInstance(false));
                setToolbarTitle(R.string.privacy);
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        registerBroadcast();
        addFragment(new FragmentMyServices());

        CoreApp.getInstance().initializeBubblesManager();

        // QuickBlox register
        if (globals.getUserData().Data.quickBloxId == 0) {
            QBUser qbUser = new QBUser();
            qbUser.setFullName(globals.getUserData().Data.firstName +
                    (globals.getUserData().Data.lastName == null ? "" : " " + globals.getUserData().Data.lastName));
            qbUser.setLogin(String.valueOf(globals.getUserData().Data.userId));
            qbUser.setPassword(Constants.SBU_QBPasssword);

            RegisterNewUserTOQB.getInstance().setUpQBUser(SellerLandingActivity.this,
                    globals.getUserData().Data,
                    onQBRegisterListener);
        }

        if (globals.ifOverlay()) {
            CoreApp.getInstance().initChatHeadService();
        } else {
            Intent myIntent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
            myIntent.setData(Uri.parse("package:" + getPackageName()));
            startActivityForResult(myIntent, 101);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == RESULT_OK) {
            CoreApp.getInstance().initChatHeadService();
        }
    }

    public void registerBroadcast() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mCountsUpdateReceiver,
                new IntentFilter(Constants.SBU_ActionUpdateCounts));
    }

    void addFragment(Fragment fragment) {
        if (getSupportFragmentManager().findFragmentById(R.id.FragmentContainer) != null) {
            // Remove currently loaded fragment from container
            getSupportFragmentManager()
                    .beginTransaction().
                    remove(getSupportFragmentManager().findFragmentById(R.id.FragmentContainer)).commit();
        }
        // Replace with new fragment
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.FragmentContainer, fragment);
        ft.addToBackStack(null);
        ft.commitAllowingStateLoss();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_seller, menu);
        setUpMenuNotification(menu);
        return true;
    }

    private void setUpMenuNotification(Menu menu) {
        final MenuItem action_notification = menu.findItem(R.id.action_notification);
        unread_notification = action_notification.getActionView().findViewById(R.id.unread_notification);
        action_notification.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doNotoficationIntent();
            }
        });
        getUnreadNotification();
    }

    private void doNotoficationIntent() {
        startActivity(new Intent(SellerLandingActivity.this, NotificationActivity.class));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_notification:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            // Close drawer if open
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                finish();
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, getString(R.string.back_again), Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    protected void setFragment(Fragment fragment) {
        if (getSupportFragmentManager().findFragmentById(R.id.FragmentContainer) != null) {
            getSupportFragmentManager()
                    .beginTransaction().
                    remove(getSupportFragmentManager().findFragmentById(R.id.FragmentContainer)).commit();
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.FragmentContainer, fragment);
        ft.addToBackStack(null);
        ft.commitAllowingStateLoss();
    }

    private void setToolbarTitle(int title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            img_Logo.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_my_services:
                setToolbarTitle(R.string.toolbar_title_my_services);
                setFragment(new FragmentMyServices());
                break;
            case R.id.nav_wanted_service:
                setToolbarTitle(R.string.wanted_service);
                setFragment(new WantedServiceTabFragment());
                break;
            case R.id.nav_request:
                setToolbarTitle(R.string.toolbar_title_my_requests);
                setFragment(new FragmentMyRequest());
                break;
            case R.id.nav_appoinments:
                setToolbarTitle(R.string.toolbar_title_my_appointment);
                setFragment(new FragmentMyAppointments());
                break;
            case R.id.nav_my_sales:
                setToolbarTitle(R.string.my_sales);
                setFragment(new FragmentMySales());
                break;
            case R.id.nav_buisnessprofile:
                setToolbarTitle(R.string.business_profile);
                setFragment(new FragmentBusinessProfile());
                break;
            case R.id.nav_profile:
                setToolbarTitle(R.string.profile);
                setFragment(new FragmentMyProfile());
                break;
            case R.id.nav_settings:
                setToolbarTitle(R.string.settings);
                setFragment(new FragmentSettings());
                break;
            case R.id.nav_buyonuber:
                showSwitchProfileAlert();
                break;
            case R.id.nav_terms:
                setToolbarTitle(R.string.terms);
                setFragment(new FragmentTermsAndCondition().newInstance(true));
                break;
            case R.id.nav_chat:
                setToolbarTitle(R.string.chat);
                setFragment(new DialogListFragment());
                break;
            case R.id.nav_commercial:
                startActivity(new Intent(SellerLandingActivity.this, VideoCommercialActivity.class)
                        .putExtra(Constants.SBU_IsFlag, true));
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void getUnreadNotification() {
        if (!Globals.internetCheck(this)) {
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().getUnreadNotification(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId);

        HttpRequestHandler.getInstance().post(this, getString(R.string.getUnreadNotificationCount), params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel.Result && dataModel != null && dataModel.Data != null) {
                    NotificationCountHolder.getInstance().setUnreads(dataModel.Data.userUnreadNotificationCount);
                    setUnreads();
                }
            }
        });
    }

    private void setUnreads() {
        if (unread_notification != null) {
            if (NotificationCountHolder.getInstance().getUnreads() > 0) {
                unread_notification.setVisibility(View.VISIBLE);
                unread_notification.setText(String.valueOf(NotificationCountHolder.getInstance().getUnreads()));
            } else
                unread_notification.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUnreads();
    }

    public void showSwitchProfileAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Title
        alertDialog.setTitle(getString(R.string.sure));
        // Setting Dialog Message
        alertDialog.setMessage(getString(R.string.switch_to_buyer));
        alertDialog.setCancelable(false);
        // On pressing Settings button
        alertDialog.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                doSwitchUser();
            }
        });
        alertDialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    // perform logout
    public void doLogout(String userId, String sessionId) {
        JSONObject params = HttpRequestHandler.getInstance().getLogoutParams(userId, sessionId);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(this);

        HttpRequestHandler.getInstance().post(this, getString(R.string.signOutURL), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                gotoRegister();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                gotoRegister();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                gotoRegister();
            }
        });
    }

    private void gotoRegister() {
        NotificationManager nMgr = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancelAll();
        NotificationCountHolder.getInstance().setUnreads(0);
        globals.setUserData(null);
        startActivity(new Intent(this, RegisterActivity.class)
                .putExtra(Constants.SBU_UserRole, UserRole.BUYER));
        this.finishAffinity();
    }

    private void doSwitchUser() {
        if (!Globals.internetCheck(this)) {
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().getSwitchUserParam(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId, UserRole.BUYER.getRole());

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(SellerLandingActivity.this);

        HttpRequestHandler.getInstance().post(this, getString(R.string.switchUserLogin), params, new JsonHttpResponseHandler() {

            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                UserModel switchUserLoginModel = new Gson().fromJson(response.toString(), UserModel.class);
                if (switchUserLoginModel.Result) {
                    if (switchUserLoginModel != null && switchUserLoginModel.Data != null) {
                        try {
                            NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            nMgr.cancelAll();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        globals.setUserData(switchUserLoginModel);
                        startActivity(new Intent(SellerLandingActivity.this, UserLandingActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                                .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                        finish();
                    }
                } else {
                    Toaster.shortToast(switchUserLoginModel.Message);
                    if (switchUserLoginModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(SellerLandingActivity.this);
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    @Override
    public void onRegisterSuccess(Integer qbUserId) {

    }

    @Override
    public void onRegisterFailed(String errorMessage) {

    }

    @Override
    public void onPermissionGranted() {
        init();
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermissions) {
        finish();
    }
}
