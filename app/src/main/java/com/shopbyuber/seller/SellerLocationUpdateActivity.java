package com.shopbyuber.seller;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.MarkerViewOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.plugins.locationlayer.LocationLayerMode;
import com.mapbox.mapboxsdk.plugins.locationlayer.LocationLayerPlugin;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher;
import com.mapbox.services.android.navigation.ui.v5.NavigationViewOptions;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;
import com.mapbox.services.android.navigation.v5.navigation.NavigationUnitType;
import com.mapbox.services.android.telemetry.location.LocationEngine;
import com.mapbox.services.android.telemetry.location.LocationEngineListener;
import com.mapbox.services.android.telemetry.location.LostLocationEngine;
import com.mapbox.services.api.utils.turf.TurfConstants;
import com.mapbox.services.api.utils.turf.TurfMeasurement;
import com.mapbox.services.commons.geojson.LineString;
import com.mapbox.services.commons.models.Position;
import com.orhanobut.logger.Logger;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.channel_group.PNChannelGroupsDeleteGroupResult;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.ServiceStatus;
import com.shopbyuber.enums.UserRole;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.model.PubnubDataModel;
import com.shopbyuber.model.UserModel;
import com.shopbyuber.user.OrderDetailActivity;
import com.shopbyuber.user.UserLandingActivity;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mapbox.services.android.navigation.ui.v5.R.string;
import static com.mapbox.services.android.telemetry.location.LocationEnginePriority.LOW_POWER;

public class SellerLocationUpdateActivity extends BaseActivity implements OnMapReadyCallback,
        LocationEngineListener, Callback<DirectionsResponse>, PermissionListener {
    @BindView(R.id.mapView)
    MapView mapView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.layout_vendor_details)
    LinearLayout layout_vendor_details;
    @BindView(R.id.lnr_start_navigation)
    LinearLayout lnr_start_navigation;
    @BindView(R.id.btn_start_navigation)
    Button btn_start_navigation;

    @BindView(R.id.tv_provider_name)
    TextView tvProviderName;
    @BindView(R.id.tv_provider_type)
    TextView tvProviderType;
    @BindView(R.id.img_service_provider)
    ImageView imgServiceProvider;
    @BindView(R.id.tv_time_remains)
    TextView tvTimeRemains;

    private LocationLayerPlugin locationLayer;
    private LocationEngine locationEngine;
    private MapboxMap mapboxMap;

    private Point originePosition;
    private Point destination;
    private Point currentPosition;
    private static final int CAMERA_ANIMATION_DURATION = 1000;

    private NavigationMapRoute mapRoute;
    private DirectionsRoute route, sellerUpdateRoute;

    Globals globals;
    UserModel userModel;
    PermissionListener permissionListener;
    ArrayList<String> createdPubChannelNames;
    String requestId = "";
    PubnubDataModel pubnubDataModel;
    ACProgressFlower dialog;

    PubNub pubnub;
    SubscribeCallback subscribeCallback;
    Marker vendorMarker, estimateMarker;
    private double targetDistance;
    int reqCode = 1;
    boolean completeService = false, isArrieved = false;

    private static final int CAMERA_TILT = 57;
    private static final int CAMERA_ZOOM = 16;

    public static SellerLocationUpdateActivity mContext;

    public static SellerLocationUpdateActivity getInstance() {
        return mContext;
    }

    //display rating popup when vendor arrived
    private BroadcastReceiver showRatingPopupReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            if (intent.getAction().equals(Constants.SBU_ActionVendorArrieved)) {
                Bundle bundle = intent.getExtras();
                String requstId = bundle.getString(Constants.SBU_RequestId);
                if (pubnubDataModel.Data != null && requstId.equals(pubnubDataModel.Data.mapData.get(0).requestId)) {
                    removePubnubLisner();
                    showRatingPopup();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            tvTimeRemains.setText(getString(R.string.provider_is_arrived_to_your_location));
                        }
                    }, 3000);

                }
            }
        }
    };

    private void removePubnubLisner() {
        if (userModel.Data.userRole == UserRole.BUYER.getRole() &&
                pubnub != null) {
            try {
                pubnub.removeListener(subscribeCallback);
            } catch (Exception e) {
                Logger.e(e.getMessage());
            }
        }
    }

    private void showRatingPopup() {
        final Dialog dialog = new Dialog(SellerLocationUpdateActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_rate_review);
        dialog.setCancelable(false);

        Toolbar dialogToolbar = dialog.findViewById(R.id.toolbar);
        CircleImageView profileImg = dialog.findViewById(R.id.profile_img);
        TextView profileName = dialog.findViewById(R.id.profile_name);
        final RatingBar ratingBar = dialog.findViewById(R.id.ratingBar);
        final EditText edtFeedback = dialog.findViewById(R.id.edt_feedback);
        final Button btn_submit = dialog.findViewById(R.id.btn_submit);

        dialogToolbar.setVisibility(View.GONE);
        if (pubnubDataModel.Data != null) {
            Glide.with(this)
                    .load(pubnubDataModel.Data.mapData.get(0).serviceProviderImage)
                    .centerCrop()
                    .placeholder(R.drawable.image_place_holder)
                    .crossFade()
                    .dontAnimate()
                    .into(profileImg);

            profileName.setText(pubnubDataModel.Data.mapData.get(0).serviceProvider != null ? pubnubDataModel.Data.mapData.get(0).serviceProvider : "");

            ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar1, float rating, boolean fromUser) {
                    if (rating > 0) {
                        btn_submit.setEnabled(true);
                        btn_submit.setAlpha(1);
                    } else {
                        btn_submit.setEnabled(false);
                        btn_submit.setAlpha(.5f);
                    }
                }
            });

            btn_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    doGiveRatingFeedbackService(ratingBar.getRating(),
                            edtFeedback.getText().toString().trim());
                }
            });
        }
        dialog.show();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_view);
        ButterKnife.bind(this);
        mapView.onCreate(savedInstanceState);
        checkPermission();
    }

    private void checkPermission() {
        // Check if location permission is granted??
        permissionListener = this;
        new TedPermission(this)
                .setPermissionListener(permissionListener)
                .setRationaleMessage(R.string.rationale_message)
                .setDeniedMessage(R.string.denied_message)
                .setGotoSettingButtonText(R.string.ok)
                .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                .check();
    }

    private void init() {
        mContext = this;
        globals = ((Globals) getApplicationContext());
        userModel = globals.getUserData();
        createdPubChannelNames = new ArrayList<>();

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            if (userModel.Data.userRole == UserRole.BUYER.getRole()) {
                layout_vendor_details.setVisibility(View.VISIBLE);
                getSupportActionBar().setTitle(R.string.tracking_progress);
            } else {
                lnr_start_navigation.setVisibility(View.VISIBLE);
                getSupportActionBar().setTitle(R.string.navigation_to_buyer_location);
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        registerBroadcast();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            createdPubChannelNames = bundle.getStringArrayList(Constants.SBU_CreatedPubChannelNames);
        }

        mapView.getMapAsync(this);
    }

    public void registerBroadcast() {
        LocalBroadcastManager.getInstance(this).registerReceiver(showRatingPopupReceiver,
                new IntentFilter(Constants.SBU_ActionVendorArrieved));
    }

    @SuppressWarnings({"MissingPermission"})
    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(showRatingPopupReceiver);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @OnClick(R.id.btn_start_navigation)
    public void startNavigation() {
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showSettingAlert();
            return;
        }
        //redirect to route screen or start service
        if (!completeService) {
            if (route != null) {
                NavigationViewOptions.Builder optionsBuilder = NavigationViewOptions.builder()
                        .unitType(NavigationUnitType.TYPE_IMPERIAL)
                        .shouldSimulateRoute(false);
                optionsBuilder.directionsRoute(route);

                NavigationLauncher.startNavigation(this, optionsBuilder.build(), createdPubChannelNames.get(0));
            }
        } else {
            deletePubnubChannel();
            isArrieved = true;
            doChangeRequestStatus(ServiceStatus.StartService.getService());
        }
    }

    private void deletePubnubChannel() {
        try {
            this.pubnub.deleteChannelGroup().channelGroup(createdPubChannelNames.get(0))
                    .async(new PNCallback<PNChannelGroupsDeleteGroupResult>() {
                        @Override
                        public void onResponse(PNChannelGroupsDeleteGroupResult result, PNStatus status) {
                            if (!status.isError()) {
                                Logger.d(status);
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_has_your_vendor_arrived)
    public void completeService() {
        displaycompleteConfirmation();
    }

    private void displaycompleteConfirmation() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setMessage(mContext.getString(R.string.complete_the_task));
        alertDialog.setCancelable(false);
        // On pressing Settings button
        alertDialog.setPositiveButton(mContext.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                deletePubnubChannel();
                doChangeRequestStatus(ServiceStatus.Completed.getService());
            }
        });

        alertDialog.setNegativeButton(mContext.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public void doChangeRequestStatus(final int status) {
        if (!Globals.internetCheck(SellerLocationUpdateActivity.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().ConfirmRequestParams(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                requestId,
                status,
                createdPubChannelNames.get(0));

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(SellerLocationUpdateActivity.this);

        HttpRequestHandler.getInstance().post(SellerLocationUpdateActivity.this, getString(R.string.changeRequestStatus), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel.Result) {
                    if (isArrieved) {
                        Intent intent = new Intent(SellerLocationUpdateActivity.this, ViewRequestActivity.class);
                        intent.putExtra(Constants.SBU_RequestId, requestId);
                        intent.putExtra(Constants.SBU_Status, status);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                                Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        finish();
                    } else if (userModel.Data.userRole == UserRole.BUYER.getRole() && status == ServiceStatus.Completed.getService()) {
                        showRatingPopup();
                    }
                } else {
                    Toaster.shortToast(dataModel.Message);
                    if (dataModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(SellerLocationUpdateActivity.this);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    @SuppressWarnings({"MissingPermission"})
    private void initLocationEngine() {
        locationEngine = new LostLocationEngine(this);
        locationEngine.setPriority(LOW_POWER);
        locationEngine.setInterval(0);
        locationEngine.setFastestInterval(1000);
        locationEngine.addLocationEngineListener(this);
        locationEngine.activate();

        if (userModel.Data.userRole == UserRole.SELLER.getRole()) {
            if (locationEngine.getLastLocation() != null) {
                Location lastLocation = locationEngine.getLastLocation();
                originePosition = Point.fromLngLat(lastLocation.getLongitude(), lastLocation.getLatitude());
            } else {
                originePosition = Point.fromLngLat(pubnubDataModel.Data.mapData.get(0).companyLongitude,
                        pubnubDataModel.Data.mapData.get(0).companyLatitude);
            }
        } else {
            originePosition = Point.fromLngLat(pubnubDataModel.Data.mapData.get(0).companyLongitude,
                    pubnubDataModel.Data.mapData.get(0).companyLatitude);
        }
    }

    @SuppressWarnings({"MissingPermission"})
    private void initLocationLayer() {
        locationLayer = new LocationLayerPlugin(mapView, mapboxMap, locationEngine);
        locationLayer.setLocationLayerEnabled(LocationLayerMode.COMPASS);
    }

    private void initMapRoute() {
        mapRoute = new NavigationMapRoute(mapView, mapboxMap);
    }

    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        doGetPubnubData();
    }

    private void fetchRoute() {
        NavigationRoute.builder()
                .accessToken(Mapbox.getAccessToken())
                .origin(originePosition)
                .destination(destination)
                .alternatives(true)
                .build()
                .getRoute(this);
    }

    private void fetchUpdatedTime() {
        Logger.d(currentPosition.latitude() + "_" + currentPosition.longitude());
        NavigationRoute.builder()
                .accessToken(Mapbox.getAccessToken())
                .origin(currentPosition)
                .destination(destination)
                .build()
                .getRoute(new Callback<DirectionsResponse>() {
                    @Override
                    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                        if (validRouteResponse(response)) {
                            sellerUpdateRoute = response.body().routes().get(0);
                            String time = Globals.getEstimatedTime(sellerUpdateRoute.duration());
                            if (userModel.Data.userRole == UserRole.BUYER.getRole()) {
                                if (ServiceStatus.getEnum(pubnubDataModel.Data.mapData.get(0).status) != ServiceStatus.StartService) {
                                    tvTimeRemains.setText(getString(R.string.provider_time_remain,
                                            time));
                                } else
                                    tvTimeRemains.setText(getString(R.string.provider_is_arrived_to_your_location));

                            }
                            addEstimationMarker(time);
                        }
                    }

                    @Override
                    public void onFailure(Call<DirectionsResponse> call, Throwable t) {

                    }
                });
    }

    @Override
    public void onConnected() {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (userModel.Data.userRole == UserRole.SELLER.getRole()) {
            originePosition = Point.fromLngLat(location.getLongitude(), location.getLatitude());
        }
    }

    @Override
    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
        if (validRouteResponse(response)) {
            route = response.body().routes().get(0);
            String time = Globals.getEstimatedTime(route.duration());
            if (userModel.Data.userRole == UserRole.BUYER.getRole()) {
                if (ServiceStatus.getEnum(pubnubDataModel.Data.mapData.get(0).status) != ServiceStatus.StartService) {
                    tvTimeRemains.setText(getString(R.string.provider_time_remain,
                            time));
                } else
                    tvTimeRemains.setText(getString(R.string.provider_is_arrived_to_your_location));
            }
            boundCameraToRoute(time);
        } else {
            showNoRouteAvailablePopup();
        }
    }

    private void showNoRouteAvailablePopup() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        alertDialog.setMessage(getString(R.string.err_cant_find_route));
        alertDialog.setCancelable(false);
        // On pressing Settings button
        alertDialog.setPositiveButton(mContext.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                finish();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    private void addEstimationMarker(String estimatedTime) {
        String[] mEstimatedTime = estimatedTime.split(" ");
        View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.marker_layout, null);
        TextView tvTime = marker.findViewById(R.id.tv_time);
        if (mEstimatedTime.length > 0)
            tvTime.setText(mEstimatedTime[0] + "\n" + mEstimatedTime[1]);

        IconFactory iconFactory = IconFactory.getInstance(this);
        Icon icon = iconFactory.fromBitmap(createBitmapFromView(SellerLocationUpdateActivity.this, marker));

        if (estimateMarker == null)
            estimateMarker = mapboxMap.addMarker(new MarkerViewOptions()
                    .position(new LatLng(pubnubDataModel.Data.mapData.get(0).userLatitude,
                            pubnubDataModel.Data.mapData.get(0).userLongitude))
                    .icon(icon));
        else
            estimateMarker.setIcon(icon);
    }

    public Bitmap createBitmapFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    public void boundCameraToRoute(final String time) {
        if (route != null) {
            List<Position> routeCoords = LineString.fromPolyline(route.geometry(),
                    com.mapbox.services.Constants.PRECISION_6).getCoordinates();
            List<LatLng> bboxPoints = new ArrayList<>();
            for (Position position : routeCoords) {
                bboxPoints.add(new LatLng(position.getLatitude(), position.getLongitude()));
            }

            try {
                mapboxMap.addPolyline(new com.mapbox.mapboxsdk.annotations.PolylineOptions()
                        .addAll(bboxPoints).width(6f)
                        .color(ContextCompat.getColor(this, R.color.mapbox_navigation_route_layer_blue)));

                LatLngBounds bounds = new LatLngBounds.Builder()
                        .includes(bboxPoints)
                        .build();
                animateCameraBbox(bounds, CAMERA_ANIMATION_DURATION, new int[]{50, 200, 50, 130});
                hideLoading();

                addVendorMarker(Position.fromCoordinates(pubnubDataModel.Data.mapData.get(0).companyLongitude,
                        pubnubDataModel.Data.mapData.get(0).companyLatitude));
                addEstimationMarker(time);

                if (userModel.Data.userRole == UserRole.SELLER.getRole() &&
                        route.distance() <= com.mapbox.services.android.navigation.ui.v5.Constants.ARRIVAL_MILESTONE_METER)
                    setReachedAndStart();

                initPubNub();
            } catch (Exception e) {
                Toaster.shortToast(getString(R.string.error_went_wrong));
                finish();
            }
        }
    }

    private void initPubNub() {
        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey(getString(string.SBU_Subscribe_key));
        pnConfiguration.setPublishKey(getString(string.SBU_Publish_key));
        pubnub = new PubNub(pnConfiguration);
        if (userModel.Data.userRole == UserRole.BUYER.getRole() &&
                ServiceStatus.getEnum(pubnubDataModel.Data.mapData.get(0).status) != ServiceStatus.StartService)
            subscribeToVendorChannel();
    }

    private boolean validRouteResponse(Response<DirectionsResponse> response) {
        return response.body() != null
                && response.body().routes() != null
                && response.body().routes().size() > 0;
    }

    @Override
    public void onFailure(Call<DirectionsResponse> call, Throwable t) {

    }

    private void animateCameraBbox(LatLngBounds bounds, int animationTime, int[] padding) {
        mapboxMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds,
                padding[0], padding[1], padding[2], padding[3]), animationTime);
    }

    private void animateCamera(LatLng point) {
        mapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(point, 16), CAMERA_ANIMATION_DURATION);
    }

    private void hideLoading() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPermissionGranted() {
        init();
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermissions) {
        finish();
    }

    public void doGetPubnubData() {
        if (!Globals.internetCheck(SellerLocationUpdateActivity.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getPubnubData(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                createdPubChannelNames);

        dialog = HttpRequestHandler.getInstance().getProgressBar(SellerLocationUpdateActivity.this);

        HttpRequestHandler.getInstance().post(SellerLocationUpdateActivity.this, getString(R.string.getPubnubData), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                pubnubDataModel = new Gson().fromJson(response.toString(), PubnubDataModel.class);
                if (pubnubDataModel.Result) {
                    if (ServiceStatus.getEnum(pubnubDataModel.Data.mapData.get(0).status) == ServiceStatus.StartService)
                        showRatingPopup();

                    setUpData();
                } else {
                    hideLoading();
                    Toaster.shortToast(pubnubDataModel.Message);
                    if (pubnubDataModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(SellerLocationUpdateActivity.this);
                    else
                        onBackPressed();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                hideLoading();
                Toaster.shortToast(R.string.error_went_wrong);
                onBackPressed();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                hideLoading();
                Toaster.shortToast(R.string.error_went_wrong);
                onBackPressed();
            }
        });
    }

    private void setUpData() {
        ServiceStatus serviceStatus = ServiceStatus.getEnum(pubnubDataModel.Data.mapData.get(0).status);
        if (serviceStatus == ServiceStatus.Completed ||
                serviceStatus == ServiceStatus.Cancelled) {
            hideLoading();
            Toaster.longToast(pubnubDataModel.Data.mapData.get(0).displayMessage);
            onBackPressed();
        } else {
            requestId = pubnubDataModel.Data.mapData.get(0).requestId;
            destination = Point.fromLngLat(pubnubDataModel.Data.mapData.get(0).userLongitude,
                    pubnubDataModel.Data.mapData.get(0).userLatitude);
            initLocationEngine();
            initLocationLayer();
            initMapRoute();

            if (originePosition != null) {
                fetchRoute();
            }

            if (userModel.Data.userRole == UserRole.BUYER.getRole()) {
                tvProviderName.setText(pubnubDataModel.Data.mapData.get(0).serviceProvider);
                tvProviderType.setText(pubnubDataModel.Data.mapData.get(0).catName);
                Glide.with(mContext)
                        .load(pubnubDataModel.Data.mapData.get(0).serviceProviderImage)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .centerCrop()
                        .placeholder(R.drawable.image_place_holder)
                        .crossFade()
                        .dontAnimate()
                        .into(imgServiceProvider);

            }
        }
    }

    private void addVendorMarker(Position destination) {
        IconFactory iconFactory = IconFactory.getInstance(this);
        Icon icon = iconFactory.fromResource(R.drawable.car);
        LatLng markerPosition = new LatLng(destination.getLatitude(),
                destination.getLongitude());
        vendorMarker = mapboxMap.addMarker(new MarkerOptions()
                .position(markerPosition)
                .icon(icon));
    }

    private void subscribeToVendorChannel() {
        subscribeCallback = new SubscribeCallback() {
            @Override
            public void status(PubNub pubnub, PNStatus status) {
                Logger.d("PubNub status: " + status.getStatusCode());
            }

            @Override
            public void message(PubNub pubnub, final PNMessageResult message) {
                try {
                    final JSONObject jsonObject = new JSONObject(message.getMessage().toString());
                    Logger.json(String.valueOf(jsonObject));

                    Location targetLocation = new Location("");//provider name is unnecessary
                    targetLocation.setLatitude(jsonObject.getDouble(Constants.SBU_Latitude));
                    targetLocation.setLongitude(jsonObject.getDouble(Constants.SBU_Longitude));
                    float bearing = (float) jsonObject.getDouble(Constants.SBU_Bearing);
                    if (bearing > 0)
                        targetLocation.setBearing(bearing);

                    easeCameraToLocation(targetLocation);

                    currentPosition = Point.fromLngLat(targetLocation.getLongitude(), targetLocation.getLatitude());
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (currentPosition != null)
                                fetchUpdatedTime();
                        }
                    }, Constants.TimeUpdateInterval);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void presence(PubNub pubnub, PNPresenceEventResult presence) {

            }
        };

        this.pubnub.addListener(subscribeCallback);
        this.pubnub.subscribe().channels(Arrays.asList(createdPubChannelNames.get(0))).execute();
    }

    @Override
    public void onBackPressed() {
        removePubnubLisner();
        if (isTaskRoot()) {
            if (globals.getUserData().Data.userRole == UserRole.BUYER.getRole())
                startActivity(new Intent(this, UserLandingActivity.class));
            else
                startActivity(new Intent(this, SellerLandingActivity.class));
        }
        finish();
    }

    private void easeCameraToLocation(final Location location) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                CameraPosition cameraPosition = buildCameraPositionFromLocation(location);
                mapboxMap.easeCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 1000, false);
                animateCameraToPosition(cameraPosition);
                animateCar(location);
            }
        });
    }

    private void animateCameraToPosition(CameraPosition position) {
        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 2000,
                new MapboxMap.CancelableCallback() {
                    @Override
                    public void onCancel() {
                        Logger.d("onCancel");
                    }

                    @Override
                    public void onFinish() {
                        Logger.d("onFinish");
                    }
                });
    }

    @NonNull
    private CameraPosition buildCameraPositionFromLocation(Location location) {
        Position targetPosition = TurfMeasurement.destination(
                Position.fromCoordinates(location.getLongitude(), location.getLatitude()),
                targetDistance, location.getBearing(), TurfConstants.UNIT_METERS
        );

        LatLng target = new LatLng(
                targetPosition.getLatitude(),
                targetPosition.getLongitude()
        );

        return new CameraPosition.Builder()
                .tilt(CAMERA_TILT)
                .zoom(CAMERA_ZOOM)
                .target(target)
                .bearing(location.getBearing())
                .build();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == reqCode && resultCode == RESULT_OK) {
            setReachedAndStart();
        }
    }

    private void setReachedAndStart() {
        completeService = true;
        btn_start_navigation.setText(R.string.reached_start);
        doChangeRequestStatus(ServiceStatus.StartService.getService());
    }

    private void animateCar(final Location destination) {
        if (vendorMarker != null) {
            final LatLng startPosition = vendorMarker.getPosition();
            final LatLng endPosition = new LatLng(destination.getLatitude(),
                    destination.getLongitude());

            final LatLngInterpolator latLngInterpolator = new LatLngInterpolator.LinearFixed();

            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(2000); // duration 3 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    float v = valueAnimator.getAnimatedFraction();
                    LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                    vendorMarker.setPosition(newPosition);
                }
            });
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                }
            });
            valueAnimator.start();
        }
    }

    private interface LatLngInterpolator {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolator {
            @Override
            public LatLng interpolate(float fraction, LatLng a,
                                      LatLng b) {
                double lat = (b.getLatitude() - a.getLatitude()) * fraction + a.getLatitude();
                double lngDelta = b.getLongitude() - a.getLongitude();
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.getLongitude();
                return new LatLng(lat, lng);
            }
        }
    }

    public void showSettingAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        // Setting Dialog Title
        alertDialog.setTitle(getString(R.string.gps_setting));
        // Setting Dialog Message
        alertDialog.setMessage(getString(R.string.gps_not_enabled));
        alertDialog.setCancelable(false);
        // On pressing Settings button
        alertDialog.setPositiveButton(getString(R.string.settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });
        alertDialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public void doGiveRatingFeedbackService(float rating, String feedback) {
        if (!Globals.internetCheck(this)) {
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().giveRatingFeedbackService(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                pubnubDataModel.Data.mapData.get(0).serviceId,
                pubnubDataModel.Data.mapData.get(0).companyId,
                requestId,
                rating,
                feedback);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(SellerLocationUpdateActivity.this);

        HttpRequestHandler.getInstance().post(SellerLocationUpdateActivity.this, getString(R.string.giveRatingFeedbackService), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel.Result) {
                    Intent intent = new Intent(SellerLocationUpdateActivity.this, OrderDetailActivity.class);
                    intent.putExtra(Constants.SBU_RequestId, requestId);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                            Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(intent);
                    finish();
                } else if (dataModel.Message.contains(getString(R.string.invalid_session)))
                    globals.doLogout(SellerLocationUpdateActivity.this);
                Toaster.shortToast(dataModel.Message);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

}
