package com.shopbyuber.seller;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.shopbyuber.R;
import com.shopbyuber.model.MyRequestModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

class AdapterMyRequest extends RecyclerView.Adapter<AdapterMyRequest.ViewHolder> {

    private ArrayList<MyRequestModel.Requests> mValues;
    private final Context mContext;
    private AdapterView.OnItemClickListener onItemClickListener;

    AdapterMyRequest(Context context) {
        mContext = context;
    }

    public void doRefresh(ArrayList<MyRequestModel.Requests> services) {
        mValues = services;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_request_item, parent, false);
        return new ViewHolder(view, this);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private AdapterMyRequest adapterMyRequest;
        @BindView(R.id.img_profile)
        ImageView img_profile;
        @BindView(R.id.tv_request_title)
        TextView tv_request_title;
        @BindView(R.id.tv_duration)
        TextView tv_duration;

        public ViewHolder(View itemView, AdapterMyRequest adapterMyRequest) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.adapterMyRequest = adapterMyRequest;
            itemView.setOnClickListener(this);
        }

        void setDataToView(MyRequestModel.Requests mItem, ViewHolder viewHolder) {
            viewHolder.tv_request_title.setText(mItem.screenName);
            viewHolder.tv_duration.setText(mItem.duration);
            Glide.with(mContext)
                    .load(mItem.serviceImage)
                    .centerCrop()
                    .placeholder(R.drawable.image_place_holder)
                    .crossFade()
                    .dontAnimate()
                    .into(viewHolder.img_profile);
        }

        @Override
        public void onClick(View v) {
            adapterMyRequest.onItemHolderClick(ViewHolder.this);
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        MyRequestModel.Requests mItem = mValues.get(position);
        holder.setDataToView(mItem, holder);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void onItemHolderClick(ViewHolder holder) {
        if (onItemClickListener != null)
            onItemClickListener.onItemClick(null, holder.itemView, holder.getAdapterPosition(), holder.getItemId());
    }
}
