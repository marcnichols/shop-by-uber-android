package com.shopbyuber.seller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.RadioButton;

import com.shopbyuber.R;
import com.shopbyuber.model.ServiceOccupiedDataModel.slotes;

import java.util.ArrayList;


public class DaySelectionDialogAdapter extends BaseAdapter implements AdapterView.OnItemClickListener {

    private ArrayList<slotes> dataSet;
    private Context mContext;

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    public DaySelectionDialogAdapter(Context context,ArrayList<slotes> slotes) {
        this.mContext = context;
        this.dataSet = slotes;
    }

    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public Object getItem(int position) {
        return dataSet.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.custom_dialog_list_item, parent, false);
            viewHolder.radioButton = convertView.findViewById(R.id.radiobutton);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.radioButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.trash, 0);
        viewHolder.radioButton.setText(dataSet.get(position).time);

        return convertView;
    }

    // View lookup cache
    private static class ViewHolder {
        RadioButton radioButton;
    }
}
