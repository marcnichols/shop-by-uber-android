package com.shopbyuber.seller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemSpanLookup;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.AddServiceFrom;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.model.MyServicesModel;
import com.shopbyuber.model.MyServicesModel.services;
import com.shopbyuber.profile.AddNewService;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;
import com.shopbyuber.utils.paginationProgressBarAdapter;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;

import static android.app.Activity.RESULT_OK;
import static com.shopbyuber.utils.Globals.internetCheck;


public class FragmentMyServices extends Fragment implements AdapterView.OnItemClickListener
        , Paginate.Callbacks, SwipeRefreshLayout.OnRefreshListener {
    private static int REQUEST_ADD_SERVICE = 0;

    Globals globals;

    @BindView(R.id.list)
    RecyclerView myServicesRecyclerView;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tv_no_record_found)
    TextView tv_no_record_found;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    MyServicesModel myServicesModel;
    ArrayList<services> services;
    AdapterMyServices adapterMyServices;

    private Paginate paginate;
    int pageNo = 1;
    private boolean loading = false;

    public static FragmentMyServices mContext;

    public static FragmentMyServices getInstance() {
        return mContext;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_services, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        mContext = this;
        services = new ArrayList<>();
        globals = ((Globals) getActivity().getApplicationContext());

        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorToolbarTitle);
        swipeRefreshLayout.setOnRefreshListener(this);

        doGetMyServicesRequest(true);
    }

    @OnClick(R.id.add_service)
    public void addNewService() {
        startActivityForResult(new Intent(getActivity(), AddNewService.class)
                .putExtra(Constants.SBU_Add_service_from, AddServiceFrom.FROM_MY_SERVICE), REQUEST_ADD_SERVICE);
    }

    private void showNoRecordFound() {
        loading = false;
        if (services.isEmpty()) {
            if (tv_no_record_found.getVisibility() == View.GONE)
                tv_no_record_found.setText(getString(R.string.err_no_service));
            tv_no_record_found.setVisibility(View.VISIBLE);
        }
    }

    private void hideNoRecordFound() {
        if (tv_no_record_found.getVisibility() == View.VISIBLE)
            tv_no_record_found.setVisibility(View.GONE);
    }

    public void doGetMyServicesRequest(final boolean showProgress) {
        if (!Globals.internetCheck(getActivity())) {
            showNoRecordFound();
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().getMyServicesParams(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                pageNo);


        HttpRequestHandler.getInstance().post(getActivity(), getString(R.string.getMyServices), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                if (!swipeRefreshLayout.isRefreshing() && showProgress)
                    progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (progressBar.getVisibility() == View.VISIBLE)
                    progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                myServicesModel = new Gson().fromJson(response.toString(), MyServicesModel.class);
                if (myServicesModel.Result) {
                    if (swipeRefreshLayout.isRefreshing()) {
                        services.clear();
                        adapterMyServices.notifyDataSetChanged();
                    }
                    setupList(myServicesModel.Data.services);
                } else {
                    showNoRecordFound();
                    if (myServicesModel.Message.contains(getString(R.string.invalid_session))) {
                        Toaster.shortToast(myServicesModel.Message);
                        globals.doLogout(getActivity());
                    }
                }
                stopRefreshing();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                stopRefreshing();
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                stopRefreshing();
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void stopRefreshing() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    public void setServiceAvailability(int position) {
        doSetServiceAvailabilityRequest(position);
    }

    public void doSetServiceAvailabilityRequest(final int position) {
        if (!internetCheck(getActivity())) {
            return;
        }
        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(getActivity());
        JSONObject params = HttpRequestHandler.getInstance().getSetServiceAvailabilityParams(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                services.get(position).serviceId,
                !services.get(position).isAvailable);

        HttpRequestHandler.getInstance().post(getActivity(), getString(R.string.setServiceAvailability), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel.Result) {
                    services.get(position).isAvailable = !services.get(position).isAvailable;
                    adapterMyServices.notifyDataSetChanged();
                } else {
                    Toaster.shortToast(dataModel.Message);
                    if (dataModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(getActivity());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void setupList(ArrayList<services> servicesArrayList) {
        if (servicesArrayList != null && !servicesArrayList.isEmpty()) {
            services.addAll(servicesArrayList);
            setAdapter();
        } else
            showNoRecordFound();
    }

    private void setAdapter() {
        hideNoRecordFound();
        if (adapterMyServices == null) {
            if (paginate != null) {
                paginate.unbind();
            }
            adapterMyServices = new AdapterMyServices(getActivity());
            adapterMyServices.setOnItemClickListener(this);
        }
        loading = false;
        adapterMyServices.doRefresh(services);

        if (myServicesRecyclerView.getAdapter() == null) {
            myServicesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            myServicesRecyclerView.setItemAnimator(new DefaultItemAnimator());
            myServicesRecyclerView.setAdapter(adapterMyServices);
            if (services.size() < myServicesModel.Data.recordCount) {
                paginate = Paginate.with(myServicesRecyclerView, this)
                        .setLoadingTriggerThreshold(Constants.progress_threshold_2)
                        .addLoadingListItem(Constants.addLoadingRow)
                        .setLoadingListItemCreator(new paginationProgressBarAdapter())
                        .setLoadingListItemSpanSizeLookup(new LoadingListItemSpanLookup() {
                            @Override
                            public int getSpanSize() {
                                return Constants.GRID_SPAN;
                            }
                        })
                        .build();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        startActivityForResult(new Intent(getActivity(), AddNewService.class)
                .putExtra(Constants.SBU_Add_service_from, AddServiceFrom.FROM_EDIT_SERVICE)
                .putExtra(Constants.SBU_Service, services.get(position)), REQUEST_ADD_SERVICE);
    }

    @Override
    public void onLoadMore() {
        loading = true;
        pageNo++;
        doGetMyServicesRequest(false);
    }

    @Override
    public boolean isLoading() {
        return loading;
    }


    @Override
    public boolean hasLoadedAllItems() {
        return services.size() == myServicesModel.Data.recordCount;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ADD_SERVICE && resultCode == RESULT_OK) {
            if (services != null && !services.isEmpty())
                services.clear();
            if (adapterMyServices != null)
                adapterMyServices.notifyDataSetChanged();
            onRefresh();
        }
    }

    @Override
    public void onRefresh() {
        pageNo = 1;
        doGetMyServicesRequest(true);
    }
}
