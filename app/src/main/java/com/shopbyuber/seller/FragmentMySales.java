package com.shopbyuber.seller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemSpanLookup;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.model.SalesListModel;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;
import com.shopbyuber.utils.paginationProgressBarAdapter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;


public class FragmentMySales extends Fragment implements AdapterView.OnItemClickListener, Paginate.Callbacks, SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = FragmentMySales.class.getSimpleName();
    Globals globals;

    @BindView(R.id.list)
    RecyclerView mySalesRecyclerView;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tv_no_record_found)
    TextView tv_no_record_found;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    SalesListModel salesListModel;
    ArrayList<SalesListModel.Sales> arrSales;
    AdapterMySales adapterMySales;

    private Paginate paginate;
    int pageNo = 1;
    private boolean loading = false;

    public static FragmentMySales mContext;

    public static FragmentMySales getInstance() {
        return mContext;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_sales, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        mContext = this;
        arrSales = new ArrayList<>();
        globals = ((Globals) getActivity().getApplicationContext());

        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorToolbarTitle);
        swipeRefreshLayout.setOnRefreshListener(this);

        doGetMyServicesRequest(true);
    }

    private void showNoRecordFound() {
        loading = false;
        if (arrSales.isEmpty()) {
            mySalesRecyclerView.setVisibility(View.GONE);
            if (tv_no_record_found.getVisibility() == View.GONE)
                tv_no_record_found.setText(getString(R.string.you_do_not_have_any_sales));
            tv_no_record_found.setVisibility(View.VISIBLE);
        }
    }

    private void hideNoRecordFound() {
        mySalesRecyclerView.setVisibility(View.VISIBLE);
        if (tv_no_record_found.getVisibility() == View.VISIBLE)
            tv_no_record_found.setVisibility(View.GONE);
    }

    public void doGetMyServicesRequest(final boolean showProgress) {
        if (!Globals.internetCheck(getActivity())) {
            showNoRecordFound();
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().getMySalesParam(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                pageNo);

        HttpRequestHandler.getInstance().post(getActivity(), getString(R.string.getMySales), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                if (!swipeRefreshLayout.isRefreshing() && showProgress)
                    progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (progressBar.getVisibility() == View.VISIBLE)
                    progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                salesListModel = new Gson().fromJson(response.toString(), SalesListModel.class);
                if (salesListModel.Result) {
                    if (swipeRefreshLayout.isRefreshing()) {
                        arrSales.clear();
                        adapterMySales.notifyDataSetChanged();
                    }
                    setupList(salesListModel.Data.sales);
                } else {
                    showNoRecordFound();
                    if (salesListModel.Message.contains(getString(R.string.invalid_session))) {
                        Toaster.shortToast(salesListModel.Message);
                        globals.doLogout(getActivity());
                    }
                }
                stopRefreshing();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                stopRefreshing();
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                stopRefreshing();
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void stopRefreshing() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void setupList(List<SalesListModel.Sales> servicesArrayList) {
        if (servicesArrayList != null && !servicesArrayList.isEmpty()) {
            arrSales.addAll(servicesArrayList);
            setAdapter();
        } else
            showNoRecordFound();
    }

    private void setAdapter() {
        hideNoRecordFound();
        if (adapterMySales == null) {
            if (paginate != null) {
                paginate.unbind();
            }
            adapterMySales = new AdapterMySales(getActivity(), false);
            adapterMySales.setOnItemClickListener(this);
        }
        loading = false;
        adapterMySales.doRefresh(arrSales);

        if (mySalesRecyclerView.getAdapter() == null) {
            mySalesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mySalesRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mySalesRecyclerView.setAdapter(adapterMySales);
            if (arrSales.size() < salesListModel.Data.recordCount) {
                paginate = Paginate.with(mySalesRecyclerView, this)
                        .setLoadingTriggerThreshold(Constants.progress_threshold_2)
                        .addLoadingListItem(Constants.addLoadingRow)
                        .setLoadingListItemCreator(new paginationProgressBarAdapter())
                        .setLoadingListItemSpanSizeLookup(new LoadingListItemSpanLookup() {
                            @Override
                            public int getSpanSize() {
                                return Constants.GRID_SPAN;
                            }
                        })
                        .build();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Logger.d(TAG, arrSales.get(position));
        startActivity(new Intent(getActivity(), ViewRequestActivity.class)
                .putExtra(Constants.SBU_RequestId, arrSales.get(position).requestId)
                .putExtra(Constants.SBU_Status, arrSales.get(position).status));
    }

    @Override
    public void onLoadMore() {
        loading = true;
        pageNo++;
        doGetMyServicesRequest(false);
    }

    @Override
    public boolean isLoading() {
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return arrSales.size() == salesListModel.Data.recordCount;
    }

    @Override
    public void onRefresh() {
        pageNo = 1;
        doGetMyServicesRequest(true);
    }
}
