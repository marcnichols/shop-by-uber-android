package com.shopbyuber.seller;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.shopbyuber.R;
import com.shopbyuber.enums.ServiceStatus;
import com.shopbyuber.model.SalesListModel;
import com.shopbyuber.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterMySales extends RecyclerView.Adapter<AdapterMySales.ViewHolder> {

    private ArrayList<SalesListModel.Sales> mValues;
    private final Context mContext;
    private AdapterView.OnItemClickListener onItemClickListener;
    private boolean fromRecentService;

    public AdapterMySales(Context context, boolean fromRecentService) {
        mContext = context;
        this.fromRecentService = fromRecentService;
    }

    public void doRefresh(ArrayList<SalesListModel.Sales> purchases) {
        mValues = purchases;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_purchases_item, parent, false);
            return new ViewHolder(view, this);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_sales_item, parent, false);
            return new ViewHolder(view, this);
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private AdapterMySales adapterMySales;
        @BindView(R.id.img_profile)
        ImageView img_profile;
        @BindView(R.id.tv_title)
        TextView tv_title;
        @BindView(R.id.tv_price)
        TextView tv_price;
        @BindView(R.id.tv_status)
        TextView tv_status;

        @Nullable
        @BindView(R.id.tv_name)
        TextView tv_name;
        @Nullable
        @BindView(R.id.ly_name)
        LinearLayout ly_name;


        public ViewHolder(View itemView, AdapterMySales adapterMySales) {
            super(itemView);
            this.adapterMySales = adapterMySales;
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        void setDataToView(SalesListModel.Sales mItem, ViewHolder viewHolder) {
            viewHolder.tv_title.setText(mItem.screenName);
            viewHolder.tv_price.setText(Constants.SBU_Currency + " " + mItem.flatFee);

            if (!fromRecentService) {
                ly_name.setVisibility(View.VISIBLE);
                tv_name.setText(mItem.bookedBy);
            }

            ServiceStatus serviceStatus = ServiceStatus.getEnum(mItem.status);

            //set status text
            String status = "";
            switch (serviceStatus) {
                case Confirmed:
                    status = mContext.getString(R.string.waiting_for_payment);
                    break;
                case Completed:
                    status = mContext.getString(R.string.completed_gray);
                    break;
                case Cancelled:
                    status = mContext.getString(R.string.cancelled_red);
                    break;
                default:
                    break;

            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                viewHolder.tv_status.setText(Html.fromHtml(status, Html.FROM_HTML_MODE_COMPACT));
            } else {
                viewHolder.tv_status.setText(Html.fromHtml(status));
            }

            Glide.with(mContext)
                    .load(mItem.serviceImage)
                    .centerCrop()
                    .placeholder(R.drawable.image_place_holder)
                    .crossFade()
                    .dontAnimate()
                    .into(viewHolder.img_profile);
        }

        @Override
        public void onClick(View v) {
            adapterMySales.onItemHolderClick(ViewHolder.this);
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        SalesListModel.Sales mItem = mValues.get(position);
        holder.setDataToView(mItem, holder);
    }

    @Override
    public int getItemViewType(int position) {
        if (fromRecentService) return 0;
        else return 1;
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void onItemHolderClick(ViewHolder holder) {
        if (onItemClickListener != null)
            onItemClickListener.onItemClick(null, holder.itemView, holder.getAdapterPosition(), holder.getItemId());
    }
}

