package com.shopbyuber.seller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemSpanLookup;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.model.AppointmentModel;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;
import com.shopbyuber.utils.paginationProgressBarAdapter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

import static android.app.Activity.RESULT_OK;

public class FragmentRecentAppointment extends Fragment implements AdapterView.OnItemClickListener, Paginate.Callbacks, SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = FragmentRecentAppointment.class.getSimpleName();
    Globals globals;

    @BindView(R.id.list)
    RecyclerView myAppointmentRecyclerView;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tv_no_record_found)
    TextView tv_no_record_found;

    @BindView(R.id.add_service)
    TextView add_service;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private Paginate paginate;
    int pageNo = 1;
    private boolean loading = false;

    AppointmentModel appointmentModel;
    ArrayList<AppointmentModel.Appointment> arrAppointments;
    AdapterAppointment adapterAppointment;

    int reqCode = 154;

    public static FragmentRecentAppointment mContext;

    public static FragmentRecentAppointment getInstance() {
        return mContext;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_services, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        mContext = this;
        arrAppointments = new ArrayList<>();
        globals = ((Globals) getActivity().getApplicationContext());
        add_service.setVisibility(View.GONE);

        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorToolbarTitle);
        swipeRefreshLayout.setOnRefreshListener(this);

        doGetRecentAppointments(true);
    }

    public void doGetRecentAppointments(final boolean showProgress) {
        if (!Globals.internetCheck(getActivity())) {
            showNoRecordFound();
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().getRecentAppointmentsParams(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                pageNo);

        HttpRequestHandler.getInstance().post(getActivity(), getString(R.string.getRecentAppointments), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                if (!swipeRefreshLayout.isRefreshing() && showProgress)
                    progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (progressBar.getVisibility() == View.VISIBLE)
                    progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                appointmentModel = new Gson().fromJson(response.toString(), AppointmentModel.class);
                if (appointmentModel.Result) {
                    if (swipeRefreshLayout.isRefreshing()) {
                        arrAppointments.clear();
                        if (adapterAppointment != null)
                            adapterAppointment.notifyDataSetChanged();
                    }
                    setupList(appointmentModel.Data.appointment);
                } else {
                    showNoRecordFound();
                    if (appointmentModel.Message.contains(getString(R.string.invalid_session))) {
                        Toaster.shortToast(appointmentModel.Message);
                        globals.doLogout(getActivity());
                    }
                }
                stopRefreshing();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                stopRefreshing();
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                stopRefreshing();
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void stopRefreshing() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void setupList(List<AppointmentModel.Appointment> requestArrayList) {
        if (requestArrayList != null && !requestArrayList.isEmpty()) {
            arrAppointments.addAll(requestArrayList);
            setAdapter();
        } else
            showNoRecordFound();
    }

    private void setAdapter() {
        hideNoRecordFound();
        if (adapterAppointment == null) {
            if (paginate != null) {
                paginate.unbind();
            }
            adapterAppointment = new AdapterAppointment(getActivity());
            adapterAppointment.setOnItemClickListener(this);
        }
        loading = false;
        adapterAppointment.doRefresh(arrAppointments);

        if (myAppointmentRecyclerView.getAdapter() == null) {
            myAppointmentRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            myAppointmentRecyclerView.setItemAnimator(new DefaultItemAnimator());
            myAppointmentRecyclerView.setAdapter(adapterAppointment);
            if (arrAppointments.size() < appointmentModel.Data.recordCount) {
                paginate = Paginate.with(myAppointmentRecyclerView, this)
                        .setLoadingTriggerThreshold(Constants.progress_threshold_2)
                        .addLoadingListItem(Constants.addLoadingRow)
                        .setLoadingListItemCreator(new paginationProgressBarAdapter())
                        .setLoadingListItemSpanSizeLookup(new LoadingListItemSpanLookup() {
                            @Override
                            public int getSpanSize() {
                                return Constants.GRID_SPAN;
                            }
                        })
                        .build();
            }
        }
    }

    private void showNoRecordFound() {
        loading = false;
        if (arrAppointments.isEmpty()) {
            myAppointmentRecyclerView.setVisibility(View.GONE);
            if (tv_no_record_found.getVisibility() == View.GONE)
                tv_no_record_found.setText(getString(R.string.you_do_not_have_any_recent_appointment));
            tv_no_record_found.setVisibility(View.VISIBLE);
        }
    }

    private void hideNoRecordFound() {
        myAppointmentRecyclerView.setVisibility(View.VISIBLE);
        if (tv_no_record_found.getVisibility() == View.VISIBLE)
            tv_no_record_found.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Logger.d(TAG, arrAppointments.get(position));
        startActivityForResult(new Intent(getActivity(), ViewRequestActivity.class)
                        .putExtra(Constants.SBU_RequestId, arrAppointments.get(position).requestId)
                        .putExtra(Constants.SBU_Status, arrAppointments.get(position).status),
                reqCode);
    }

    @Override
    public void onLoadMore() {
        loading = true;
        pageNo++;
        doGetRecentAppointments(false);
    }

    @Override
    public boolean isLoading() {
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return arrAppointments.size() == appointmentModel.Data.recordCount;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == reqCode)
            if (resultCode == RESULT_OK) {
                arrAppointments.clear();
                adapterAppointment.doRefresh(arrAppointments);
                adapterAppointment.notifyDataSetChanged();
                pageNo = 1;
                doGetRecentAppointments(true);
            }
    }

    @Override
    public void onRefresh() {
        pageNo = 1;
        doGetRecentAppointments(true);
    }
}
