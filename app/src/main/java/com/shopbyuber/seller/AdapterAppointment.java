package com.shopbyuber.seller;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.shopbyuber.R;
import com.shopbyuber.model.AppointmentModel.Appointment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

class AdapterAppointment extends RecyclerView.Adapter<AdapterAppointment.ViewHolder> {

    private ArrayList<Appointment> mValues;
    private final Context mContext;
    private AdapterView.OnItemClickListener onItemClickListener;

    AdapterAppointment(Context context) {
        mContext = context;
    }

    public void doRefresh(ArrayList<Appointment> services) {
        mValues = services;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_request_item, parent, false);
        return new ViewHolder(view, this);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private AdapterAppointment adapterMyRequest;

        @BindView(R.id.img_profile)
        ImageView img_profile;
        @BindView(R.id.tv_request_title)
        TextView tv_request_title;
        @BindView(R.id.tv_duration)
        TextView tv_duration;

        public ViewHolder(View itemView, AdapterAppointment adapterAppointment) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.adapterMyRequest = adapterAppointment;
            itemView.setOnClickListener(this);
        }

        void setDataToView(Appointment mItem, ViewHolder viewHolder) {
            viewHolder.tv_request_title.setText(mItem.screenName);
            viewHolder.tv_duration.setText(mItem.duration);
            Glide.with(mContext)
                    .load(mItem.serviceImage)
                    .centerCrop()
                    .placeholder(R.drawable.image_place_holder)
                    .crossFade()
                    .dontAnimate()
                    .into(viewHolder.img_profile);
        }

        @Override
        public void onClick(View v) {
            adapterMyRequest.onItemHolderClick(ViewHolder.this);
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Appointment mItem = mValues.get(position);
        holder.setDataToView(mItem, holder);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void onItemHolderClick(ViewHolder holder) {
        if (onItemClickListener != null)
            onItemClickListener.onItemClick(null, holder.itemView, holder.getAdapterPosition(), holder.getItemId());
    }
}
