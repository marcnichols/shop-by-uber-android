package com.shopbyuber.seller;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.shopbyuber.R;
import com.shopbyuber.model.FavouritesOfUserModel;
import com.shopbyuber.model.FavouritesOfUserModel.Favourites;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterFavouriteOfUser extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Favourites> mValues;
    private final Context mContext;
    private AdapterView.OnItemClickListener onItemClickListener;
    LayoutInflater inflater;
    private final int View_Service = 1, View_Company = 2;

    public AdapterFavouriteOfUser(Context context) {
        mContext = context;
        inflater = LayoutInflater.from(context.getApplicationContext());
    }

    public void doRefresh(ArrayList<Favourites> favourites) {
        mValues = favourites;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return mValues.get(position).type == 1 ? View_Service : View_Company;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == View_Service) {
            return new FavouriteServiceViewHolder(inflater.inflate(R.layout.favorite_services_item, parent, false), this);
        } else {
            return new FavouriteCompanyViewHolder(inflater.inflate(R.layout.your_activity_available_item, parent, false), this);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder.getItemViewType() == View_Service) {
            setFavouriteServiceData(holder, position);
        } else {
            setFavouriteCompanyData(holder, position);
        }
    }

    class FavouriteServiceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        AdapterFavouriteOfUser adapterFavouriteOfUser;

        @BindView(R.id.img_profile)
        ImageView img_profile;
        @BindView(R.id.tv_service_title)
        TextView tv_service_title;
        @BindView(R.id.tv_price)
        TextView tv_price;
        @BindView(R.id.rb_favourite)
        RadioButton rb_favourite;

        FavouriteServiceViewHolder(View v, AdapterFavouriteOfUser adapterFavouriteOfUser) {
            super(v);
            ButterKnife.bind(this, v);
            this.adapterFavouriteOfUser = adapterFavouriteOfUser;
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            adapterFavouriteOfUser.onItemHolderClick(FavouriteServiceViewHolder.this);
        }
    }

    class FavouriteCompanyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        AdapterFavouriteOfUser adapterFavouriteOfUser;

        @BindView(R.id.img_profile)
        ImageView img_profile;
        @BindView(R.id.tv_content)
        TextView tv_content;
        @BindView(R.id.tv_detail)
        TextView tv_detail;
        @BindView(R.id.tv_available)
        TextView tv_available;
        @BindView(R.id.rb_favourite)
        RadioButton rb_favourite;

        FavouriteCompanyViewHolder(View v, AdapterFavouriteOfUser adapterFavouriteOfUser) {
            super(v);
            ButterKnife.bind(this, v);
            this.adapterFavouriteOfUser = adapterFavouriteOfUser;
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            adapterFavouriteOfUser.onItemHolderClick(FavouriteCompanyViewHolder.this);
        }
    }

    private void setFavouriteServiceData(final RecyclerView.ViewHolder holder, final int position) {
        FavouriteServiceViewHolder viewHolder = ((FavouriteServiceViewHolder) holder);
        FavouritesOfUserModel.Details data = mValues.get(position).details;

        if (Double.parseDouble(data.flatFee) > 0)
            viewHolder.tv_price.setText(mContext.getString(R.string.currency_symbol) + " " + data.flatFee);
        else
            viewHolder.tv_price.setText("");

        viewHolder.tv_service_title.setText(data.screenName);
        viewHolder.rb_favourite.setChecked(data.isFavourite);

        Glide.with(mContext)
                .load(data.profileImage)
                .centerCrop()
                .placeholder(R.drawable.image_place_holder)
                .crossFade()
                .dontAnimate()
                .into(viewHolder.img_profile);

    }

    private void setFavouriteCompanyData(final RecyclerView.ViewHolder holder, final int position) {
        FavouriteCompanyViewHolder viewHolder = ((FavouriteCompanyViewHolder) holder);
        FavouritesOfUserModel.Details data = mValues.get(position).details;

        viewHolder.rb_favourite.setChecked(data.isFavourite);
        viewHolder.tv_content.setText(data.screenName);

        if (data.serviceDescription.isEmpty())
            viewHolder.tv_detail.setText(mContext.getString(R.string.service_details_null));
        else
            viewHolder.tv_detail.setText(data.serviceDescription);

        if (data.isAvailable) {
            viewHolder.tv_available.setTextColor(ContextCompat.getColor(mContext, R.color.available_orange));
            viewHolder.tv_available.setText(mContext.getString(R.string.available_now));
        } else {
            viewHolder.tv_available.setTextColor(ContextCompat.getColor(mContext, R.color.green));
            viewHolder.tv_available.setText(mContext.getString(R.string.available_by_appointment));
        }

        Glide.with(mContext)
                .load(data.profileImage)
                .centerCrop()
                .placeholder(R.drawable.image_place_holder)
                .crossFade()
                .dontAnimate()
                .into(viewHolder.img_profile);

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void onItemHolderClick(RecyclerView.ViewHolder holder) {
        if (onItemClickListener != null)
            onItemClickListener.onItemClick(null, holder.itemView, holder.getAdapterPosition(), holder.getItemId());
    }
}

