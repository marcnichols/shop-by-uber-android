package com.shopbyuber.seller;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shopbyuber.R;
import com.shopbyuber.user.ViewPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentMyAppointments extends Fragment {
    @BindView(R.id.item_viewpager)
    ViewPager item_viewpager;
    @BindView(R.id.tb_list)
    TabLayout tb_list;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_home, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        setupViewPager(item_viewpager);
        tb_list.setupWithViewPager(item_viewpager);
        tb_list.setTabMode(TabLayout.MODE_FIXED);
        item_viewpager.setOffscreenPageLimit(1);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        viewPagerAdapter.addFrag(new FragmentRecentAppointment(), getString(R.string.recent));
        viewPagerAdapter.addFrag(new FragmentUpComingAppointment(), getString(R.string.upcoming));
        viewPager.setAdapter(viewPagerAdapter);
    }
}