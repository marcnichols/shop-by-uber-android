package com.shopbyuber.seller;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.pubnub.api.PubNub;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.ServiceStatus;
import com.shopbyuber.enums.UserRole;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.model.RemoteNotificationModel;
import com.shopbyuber.model.ViewRequestDetailModel;
import com.shopbyuber.user.OrderRequestCancellationActivity;
import com.shopbyuber.user.UserLandingActivity;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.CoreApp;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;

import static com.shopbyuber.enums.ServiceStatus.Completed;
import static com.shopbyuber.enums.ServiceStatus.Confirmed;
import static com.shopbyuber.enums.ServiceStatus.OnRouteToClient;
import static com.shopbyuber.enums.ServiceStatus.PayDone;
import static com.shopbyuber.enums.ServiceStatus.Requested;

public class ViewRequestActivity extends BaseActivity implements PermissionListener {
    Globals globals;
    PubNub pubnub;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.img_request)
    ImageView imgRequest;

    @BindView(R.id.tv_request_title)
    TextView tvRequestTitle;

    @BindView(R.id.tv_request_from)
    TextView tvRequestFrom;

    @BindView(R.id.tv_request_description)
    TextView tvRequestDescription;

    @BindView(R.id.layout_request_from)
    LinearLayout layoutRequestFrom;

    @BindView(R.id.tv_lbl_amount)
    TextView tvLblAmount;

    @BindView(R.id.tv_amount_paid)
    TextView tvAmountPaid;

    @BindView(R.id.layout_amount_paid)
    LinearLayout layoutAmountPaid;

    @BindView(R.id.tv_refund_method_txt)
    TextView tvRefundMethodTxt;

    @BindView(R.id.tv_refund_method)
    TextView tvRefundMethod;

    @BindView(R.id.layout_refund_method)
    LinearLayout layoutRefundMethod;

    @BindView(R.id.tv_category)
    TextView tvCategory;

    @BindView(R.id.tv_sub_category)
    TextView tv_sub_category;

    @BindView(R.id.tv_status)
    TextView tvStatus;

    @BindView(R.id.layout_status)
    LinearLayout layoutStatus;

    @BindView(R.id.tv_schedule)
    TextView tvSchedule;

    @BindView(R.id.tv_address)
    TextView tvAddress;

    @BindView(R.id.layout_schedule)
    LinearLayout layoutSchedule;

    @BindView(R.id.btn_request)
    Button btnRequest;

    @BindView(R.id.btn_cancel_request)
    Button btnCancelRequest;

    String requestId = "", pubChannelName = "";
    ServiceStatus serviceStatus;

    ViewRequestDetailModel data;
    @BindView(R.id.layout_main)
    LinearLayout layoutMain;

    @BindView(R.id.layout_reason)
    LinearLayout layoutReason;
    @BindView(R.id.tv_reason)
    TextView tvReason;

    int reqCode = 1;
    PermissionListener permissionListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_request);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        globals = (Globals) getApplicationContext();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            requestId = bundle.getString(Constants.SBU_RequestId) != null ? bundle.getString(Constants.SBU_RequestId) : "";
            if (bundle.containsKey(Constants.SBU_NotificationId) && Globals.isNetworkAvailable(this)) {
                globals.doMakeNotificationUnread(bundle.getString(Constants.SBU_NotificationId));
            }
        }

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        setUpData();
    }

    private void setUpData() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        doViewRequestDetailRequest();
        initPubNub();
    }

    @Override
    public void onBackPressed() {
        if (isTaskRoot()) {
            if (globals.getUserData().Data.userRole == UserRole.BUYER.getRole())
                startActivity(new Intent(this, UserLandingActivity.class));
            else
                startActivity(new Intent(this, SellerLandingActivity.class));
            finish();
        } else
            super.onBackPressed();
    }

    public void doViewRequestDetailRequest() {
        if (!Globals.internetCheck(ViewRequestActivity.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().ViewRequestDetailParams(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                requestId);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(ViewRequestActivity.this);

        HttpRequestHandler.getInstance().post(ViewRequestActivity.this, getString(R.string.viewRequestDetail), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                data = new Gson().fromJson(response.toString(), ViewRequestDetailModel.class);
                if (data.Result) {
                    layoutMain.setVisibility(View.VISIBLE);
                    setData();
                } else {
                    Toaster.shortToast(data.Message);
                    if (data.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(ViewRequestActivity.this);
                    else
                        onBackPressed();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
                onBackPressed();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
                onBackPressed();
            }
        });
    }

    public void setData() {
        Glide.with(this)
                .load(data.Data.userProfileImage)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .centerCrop()
                .placeholder(R.drawable.image_place_holder)
                .crossFade()
                .dontAnimate()
                .into(imgRequest);

        tvRequestDescription.setText(data.Data.serviceDetail);
        tvRequestTitle.setText(data.Data.screenName);
        tvCategory.setText(data.Data.catName);
        tv_sub_category.setText(data.Data.subCatName == null ? "" : data.Data.subCatName);

        tvSchedule.setText(data.Data.schedule);
        tvAddress.setText(data.Data.userAddress);
        serviceStatus = ServiceStatus.getEnum(data.Data.status);
        tvAmountPaid.setText(getString(R.string.currency_symbol) + " " + data.Data.amountPaid);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvReason.setText(Html.fromHtml(getString(R.string.lbl_reason_to_cancel_with_color,
                    data.Data.cancelReason == null ? "" : data.Data.cancelReason),
                    Html.FROM_HTML_MODE_COMPACT));
        } else {
            tvReason.setText(Html.fromHtml(getString(R.string.lbl_reason_to_cancel_with_color,
                    data.Data.cancelReason == null ? "" : data.Data.cancelReason)));
        }

        //display screen as per service status
        String toolBarTitle = "";
        switch (serviceStatus) {
            case Requested:
                toolBarTitle = getString(R.string.toolbar_title_view_request);
                setConfirmApointment();
                break;
            case Confirmed:
                toolBarTitle = getString(R.string.toolbar_title_view_request);
                setConfirmApointment();
                layoutStatus.setVisibility(View.VISIBLE);
                setStatus(getString(R.string.waiting_for_payment));
                btnRequest.setVisibility(View.GONE);
                break;
            case PayDone:
                toolBarTitle = getString(R.string.toolbar_title_view_appointment);
                btnCancelRequest.setVisibility(View.GONE);
                setOnRouteToClient();
                break;
            case OnRouteToClient:
                toolBarTitle = getString(R.string.toolbar_title_view_appointment);
                btnCancelRequest.setVisibility(View.GONE);
                setStartTracking();
                break;
            case StartService:
                toolBarTitle = getString(R.string.toolbar_title_view_appointment);
                btnCancelRequest.setVisibility(View.GONE);
                setCompleteService();
                break;
            case Completed:
                toolBarTitle = getString(R.string.toolbar_title_view_sale);
                setCompletedService(false);
                btnRequest.setVisibility(View.GONE);
                btnCancelRequest.setVisibility(View.GONE);
                break;
            case Cancelled:
                toolBarTitle = getString(R.string.toolbar_title_view_sale);
                setCompletedService(true);
                btnRequest.setVisibility(View.GONE);
                btnCancelRequest.setVisibility(View.GONE);
                layoutReason.setVisibility(View.VISIBLE);
                break;
            default:
                break;

        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(toolBarTitle);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    public void setConfirmApointment() {
        tvLblAmount.setText(getString(R.string.amount));
        btnRequest.setBackgroundColor(ContextCompat.getColor(this, R.color.available_orange));
        layoutAmountPaid.setVisibility(View.VISIBLE);
        btnRequest.setText(getString(R.string.confirm_appointment));
        tvRequestFrom.setText(data.Data.requestFrom);
    }

    public void setOnRouteToClient() {
        layoutRequestFrom.setVisibility(View.GONE);

        layoutAmountPaid.setVisibility(View.VISIBLE);
        layoutRefundMethod.setVisibility(View.VISIBLE);
        layoutStatus.setVisibility(View.VISIBLE);

        btnRequest.setText(getString(R.string.on_route_to_client));

        tvAmountPaid.setText(getString(R.string.currency_symbol) + " " + data.Data.amountPaid);
        tvRefundMethod.setText(data.Data.refundMethod);

        setStatus(getString(R.string.not_started_black));
    }

    public void setStartTracking() {
        layoutRequestFrom.setVisibility(View.GONE);

        layoutAmountPaid.setVisibility(View.VISIBLE);
        layoutRefundMethod.setVisibility(View.VISIBLE);
        layoutStatus.setVisibility(View.VISIBLE);

        btnRequest.setText(getString(R.string.directions_to_buyer));

        tvAmountPaid.setText(getString(R.string.currency_symbol) + " " + data.Data.amountPaid);
        tvRefundMethod.setText(data.Data.refundMethod);

        setStatus(getString(R.string.on_the_way_black));
    }

    public void setCompleteService() {
        layoutRequestFrom.setVisibility(View.GONE);

        layoutAmountPaid.setVisibility(View.VISIBLE);
        layoutRefundMethod.setVisibility(View.VISIBLE);
        layoutStatus.setVisibility(View.VISIBLE);

        btnRequest.setText(getString(R.string.complete_service));

        tvAmountPaid.setText(getString(R.string.currency_symbol) + " " + data.Data.amountPaid);
        tvRefundMethod.setText(data.Data.refundMethod);

        setStatus(getString(R.string.inprogress_green));
    }

    public void setCompletedService(boolean isCancelled) {

        layoutRequestFrom.setVisibility(View.GONE);
        layoutStatus.setVisibility(View.VISIBLE);

        tvAmountPaid.setText(getString(R.string.currency_symbol) + " " + data.Data.amountPaid);
        tvRefundMethod.setText(data.Data.refundMethod);

        if (isCancelled) {
            tvRefundMethodTxt.setText(R.string.refund_method);
            if (data.Data.isRefund) {
                layoutAmountPaid.setVisibility(View.VISIBLE);
                layoutRefundMethod.setVisibility(View.VISIBLE);
            }
            setStatus(getString(R.string.cancelled_by_text, data.Data.cancelByText));

        } else {
            layoutAmountPaid.setVisibility(View.VISIBLE);
            layoutRefundMethod.setVisibility(View.VISIBLE);
            setStatus(getString(R.string.completed_black));
        }
    }

    public void setStatus(String status) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvStatus.setText(Html.fromHtml(status, Html.FROM_HTML_MODE_COMPACT));
        } else {
            tvStatus.setText(Html.fromHtml(status));
        }
    }

    @OnClick(R.id.btn_request)
    public void onViewClicked() {
        int status = 0;
        switch (serviceStatus) {
            case Requested:
                status = Confirmed.getService();
                doChangeRequestStatus(status);
                break;
            case PayDone:
                pubChannelName = requestId;
                status = OnRouteToClient.getService();
                doChangeRequestStatus(status);
                break;
            case OnRouteToClient:
                ArrayList<String> createdPubChannelNames = new ArrayList<>();
                createdPubChannelNames.add(requestId);
                startActivity(new Intent(ViewRequestActivity.this, SellerLocationUpdateActivity.class)
                        .putStringArrayListExtra(Constants.SBU_CreatedPubChannelNames, createdPubChannelNames));
                break;
            case StartService:
                status = Completed.getService();
                doChangeRequestStatus(status);
                break;
            default:
                break;
        }
    }

    @OnClick(R.id.btn_cancel_request)
    public void onCancelRequest() {
        startActivityForResult(new Intent(this, OrderRequestCancellationActivity.class)
                .putExtra(Constants.SBU_RequestId, data.Data.requestId), reqCode);
    }

    public void doChangeRequestStatus(final int status) {
        if (!Globals.internetCheck(ViewRequestActivity.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().ConfirmRequestParams(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                requestId,
                status,
                pubChannelName);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(ViewRequestActivity.this);

        HttpRequestHandler.getInstance().post(ViewRequestActivity.this, getString(R.string.changeRequestStatus), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing() && !isFinishing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel.Result) {
                    if (serviceStatus == PayDone || serviceStatus == OnRouteToClient) {
                        doStartTracking();
                    } else {
                        if (serviceStatus == Requested)
                            Toaster.shortToast(dataModel.Message);
                        if (status == Completed.getService()) {
                            broadcastComplete();
                        }
                        setResult(RESULT_OK);
                        if (isTaskRoot()) {
                            if (globals.getUserData().Data.userRole == UserRole.BUYER.getRole())
                                startActivity(new Intent(ViewRequestActivity.this, UserLandingActivity.class));
                            else
                                startActivity(new Intent(ViewRequestActivity.this, SellerLandingActivity.class));
                            finish();
                        } else {
                            finish();
                        }
                    }
                } else {
                    Toaster.shortToast(dataModel.Message);
                    if (dataModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(ViewRequestActivity.this);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void broadcastComplete() {
        if (CoreApp.getInstance().getChatHeadService() != null)
            CoreApp.getInstance().getChatHeadService().removeChatHead(data.Data.receiverId);

        Intent intentServiceComplete = new Intent(Constants.SBU_ServiceComplete);
        intentServiceComplete.putExtra(Constants.SBU_RequestId, requestId);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intentServiceComplete);
    }

    public void doStartTracking() {
        ArrayList<String> createdPubChannelNames = new ArrayList<>();
        createdPubChannelNames.add(requestId);
        startActivity(new Intent(ViewRequestActivity.this, SellerLocationUpdateActivity.class)
                .putStringArrayListExtra(Constants.SBU_CreatedPubChannelNames, createdPubChannelNames));
        finish();
    }

    private void initPubNub() {
        pubnub = CoreApp.getInstance().initPubnub();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            requestId = bundle.getString(Constants.SBU_RequestId) != null
                    ? bundle.getString(Constants.SBU_RequestId) : "";
            setUpData();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == reqCode && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            if (isTaskRoot()) {
                if (globals.getUserData().Data.userRole == UserRole.BUYER.getRole())
                    startActivity(new Intent(ViewRequestActivity.this, UserLandingActivity.class));
                else
                    startActivity(new Intent(ViewRequestActivity.this, SellerLandingActivity.class));
                finish();
            } else {
                finish();
            }
        }
    }


    @OnClick(R.id.layout_address)
    public void onAddressClick() {
        checkPermission();
    }

    private void checkPermission() {
        // Check if location permission is granted??
        permissionListener = this;
        new TedPermission(this)
                .setPermissionListener(permissionListener)
                .setRationaleMessage(R.string.rationale_message)
                .setDeniedMessage(R.string.denied_message)
                .setGotoSettingButtonText(R.string.ok)
                .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                .check();
    }

    @Override
    public void onPermissionGranted() {
        startActivity(new Intent(this, UserAddressDetailsActivity.class)
                .putExtra(Constants.SBU_UserLatitude, data.Data.userLatitude)
                .putExtra(Constants.SBU_UserLongitude, data.Data.userLongitude)
                .putExtra(Constants.SBU_Latitude, data.Data.companyLatitude)
                .putExtra(Constants.SBU_Longitude, data.Data.companyLongitude));
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

    }
}
