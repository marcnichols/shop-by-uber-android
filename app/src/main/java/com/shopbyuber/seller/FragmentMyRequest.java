package com.shopbyuber.seller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemSpanLookup;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.model.MyRequestModel;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;
import com.shopbyuber.utils.paginationProgressBarAdapter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

import static android.app.Activity.RESULT_OK;

public class FragmentMyRequest extends Fragment implements AdapterView.OnItemClickListener, Paginate.Callbacks, SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = FragmentMyRequest.class.getSimpleName();
    Globals globals;

    @BindView(R.id.list)
    RecyclerView myServicesRecyclerView;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tv_no_record_found)
    TextView tv_no_record_found;

    @BindView(R.id.add_service)
    TextView add_service;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private Paginate paginate;
    int pageNo = 1;
    private boolean loading = false;

    MyRequestModel myRequestModel;
    ArrayList<MyRequestModel.Requests> arrRequests;
    AdapterMyRequest adapterMyRequest;

    int reqCode = 1123;

    public static FragmentMyRequest mContext;

    public static FragmentMyRequest getInstance() {
        return mContext;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_services, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        mContext = this;
        arrRequests = new ArrayList<>();
        globals = ((Globals) getActivity().getApplicationContext());
        add_service.setVisibility(View.GONE);

        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorToolbarTitle);
        swipeRefreshLayout.setOnRefreshListener(this);

        doGetMyRequest(true);
    }

    public void doGetMyRequest(final boolean showProgress) {
        if (!Globals.internetCheck(getActivity())) {
            showNoRecordFound();
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().getMyRequestParams(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                pageNo);

        HttpRequestHandler.getInstance().post(getActivity(), getString(R.string.getMyRequest), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                if (!swipeRefreshLayout.isRefreshing() && showProgress)
                    progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (progressBar.getVisibility() == View.VISIBLE)
                    progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                myRequestModel = new Gson().fromJson(response.toString(), MyRequestModel.class);
                if (myRequestModel.Result) {
                    if (swipeRefreshLayout.isRefreshing()) {
                        arrRequests.clear();
                        adapterMyRequest.notifyDataSetChanged();
                    }
                    setupList(myRequestModel.Data.requests);
                } else {
                    showNoRecordFound();
                    if (myRequestModel.Message.contains(getString(R.string.invalid_session))) {
                        Toaster.shortToast(myRequestModel.Message);
                        globals.doLogout(getActivity());
                    }
                }
                stopRefreshing();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                stopRefreshing();
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                stopRefreshing();
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void stopRefreshing() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void setupList(List<MyRequestModel.Requests> requestArrayList) {
        if (requestArrayList != null && !requestArrayList.isEmpty()) {
            arrRequests.addAll(requestArrayList);
            setAdapter();
        } else
            showNoRecordFound();
    }

    private void setAdapter() {
        hideNoRecordFound();
        if (adapterMyRequest == null) {
            if (paginate != null) {
                paginate.unbind();
            }
            adapterMyRequest = new AdapterMyRequest(getActivity());
            adapterMyRequest.setOnItemClickListener(this);
        }
        loading = false;
        adapterMyRequest.doRefresh(arrRequests);

        if (myServicesRecyclerView.getAdapter() == null) {
            myServicesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            myServicesRecyclerView.setItemAnimator(new DefaultItemAnimator());
            myServicesRecyclerView.setAdapter(adapterMyRequest);
            if (arrRequests.size() < myRequestModel.Data.recordCount) {
                paginate = Paginate.with(myServicesRecyclerView, this)
                        .setLoadingTriggerThreshold(Constants.progress_threshold_2)
                        .addLoadingListItem(Constants.addLoadingRow)
                        .setLoadingListItemCreator(new paginationProgressBarAdapter())
                        .setLoadingListItemSpanSizeLookup(new LoadingListItemSpanLookup() {
                            @Override
                            public int getSpanSize() {
                                return Constants.GRID_SPAN;
                            }
                        })
                        .build();
            }
        }
    }

    private void showNoRecordFound() {
        loading = false;
        if (arrRequests.isEmpty()) {
            myServicesRecyclerView.setVisibility(View.GONE);
            if (tv_no_record_found.getVisibility() == View.GONE)
                tv_no_record_found.setText(getString(R.string.you_do_not_have_any_request));
            tv_no_record_found.setVisibility(View.VISIBLE);
        }
    }

    private void hideNoRecordFound() {
        myServicesRecyclerView.setVisibility(View.VISIBLE);
        if (tv_no_record_found.getVisibility() == View.VISIBLE)
            tv_no_record_found.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Logger.d(TAG, arrRequests.get(position));
        startActivityForResult(new Intent(getActivity(), ViewRequestActivity.class)
                .putExtra(Constants.SBU_RequestId, arrRequests.get(position).requestId)
                .putExtra(Constants.SBU_Status, arrRequests.get(position).status), reqCode);
    }

    @Override
    public void onLoadMore() {
        loading = true;
        pageNo++;
        doGetMyRequest(false);
    }

    @Override
    public boolean isLoading() {
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return arrRequests.size() == myRequestModel.Data.recordCount;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == reqCode && resultCode == RESULT_OK) {
            arrRequests.clear();
            adapterMyRequest.doRefresh(arrRequests);
            adapterMyRequest.notifyDataSetChanged();
            pageNo = 1;
            doGetMyRequest(true);
        }
    }

    @Override
    public void onRefresh() {
        pageNo = 1;
        doGetMyRequest(true);
    }
}
