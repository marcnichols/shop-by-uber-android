package com.shopbyuber.seller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.RadioButton;

import com.shopbyuber.R;
import com.shopbyuber.enums.Days;
import com.shopbyuber.enums.ServiceSpinnerType;
import com.shopbyuber.model.ServiceCatagoryModel;
import com.shopbyuber.model.ServiceOccupiedDataModel;
import com.shopbyuber.model.ServiceOccupiedDataModel.availableDays;
import com.shopbyuber.model.ServiceOccupiedDataModel.slotes;
import com.shopbyuber.model.ServiceOccupiedDataModel.timeslots;
import com.shopbyuber.utils.Constants;

import java.util.ArrayList;

public class CustomDialogAdapter extends BaseAdapter implements AdapterView.OnItemClickListener {

    private ServiceOccupiedDataModel dataSet;
    private ServiceCatagoryModel dataSetCategory;
    private Context mContext;
    private int type;

    public CustomDialogAdapter(Context context, ServiceCatagoryModel serviceCatagoryModel, int type) {
        this.dataSetCategory = serviceCatagoryModel;
        this.mContext = context;
        this.type = type;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    public CustomDialogAdapter(Context context, ServiceOccupiedDataModel data, int type) {
        this.dataSet = data;
        this.mContext = context;
        this.type = type;
    }

    @Override
    public int getCount() {
        if (type == ServiceSpinnerType.DURATION.getType())
            return dataSet.Data.timeslots.size();
        else if (type == ServiceSpinnerType.DAYS.getType()) {
            return dataSet.Data.availableDays.size();
        } else if (type == ServiceSpinnerType.CATEGORY.getType()) {
            return dataSetCategory.Data.categories.size();
        }else if (type==ServiceSpinnerType.SUB_CATEGORY.getType()){
            return dataSetCategory.Data.subCategories.size();
        }else
            return getTimeSlot().size();
    }

    @Override
    public Object getItem(int position) {
        return dataSet.Data.timeslots.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.custom_dialog_list_item, parent, false);
            viewHolder.radioButton = convertView.findViewById(R.id.radiobutton);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (type == ServiceSpinnerType.DURATION.getType()) {
            viewHolder.radioButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.rbtn_selector, 0);
            timeslots timeslots = dataSet.Data.timeslots.get(position);
            viewHolder.radioButton.setText(timeslots.duration > 1
                    ? timeslots.duration + Constants.SBU_Hours
                    : timeslots.duration + Constants.SBU_Hour);
            viewHolder.radioButton.setChecked(timeslots.isSelected);
        } else if (type == ServiceSpinnerType.DAYS.getType()) {
            viewHolder.radioButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.rbtn_multiple_selector, 0);
            availableDays availableDays = dataSet.Data.availableDays.get(position);
            viewHolder.radioButton.setText(Days.getDay(availableDays.day));
            viewHolder.radioButton.setChecked(availableDays.isAllocate);
        } else if (type == ServiceSpinnerType.CATEGORY.getType()) {
            viewHolder.radioButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.rbtn_selector, 0);
            viewHolder.radioButton.setText(dataSetCategory.Data.categories.get(position).catName);
            viewHolder.radioButton.setChecked(dataSetCategory.Data.categories.get(position).isAllocate);
        }  else if (type == ServiceSpinnerType.SUB_CATEGORY.getType()) {
            viewHolder.radioButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.rbtn_selector, 0);
            viewHolder.radioButton.setText(dataSetCategory.Data.subCategories.get(position).subcatName);
            viewHolder.radioButton.setChecked(dataSetCategory.Data.subCategories.get(position).isAllocate);
        }else {
            viewHolder.radioButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.rbtn_multiple_selector, 0);
            slotes slote = getTimeSlot().get(position);
            viewHolder.radioButton.setText(slote.time);
            viewHolder.radioButton.setChecked(slote.isAllocate);
        }
        return convertView;
    }

    private ArrayList<slotes> getTimeSlot() {
        for (timeslots timeslot : dataSet.Data.timeslots) {
            if (timeslot.isSelected)
                return timeslot.slotes;
        }
        return new ArrayList<>();
    }

    // View lookup cache
    private static class ViewHolder {
        RadioButton radioButton;
    }
}
