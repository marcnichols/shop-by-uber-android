package com.shopbyuber.seller;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.shopbyuber.R;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserAddressDetailsActivity extends BaseActivity implements OnMapReadyCallback {

    @BindView(R.id.map_container)
    RelativeLayout map_container;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    double userLatitude, userLongitude, companyLatitude, companyLongitude;
    SupportMapFragment mapFragment;
    GoogleMap mGoogleMap;

    LatLngBounds.Builder latLngBoundsBuilder;
    LatLngBounds bounds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_address_details);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.user_location);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            userLatitude = bundle.getDouble(Constants.SBU_UserLatitude);
            userLongitude = bundle.getDouble(Constants.SBU_UserLongitude);
            companyLatitude = bundle.getDouble(Constants.SBU_Latitude);
            companyLongitude = bundle.getDouble(Constants.SBU_Longitude);
        }

        setupMap();

    }

    private void setupMap() {
        // add map fragment to container
        FragmentManager fm = getSupportFragmentManager();
        mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map_container);
        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.map_container, mapFragment).commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mGoogleMap = googleMap;
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.setMyLocationEnabled(true);

        drawMarker(new LatLng(userLatitude, userLongitude));
        drawVendorMarker(new LatLng(companyLatitude, companyLongitude));
        mGoogleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                moveCameraToPosition();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleMap == null) {
            mapFragment.getMapAsync(this);
        }
    }

    private void drawMarker(LatLng point) {
        // Creating an instance of MarkerOptions
        MarkerOptions markerOptions = new MarkerOptions();

        // Setting latitude and longitude for the marker
        markerOptions.position(point);

        // Adding marker on the Google Map
        mGoogleMap.addMarker(markerOptions).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.map_green));

    }

    private void drawVendorMarker(LatLng point) {
        // Creating an instance of MarkerOptions
        MarkerOptions markerOptions = new MarkerOptions();

        // Setting latitude and longitude for the marker
        markerOptions.position(point);

        // Adding marker on the Google Map
        mGoogleMap.addMarker(markerOptions).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.car));

    }

    private void moveCameraToPosition() {

        if (latLngBoundsBuilder == null) {
            latLngBoundsBuilder = new LatLngBounds.Builder();
        }

        bounds = new LatLngBounds(new LatLng(userLatitude, userLongitude), new LatLng(userLatitude, userLongitude));
        latLngBoundsBuilder.include(new LatLng(userLatitude, userLongitude));
        bounds = new LatLngBounds(new LatLng(companyLatitude, companyLongitude), new LatLng(companyLatitude, companyLongitude));
        latLngBoundsBuilder.include(new LatLng(companyLatitude, companyLongitude));

        if (bounds != null) {
            bounds = latLngBoundsBuilder.build();
            int padding = 60; // offset from edges of the map in pixels
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            mGoogleMap.animateCamera(cu);
        }
    }
}
