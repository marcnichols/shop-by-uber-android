package com.shopbyuber.seller;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.shopbyuber.R;
import com.shopbyuber.model.MyServicesModel.services;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

class AdapterMyServices extends RecyclerView.Adapter<AdapterMyServices.ViewHolder> {

    private ArrayList<services> mValues;
    private final Context mContext;
    private AdapterView.OnItemClickListener onItemClickListener;

    AdapterMyServices(Context context) {
        mContext = context;
    }

    public void doRefresh(ArrayList<services> services) {
        mValues = services;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_services_item, parent, false);
        return new ViewHolder(view, this);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private AdapterMyServices adapterMyServices;
        @BindView(R.id.img_profile)
        ImageView img_profile;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.radiobutton)
        RadioButton radiobutton;
        @BindView(R.id.tv_avail)
        TextView tv_avail;

        public ViewHolder(View itemView, AdapterMyServices adapterMyServices) {
            super(itemView);
            this.adapterMyServices = adapterMyServices;
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        void setDataToView(services mItem, final int position, ViewHolder viewHolder) {
            viewHolder.title.setText(mItem.screenName);
            viewHolder.price.setText(mContext.getString(R.string.currency_symbol) + " " + mItem.flatFee);
            viewHolder.tv_avail.setText(mItem.isAvailable ? mContext.getString(R.string.available_now) : mContext.getString(R.string.available_by_appointment));
            viewHolder.radiobutton.setChecked(mItem.isAvailable);
            Glide.with(mContext)
                    .load(mItem.serviceImage)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .centerCrop()
                    .placeholder(R.drawable.image_place_holder)
                    .crossFade()
                    .dontAnimate()
                    .into(viewHolder.img_profile);
            viewHolder.radiobutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentMyServices.getInstance().setServiceAvailability(position);
                }
            });
        }

        @Override
        public void onClick(View v) {
            adapterMyServices.onItemHolderClick(ViewHolder.this);
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        services mItem = mValues.get(position);
        holder.setDataToView(mItem, position, holder);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void onItemHolderClick(ViewHolder holder) {
        if (onItemClickListener != null)
            onItemClickListener.onItemClick(null, holder.itemView, holder.getAdapterPosition(), holder.getItemId());
    }
}
