package com.shopbyuber.wantedservice;


import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.shopbyuber.R;
import com.shopbyuber.model.WantedServiceModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterWantedService extends RecyclerView.Adapter<AdapterWantedService.ViewHolder> {


    private ArrayList<WantedServiceModel.WantedList> wantedLists;
    private final Context mContext;
    private AdapterView.OnItemClickListener onItemClickListener;
    private boolean fromDrawer;

    AdapterWantedService(Context context, boolean fromDrawer) {
        mContext = context;
        this.fromDrawer = fromDrawer;
    }

    public void doRefresh(ArrayList<WantedServiceModel.WantedList> wantedLists) {
        this.wantedLists = wantedLists;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.wanted_service_item, parent, false);
        return new ViewHolder(view, this);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private AdapterWantedService adapterWantedService;
        @BindView(R.id.img_profile)
        ImageView imgProfile;

        @BindView(R.id.tv_service_title)
        TextView tvServiceTitle;

        @BindView(R.id.tv_cat_name)
        TextView tvCatName;

        @BindView(R.id.tv_description)
        TextView tvDescription;

        @BindView(R.id.tv_service_available)
        TextView tvServiceAvailable;

        @BindView(R.id.img_see_more)
        ImageView imgSeeMore;

        @BindView(R.id.card_item)
        CardView card_item;

        public ViewHolder(View itemView, AdapterWantedService adapterShopByUberPicks) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.adapterWantedService = adapterShopByUberPicks;
        }

        void setDataToView(WantedServiceModel.WantedList mItem, ViewHolder viewHolder) {

            if (mItem.isServiceCreated) {
                tvServiceAvailable.setVisibility(View.VISIBLE);
                tvServiceAvailable.setText(mItem.serviceAvailableNow);
            } else tvServiceAvailable.setVisibility(View.GONE);

            if (fromDrawer) viewHolder.imgProfile.setVisibility(View.GONE);
            else viewHolder.imgProfile.setVisibility(View.VISIBLE);

            viewHolder.tvServiceTitle.setText(mItem.serviceName);
            viewHolder.tvCatName.setText(mItem.catName);
            viewHolder.tvDescription.setText(mItem.serviceDescription);

            Glide.with(mContext)
                    .load(mItem.image)
                    .centerCrop()
                    .placeholder(R.drawable.image_place_holder)
                    .crossFade()
                    .dontAnimate()
                    .into(viewHolder.imgProfile);
        }
    }

    private void infoDialog(String description) {
        final Dialog dialog = new Dialog(mContext, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.service_info_dialog);
        dialog.setCancelable(false);

        TextView tv_detail = dialog.findViewById(R.id.tv_detail);
        tv_detail.setText(description);
        tv_detail.setMovementMethod(new ScrollingMovementMethod());

        dialog.show();

        Button btn_ok = dialog.findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final WantedServiceModel.WantedList mItem = wantedLists.get(position);
        holder.setDataToView(mItem, holder);

        holder.imgSeeMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infoDialog(mItem.serviceDescription);
            }
        });

        holder.card_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemHolderClick(v, holder);
            }
        });

    }

    @Override
    public int getItemCount() {
        return wantedLists.size();
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void onItemHolderClick(View view, ViewHolder holder) {
        if (onItemClickListener != null)
            onItemClickListener.onItemClick(null, view, holder.getAdapterPosition(), holder.getItemId());
    }
}

