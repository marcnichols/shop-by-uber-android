package com.shopbyuber.wantedservice;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemSpanLookup;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.UserRole;
import com.shopbyuber.model.WantedServiceModel;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;
import com.shopbyuber.utils.paginationProgressBarAdapter;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;


public class FragmentWantedService extends Fragment implements AdapterView.OnItemClickListener, Paginate.Callbacks, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.list)
    RecyclerView rvWantedService;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tv_no_record_found)
    TextView tvNoRecordFound;

    @BindView(R.id.tv_request_service)
    TextView tvRequestService;

    private Globals globals;
    private static final int RequestServiceWanted = 1150;
    int pageNo = 1;
    private boolean loading = false;

    private WantedServiceModel serviceModel;
    private ArrayList<WantedServiceModel.WantedList> wantedLists;
    private AdapterWantedService adapterWantedService;

    private Paginate paginate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wanted_service, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        globals = (Globals) getActivity().getApplicationContext();

        swipeRefreshLayout.setColorSchemeResources(R.color.colorToolbarTitle);
        swipeRefreshLayout.setOnRefreshListener(this);
        wantedLists = new ArrayList<>();
        pageNo = 1;
        if (globals.getUserData().Data.userRole == UserRole.BUYER.getRole())
            tvRequestService.setVisibility(View.VISIBLE);

        Globals.showInfoDialog(getActivity());

        dogetMyWantedServiceRequest(true);
    }

    @OnClick(R.id.tv_request_service)
    public void OnRequestServiceClick() {
        startActivityForResult(new Intent(getActivity(), RequestServiceActivity.class), RequestServiceWanted);
    }


    private void dogetMyWantedServiceRequest(final boolean showProgress) {
        if (!Globals.internetCheck(getActivity())) {
            showNoRecordFound(getString(R.string.you_do_not_have_any_requested_service));
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().getAllWantedListParams(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                pageNo, false);

        HttpRequestHandler.getInstance().post(getActivity(), getString(R.string.getAllwantedList), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                if (!swipeRefreshLayout.isRefreshing() && showProgress)
                    progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (progressBar.getVisibility() == View.VISIBLE)
                    progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                serviceModel = new Gson().fromJson(response.toString(), WantedServiceModel.class);
                if (serviceModel.Result) {
                    if (swipeRefreshLayout.isRefreshing()) {
                        wantedLists.clear();
                        if (adapterWantedService != null)
                            adapterWantedService.notifyDataSetChanged();
                    }
                    setupList(serviceModel);
                } else {
                    showNoRecordFound(serviceModel.Message);
                    Toaster.shortToast(serviceModel.Message);
                    if (serviceModel.Message.contains(getString(R.string.invalid_session))) {
                        globals.doLogout(getActivity());
                    }
                }
                stopRefreshing();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                stopRefreshing();
                showNoRecordFound(getString(R.string.you_do_not_have_any_requested_service));
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                stopRefreshing();
                showNoRecordFound(getString(R.string.you_do_not_have_any_requested_service));
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void setupList(WantedServiceModel model) {
        if (model.Data.wantedList != null && !model.Data.wantedList.isEmpty()) {
            wantedLists.addAll(model.Data.wantedList);
            setAdapter();
        } else
            showNoRecordFound(model.Message);
    }


    private void setAdapter() {
        hideNoRecordFound();
        if (adapterWantedService == null) {
            if (paginate != null) {
                paginate.unbind();
            }
            adapterWantedService = new AdapterWantedService(getActivity(), true);
            adapterWantedService.setOnItemClickListener(this);
        }
        loading = false;
        adapterWantedService.doRefresh(wantedLists);

        if (rvWantedService.getAdapter() == null) {
            rvWantedService.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvWantedService.setItemAnimator(new DefaultItemAnimator());
            rvWantedService.setAdapter(adapterWantedService);
            if (wantedLists.size() < serviceModel.Data.recordCount) {
                paginate = Paginate.with(rvWantedService, this)
                        .setLoadingTriggerThreshold(Constants.progress_threshold_2)
                        .addLoadingListItem(Constants.addLoadingRow)
                        .setLoadingListItemCreator(new paginationProgressBarAdapter())
                        .setLoadingListItemSpanSizeLookup(new LoadingListItemSpanLookup() {
                            @Override
                            public int getSpanSize() {
                                return Constants.GRID_SPAN;
                            }
                        })
                        .build();
            }
        }
    }

    private void hideNoRecordFound() {
        rvWantedService.setVisibility(View.VISIBLE);
        if (tvNoRecordFound.getVisibility() == View.VISIBLE)
            tvNoRecordFound.setVisibility(View.GONE);
    }


    private void showNoRecordFound(String message) {
        loading = false;
        if (wantedLists.isEmpty()) {
            rvWantedService.setVisibility(View.GONE);
            if (tvNoRecordFound.getVisibility() == View.GONE)
                tvNoRecordFound.setText(message);
            tvNoRecordFound.setVisibility(View.VISIBLE);
        }
    }

    private void stopRefreshing() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onLoadMore() {
        loading = true;
        pageNo++;
        dogetMyWantedServiceRequest(false);
    }

    @Override
    public boolean isLoading() {
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return wantedLists.size() == serviceModel.Data.recordCount;
    }

    @Override
    public void onRefresh() {
        pageNo = 1;
        dogetMyWantedServiceRequest(true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestServiceWanted && resultCode == Activity.RESULT_OK) {
            pageNo = 1;
            wantedLists.clear();
            dogetMyWantedServiceRequest(true);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        startActivityForResult(new Intent(getActivity(), WantedServiceDetailActivity.class)
                .putExtra(Constants.SBU_RequestId, wantedLists.get(position).requestId)
                .putExtra(Constants.SBU_GetAll, false), RequestServiceWanted);
    }
}
