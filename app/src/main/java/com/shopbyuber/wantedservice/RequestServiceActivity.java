package com.shopbyuber.wantedservice;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.ServiceSpinnerType;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.model.ServiceCatagoryModel;
import com.shopbyuber.model.UserModel;
import com.shopbyuber.model.WantedDetailsModel;
import com.shopbyuber.profile.AddNewService;
import com.shopbyuber.seller.CustomDialogAdapter;
import com.shopbyuber.user.PaymentConfirmationActivity;
import com.shopbyuber.user.UserLandingActivity;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;

public class RequestServiceActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.sp_service_catagory)
    TextView spServiceCatagory;

    @BindView(R.id.sp_service_sub_catagory)
    TextView spServiceSubCatagory;

    @BindView(R.id.cardview_category)
    CardView cardviewCategory;

    @BindView(R.id.edt_service_name)
    EditText edtServiceName;

    @BindView(R.id.cardview_screen_name)
    CardView cardviewScreenName;

    @BindView(R.id.edt_description)
    EditText edtDescription;

    @BindView(R.id.cardview_offer_details)
    CardView cardviewOfferDetails;

    @BindView(R.id.btn_submit)
    Button btnSubmit;

    @BindView(R.id.tv_note)
    TextView tv_note;

    private TextView dialogSpServiceCategory, dialogSpServiceSubCategory;
    private EditText dialogEdtName, dialogEdtDesc;

    private Globals globals;
    private UserModel userModel;

    private ServiceCatagoryModel serviceCatagoryModel;
    private ServiceCatagoryModel serviceSubCatagoryModel;
    private String categoryId = "", subCategoryId = "", selectedCategory = "";
    boolean isFromFirstCall;

    private boolean isForUpdate;
    private WantedDetailsModel wantedDetailsModel;

    public static RequestServiceActivity mContext;

    public static RequestServiceActivity getInstance() {
        return mContext;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_service);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        mContext = this;
        globals = (Globals) getApplicationContext();
        userModel = globals.getUserData();
        isForUpdate = false;

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(Constants.SBU_UpdateWantedService))
                isForUpdate = bundle.getBoolean(Constants.SBU_UpdateWantedService);

            wantedDetailsModel = (WantedDetailsModel) bundle.getSerializable(Constants.SBU_WantedServiceModel);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            tv_note.setText(Html.fromHtml(mContext.getString(R.string.note_please_fill_in_the_details_of_the_service_you_requesting_from_service_provider), Html.FROM_HTML_MODE_COMPACT));
        else
            tv_note.setText(Html.fromHtml(mContext.getString(R.string.note_please_fill_in_the_details_of_the_service_you_requesting_from_service_provider)));

        //if request for update call category api
        if (isForUpdate) doGetCategoryListRequest();
        else showNoteAlert();
    }

    @OnClick(R.id.btn_submit)
    public void OnSubmitbuttonClick() {
        if (isValid())
            showEditDialog();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void showEditDialog() {
        final Dialog dialog = new Dialog(this, R.style.CustomDialogTermsNConditionTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_request_service);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);


        Toolbar dialogToolbar = dialog.findViewById(R.id.toolbar);
        dialogSpServiceCategory = dialog.findViewById(R.id.sp_service_catagory);
        dialogSpServiceSubCategory = dialog.findViewById(R.id.sp_service_sub_catagory);
        dialogEdtName = dialog.findViewById(R.id.edt_service_name);
        dialogEdtDesc = dialog.findViewById(R.id.edt_description);
        TextView dialogTvNote = dialog.findViewById(R.id.tv_note);
        Button btn_ok = dialog.findViewById(R.id.btn_submit);

        dialogTvNote.setVisibility(View.GONE);
        btn_ok.setText(getString(R.string.ok));
        dialogToolbar.setVisibility(View.GONE);

        dialogSpServiceCategory.setText(spServiceCatagory.getText());
        dialogSpServiceSubCategory.setText(spServiceSubCatagory.getText());
        dialogEdtName.setText(edtServiceName.getText());
        dialogEdtDesc.setText(edtDescription.getText());

        dialog.show();

        dialogSpServiceCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCatagoryDialog();
            }
        });

        dialogSpServiceSubCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //call default method and selection method is same for both
                showSubCategoryDialog();
            }
        });

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isDialogValid()) {
                    doRequestForService();
                    dialog.dismiss();
                }
            }
        });
    }

    private boolean isDialogValid() {
        if (dialogSpServiceCategory.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_select_a_category_for_your_service);
            return false;
        }
        if (dialogSpServiceSubCategory.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_select_a_sub_category_for_your_service);
            return false;
        }
        if (dialogEdtName.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_please_enter_your_service_name);
            return false;
        }
        if (dialogEdtDesc.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_please_enter_description_for_your_service);
            return false;
        }
        return true;
    }

    private void showNoteAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        // Setting Dialog Message
        alertDialog.setMessage(mContext.getString(R.string.please_fill_in_the_details_of_the_service_you_requesting_from_service_provider));
        alertDialog.setCancelable(false);
        // On pressing Settings button
        alertDialog.setPositiveButton(mContext.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                doGetCategoryListRequest();
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    private void displaySuccessAlert(String displayMessage) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        // Setting Dialog Message
        alertDialog.setMessage(displayMessage);
        alertDialog.setCancelable(false);
        // On pressing Settings button
        alertDialog.setPositiveButton(mContext.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();

                Intent intent = new Intent(RequestServiceActivity.this, UserLandingActivity.class);
                intent.putExtra(Constants.SBU_For, Constants.SBU_LoadWanted);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                        Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                finish();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void doGetCategoryListRequest() {
        if (!Globals.internetCheck(RequestServiceActivity.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getServiceCatagoryParams(
                userModel.Data.userId, userModel.Data.sessionId);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(RequestServiceActivity.this);

        HttpRequestHandler.getInstance().post(RequestServiceActivity.this, getString(R.string.getCategoryList), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing() && !isFinishing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                serviceCatagoryModel = new Gson().fromJson(response.toString(), ServiceCatagoryModel.class);
                if (serviceCatagoryModel.Result) {
                    if (isForUpdate) {
                        showSelectedWantedCategory();
                        setWantedServiceData();
                        categoryId = wantedDetailsModel.Data.categoryId;
                        doGetSubCategory(true);
                    }
                } else {
                    Toaster.shortToast(serviceCatagoryModel.Message);
                    if (serviceCatagoryModel.Message.contains(getString(R.string.invalid_session))) {
                        globals.doLogout(RequestServiceActivity.this);
                    } else
                        finish();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }


    private void showSelectedWantedCategory() {
        // show selected category
        for (ServiceCatagoryModel.Categories category : serviceCatagoryModel.Data.categories) {
            if (category._id.equalsIgnoreCase(wantedDetailsModel.Data.categoryId)) {
                spServiceCatagory.setText(category.catName);
                categoryId = category._id;
                category.isAllocate = true;
            }
        }
    }

    //use for prepopulate data
    private void setWantedServiceData() {
        edtServiceName.setText(wantedDetailsModel.Data.serviceName.isEmpty() ? "" : wantedDetailsModel.Data.serviceName);
        edtDescription.setText(wantedDetailsModel.Data.serviceDescription.isEmpty() ? "" : wantedDetailsModel.Data.serviceDescription);
    }

    private void setSubCategoryWantedService() {
        // show selected sub category
        for (ServiceCatagoryModel.SubCategories subCategories : serviceSubCatagoryModel.Data.subCategories) {
            if (subCategories._id.equalsIgnoreCase(wantedDetailsModel.Data.subCategoryId)) {
                spServiceSubCatagory.setText(subCategories.subcatName);
                subCategoryId = subCategories._id;
                subCategories.isAllocate = true;
            }
        }
    }

    // ----------------------------------------- showCatagoryDialog -----------------------------------------

    @OnClick(R.id.sp_service_catagory)
    public void showCatagoryDialog() {
        final Dialog dialog = new Dialog(RequestServiceActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog_layout);
        dialog.setCancelable(false);

        TextView tv_dg_main_title = dialog.findViewById(R.id.tv_dg_main_title);
        tv_dg_main_title.setText(getString(R.string.select_a_category));
        final ListView listview = dialog.findViewById(R.id.listview);

        final CustomDialogAdapter customDialogAdapter = new CustomDialogAdapter(this, serviceCatagoryModel, ServiceSpinnerType.CATEGORY.getType());
        listview.setAdapter(customDialogAdapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                for (int i = 0; i < serviceCatagoryModel.Data.categories.size(); i++) {
                    ServiceCatagoryModel.Categories categories = serviceCatagoryModel.Data.categories.get(i);
                    categories.isAllocate = i == position;

                }

                customDialogAdapter.notifyDataSetChanged();
            }
        });

        dialog.show();

        Button btn_submit = dialog.findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedCategory = categoryId;
                setCatagory();
                if (!categoryId.equalsIgnoreCase(selectedCategory)) {
                    spServiceSubCatagory.setText("");

                    //if edit dialog open clear the dialog sub category display text
                    if (dialogSpServiceSubCategory != null)
                        dialogSpServiceSubCategory.setText("");

                    doGetSubCategory(false);
                }
                dialog.dismiss();
            }
        });
    }

    private void setCatagory() {
        for (ServiceCatagoryModel.Categories categories : serviceCatagoryModel.Data.categories) {
            if (categories.isAllocate) {
                categoryId = categories._id;
                spServiceCatagory.setText(categories.catName);

                //if dialog open set selected category
                if (dialogSpServiceCategory != null)
                    dialogSpServiceCategory.setText(categories.catName);
            }
        }
    }

    public void doGetSubCategory(final boolean setFirstForm) {
        if (!Globals.internetCheck(RequestServiceActivity.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().doSubCategoryParam(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                categoryId, false);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(RequestServiceActivity.this);

        HttpRequestHandler.getInstance().post(RequestServiceActivity.this, getString(R.string.getSubSubcategories), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing() && !isFinishing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                serviceSubCatagoryModel = new Gson().fromJson(response.toString(), ServiceCatagoryModel.class);
                isFromFirstCall = setFirstForm;
                if (serviceSubCatagoryModel.Result) {
                    if (isForUpdate)
                        setSubCategoryWantedService();

                    if (serviceSubCatagoryModel.Data.subCategories != null && !serviceSubCatagoryModel.Data.subCategories.isEmpty())
                        serviceSubCatagoryModel.Data.subCategories.get(0).isAllocate = true;
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    // ----------------------------------------- showSubCatagoryDialog -----------------------------------------

    @OnClick(R.id.sp_service_sub_catagory)
    public void showSubCategoryDialog() {

        if (categoryId.isEmpty()) {
            Toaster.shortToast(R.string.err_select_a_category_for_your_service);
            return;
        }

        if (serviceSubCatagoryModel.Result && serviceSubCatagoryModel.Data != null) {
            if (serviceSubCatagoryModel.Data.subCategories != null && !serviceSubCatagoryModel.Data.subCategories.isEmpty()) {
                if (!isFromFirstCall) {
                   /* if (serviceFrom == AddServiceFrom.FROM_EDIT_SERVICE) {
                        for (ServiceCatagoryModel.SubCategories subCategories : serviceSubCatagoryModel.Data.subCategories) {
                            if (subCategories._id.equalsIgnoreCase(subCategoryId)) {
                                subCategories.isAllocate = true;
                            }
                        }
                    }*/
                    doDisplaySubCategory(true);
                }
            }
        } else {
            doDisplaySubCategory(false);
            if (serviceSubCatagoryModel.Message.contains(getString(R.string.invalid_session))) {
                globals.doLogout(RequestServiceActivity.this);
                Toaster.shortToast(serviceSubCatagoryModel.Message);
            }
        }

    }

    private void setSubCatagory() {
        for (ServiceCatagoryModel.SubCategories subCategories : serviceSubCatagoryModel.Data.subCategories) {
            if (subCategories.isAllocate) {
                subCategoryId = subCategories._id;
                spServiceSubCatagory.setText(subCategories.subcatName);

                if (dialogSpServiceSubCategory != null)
                    dialogSpServiceSubCategory.setText(subCategories.subcatName);
            }
        }
    }

    private void doDisplaySubCategory(final boolean isSubCategory) {

        final Dialog dialog = new Dialog(RequestServiceActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog_layout);
        dialog.setCancelable(false);

        TextView tv_dg_main_title = dialog.findViewById(R.id.tv_dg_main_title);
        TextView tv_no_record_found = dialog.findViewById(R.id.tv_no_record_found);
        tv_dg_main_title.setText(getString(R.string.select_a_sub_category));
        final ListView listview = dialog.findViewById(R.id.listview);

        if (isSubCategory) {
            final CustomDialogAdapter customDialogAdapter = new CustomDialogAdapter(this, serviceSubCatagoryModel, ServiceSpinnerType.SUB_CATEGORY.getType());
            listview.setAdapter(customDialogAdapter);
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    for (int i = 0; i < serviceSubCatagoryModel.Data.subCategories.size(); i++) {
                        ServiceCatagoryModel.SubCategories subCategories = serviceSubCatagoryModel.Data.subCategories.get(i);
                        subCategories.isAllocate = i == position;
                    }
                    customDialogAdapter.notifyDataSetChanged();
                }
            });
        } else {
            tv_no_record_found.setVisibility(View.VISIBLE);
            tv_no_record_found.setText(serviceSubCatagoryModel.Message);
        }


        dialog.show();

        Button btn_submit = dialog.findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isSubCategory)
                    setSubCatagory();

                dialog.dismiss();
            }
        });
    }

    private boolean isValid() {
        if (spServiceCatagory.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_select_a_category_for_your_service);
            return false;
        }
        if (spServiceSubCatagory.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_select_a_sub_category_for_your_service);
            return false;
        }
        if (edtServiceName.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_please_enter_your_service_name);
            return false;
        }
        if (edtDescription.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_please_enter_description_for_your_service);
            return false;
        }
        return true;
    }

    private void doRequestForService() {
        if (!Globals.internetCheck(RequestServiceActivity.this))
            return;

        String api = isForUpdate ? getString(R.string.updateWantedService) : getString(R.string.addwantedservices);

        JSONObject params = HttpRequestHandler.getInstance().getRequestServiceParams(
                userModel.Data.userId,
                userModel.Data.sessionId,
                dialogEdtName.getText().toString().trim(),
                categoryId,
                subCategoryId,
                dialogEdtDesc.getText().toString().trim(),
                isForUpdate,
                wantedDetailsModel == null ? "" : wantedDetailsModel.Data.requestId);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(RequestServiceActivity.this);

        HttpRequestHandler.getInstance().post(RequestServiceActivity.this, api, params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing() && !isFinishing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel model = new Gson().fromJson(response.toString(), DataModel.class);
                if (model.Result) {
                    displaySuccessAlert(model.Message);
                } else {
                    Toaster.shortToast(model.Message);
                    if (model.Message.contains(getString(R.string.invalid_session))) {
                        globals.doLogout(RequestServiceActivity.this);
                    } else
                        finish();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

}
