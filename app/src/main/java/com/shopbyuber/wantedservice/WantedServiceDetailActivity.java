package com.shopbyuber.wantedservice;

import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.AddServiceFrom;
import com.shopbyuber.enums.UserRole;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.model.ShopByUberPicksModel;
import com.shopbyuber.model.UserModel;
import com.shopbyuber.model.WantedDetailsModel;
import com.shopbyuber.profile.AddNewService;
import com.shopbyuber.profile.CreateProfileActivity;
import com.shopbyuber.seller.SellerLandingActivity;
import com.shopbyuber.user.AdapterShopByUberPicks;
import com.shopbyuber.user.GetPicksCompanyServiceActivity;
import com.shopbyuber.user.UserLandingActivity;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

public class WantedServiceDetailActivity extends BaseActivity implements AdapterView.OnItemClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.img_service)
    CircleImageView imgService;

    @BindView(R.id.tv_request_title)
    TextView tvRequestTitle;

    @BindView(R.id.tv_service_description)
    TextView tvServiceDescription;

    @BindView(R.id.tv_category)
    TextView tvCategory;

    @BindView(R.id.tv_buyer_name)
    TextView tvBuyerName;

    @BindView(R.id.tv_city)
    TextView tvCity;

    @BindView(R.id.tv_state)
    TextView tvState;

    @BindView(R.id.btn_create_service)
    Button btnCreateService;

    @BindView(R.id.btn_cancel_request)
    Button btnCancelRequest;

    @BindView(R.id.btn_update_service)
    Button btnUpdateService;

    @BindView(R.id.ll_user_profile)
    LinearLayout llUserProfile;

    @BindView(R.id.layout_city)
    LinearLayout layoutCity;

    @BindView(R.id.layout_state)
    LinearLayout layoutState;

    @BindView(R.id.list)
    RecyclerView rvWantedService;

    private String requestId = "";
    private boolean fromGetAll;
    private Globals globals;
    private UserModel userModel;
    private WantedDetailsModel wantedDetailsModel;

    private static final int UpdateServiceWanted = 1152;

    private ShopByUberPicksModel modelShopByUberPicks;
    private ArrayList<ShopByUberPicksModel.CompanyListing> companyListings;
    private AdapterShopByUberPicks adapterShopByUberPicks;

    public static WantedServiceDetailActivity mContext;

    public static WantedServiceDetailActivity getInstance() {
        return mContext;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wanted_service_detail);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        mContext = this;
        globals = (Globals) getApplicationContext();
        userModel = globals.getUserData();
        companyListings = new ArrayList<>();

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            requestId = bundle.getString(Constants.SBU_RequestId) != null ? bundle.getString(Constants.SBU_RequestId) : "";
            fromGetAll = bundle.getBoolean(Constants.SBU_GetAll);
            if (getIntent().getExtras().containsKey(Constants.SBU_NotificationId) && Globals.isNetworkAvailable(this)) {
                globals.doMakeNotificationUnread(getIntent().getExtras().getString(Constants.SBU_NotificationId));
            }
        }
        doGetDetailRequest();
    }

    @OnClick(R.id.btn_create_service)
    public void OnCreateServiceClick() {
        if (userModel.Data.userRole == UserRole.SELLER.getRole())
            displayAddNewService();
        else doSwitchUser();
    }

    @OnClick(R.id.btn_cancel_request)
    public void OnCancelRequestClick() {
        displayConfirmationDialog();
    }

    @OnClick(R.id.btn_update_service)
    public void OnUpdateRequestClick() {
        startActivityForResult(new Intent(this, RequestServiceActivity.class)
                        .putExtra(Constants.SBU_UpdateWantedService, true)
                        .putExtra(Constants.SBU_WantedServiceModel, wantedDetailsModel)
                , UpdateServiceWanted);
    }

    @Override
    public void onBackPressed() {
        if (isTaskRoot()) {
            if (globals.getUserData().Data.userRole == UserRole.BUYER.getRole())
                startActivity(new Intent(this, UserLandingActivity.class));
            else
                startActivity(new Intent(this, SellerLandingActivity.class));
            finish();
        } else
            super.onBackPressed();
    }

    private void displayConfirmationDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        // Setting Dialog Title
        // Setting Dialog Message
        alertDialog.setTitle(mContext.getString(R.string.sure));
        alertDialog.setMessage(mContext.getString(R.string.do_you_want_to_cancel_this_request));
        alertDialog.setCancelable(false);
        // On pressing Settings button
        alertDialog.setPositiveButton(mContext.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                doCancelRequest();
                dialog.cancel();
            }
        });

        alertDialog.setNegativeButton(mContext.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    private void doCancelRequest() {
        if (!Globals.internetCheck(WantedServiceDetailActivity.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getWantedDetailsParams(
                userModel.Data.userId, userModel.Data.sessionId, requestId);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(WantedServiceDetailActivity.this);

        HttpRequestHandler.getInstance().post(WantedServiceDetailActivity.this, getString(R.string.cancelWantedService), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing() && !isFinishing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel != null && dataModel.Result) {
                    setResult(RESULT_OK);
                    finish();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void doGetDetailRequest() {
        if (!Globals.internetCheck(WantedServiceDetailActivity.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getWantedDetailsParams(
                userModel.Data.userId, userModel.Data.sessionId, requestId);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(WantedServiceDetailActivity.this);

        HttpRequestHandler.getInstance().post(WantedServiceDetailActivity.this, getString(R.string.showWantedDetails), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing() && !isFinishing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                wantedDetailsModel = new Gson().fromJson(response.toString(), WantedDetailsModel.class);
                if (wantedDetailsModel.Result) {
                    if (wantedDetailsModel.Data.isServiceCreated && userModel.Data.userRole == UserRole.BUYER.getRole())
                        dogetWantedListVendorRequest();

                    setData();
                } else {
                    Toaster.shortToast(wantedDetailsModel.Message);
                    if (wantedDetailsModel.Message.contains(getString(R.string.invalid_session))) {
                        globals.doLogout(WantedServiceDetailActivity.this);
                    } else
                        finish();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void setData() {
        //set button visibility based on role or redirected screen
        if (!fromGetAll || wantedDetailsModel.Data.userId.equals(userModel.Data.userId)) {
            btnCreateService.setVisibility(View.GONE);
            btnUpdateService.setVisibility(View.VISIBLE);
            btnCancelRequest.setVisibility(View.VISIBLE);
        } else if (fromGetAll) {
            btnCreateService.setVisibility(View.VISIBLE);
        }

        if (fromGetAll)
            llUserProfile.setVisibility(View.VISIBLE);

        tvBuyerName.setText(wantedDetailsModel.Data.name.isEmpty() ? "" : wantedDetailsModel.Data.name);
        tvRequestTitle.setText(wantedDetailsModel.Data.serviceName.isEmpty() ? "" : wantedDetailsModel.Data.serviceName);
        tvServiceDescription.setText(wantedDetailsModel.Data.serviceDescription.isEmpty() ? "" : wantedDetailsModel.Data.serviceDescription);
        tvCategory.setText(wantedDetailsModel.Data.catName.isEmpty() ? "" : wantedDetailsModel.Data.catName);
        tvCity.setText(wantedDetailsModel.Data.city.isEmpty() ? "" : wantedDetailsModel.Data.city);
        tvState.setText(wantedDetailsModel.Data.state.isEmpty() ? "" : wantedDetailsModel.Data.state);

        Glide.with(this)
                .load(wantedDetailsModel.Data.image)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .centerCrop()
                .placeholder(R.drawable.profile_placeholder)
                .crossFade()
                .dontAnimate()
                .into(imgService);
    }

    public void dogetWantedListVendorRequest() {
        if (!Globals.internetCheck(WantedServiceDetailActivity.this)) {
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().getWantedReponseListParams(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                wantedDetailsModel.Data.requestId);

        HttpRequestHandler.getInstance().post(WantedServiceDetailActivity.this, getString(R.string.getwantedreponselist), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                modelShopByUberPicks = new Gson().fromJson(response.toString(), ShopByUberPicksModel.class);
                if (modelShopByUberPicks.Result) {
                    setupList(modelShopByUberPicks.Data.companyListing);
                } else {
                    if (modelShopByUberPicks.Message.contains(getString(R.string.invalid_session))) {
                        Toaster.shortToast(modelShopByUberPicks.Message);
                        globals.doLogout(WantedServiceDetailActivity.this);
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void setupList(List<ShopByUberPicksModel.CompanyListing> companyListing) {
        if (companyListing != null && !companyListing.isEmpty()) {
            companyListings.addAll(companyListing);
            setAdapter();
        }
    }

    private void setAdapter() {
        if (adapterShopByUberPicks == null) {
            adapterShopByUberPicks = new AdapterShopByUberPicks(WantedServiceDetailActivity.this, false);
            adapterShopByUberPicks.setOnItemClickListener(this);
        }
        adapterShopByUberPicks.doRefresh(companyListings);

        if (rvWantedService.getAdapter() == null) {
            rvWantedService.setLayoutManager(new LinearLayoutManager(WantedServiceDetailActivity.this));
            rvWantedService.setItemAnimator(new DefaultItemAnimator());
            rvWantedService.setAdapter(adapterShopByUberPicks);
        }
    }

    private void doSwitchUser() {
        if (!Globals.internetCheck(this)) {
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().getSwitchUserParam(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId, UserRole.SELLER.getRole());

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(WantedServiceDetailActivity.this);
        HttpRequestHandler.getInstance().post(this, getString(R.string.switchUserLogin), params, new JsonHttpResponseHandler() {

            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                Logger.json(response.toString());
                UserModel userModel = new Gson().fromJson(response.toString(), UserModel.class);
                if (userModel.Result) {
                    if (userModel != null && userModel.Data != null) {
                        try {
                            NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            nMgr.cancelAll();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        globals.setUserData(userModel);
                        if (!userModel.Data.isCompanyProfileCreated) {
                            displayCreateServiceScreen();
                        } else
                            displayAddNewService();
                    }
                } else {
                    Toaster.shortToast(userModel.Message);
                    if (userModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(WantedServiceDetailActivity.this);
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void displayCreateServiceScreen() {
        Intent intent = new Intent(WantedServiceDetailActivity.this, CreateProfileActivity.class);
        intent.putExtra(Constants.SBU_OwnerName, userModel.Data.name);
        intent.putExtra(Constants.SBU_CompanyName, "");
        intent.putExtra(Constants.SBU_MobilePhone, userModel.Data.phoneNumber);
        intent.putExtra(Constants.SBU_CountryCode, userModel.Data.countryCode);
        startActivity(intent);
        finishAffinity();
    }

    private void displayAddNewService() {
        startActivity(new Intent(WantedServiceDetailActivity.this, AddNewService.class)
                .putExtra(Constants.SBU_RequestId, wantedDetailsModel.Data.requestId)
                .putExtra(Constants.SBU_Add_service_from, AddServiceFrom.FROM_MY_SERVICE)
                .putExtra(Constants.SBU_GetAll, true));

//        if (userModel.Data.userRole == UserRole.BUYER.getRole())
        finishAffinity();
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        startActivity(new Intent(WantedServiceDetailActivity.this, GetPicksCompanyServiceActivity.class)
                .putExtra(Constants.SBU_CompanyId, companyListings.get(position)._id)
                .putExtra(Constants.SBU_CompanyName, companyListings.get(position).comapnyScreenName));
    }
}
