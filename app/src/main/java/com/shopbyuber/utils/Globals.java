package com.shopbyuber.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.mapbox.mapboxsdk.Mapbox;
import com.orhanobut.logger.Logger;
import com.quickblox.auth.session.QBSettings;
import com.quickblox.chat.model.QBChatDialog;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.login.LoginActivity;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.model.UserModel;
import com.shopbyuber.quickblox.ChatHelper;
import com.shopbyuber.quickblox.SampleConfigs;
import com.shopbyuber.quickblox.Utils.QbDialogHolder;
import com.shopbyuber.quickblox.configs.ConfigUtils;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterSession;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import cz.msebera.android.httpclient.Header;

public class Globals extends CoreApp {
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    static Context context;
    private static SampleConfigs sampleConfigs;

    public static Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        initSampleConfigs();
        initQBConfig();
    }

    private void initSampleConfigs() {
        try {
            sampleConfigs = ConfigUtils.getSampleConfigs(Constants.SBU_QB_CONFIG_FILE_NAME);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initQBConfig() {
        QBSettings.getInstance().init(this, Constants.SBU_AppId, Constants.SBU_AuthKey, Constants.SBU_AuthSecrets);
        QBSettings.getInstance().setAccountKey(Constants.SBU_QBAccountKey);
    }

    public static SampleConfigs getSampleConfigs() {
        return sampleConfigs;
    }

    public SharedPreferences.Editor getEditor() {
        return editor = (editor == null) ? getSharedPref().edit() : editor;
    }

    public SharedPreferences getSharedPref() {
        return sp = (sp == null) ? getSharedPreferences(Constants.SBU_Secrets, Context.MODE_PRIVATE) : sp;
    }

    public void setSearchHistory(Set<String> searchHistory) {
        getEditor().putStringSet(Constants.SBU_History, searchHistory);
        getEditor().commit();
    }

    public Set<String> getSearchHistory() {
        return getSharedPref().getStringSet(Constants.SBU_History, new HashSet<String>());
    }

    public void setEnableHistory(boolean isEnable) {
        getEditor().putBoolean(Constants.SBU_Is_History_Enable, isEnable);
        getEditor().commit();
    }

    public boolean getEnableHistory() {
        return getSharedPref().getBoolean(Constants.SBU_Is_History_Enable, true);
    }

    public void setFCM_DeviceToken(String token) {
        getEditor().putString(Constants.SBU_DeviceToken, token);
        getEditor().commit();
    }

    public String getFCM_DeviceToken() {
        return getSharedPref().getString(Constants.SBU_DeviceToken, "");
    }

    public void clearAllSharedPreferences() {
        SharedPreferences sharedPref = getSharedPref();
        sharedPref.edit().clear().apply();
    }

    /**
     * check if network is available or not
     *
     * @param context
     * @return true or false
     */
    public static boolean isNetworkAvailable(Context context) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Network[] networks = connectivityManager.getAllNetworks();
                NetworkInfo networkInfo;
                for (Network mNetwork : networks) {
                    networkInfo = connectivityManager.getNetworkInfo(mNetwork);
                    if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                        return true;
                    }
                }
            } else {
                if (connectivityManager != null) {
                    NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
                    if (info != null) {
                        for (NetworkInfo anInfo : info) {
                            if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                                return true;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            return true;
        }
        return false;
    }

    public static boolean internetCheck(Context context) {
        if (isNetworkAvailable(context))
            return true;

        Toast.makeText(context, context.getString(R.string.no_internet_msg), Toast.LENGTH_SHORT).show();
        return false;
    }

    public UserModel getUserData() {
        return toUserDetails(getSharedPref().getString(Constants.SBU_UserModel, null));
    }

    public void setUserData(UserModel userMap) {
        getEditor().putString(Constants.SBU_UserModel, toJsonString(userMap));
        getEditor().commit();
    }

    public void setCommercialStatus(boolean isViewed) {
        getEditor().putBoolean(Constants.SBU_CommercialStatus, isViewed);
        getEditor().commit();
    }

    public boolean ifCommercialViewed() {
        /*return getSharedPref().getBoolean(Constants.SBU_CommercialStatus, false);*/
        return true;
    }

    /**
     * Convert custom model to string
     *
     * @param params : custom model class
     * @return string
     */
    public static String toJsonString(UserModel params) {
        if (params == null) {
            return null;
        }
        Type mapType = new TypeToken<UserModel>() {
        }.getType();
        Gson gson = new Gson();
        String postData = gson.toJson(params, mapType);
        return postData;
    }

    /**
     * Convert string to custom model
     *
     * @param params string
     * @return custom model class
     */
    public static UserModel toUserDetails(String params) {
        if (params == null)
            return null;
        Type mapType = new TypeToken<UserModel>() {
        }.getType();
        Gson gson = new Gson();
        UserModel postData = gson.fromJson(params, mapType);
        return postData;
    }

    public static boolean validateEmailAddress(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static void hideKeyboard(Activity activity) {
        if (activity != null && activity.getWindow() != null && activity.getWindow().getDecorView() != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
        }

        try {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            Logger.e("Globals", "Exception : Try to close keyboard that is already not visible");
        }
    }

    /**
     * retrive postal code from latitude and longitude
     *
     * @param latLng
     * @return postal code
     */
    public String getPostal(LatLng latLng) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            /*String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();*/
            String postalCode = addresses.get(0).getPostalCode();
            return postalCode;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getMobile(String myString) {
        if (myString.length() > 10)
            return myString.substring(myString.length() - 10);
        else
            return myString;
    }

    public String parseDateToMMddyyyy(String datetime) {
        String inputPattern = "dd/MM/yyyy hh:mm"; // 13/06/2017 07:05
        String outputPattern = "MM/dd/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        String str = "";

        try {
            str = outputFormat.format(inputFormat.parse(datetime));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }

    public void disconnectFromFacebook() {

        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                LoginManager.getInstance().logOut();
            }
        }).executeAsync();
    }

    public void logoutTwitter() {
        TwitterSession twitterSession = TwitterCore.getInstance().getSessionManager().getActiveSession();
        if (twitterSession != null) {
            ClearCookies(getApplicationContext());
            //  Twitter.getSessionManager().clearActiveSession();
            //  Twitter.logOut();

        }
    }

    public static void ClearCookies(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }

    public void doMakeNotificationUnread(String notificationId) {
        if (!notificationId.isEmpty()) {
            JSONObject params = HttpRequestHandler.getInstance().makeNotificationUnread(
                    getUserData().Data.userId,
                    getUserData().Data.sessionId,
                    notificationId);


            HttpRequestHandler.getInstance().post(context, getString(R.string.makeNotificationUnread), params, new JsonHttpResponseHandler() {

                @Override
                public void onStart() {
                    super.onStart();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Logger.e(response.toString());
                    DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                    if (dataModel != null && dataModel.Data != null) {
                        NotificationCountHolder.getInstance().setUnreads(dataModel.Data.userUnreadNotificationCount);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                }
            });
        }
    }

    public void doLogout(Activity activity) {
        ChatHelper.getInstance().logoutFromChat(activity);
        if (CoreApp.chatHeadService != null)
            CoreApp.chatHeadService.removeAllChatHeads();
        QbDialogHolder.getInstance().clearDialogs();
        CoreApp.getInstance().removeBubble();
        setUserData(null);
        Intent intent = new Intent(activity, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        activity.finishAffinity();
    }

    public String getContries() {
        List<String> countries = Arrays.asList(getResources().getStringArray(R.array.countries));
        return TextUtils.join(",", countries);
    }

    public void getReleaseKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.shopbyuber",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash", "KeyHash:" + Base64.encodeToString(md.digest(),
                        Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Logger.e(e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            Logger.e(e.getMessage());
        }
    }

    public static String getMapboxAccessToken(@NonNull Context context) {
        try {
            // Read out AndroidManifest
            String token = Mapbox.getAccessToken();
            if (token == null || token.isEmpty()) {
                throw new IllegalArgumentException();
            }
            return token;
        } catch (Exception exception) {
            // Use fallback on string resource, used for development
            int tokenResId = context.getResources()
                    .getIdentifier("mapbox_access_token", "string", context.getPackageName());
            return tokenResId != 0 ? context.getString(tokenResId) : null;
        }
    }

    public static String getEstimatedTime(double totalSecs) {
        int hours = (int) (totalSecs / 3600);
        int minutes = (int) ((totalSecs % 3600) / 60);
        int seconds = (int) (totalSecs % 60);
        String duration = "";
        if (hours > 0)
            if (minutes > 0)
                duration = hours + (hours > 1 ? " hrs" : " hr") + " " + minutes + (minutes > 1 ? " mins." : " min.");
            else
                duration = hours + (hours > 1 ? " hrs." : " hr.");
        else
            duration = minutes + (minutes > 1 ? " mins." : " min.");

        return duration;
    }

    public boolean ifOverlay() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        } else {
            return Settings.canDrawOverlays(context);
        }
    }

    public static String getInitials(String fullName) {
        if (fullName.contains(" ")) {
            String[] names = fullName.split(" ");
            int count = 0;
            StringBuilder b = new StringBuilder();
            for (String name : names) {
                String t = name.trim();
                if (t.isEmpty()) continue;
                b.append(("" + t.charAt(0)).toUpperCase());
                if (++count >= 2) break;
            }
            return b.toString();
        } else {
            return ("" + fullName.trim().charAt(0)).toUpperCase();
        }
    }

    public boolean isWhitespace(String str) {
        if (str == null) {
            return false;
        }
        int sz = str.length();
        for (int i = 0; i < sz; i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public boolean showNotification(Integer quickBloxId) {
        if (ChatHelper.getInstance().getCurrentChatDialog() != null) {
            QBChatDialog qbChatDialog = QbDialogHolder.getInstance()
                    .getDialogFromOpponentId(quickBloxId);
            if (qbChatDialog != null &&
                    qbChatDialog.getDialogId().equalsIgnoreCase(ChatHelper.getInstance().getCurrentChatDialog().getDialogId()))
                return false;
        }
        return true;
    }

    public static void showInfoDialog(Context context) {
        final Dialog dialog = new Dialog(context, R.style.CustomDialogTermsNConditionTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custome_dialog_wanted_service);
        dialog.setCancelable(false);

        TextView tv_detail_wanted_service = dialog.findViewById(R.id.tv_detail_wanted_service);
        Button btn_submit = dialog.findViewById(R.id.btn_submit);

        tv_detail_wanted_service.setMovementMethod(new ScrollingMovementMethod());

        dialog.show();


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }
}
