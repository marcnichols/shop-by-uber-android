package com.shopbyuber.utils;

import com.google.android.gms.maps.model.LatLng;


public class Constants {
    public static LatLng SBU_LOS_ANGELES = new LatLng(34.053395, -118.242594);
    public static String SBU_DEAFAULT_FONT = "fonts/SOURCESANSPRO-REGULAR.OTF";

    public static boolean SBU_EnableFabric = true;

    public static int TimeUpdateInterval = 3000;

    //    FlurryAgent Testing key
    //    public static final String SBU_FlurryKey = "FQ335Y5JXYV2D37M4CB4";

    //  FlurryAgent  marcnichols Key
    public static final String SBU_FlurryKey = "J52WFF5NS8MZ535C4Q7B";

    //QuickBlox
    public static String SBU_QB_CONFIG_FILE_NAME = "qb_config.json";
    public static final String SBU_actionUpdateDialogs = "updateDialogList";
    public static final String SBU_actionUpdateMessage = "updateMessage";

    // Live account
    public static String SBU_AppId = "68211";
    public static String SBU_AuthKey = "S4mVe3PZJddjjmw";
    public static String SBU_AuthSecrets = "vWjG5JUNN6Zm4eE";
    public static String SBU_QBAccountKey = "HbhL1XeBEh4Ez_xxbW7c";

    // Developer account
    /*public static String SBU_AppId = "69295";
    public static String SBU_AuthKey = "kSzCdgkyfXrjgjX";
    public static String SBU_AuthSecrets = "DaMU4aqCrJEJ8WP";
    public static String SBU_QBAccountKey = "EYerWtymzgkyEMzvss1a";*/

    public static String SBU_QBPasssword = "T8EZ07L27tezl";

    public static final String SBU_QBDialogId = "QBDialogId";
    public static final String SBU_From = "from";
    public static final String SBU_From_dialogList = "from_dialogList";

    public static final String SBU_QuickBloxId = "quickBloxId";
    public static final String SBU_QuickBloxIds = "quickBloxIds";
    public static final String SBU_OpponentId = "opponentId";

    public static final String SBU_Notify_Adapter = "notify";
    public static final String SBU_QBuserId = "QBuserId";

    public static String SBU_ReceiverId = "receiverId";
    public static String SBU_Message = "message";
    public static String SBU_DialogId = "dialogId";
    public static String SBU_DialogName = "dialogName";

    public static String IM_total_entries = "total_entries";
    public static int SBU_err_qb_login_taken = 422;
    public static String SBU_err_qb_logged_in = "You have already logged in chat";

    public static String SBU_Is_History_Enable = "historyEnable";
    public static String SBU_History = "history";

    public static final String SBU_actionUpdateTotalUnread = "updateTotalUnread";

    // Twitter
    public static final String TWITTER_KEY = "32JD4KUIfeMchpGJE1BQV8xEC";
    public static final String TWITTER_SECRET = "ARbM9ox4VGwy2PJJSSfIlaGWVdd6WWPFK4SJAJi0xxJYZUJFPS";

    public static final String SBU_Secrets = "secrets";
    public static String SBU_FirstName = "firstName";
    public static String SBU_LastName = "lastName";
    public static String SBU_MobilePhone = "mobilePhone";
    public static String SBU_CountryCode = "countryCode";
    public static String SBU_PhoneNumber = "phoneNumber";
    public static String SBU_UserRole = "userRole";
    public static String SBU_DeviceToken = "deviceToken";
    public static String SBU_TimeZone = "timeZone";
    public static String SBU_DeviceType = "deviceType";
    public static String SBU_FacebookId = "facebookId";
    public static String SBU_TwitterId = "twitterId";

    public static String SBU_Email = "email";
    public static String SBU_Password = "password";

    public static String SBU_Currency = "$";
    public static String SBU_UserId = "userId";
    public static String SBU_VerifyCode = "verifyCode";
    public static String SBU_UserModel = "UserModel";

    public static String SBU_CommercialStatus = "CommercialStatus";

    public static String SBU_Name = "name";
    public static String SBU_Mobile = "mobilePhone";
    public static String SBU_ProfileImage = "profileImage";
    public static String SBU_ImageType = "imageType";
    public static String SBU_Description = "description";
    public static String SBU_Speciality = "speciality";

    public static String SBU_SessionId = "sessionId";
    public static String SBU_VendorId = "vendorId";
    public static String SBU_CompanyName = "companyName";
    public static String SBU_ComapnyScreenName = "comapnyScreenName";
    public static String SBU_OwnerName = "ownerName";
    public static String SBU_CompanyAddress = "companyAddress";
    public static String SBU_Zipcode = "zipcode";
    public static String SBU_Latitude = "latitude";
    public static String SBU_Longitude = "longitude";
    public static String SBU_Bearing = "bearing";
    public static String SBU_CompanyPhone = "companyPhone";
    public static String SBU_CompanyPhoneNumber = "companyPhoneNumber";
    public static String SBU_WebsiteUrl = "websiteUrl";
    public static String SBU_ServiceDescription = "serviceDescription";
    public static String SBU_Landmark = "landmark";

    public static String SBU_Service = "service";
    public static String SBU_ServiceId = "serviceId";
    public static String SBU_IsAvailable = "isAvailable";

    public static String SBU_CompanyProfileImage = "companyProfileImage";
    public static String SBU_CompanyId = "companyId";

    public static String SBU_Hour = " Hour";
    public static String SBU_Hours = " Hours";
    public static String SBU_Month = "month";
    public static String SBU_Year = "year";
    public static String SBU_Duration = "duration";
    public static String SBU_Day = "day";
    public static String SBU_Timings = "timings";
    public static String SBU_RequestFee = "requestFee";
    public static String SBU_UserInfo = "userInfo";
    // Pagination
    public static String SBU_PageNo = "pageNo";
    public static String SBU_NumOfRecords = "numOfRecords";
    public static int NumberOfRecords = 10;
    public static int NumberOfMessagesPerPage = 50;
    public static final int progress_threshold_2 = 2;
    public static final int progress_threshold_5 = 5;
    public static final boolean addLoadingRow = true;
    public static final int GRID_SPAN = 2;

    public static String SBU_GetVendorFrom = "getvendorfrom";

    public static String SBU_CatName = "catName";
    public static String SBU_AppointmentTimeId = "appointmentTimeId";
    public static String SBU_Status = "status";

    // AddService
    public static String SBU_CategoryId = "categoryId";
    public static String SBU_SubCategoryId = "subCategoryId";
    public static String SBU_ScreenName = "screenName";
    public static String SBU_FlatFee = "flatFee";
    public static String SBU_ServiceDetail = "serviceDetail";
    public static String SBU_DayScheduling = "dayScheduling";
    public static String SBU_AvailableSchedule = "availableSchedule";

    public static String SBU_Date = "date";
    public static String SBU_Timeslot = "timeslot";
    public static String SBU_Time = "time";
    public static String SBU_Allocated = "allocated";

    public static String SBU_Add_service_from = "add_service_from";

    public static String SBU_SearchText = "searchText";

    public static String SBU_RequestId = "requestId";
    public static String SBU_PubChannelName = "pubChannelName";
    public static String SBU_TimeSlotId = "timeSlotId";
    public static String SBU_CheckDate = "checkDate";
    public static String SBU_Keywords = "keywords";

    //pay for service
    public static String SBU_Services = "services";
    public static String SBU_TransactionId = "TransactionId";
    public static String SBU_Total = "Total";
    public static String SBU_PaymentDateTime = "paymentDateTime";
    public static String SBU_Address = "address";

    public static String SBU_Rating = "rating";
    public static String SBU_Feedback = "feedback";

    public static String SBU_NotificationId = "notificationId";
    public static String SBU_NotifyId = "notifyId";
    public static String SBU_IsEnabled = "isEnabled";

    public static String SBU_PaypalEmail = "paypalEmail";
    public static String SBU_ServiceProvider_Image = "serviceProviderImage";
    public static String SBU_ServiceProvider = "serviceProvider";

    //  Notifications
    public static String SBU_Alert = "alert";
    public static String SBU_Title = "title";
    public static String SBU_Params = "params";
    public static String SBU_PlaySound = "playSound";
    public static String SBU_Vibrate = "vibrate";
    public static String SBU_PulseLight = "pulseLight";
    public static String SBU_NotifyType = "notifyType";
    public static String SBU_StateId = "stateId";
    public static String SBU_PaymentDetail = "paymentDetail";
    public static String SBU_StreetAddress = "streetAddress";
    public static String SBU_City = "city";
    public static String SBU_State = "state";
    public static String SBU_RoutingNumber = "routingNumber";
    public static String SBU_AccountNumber = "accountNumber";
    public static String SBU_PaymentMethod = "paymentMethod";
    public static String SBU_PaymentZipCode = "zipCode";


    public static int Reason = 123;

    public static String SBU_Id = "id";


    public static String SBU_STRIPE_KEY = "pk_test_vxPSxLscb2NDyzMSvRM9l3Cy";

    //PubNUb SubScribe Key and Publish Key
    public static String SBU_Subscribe_key = "sub-c-039de6ee-6978-11e7-a3eb-0619f8945a4f";
    public static String SBU_Publish_key = "pub-c-d4c80b59-08af-4284-b8c5-eca7639dad30";



    /*------------------------------------------------------------------------------------------------*/
    /* TheEZList */


    /*Sandbox*/
    public static String SBU_PaypalClientID = "AXpbE1YKLusg5IlXvykVNg7vBplzK4rR-BbVEWaRNlVASbUFWbC0sLGxeoX9R2Otx_W_IYbBLxWz-85_";

    //Live
    /*public static String SBU_PaypalClientID = "ARnYF_wuYZQdRep1lJPuFkgm0xhzLm3KVApa1gJO5WT_K315KtdCA9IFdSF9vV6r5dDYVQ0zpkFco1aZ";*/
    /*------------------------------------------------------------------------------------------------*/



    /*------------------------------------------------------------------------------------------------*/
    /*Pro account detail*/

    /*Sandbox*/
    /*public static String SBU_PaypalClientID = "AcHKz-o2MgbkUJhieww9XZkf5PyDFdaRxDUkABERkXFOGPNW_8gW7DNXFlBtPYtZRy0gBG6NBhLWcZNS";*/

    /*Live*/
//    public static String SBU_PaypalClientID = "Ab9mRwkuVn-yMpGKVwVEdKnQMUrUDpeCbSqXyC8Eqhz8GUmo5-MmdcFU8KxJ5_xRkPhkCF3oqJL0U1qj";
    /*------------------------------------------------------------------------------------------------*/


    public static String SBU_ActionUpdateCounts = "updateCounts";
    public static String SBU_ActionVendorArrieved = "vendorArrieved";
    public static String SBU_ServiceComplete = "serviuceComplete";
    public static String SBU_LoadYourActivity = "loadYourActivity";
    public static String SBU_For = "for";
    public static String SBU_LoadLocal = "loadLocal";
    public static String SBU_RefreshList = "refreshList";

    public static String SBU_CreatedPubChannelNames = "createdPubChannelNames";
    public static String SBU_RequestIds = "requestIds";

    public static String SBU_UserLatitude = "userLatitude";
    public static String SBU_UserLongitude = "userLongitude";
    public static String SBU_isTermCondition = "isTermCondition";

    public static String SBU_VendorData = "vendorData";

    public static String SBU_CancelReason = "cancelReason";
    public static String SBU_PaymentURL = "paymentURL";
    public static String SBU_PayKey = "PayKey";
    public static String SBU_ServiceEditAble = "serviceEditAble";

    public static String SBU_RegisterFrom = "registerFrom";
    public static String SBU_Android = "android";
    public static String SBU_EzListScreenName = "ezListScreenName";
    public static String SBU_SwitchTo = "switchTo";

    public static String SBU_ConfirmService = "confirmServices";
    public static String SBU_StripeKey;
    public static String SBU_StripeScreteKey;
    public static String SBU_Token = "token";
    public static String SBU_PromoCode = "promoCode";
    public static String SBU_AllSubcategoryRequired = "allSubcategoryRequired";
    public static String SBU_UserName = "userName";
    public static String SBU_QBDetails = "qbDetails";

    public static String SBU_Country = "country";
    public static String SBU_IsFlag = "isFlag";

    public static String SBU_ServiceName = "serviceName";
    public static String SBU_GetAll = "getAll";
    public static String SBU_UpdateWantedService = "updateWantedService";
    public static String SBU_WantedServiceModel = "wantedServiceModel";
    public static String SBU_LoadWanted = "loadWanted";

}
