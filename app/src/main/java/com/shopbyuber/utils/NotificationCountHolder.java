package com.shopbyuber.utils;

public class NotificationCountHolder {
    private static NotificationCountHolder instance;
    int unreads;

    public static synchronized NotificationCountHolder getInstance() {
        if (instance == null) {
            instance = new NotificationCountHolder();
        }
        return instance;
    }

    public void setUnreads(int count) {
        unreads = count;
    }

    public int getUnreads() {
        return unreads;
    }
}
