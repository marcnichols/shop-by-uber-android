package com.shopbyuber.utils;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.IBinder;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.flurry.android.FlurryAgent;
import com.mapbox.mapboxsdk.Mapbox;
import com.orhanobut.logger.Logger;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.shopbyuber.R;
import com.shopbyuber.customclasses.CustomViewWithTypefaceSupport;
import com.shopbyuber.customclasses.TextField;
import com.shopbyuber.main.ChatHeadService;
import com.txusballesteros.bubbles.BubbleLayout;
import com.txusballesteros.bubbles.BubblesManager;
import com.txusballesteros.bubbles.OnInitializedCallback;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


public class CoreApp extends MultiDexApplication {
    private static CoreApp instance;
    private static final String LOG_TAG = CoreApp.class.getSimpleName();

    BubblesManager bubblesManager;
    BubbleLayout bubbleView;
    TextView tvBubblePromoCode;

    Globals globals;

    public static ChatHeadService chatHeadService;
    static boolean bound;
    static ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            ChatHeadService.LocalBinder binder = (ChatHeadService.LocalBinder) service;
            chatHeadService = binder.getService();
            bound = true;
            chatHeadService.minimize();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            bound = false;
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        globals = ((Globals) getApplicationContext());

        if (Constants.SBU_EnableFabric)
            Fabric.with(this, new Crashlytics());
        String mapboxAccessToken = Globals.getMapboxAccessToken(getApplicationContext());
        if (TextUtils.isEmpty(mapboxAccessToken) || mapboxAccessToken.equals(getString(R.string.MAPBOX_ACCESS_TOKEN))) {
            Logger.e(LOG_TAG + " : Warning: access token isn't set.");
        }
        Mapbox.getInstance(getApplicationContext(), mapboxAccessToken);

        new FlurryAgent.Builder()
                .withLogEnabled(true)
                .build(this, Constants.SBU_FlurryKey);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder().setDefaultFontPath(Constants.SBU_DEAFAULT_FONT)
                .setFontAttrId(R.attr.fontPath)
                .addCustomViewWithSetTypeface(CustomViewWithTypefaceSupport.class)
                .addCustomStyle(TextField.class, R.attr.textFieldStyle)
                .build()
        );

    }

    public void initChatHeadService() {
        Intent intentChatHead = new Intent(this, ChatHeadService.class);
        this.startService(intentChatHead);
        this.bindService(intentChatHead, mConnection, Context.BIND_AUTO_CREATE);
    }

    public static synchronized CoreApp getInstance() {
        return instance;
    }

    public PubNub initPubnub() {
        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey(Constants.SBU_Subscribe_key);
        pnConfiguration.setPublishKey(Constants.SBU_Publish_key);
        return new PubNub(pnConfiguration);
    }

    public void addNewBubble() {
        if (globals.getUserData().Data != null
                && !globals.getUserData().Data.promoCode.equals("")) {
            if (bubbleView == null) {
                bubbleView = (BubbleLayout) LayoutInflater.from(this).inflate(R.layout.bubble_layout, null);
                tvBubblePromoCode = bubbleView.findViewById(R.id.tv_bubble_promo_code);
                tvBubblePromoCode.setText(globals.getUserData().Data.promoCode.trim());

                bubbleView.setShouldStickToWall(true);
                bubblesManager.addBubble(bubbleView, 30, 50);
            } else {
                tvBubblePromoCode.setText(globals.getUserData().Data.promoCode.trim());
            }
        } else
            removeBubble();
    }

    public void initializeBubblesManager() {
        if (bubblesManager == null) {
            bubblesManager = new BubblesManager.Builder(this)
                    .setTrashLayout(R.layout.bubble_trash_layout)
                    .setInitializationCallback(new OnInitializedCallback() {
                        @Override
                        public void onInitialized() {
                            addNewBubble();
                        }
                    })
                    .build();
            bubblesManager.initialize();
        } else {
            addNewBubble();
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void removeBubble() {
        if (bubblesManager != null
                & bubbleView != null) {
            try {
                bubblesManager.recycle();
            } catch (Exception e) {
                e.printStackTrace();
            }
            bubbleView = null;
            bubblesManager = null;
        }
    }

    public ChatHeadService getChatHeadService() {
        return chatHeadService;
    }
}
