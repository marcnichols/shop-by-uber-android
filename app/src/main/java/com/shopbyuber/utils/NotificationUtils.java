package com.shopbyuber.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.shopbyuber.R;
import com.shopbyuber.enums.NotificationTypeBuyer;
import com.shopbyuber.enums.NotificationTypeSeller;
import com.shopbyuber.enums.UserRole;
import com.shopbyuber.model.QBNotificationModel;
import com.shopbyuber.model.RemoteNotificationModel;
import com.shopbyuber.quickblox.QBChatActivity;
import com.shopbyuber.seller.SellerLocationUpdateActivity;
import com.shopbyuber.seller.ViewRequestActivity;
import com.shopbyuber.user.NotificationActivity;
import com.shopbyuber.user.OrderDetailActivity;
import com.shopbyuber.user.PaymentConfirmationActivity;
import com.shopbyuber.user.ServiceDetailActivity;
import com.shopbyuber.wantedservice.WantedServiceDetailActivity;

import java.util.ArrayList;

public class NotificationUtils {
    static Globals globals = (Globals) Globals.getContext();
    static Intent intentUpdateCounts = new Intent(Constants.SBU_ActionUpdateCounts);
    static Intent intentVendorArrieved = new Intent(Constants.SBU_ActionVendorArrieved);
    static boolean updateCounts;
    static boolean createBubble;

    public static void showNotification(Context context, RemoteNotificationModel remoteNotificationModel) {
        if (globals.getUserData() != null) {
            boolean isChat = false;

            updateCounts = true;

            // used to manage bubble visibility
            createBubble = true;
            Intent intent = null;
            if (globals.getUserData().Data.userRole == UserRole.BUYER.getRole()) {
                NotificationTypeBuyer notificationTypeBuyer = NotificationTypeBuyer.fromInt(remoteNotificationModel.notifyType);
                switch (notificationTypeBuyer) {
                    case Messaged_Receive:
                        createBubble = globals.showNotification(remoteNotificationModel.quickBloxId);
                        isChat = true;

                        intent = new Intent(context, QBChatActivity.class);
                        intent.putExtra(Constants.SBU_QBDetails, getQbNotificationModel(remoteNotificationModel, isChat));
                        break;
                    case Vendor_has_confirmed_your_request:
                        createBubble = false;
                        intent = new Intent(context, PaymentConfirmationActivity.class);
                        break;
                    case Vendor_has_arrived_and_your_service_has_been_processed:
                        intentVendorArrieved.putExtra(Constants.SBU_RequestId, remoteNotificationModel.requestId);
                        LocalBroadcastManager.getInstance(context).sendBroadcast(intentVendorArrieved);

                        ArrayList<String> createdPubChannelNames = new ArrayList<>();
                        createdPubChannelNames.add(remoteNotificationModel.pubChannelName);
                        intent = new Intent(context, SellerLocationUpdateActivity.class);
                        intent.putStringArrayListExtra(Constants.SBU_CreatedPubChannelNames, createdPubChannelNames);
                        intent.putExtra(Constants.SBU_RequestId, remoteNotificationModel.requestId);
                        break;
                    case User_has_made_request:
                        createBubble = false;
                        intent = new Intent(context, OrderDetailActivity.class);
                        intent.putExtra(Constants.SBU_RequestId, remoteNotificationModel.requestId);
                        break;
                    case Vendor_is_on_route_to_your_location:
                    case Vendor_has_cancelled_request: {
                        intent = new Intent(context, OrderDetailActivity.class);
                        intent.putExtra(Constants.SBU_RequestId, remoteNotificationModel.requestId);
                        break;
                    }
                    case Service_has_been_completed:
                        createBubble = false;
                        broadcastComplete(remoteNotificationModel, context);

                        intent = new Intent(context, OrderDetailActivity.class);
                        intent.putExtra(Constants.SBU_RequestId, remoteNotificationModel.requestId);
                        break;
                    case I_can_review_my_recent_purchases:
                        break;
                    case New_app_features_are_available:
                        break;
                    case There_are_items_left_in_my_cart:
                        updateCounts = false;
                        createBubble = false;
                        intent = new Intent(context, PaymentConfirmationActivity.class);
                        break;
                    case New_updates_in_my_favourite_service:
                        intent = new Intent(context, ServiceDetailActivity.class);
                        intent.putExtra(Constants.SBU_CompanyId, remoteNotificationModel.companyId);
                        intent.putExtra(Constants.SBU_CategoryId, remoteNotificationModel.categoryId);
                        break;
                    case Service_completed:
                        createBubble = false;
                        broadcastComplete(remoteNotificationModel, context);
                        break;
                    case Vendor_create_wanted_service:
                        createBubble = false;
                        intent = new Intent(context, ServiceDetailActivity.class);
                        intent.putExtra(Constants.SBU_CompanyId, remoteNotificationModel.companyId);
                        intent.putExtra(Constants.SBU_CategoryId, remoteNotificationModel.categoryId);
                        intent.putExtra(Constants.SBU_SubCategoryId, remoteNotificationModel.subCategoryId);
                        intent.putExtra(Constants.SBU_ServiceId, remoteNotificationModel.serviceId);
                        intent.putExtra(Constants.SBU_RequestId, remoteNotificationModel.requestId);
                        break;
                    default:
                        break;
                }
            } else if (globals.getUserData().Data.userRole == UserRole.SELLER.getRole()) {
                NotificationTypeSeller notificationTypeSeller = NotificationTypeSeller.fromInt(remoteNotificationModel.notifyType);
                if (notificationTypeSeller != null) {
                    switch (notificationTypeSeller) {
                        case Messaged_Receive:
                            createBubble = globals.showNotification(remoteNotificationModel.quickBloxId);
                            isChat = true;

                            intent = new Intent(context, QBChatActivity.class);
                            intent.putExtra(Constants.SBU_QBDetails, getQbNotificationModel(remoteNotificationModel, isChat));
                            break;
                        case User_has_made_request:
                        case User_has_made_payment_for_confirmed_service:
                        case User_has_canceled_request_for_confirmed_service: {
                            intent = new Intent(context, ViewRequestActivity.class);
                            intent.putExtra(Constants.SBU_RequestId, remoteNotificationModel.requestId);
                            break;
                        }
                        case New_app_features_are_available:
                            break;
                        case You_have_new_buyer_activity:
                            updateCounts = false;
                            createBubble = false;
                            intent = new Intent(context, NotificationActivity.class);
                            break;
                        case Service_completed:
                            createBubble = false;
                            broadcastComplete(remoteNotificationModel, context);
                            break;
                        case User_has_request_wanted_service:
                            createBubble = false;
                            intent = new Intent(context, WantedServiceDetailActivity.class)
                                    .putExtra(Constants.SBU_RequestId, remoteNotificationModel.requestId)
                                    .putExtra(Constants.SBU_GetAll, true);
                            break;
                        default:
                            break;
                    }
                }
            }

            // generate chat or regular notification and bubble
            if (intent != null) {
                intent.putExtra(Constants.SBU_NotificationId, remoteNotificationModel.notificationId);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                if (isChat) {
                    if (globals.showNotification(remoteNotificationModel.quickBloxId))
                        generateNotofication(context, remoteNotificationModel, intent);
                    else
                        com.orhanobut.logger.Logger.e("no need to generate notification");
                } else generateNotofication(context, remoteNotificationModel, intent);

                if (updateCounts && !isChat) {
                    NotificationCountHolder.getInstance()
                            .setUnreads(NotificationCountHolder.getInstance().getUnreads() + 1);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intentUpdateCounts);
                }
                if (createBubble && remoteNotificationModel.quickBloxId != 0) {
                    if (CoreApp.getInstance().getChatHeadService() == null && globals.ifOverlay()) {
                        CoreApp.getInstance().initChatHeadService();
                    }

                    final Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.SBU_QBDetails, getQbNotificationModel(remoteNotificationModel, isChat));

                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (CoreApp.getInstance().getChatHeadService() != null) {
                                CoreApp.getInstance().getChatHeadService().addChatHead(bundle);
                            } else
                                com.orhanobut.logger.Logger.e("chatHeadService is null");
                        }
                    }, 100);
                }
            }
        }
    }

    //data used for chat screen
    private static QBNotificationModel getQbNotificationModel(RemoteNotificationModel remoteNotificationModel, boolean isChat) {
        QBNotificationModel qbNotificationModel = new QBNotificationModel();
        qbNotificationModel.profileImage = remoteNotificationModel.profileImage;
        qbNotificationModel.quickBloxId = remoteNotificationModel.quickBloxId;
        qbNotificationModel.userId = remoteNotificationModel.userId;
        qbNotificationModel.userName = remoteNotificationModel.userName;
        qbNotificationModel.dialogId = remoteNotificationModel.dialogId;
        qbNotificationModel.requestId = remoteNotificationModel.requestId;
        qbNotificationModel.isChat = isChat;
        return qbNotificationModel;
    }

    private static int currentTimeMillis() {
        return (int) (System.currentTimeMillis() % Integer.MAX_VALUE);
    }

    private static void generateNotofication(Context context, RemoteNotificationModel remoteNotificationModel, Intent intent) {
        int NOTIFICATION_ID = currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getActivity(context, NOTIFICATION_ID, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context);
        notificationBuilder.setSmallIcon(getSmallIcon());
        notificationBuilder.setWhen(System.currentTimeMillis());
        notificationBuilder.setContentTitle(remoteNotificationModel.title);
        notificationBuilder.setContentText(remoteNotificationModel.alert);
        notificationBuilder.setAutoCancel(true);

        Notification notification = new Notification();
        if (remoteNotificationModel.playSound)
            notification.defaults |= Notification.DEFAULT_SOUND;
        if (remoteNotificationModel.vibrate)
            notification.defaults |= Notification.DEFAULT_VIBRATE;
        if (remoteNotificationModel.pulseLight)
            notification.defaults |= Notification.DEFAULT_LIGHTS;

        notificationBuilder.setDefaults(notification.defaults);
        notificationBuilder.setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(context.getString(R.string.channel_id), context.getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(mChannel);
        }
        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
    }

    public static int getSmallIcon() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return R.drawable.notification_white;
        } else {
            return R.mipmap.ic_launcher;
        }
    }

    static void broadcastComplete(final RemoteNotificationModel remoteNotificationModel, final Context context) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (CoreApp.getInstance().getChatHeadService() != null) {
                    CoreApp.getInstance().getChatHeadService().removeChatHead(remoteNotificationModel.userId);
                }
            }
        });

        Intent intentServiceComplete = new Intent(Constants.SBU_ServiceComplete);
        intentServiceComplete.putExtra(Constants.SBU_RequestId, remoteNotificationModel.requestId);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intentServiceComplete);
    }
}
