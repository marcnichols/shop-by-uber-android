package com.shopbyuber.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.shopbyuber.R;

import java.io.IOException;
import java.util.List;


public class FusedLocationManager extends Service implements GoogleApiClient.ConnectionCallbacks,
        LocationListener {

    private final Activity activity;

    private GoogleApiClient mGoogleApiClient;

    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    Location location; // location
    String strAddress = "";
    Address address;
    Geocoder gc;
    // Declaring a Location Manager
    protected LocationManager locationManager;


    //    ChangeLocationForBroadcast changeLocationForBroadcast;
    public FusedLocationManager(Activity activity) {
        this.activity = activity;
        if (location == null)
            getLocation();
        if (gc == null)
            gc = new Geocoder(activity);
        if (mGoogleApiClient == null)
            initGoogleApi();

    }

    public void initGoogleApi() {
        this.buildGoogleApiClient();
        mGoogleApiClient.connect();
    }

    private synchronized void buildGoogleApiClient() {

        mGoogleApiClient = new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(this).addApi(LocationServices.API)
                .build();
    }

    @SuppressLint("MissingPermission")
    public Location getLocation() {
        try {
            locationManager = (LocationManager) activity
                    .getSystemService(LOCATION_SERVICE);


            // getting GPS statuscd
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
                this.canGetLocation = true;
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    }
                }
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    if (location == null && locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onConnected(Bundle connectionHint) {
        LocationRequest mLocationRequest = createLocationRequest();
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private LocationRequest createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
    }

    public LatLng getLatLng() {
        if (location != null)
            return new LatLng(location.getLatitude(), location.getLongitude());
        else
            return new LatLng(0, 0);
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    /**
     * Function to show settings alert dialog
     * On pressing Settings button will lauch Settings Options
     */
    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);

        alertDialog.setTitle(activity.getString(R.string.gps_setting));
        // Setting Dialog Message
        alertDialog.setMessage(activity.getString(R.string.gps_not_enabled));

        // On pressing Settings button
        alertDialog.setPositiveButton(activity.getString(R.string.settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                activity.startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton(activity.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void setAddress() {
        try {
            if (strAddress.isEmpty() && Geocoder.isPresent() && location != null) {
                List<Address> list = gc.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                if (!list.isEmpty()) {
                    address = list.get(0);
                    strAddress = address.getAddressLine(0) + ", " + address.getLocality() + ", "
                            + address.getAdminArea() + ", " + address.getCountryName() + ", " + address.getPostalCode();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Address getAdminArea() {
        setAddress();
        return address;
    }

    public String getAddress() {
        setAddress();
        return strAddress;
    }
}
