package com.shopbyuber.utils;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.paginate.abslistview.LoadingListItemCreator;
import com.shopbyuber.R;
import com.shopbyuber.user.AdapterServiceDetail.ViewHolder;



public class PaginationProgressBarListAdapter implements LoadingListItemCreator {
    @Override
    public View newView(int position, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.custom_loading_list_item, parent, false);
        view.setTag(new ViewHolder(view));
        return view;
    }

    @Override
    public void bindView(int position, View view) {
        // Bind custom loading row if needed
    }
}