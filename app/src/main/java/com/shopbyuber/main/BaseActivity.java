package com.shopbyuber.main;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BaseActivity extends AppCompatActivity {
    @Override
    protected void attachBaseContext(Context newBase) {
        // init Calligraphy for support custom fonts
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void hideUnhide(final LinearLayout contentView, final LinearLayout linearLayout) {
        // trigger keyboard open and close event
        contentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                Rect r = new Rect();
                contentView.getWindowVisibleDisplayFrame(r);
                int screenHeight = contentView.getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;

                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened
                    linearLayout.setVisibility(View.GONE);
                } else {
                    // keyboard is closed
                    linearLayout.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}
