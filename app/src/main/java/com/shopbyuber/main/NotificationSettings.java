package com.shopbyuber.main;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.model.NotificationSettingsModel;
import com.shopbyuber.model.NotificationSettingsModel.NotificationSection;
import com.shopbyuber.model.UserModel;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;

public class NotificationSettings extends BaseActivity {
    @BindView(R.id.list)
    RecyclerView list;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    Globals globals;
    UserModel userModel;
    NotificationSettingsAdapter notificationSettingsAdapter;
    public static NotificationSettings mContext;

    public static NotificationSettings getInstance() {
        return mContext;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_settings);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        mContext = this;
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.title_activity_notifications);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        globals = (Globals) getApplicationContext();
        userModel = globals.getUserData();
        doGetSettingsRequest();
    }

    public void doGetSettingsRequest() {
        if (!Globals.internetCheck(NotificationSettings.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getSessionValidateParams(
                userModel.Data.userId, userModel.Data.sessionId);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(NotificationSettings.this);

        HttpRequestHandler.getInstance().post(NotificationSettings.this, getString(R.string.getNotificationData), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                NotificationSettingsModel notificationSettingsModel = new Gson().fromJson(response.toString(),
                        NotificationSettingsModel.class);
                setUpList(notificationSettingsModel);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    public void doSetSettingsRequest(int notifyId, boolean isEnabled) {
        if (!Globals.internetCheck(NotificationSettings.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().setNotificationDataParams(
                userModel.Data.userId, userModel.Data.sessionId,
                notifyId, isEnabled);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(NotificationSettings.this);

        HttpRequestHandler.getInstance().post(NotificationSettings.this, getString(R.string.setNotificationData), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                DataModel dataModel = new Gson().fromJson(response.toString(),
                        DataModel.class);
                if (dataModel.Result) {
                    Logger.d(dataModel.Message);
                } else {
                    Toaster.shortToast(dataModel.Message);
                    if (dataModel.Message.equals(getString(R.string.invalid_session))) {
                        globals.doLogout(NotificationSettings.this);
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void setUpList(NotificationSettingsModel notificationSettingsModel) {
        if (notificationSettingsModel.Result) {
            ArrayList<NotificationSection> notificationSections = new ArrayList<>();
            NotificationSection notificationSection = new NotificationSection();
            notificationSection.notifications = notificationSettingsModel.Data.basicNotification;
            notificationSections.add(notificationSection);

            NotificationSection notificationOtherSection = new NotificationSection();
            notificationOtherSection.section = getString(R.string.notify_me_when);
            notificationOtherSection.notifications = notificationSettingsModel.Data.otherNotification;
            notificationSections.add(notificationOtherSection);

            notificationSettingsAdapter = new NotificationSettingsAdapter(notificationSections);
            list.setAdapter(notificationSettingsAdapter);
        } else {
            Toaster.shortToast(notificationSettingsModel.Message);
            if (notificationSettingsModel.Message.equals(getString(R.string.invalid_session))) {
                globals.doLogout(NotificationSettings.this);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
