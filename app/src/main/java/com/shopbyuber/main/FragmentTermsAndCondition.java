package com.shopbyuber.main;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.shopbyuber.R;
import com.shopbyuber.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentTermsAndCondition extends Fragment {
    @BindView(R.id.webview)
    WebView webView;
    boolean isTermCondition;

    public FragmentTermsAndCondition newInstance(boolean isTermCondition) {
        FragmentTermsAndCondition fragmentTermsAndCondition = new FragmentTermsAndCondition();
        Bundle args = new Bundle();
        args.putBoolean(Constants.SBU_isTermCondition, isTermCondition);
        fragmentTermsAndCondition.setArguments(args);
        return fragmentTermsAndCondition;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isTermCondition = getArguments().getBoolean(Constants.SBU_isTermCondition);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_terms_and_condition, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        if (isTermCondition) {
            webView.loadUrl(getString(R.string.base_server_url) + getString(R.string.terms_n_condition_url));
        } else {
            webView.loadUrl(getString(R.string.base_server_url) + getString(R.string.privacy_policy_url));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // hide menu for setting fragment
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }
}
