package com.shopbyuber.main;


import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.flipkart.chatheads.ui.ChatHead;
import com.flipkart.chatheads.ui.ChatHeadViewAdapter;
import com.flipkart.chatheads.ui.MaximizedArrangement;
import com.flipkart.chatheads.ui.MinimizedArrangement;
import com.flipkart.chatheads.ui.container.DefaultChatHeadManager;
import com.flipkart.chatheads.ui.container.WindowManagerContainer;
import com.flipkart.circularImageView.CircularDrawable;
import com.flipkart.circularImageView.TextDrawer;
import com.flipkart.circularImageView.notification.CircularNotificationDrawer;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.model.NotificationModel;
import com.shopbyuber.model.QBNotificationModel;
import com.shopbyuber.quickblox.QBChatActivity;
import com.shopbyuber.quickblox.Utils.ImageSaver;
import com.shopbyuber.quickblox.Utils.saveImageFromURL;
import com.shopbyuber.user.AdapterServiceNotification;
import com.shopbyuber.user.UserLandingActivity;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

public class ChatHeadService extends Service {

    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();
    private DefaultChatHeadManager<String> chatHeadManager;
    private WindowManagerContainer windowManagerContainer;
    private Map<String, View> viewCache = new HashMap<>();
    private Map<String, QBNotificationModel> notificationModelMap = new HashMap<>();
    ImageSaver imageSaver;
    Globals globals;
    private int badgeAngle = 30;

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        globals = ((Globals) getApplicationContext());
        imageSaver = new ImageSaver(ChatHeadService.this);

        windowManagerContainer = new WindowManagerContainer(this);
        chatHeadManager = new DefaultChatHeadManager<>(this, windowManagerContainer);
        chatHeadManager.setViewAdapter(new ChatHeadViewAdapter<String>() {

            @Override
            public View attachView(final String key, final ChatHead chatHead, final ViewGroup parent) {
                if (viewCache.get(key) != null)
                    viewCache.remove(key);
                View cachedView = viewCache.get(key);

                Bundle bundle = chatHead.getExtras();
                final QBNotificationModel qbNotificationModel = (QBNotificationModel) bundle.getSerializable(Constants.SBU_QBDetails);
                RecyclerView notification_recycler;
                MaterialProgressBar progress;

                resetBadge(key);

                if (cachedView == null) {
                    LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                    View view = inflater.inflate(R.layout.layout_circle_notification, parent, false);
                    TextView startChat = view.findViewById(R.id.tv_start_chat);
                    notification_recycler = view.findViewById(R.id.notification_recycler);
                    progress = view.findViewById(R.id.progress);

                    startChat.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View view, MotionEvent event) {
                            chatHead.onTouchEvent(event);
                            redirectToChat(qbNotificationModel);
                            return false;
                        }
                    });
                    cachedView = view;
                    viewCache.put(key, view);
                } else {
                    notification_recycler = cachedView.findViewById(R.id.notification_recycler);
                    progress = cachedView.findViewById(R.id.progress);
                }

                if (qbNotificationModel.requestId != null &&
                        !qbNotificationModel.requestId.equals(""))
                    loadNotification(notification_recycler, qbNotificationModel.requestId, progress);

                parent.addView(cachedView);
                return cachedView;
            }

            @Override
            public void detachView(String key, ChatHead<? extends Serializable> chatHead, ViewGroup parent) {
                View cachedView = viewCache.get(key);
                if (cachedView != null) {
                    parent.removeView(cachedView);
                }
            }

            @Override
            public void removeView(String key, ChatHead<? extends Serializable> chatHead, ViewGroup parent) {
                View cachedView = viewCache.get(key);
                if (cachedView != null) {
                    if (viewCache.size() == 1)
                        windowManagerContainer.updateContainerLayout(windowManagerContainer.getFrameLayout());
                    viewCache.remove(key);
                    removeData(key);
                    parent.removeView(cachedView);
                }
            }

            @Override
            public Drawable getChatHeadDrawable(String key) {
                return ChatHeadService.this.getChatHeadDrawable(key);
            }
        });

        chatHeadManager.setArrangement(MinimizedArrangement.class, null);

        // Keep bubble dhow even after app kill
        startForeground(1, new Notification());
    }

    private void removeData(String key) {
        try {
            notificationModelMap.remove(key);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void redirectToChat(QBNotificationModel qbNotificationModel) {
        ChatHeadService.this.startActivity(new Intent(ChatHeadService.this, QBChatActivity.class)
                .putExtra(Constants.SBU_QBDetails, qbNotificationModel)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP)
        );
    }

    private void loadNotification(final RecyclerView notification_recycler, String requestId, final MaterialProgressBar progress) {
        if (!Globals.internetCheck(ChatHeadService.this))
            return;

        progress.setVisibility(View.VISIBLE);
        JSONObject params = HttpRequestHandler.getInstance().getNotificationListFromRequestId(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                requestId);

        HttpRequestHandler.getInstance().post(ChatHeadService.this,
                getString(R.string.getNotificationsListFromRequestId), params, new JsonHttpResponseHandler() {

                    @Override
                    public void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        progress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Logger.json(response.toString());
                        NotificationModel notificationModel = new Gson()
                                .fromJson(response.toString(), NotificationModel.class);
                        if (notificationModel.Result) {
                            setUpList(notification_recycler, notificationModel.Data.notifications);
                        } else
                            Toaster.shortToast(notificationModel.Message);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        Toaster.shortToast(R.string.error_went_wrong);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        Toaster.shortToast(R.string.error_went_wrong);
                    }
                });
    }

    private void setUpList(RecyclerView notification_recycler, ArrayList<NotificationModel.Notifications> notifications) {
        AdapterServiceNotification adapterServiceNotification = new AdapterServiceNotification(this, false);
        adapterServiceNotification.doRefresh(notifications);
        notification_recycler.setLayoutManager(new LinearLayoutManager(this));
        notification_recycler.setItemAnimator(new DefaultItemAnimator());
        notification_recycler.setAdapter(adapterServiceNotification);
    }

    private Drawable getChatHeadDrawable(String key) {
        CircularDrawable circularDrawable = new CircularDrawable();
        circularDrawable = getDrawable(key);
        return circularDrawable;
    }

    private CircularDrawable getDrawable(String key) {

        final CircularDrawable circularDrawable = new CircularDrawable();

        Bitmap ezBubble = ((BitmapDrawable) getResources().getDrawable(R.drawable.ez_bubble)).getBitmap();

        circularDrawable.setBitmapOrTextOrIcon(ezBubble);

        int badgeCount = notificationModelMap.get(key).unreadCount;
        if (badgeCount > 0)
            circularDrawable.setNotificationDrawer(new CircularNotificationDrawer()
                    .setNotificationAngle(badgeAngle)
                    .setNotificationText(String.valueOf(badgeCount))
                    .setNotificationColor(Color.WHITE, Color.RED));
        circularDrawable.setBorder(ContextCompat.getColor(ChatHeadService.this, R.color.colorAccent), 3);
        return circularDrawable;
    }

    private void moveToForeground() {
        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.notification_template_icon_bg)
                .setContentTitle("Chat heads active")
                .setContentIntent(
                        PendingIntent.getActivity(this,
                                0,
                                new Intent(this, SplashActivity.class),
                                0)
                )
                .build();

        startForeground(1, notification);
    }

    public void addChatHead(Bundle bundle) {
        QBNotificationModel qbNotificationModel = (QBNotificationModel) bundle.getSerializable(Constants.SBU_QBDetails);
        View cachedView = viewCache.get(qbNotificationModel.userId);
        boolean updateCount = needToUpdateCount(qbNotificationModel);

        if (cachedView == null) {
            if (updateCount)
                qbNotificationModel.unreadCount = qbNotificationModel.unreadCount + 1;
            notificationModelMap.put(qbNotificationModel.userId, qbNotificationModel);
            chatHeadManager.addChatHead(qbNotificationModel.userId, false, true, bundle);

            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.layout_circle_notification, null, false);
            viewCache.put(qbNotificationModel.userId, view);
        } else {
            if (updateCount)
                notificationModelMap.get(qbNotificationModel.userId).unreadCount =
                        notificationModelMap.get(qbNotificationModel.userId).unreadCount + 1;

            if (qbNotificationModel.requestId != null && !qbNotificationModel.requestId.equals(""))
                notificationModelMap.get(qbNotificationModel.userId).requestId = qbNotificationModel.requestId;
            updateBadgeCount(qbNotificationModel.userId);
        }
        chatHeadManager.bringToFront(chatHeadManager.findChatHeadByKey(qbNotificationModel.userId));
    }

    private boolean needToUpdateCount(QBNotificationModel qbNotificationModel) {
        if (qbNotificationModel.isChat) {
            return globals.showNotification(qbNotificationModel.quickBloxId);
        }
        return true;
    }

    public void removeChatHead(String key) {
        chatHeadManager.removeChatHead(key, true);
    }

    // remove ChatHead when user logout
    public void removeAllChatHeads() {
        try {
            chatHeadManager.removeAllChatHeads(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void toggleArrangement() {
        if (chatHeadManager.getActiveArrangement() instanceof MinimizedArrangement) {
            chatHeadManager.setArrangement(MaximizedArrangement.class, null);
        } else {
            chatHeadManager.setArrangement(MinimizedArrangement.class, null);
        }
    }

    public void updateBadgeCount(String key) {
        chatHeadManager.reloadDrawable(key);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        windowManagerContainer.destroy();
    }

    public void minimize() {
        chatHeadManager.setArrangement(MinimizedArrangement.class, null);
    }

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public ChatHeadService getService() {
            // Return this instance of LocalService so clients can call public methods
            return ChatHeadService.this;
        }
    }

    // reset total msg count
    public void resetBadge(String key) {
        if (notificationModelMap.get(key) != null) {
            if (notificationModelMap.get(key).unreadCount != 0) {
                notificationModelMap.get(key).unreadCount = 0;
                updateBadgeCount(key);
            }
        }
    }
}