package com.shopbyuber.main;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.shopbyuber.R;
import com.shopbyuber.enums.UserRole;
import com.shopbyuber.login.LoginActivity;
import com.shopbyuber.model.UserModel;
import com.shopbyuber.seller.SellerLandingActivity;
import com.shopbyuber.user.UserLandingActivity;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VideoCommercialActivity extends BaseActivity {
    @BindView(R.id.skip)
    TextView skip;

    Globals globals;
    UserModel userModel;

    boolean isFlag;
    private SimpleExoPlayerView simpleExoPlayerView;
    private SimpleExoPlayer player;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_video_commercial);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey(Constants.SBU_IsFlag))
            isFlag = bundle.getBoolean(Constants.SBU_IsFlag);

        globals = (Globals) getApplicationContext();
        userModel = globals.getUserData();


        // 1. Create a default TrackSelector
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

        // 2. Create a default LoadControl
        LoadControl loadControl = new DefaultLoadControl();

        // 3. Create the player
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector, loadControl);
        simpleExoPlayerView = new SimpleExoPlayerView(this);
        simpleExoPlayerView = (SimpleExoPlayerView) findViewById(R.id.player_view);

        //Set media controller
        simpleExoPlayerView.setUseController(true);
        simpleExoPlayerView.requestFocus();

        // Bind the player to the view.
        simpleExoPlayerView.setPlayer(player);

//        Uri mp4VideoUri = Uri.parse("file:///android_asset/tv_commercial.mp4");
        Uri mp4VideoUri = Uri.parse("");

        DefaultBandwidthMeter bandwidthMeterA = new DefaultBandwidthMeter();
        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, "exoplayer2example"), bandwidthMeterA);
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

        MediaSource videoSource = new ExtractorMediaSource(mp4VideoUri, dataSourceFactory, extractorsFactory, null, null);

        player.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playbackState == ExoPlayer.STATE_ENDED)
                    skipCommercial();
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPositionDiscontinuity() {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }
        });

        player.prepare(videoSource);
        player.setPlayWhenReady(true); //run file/link when ready to play.
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (player != null)
            player.setPlayWhenReady(false);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (player != null)
            player.setPlayWhenReady(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (player != null)
            player.setPlayWhenReady(true);
    }

    @OnClick(R.id.skip)
    public void skipCommercial() {
        if (player != null) {
            player.stop();
            player.release();
        }
        if (isFlag)
            finish();
        else {
            globals.setCommercialStatus(true);
            redirect();
        }
    }

    private void redirect() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // get user data from prefrence
                Intent intent = null;
                if (userModel == null)
                    intent = new Intent(VideoCommercialActivity.this, LoginActivity.class);
                else {
                    switch (UserRole.getEnum(userModel.Data.userRole - 1)) {
                        case BUYER:
                            intent = new Intent(VideoCommercialActivity.this, UserLandingActivity.class);
                            break;
                        case SELLER:
                            if (!userModel.Data.isCompanyProfileCreated) {
                                globals.setUserData(null);
                                intent = new Intent(VideoCommercialActivity.this, LoginActivity.class);
                            } else
                                intent = new Intent(VideoCommercialActivity.this, SellerLandingActivity.class);
                            break;
                    }
                }
                startActivity(intent);
                finish();
            }
        }, 1000);
    }
}

