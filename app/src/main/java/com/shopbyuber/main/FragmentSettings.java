package com.shopbyuber.main;


import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.util.ArraySet;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.UserRole;
import com.shopbyuber.login.LoginActivity;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.quickblox.ChatHelper;
import com.shopbyuber.quickblox.Utils.QbDialogHolder;
import com.shopbyuber.utils.CoreApp;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.NotificationCountHolder;
import com.shopbyuber.utils.Toaster;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;

public class FragmentSettings extends Fragment {
    Globals globals;

    @Nullable
    @BindView(R.id.rb_enable_history)
    RadioButton rb_enable_history;

    @BindView(R.id.tv_connected_as)
    TextView tv_connected_as;

    public FragmentSettings() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;
        if (globals.getUserData().Data.userRole == UserRole.SELLER.getRole())
            view = inflater.inflate(R.layout.fragment_vendor_settings, container, false);
        else
            view = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        globals = (Globals) getActivity().getApplicationContext();
    }

    private void init() {
        tv_connected_as.setText(globals.getUserData().Data.name != null ? getString(R.string.connected_as) + " "
                + globals.getUserData().Data.name : getString(R.string.connected_as));

        if (globals.getUserData().Data.userRole == UserRole.BUYER.getRole())
            setRadio();
    }

    private void setRadio() {
        if (rb_enable_history != null) {
            rb_enable_history.setChecked(globals.getEnableHistory());
        }
    }

    @Optional
    @OnClick(R.id.rel_enable_history)
    public void doCheckChange() {
        if (rb_enable_history != null) {
            globals.setEnableHistory(!rb_enable_history.isChecked());
        }
        setRadio();
    }

    @OnClick(R.id.sign_out)
    public void doSignOut() {
        doLogout(getActivity(),
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId);

        globals.disconnectFromFacebook();
    }

    @Optional
    @OnClick(R.id.tv_clear_viewing_history)
    public void doClearViewingHistory() {

    }

    @Optional
    @OnClick(R.id.tv_clear_searching_history)
    public void doClearSearchingHistory() {
        globals.setSearchHistory(new ArraySet<String>());
        Toaster.shortToast(getActivity().getString(R.string.clear_history));
    }

    @OnClick(R.id.tv_about_this_app)
    public void doAboutThisApp() {
        startActivity(new Intent(getActivity(), AboutActivity.class));
    }

    //redirect to notification settings
    @OnClick(R.id.tv_notification)
    public void doNotification() {
        startActivity(new Intent(getActivity(), NotificationSettings.class));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // hide menu for setting fragment
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    // perform logout
    public void doLogout(final Activity activity, String userId, String sessionId) {
        JSONObject params = HttpRequestHandler.getInstance().getLogoutParams(userId, sessionId);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(activity);

        HttpRequestHandler.getInstance().post(activity, activity.getString(R.string.signOutURL), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                CoreApp.getInstance().removeBubble();
                Logger.json(response.toString());
                gotoSplash(activity);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                gotoSplash(activity);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                gotoSplash(activity);
            }
        });
    }

    // redirect to splash activity
    private void gotoSplash(Activity activity) {
        NotificationManager nMgr = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancelAll();
        NotificationCountHolder.getInstance().setUnreads(0);
        ChatHelper.getInstance().logoutFromChat(getActivity());
        QbDialogHolder.getInstance().clearDialogs();
        CoreApp.chatHeadService.removeAllChatHeads();
        globals.setUserData(null);
        activity.startActivity(new Intent(activity, LoginActivity.class));
        activity.finish();
    }

    @Optional
    @OnClick(R.id.tv_clear_notification)
    public void doClearNotification() {
        doClear(getActivity(),
                getActivity().getString(R.string.clearNotification),
                true);
    }

    @Optional
    @OnClick(R.id.tv_clear_your_activity)
    public void doClearYourActivity() {
        doClear(getActivity(),
                getActivity().getString(R.string.clearYourActivity)
                , false);
    }

    private void doClear(Activity activity, String apiUrl, final boolean isClearNotification) {
        JSONObject params = HttpRequestHandler.getInstance().getClearDataParams(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(activity);

        HttpRequestHandler.getInstance().post(activity, apiUrl, params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel model = new Gson().fromJson(response.toString(), DataModel.class);
                if (model.Result && isClearNotification)
                    NotificationCountHolder.getInstance().setUnreads(0);
                else {
                    if (model.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(getActivity());
                }
                Toaster.shortToast(model.Message);


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }
}
