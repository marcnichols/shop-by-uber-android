package com.shopbyuber.main;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.GetVendorsFrom;
import com.shopbyuber.model.LocalCategoriesModel;
import com.shopbyuber.model.LocalCategoriesModel.LocalCategories;
import com.shopbyuber.model.UserModel;
import com.shopbyuber.user.AdapterCategories;
import com.shopbyuber.user.AdapterSearchTheEZListPicks;
import com.shopbyuber.user.AdapterSubCategoryDialog;
import com.shopbyuber.user.GetPicksCompanyServiceActivity;
import com.shopbyuber.user.SelectedCategoryActivity;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.FusedLocationManager;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;

public class SearchActivity extends BaseActivity implements AdapterView.OnItemClickListener,
        SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.layout_search)
    LinearLayout layout_search;

    @BindView(R.id.auto_complete_search)
    AutoCompleteTextView auto_complete_search;

    @BindView(R.id.tv_no_record_found)
    TextView tv_no_record_found;

    @BindView(R.id.img_Logo)
    ImageView img_Logo;

    @BindView(R.id.img_clear)
    ImageView img_clear;

    Globals globals;
    UserModel userModel;

    @BindView(R.id.list)
    RecyclerView localListRecyclerView;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private LocalCategoriesModel localCategoriesModel;
    private ArrayList<LocalCategories> localCategories;
    private ArrayList<LocalCategoriesModel.CompanyListing> companyListingsArrayList;
    private AdapterCategories adapterLocalCategories;
    private AdapterSearchTheEZListPicks adapterShopByUberPicks;

    ImageView imgCategory;

    private String categoryId = "";
    LocalCategoriesModel subCategoriesModel;

    FusedLocationManager fusedLocationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        fusedLocationManager = new FusedLocationManager(this);
        globals = (Globals) getApplicationContext();
        setAutoCompleteSource();
        localCategories = new ArrayList<>();
        companyListingsArrayList = new ArrayList<>();
        auto_complete_search.addTextChangedListener(myTextWatcher);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorToolbarTitle);
        swipeRefreshLayout.setOnRefreshListener(this);
        userModel = globals.getUserData();

        img_Logo.setVisibility(View.GONE);
        layout_search.setVisibility(View.VISIBLE);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        //handle keyboard action
        auto_complete_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (doValidate()) {
                        doSearchCategoryRequest(true);
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void setAutoCompleteSource() {
        Set<String> stringSet = globals.getSearchHistory();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_list_item_1, stringSet.toArray(new String[stringSet.size()])) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView text = (TextView) view.findViewById(android.R.id.text1);
                text.setTextColor(Color.WHITE);
                return view;
            }
        };

        auto_complete_search.setAdapter(adapter);
    }

    @OnClick(R.id.img_clear)
    public void clearEdittext() {
        auto_complete_search.setText("");
    }

    TextWatcher myTextWatcher = new TextWatcher() {

        @Override
        public void afterTextChanged(Editable s) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.toString().equalsIgnoreCase("")) {
                img_clear.setVisibility(View.GONE);
                if (localCategories.isEmpty() && companyListingsArrayList.isEmpty()) {
                    tv_no_record_found.setText(getString(R.string.err_enter_search_category));
                    tv_no_record_found.setVisibility(View.VISIBLE);
                }
            } else {
                img_clear.setVisibility(View.VISIBLE);
                tv_no_record_found.setVisibility(View.GONE);
            }
        }
    };

    private boolean doValidate() {
        if (auto_complete_search.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_search_category);
            return false;
        }
        return true;
    }

    private void showNoRecordFound() {
        if (localCategories.isEmpty()) {
            if (tv_no_record_found.getVisibility() == View.GONE)
                tv_no_record_found.setText(getString(R.string.no_search_result_available));
            tv_no_record_found.setVisibility(View.VISIBLE);
        }
    }

    private void hideNoRecordFound() {
        if (tv_no_record_found.getVisibility() == View.VISIBLE)
            tv_no_record_found.setVisibility(View.GONE);
    }

    private void resetPage() {
        localCategories.clear();
        companyListingsArrayList.clear();
        if (adapterLocalCategories != null)
            adapterLocalCategories.notifyDataSetChanged();
        if (adapterShopByUberPicks != null)
            adapterShopByUberPicks.notifyDataSetChanged();
    }

    @OnClick(R.id.img_search)
    public void doSerachCategory() {
        if (doValidate()) {
            doSearchCategoryRequest(true);
        }
    }

    private void doSearchCategoryRequest(final boolean clearData) {
        if (!Globals.internetCheck(this)) {
            showNoRecordFound();
            return;
        }
        if (globals.getEnableHistory() && clearData && !swipeRefreshLayout.isRefreshing() &&
                !globals.getSearchHistory().contains(auto_complete_search.getText().toString().trim())) {
            Set<String> stringSet = globals.getSearchHistory();
            stringSet.add(auto_complete_search.getText().toString().trim());
            globals.setSearchHistory(stringSet);
            setAutoCompleteSource();
        }
        if (clearData)
            resetPage();
        LatLng latLng = fusedLocationManager.getLatLng();

        JSONObject params = HttpRequestHandler.getInstance().getSearchCategory(
                userModel.Data.userId,
                userModel.Data.sessionId,
                auto_complete_search.getText().toString().trim(),
                1,
                latLng.latitude, latLng.longitude);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(SearchActivity.this);
        HttpRequestHandler.getInstance().post(this, getString(R.string.getSearchData), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                if (clearData && !swipeRefreshLayout.isRefreshing())
                    dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                localCategoriesModel = new Gson().fromJson(response.toString(), LocalCategoriesModel.class);
                tv_no_record_found.setVisibility(View.GONE);
                //setup company or category list
                if (localCategoriesModel.Result && localCategoriesModel.Data != null) {
                    if (localCategoriesModel.Data.categories != null && !localCategoriesModel.Data.categories.isEmpty()) {
                        setupList(localCategoriesModel.Data.categories);
                    } else if (localCategoriesModel.Data.companyListing != null && !localCategoriesModel.Data.companyListing.isEmpty()) {
                        setupCompanyList(localCategoriesModel.Data.companyListing);
                    } else
                        showNoRecordFound();
                } else {
                    showNoRecordFound();
                    Toaster.shortToast(localCategoriesModel.Message);
                    if (localCategoriesModel.Message.contains(getString(R.string.invalid_session))) {
                        globals.doLogout(SearchActivity.this);
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                tv_no_record_found.setVisibility(View.GONE);
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                tv_no_record_found.setVisibility(View.GONE);
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void setupList(ArrayList<LocalCategories> localCategory) {
        if (adapterShopByUberPicks != null) {
            companyListingsArrayList.clear();
            adapterShopByUberPicks.notifyDataSetChanged();
            adapterShopByUberPicks = null;
            localListRecyclerView.setAdapter(null);
        }
        if (localCategory != null && !localCategory.isEmpty()) {
            localCategories.addAll(localCategory);
            setAdapter();
        }
    }

    private void setupCompanyList(ArrayList<LocalCategoriesModel.CompanyListing> companyListings) {
        if (adapterLocalCategories != null) {
            localCategories.clear();
            adapterLocalCategories.notifyDataSetChanged();
            adapterLocalCategories = null;
            localListRecyclerView.setAdapter(null);
        }
        if (companyListings != null && !companyListings.isEmpty()) {
            companyListingsArrayList.addAll(companyListings);
            setCompanyAdapter();
        }
    }

    private void setAdapter() {
        hideNoRecordFound();
        if (adapterLocalCategories == null) {
            adapterLocalCategories = new AdapterCategories(this, true);
            adapterLocalCategories.setOnItemClickListener(this);
        }
        adapterLocalCategories.doRefresh(localCategories);

        if (localListRecyclerView.getAdapter() == null) {
            localListRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            localListRecyclerView.setItemAnimator(new DefaultItemAnimator());
            localListRecyclerView.setAdapter(adapterLocalCategories);
        }
    }

    private void setCompanyAdapter() {
        hideNoRecordFound();
        if (adapterShopByUberPicks == null) {
            adapterShopByUberPicks = new AdapterSearchTheEZListPicks(this);
            adapterShopByUberPicks.setOnItemClickListener(this);
        }
        adapterShopByUberPicks.doRefresh(companyListingsArrayList);

        if (localListRecyclerView.getAdapter() == null) {
            localListRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            localListRecyclerView.setItemAnimator(new DefaultItemAnimator());
            localListRecyclerView.setAdapter(adapterShopByUberPicks);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //handle company or category click event
        if (localCategoriesModel.Data.categories != null
                && !localCategoriesModel.Data.categories.isEmpty()) {
            categoryId = localCategories.get(position)._id;
            dogetSubCategoriesRequest(true);
        } else if (localCategoriesModel.Data.companyListing != null
                && !localCategoriesModel.Data.companyListing.isEmpty()) {
            startActivity(new Intent(this, GetPicksCompanyServiceActivity.class)
                    .putExtra(Constants.SBU_CompanyId, companyListingsArrayList.get(position)._id)
                    .putExtra(Constants.SBU_CompanyName, companyListingsArrayList.get(position).comapnyScreenName));
        }

    }

    public void displaySelectedService(String id, String catName) {
        Intent displayCat = new Intent(this, SelectedCategoryActivity.class);
        displayCat.putExtra(Constants.SBU_SubCategoryId, id);
        displayCat.putExtra(Constants.SBU_CatName, catName);
        displayCat.putExtra(Constants.SBU_CategoryId, categoryId);
        displayCat.putExtra(Constants.SBU_GetVendorFrom, GetVendorsFrom.FROM_SEARCH);
        startActivity(displayCat);
    }

    @Override
    public void onRefresh() {
        doSearchCategoryRequest(true);
    }


    public void dogetSubCategoriesRequest(final boolean showProgress) {
        if (!Globals.internetCheck(this)) {
            showNoRecordFound();
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().doSubCategoryParam(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                categoryId, true);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(SearchActivity.this);
        HttpRequestHandler.getInstance().post(this, getString(R.string.getSubSubcategories), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                if (!swipeRefreshLayout.isRefreshing() && showProgress)
                    dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                subCategoriesModel = new Gson().fromJson(response.toString(), LocalCategoriesModel.class);
                if (subCategoriesModel.Result) {
                    showSubCatagoryDialog();
                } else {
                    Toaster.shortToast(subCategoriesModel.Message);
                    if (subCategoriesModel.Message.contains(getString(R.string.invalid_session))) {
                        globals.doLogout(SearchActivity.this);
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }


    public void showSubCatagoryDialog() {
        final Dialog dialog = new Dialog(SearchActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog_layout);
        dialog.setCancelable(true);

        TextView tv_dg_main_title = dialog.findViewById(R.id.tv_dg_main_title);
        tv_dg_main_title.setText(getString(R.string.select_a_sub_category));
        ListView listview = dialog.findViewById(R.id.listview);
        TextView tv_no_record_found = dialog.findViewById(R.id.tv_no_record_found);
        imgCategory = dialog.findViewById(R.id.img_category);

        if (subCategoriesModel != null && !subCategoriesModel.Data.subCategories.isEmpty()) {

            subCategoriesModel.Data.subCategories.get(0).isAllocate = true;

            final AdapterSubCategoryDialog adapterSubCategoryDialog = new AdapterSubCategoryDialog(subCategoriesModel.Data.subCategories, SearchActivity.this);
            listview.setAdapter(adapterSubCategoryDialog);
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    for (int i = 0; i < subCategoriesModel.Data.subCategories.size(); i++) {
                        LocalCategoriesModel.SubCategories subCategories = subCategoriesModel.Data.subCategories.get(i);
                        subCategories.isAllocate = i == position;
                    }

                    adapterSubCategoryDialog.notifyDataSetChanged();
                }
            });
        } else {
            listview.setVisibility(View.GONE);
            tv_no_record_found.setVisibility(View.VISIBLE);

        }
        dialog.show();

        Button btn_submit = dialog.findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (localCategoriesModel != null)
                    displaySelectedCategory(getSelectedSubCategory());
                dialog.dismiss();
            }
        });
    }

    private void displaySelectedCategory(LocalCategoriesModel.SubCategories subCategories) {
        displaySelectedService(subCategories._id, subCategories.subcatName);
    }

    private LocalCategoriesModel.SubCategories getSelectedSubCategory() {
        for (int i = 0; i < subCategoriesModel.Data.subCategories.size(); i++) {
            LocalCategoriesModel.SubCategories categories = subCategoriesModel.Data.subCategories.get(i);
            if (categories.isAllocate)
                return categories;
        }
        return null;
    }
}
