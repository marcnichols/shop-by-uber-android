package com.shopbyuber.main;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.model.NotificationSettingsModel.Notification;
import com.shopbyuber.model.NotificationSettingsModel.NotificationSection;
import com.shopbyuber.utils.SectionedRecyclerViewAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


class NotificationSettingsAdapter extends SectionedRecyclerViewAdapter<RecyclerView.ViewHolder> {
    private ArrayList<NotificationSection> dataSet;

    NotificationSettingsAdapter(ArrayList<NotificationSection> data) {
        this.dataSet = data;
    }

    @Override
    public int getSectionCount() {
        return dataSet.size();
    }

    @Override
    public int getItemCount(int section) {
        return dataSet.get(section).notifications.size();
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int section) {
        SectionViewHolder sectionViewHolder = (SectionViewHolder) holder;
        if (dataSet.get(section).section == null || dataSet.get(section).section.equalsIgnoreCase(""))
            sectionViewHolder.sectionTitle.setVisibility(View.GONE);
        else {
            sectionViewHolder.sectionTitle.setVisibility(View.VISIBLE);
            sectionViewHolder.sectionTitle.setText(dataSet.get(section).section);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int section, final int relativePosition, int absolutePosition) {
        final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        itemViewHolder.itemTitle.setText(dataSet.get(section).notifications.get(relativePosition).notifyLabel);
        itemViewHolder.rb_status.setChecked(dataSet.get(section).notifications.get(relativePosition).isEnabled);
        itemViewHolder.rel_enable_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Notification notification = dataSet.get(section).notifications.get(relativePosition);
                notification.isEnabled = !itemViewHolder.rb_status.isChecked();
                notifyDataSetChanged();
                NotificationSettings.getInstance().doSetSettingsRequest(notification.notifyId, notification.isEnabled);
            }
        });
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, boolean header) {
        View v;
        if (header) {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_section, parent, false);
            return new SectionViewHolder(v);
        } else {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_settings, parent, false);
            return new ItemViewHolder(v);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType, boolean header) {
        return null;
    }

    // SectionViewHolder Class for Sections
    static class SectionViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.sectionTitle)
        TextView sectionTitle;

        SectionViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    // ItemViewHolder Class for Items in each Section
    static class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.itemTitle)
        TextView itemTitle;
        @BindView(R.id.rb_status)
        RadioButton rb_status;
        @BindView(R.id.rel_enable_history)
        RelativeLayout rel_enable_history;
        @BindView(R.id.view_divider)
        View view_divider;

        ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Logger.d(itemTitle.getText().toString());
                }
            });
        }
    }
}