package com.shopbyuber.main;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.UserRole;
import com.shopbyuber.login.LoginActivity;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.model.UserModel;
import com.shopbyuber.seller.SellerLandingActivity;
import com.shopbyuber.user.UserLandingActivity;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

public class SplashActivity extends BaseActivity {
    Globals globals;
    UserModel userModel;

    @BindView(R.id.progress)
    MaterialProgressBar progress;
    int reqCode = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        globals = (Globals) getApplicationContext();
        userModel = globals.getUserData();

        if (userModel != null) {
            doValidateSessionRequest();
        } else
            redirect();
    }

    private void doValidateSessionRequest() {
        if (!Globals.internetCheck(SplashActivity.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getSessionValidateParams(
                userModel.Data.userId, userModel.Data.sessionId);

        HttpRequestHandler.getInstance().post(SplashActivity.this, getString(R.string.validateSession), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel.Result) {
                    if (dataModel.Data.checkUpdate) dogetPackageInfo(dataModel.Data.version);
                    else redirect();
                } else {
                    Toaster.shortToast(dataModel.Message);
                    clearDataNredirect();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                clearDataNredirect();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                clearDataNredirect();
            }
        });
    }

    private void dogetPackageInfo(double dataModelVersion) {
        PackageManager manager = getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(
                    getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        double version = Double.parseDouble(info.versionName);
        if (dataModelVersion > version) {
            doDisplayUpdateDialog();
        } else redirect();
    }

    private void doDisplayUpdateDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(getString(R.string.update_available));
        alertDialog.setMessage(getString(R.string.new_version_of_app_available));
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getString(R.string.update), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                final String appPackageName = getPackageName();
                try {
                    startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)), reqCode);
                } catch (android.content.ActivityNotFoundException e) {
                    startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)), reqCode);
                }
            }
        });
        alertDialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
                redirect();
            }
        });
        alertDialog.show();
    }

    private void clearDataNredirect() {
        globals.setUserData(null);
        userModel = globals.getUserData();
        redirect();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        redirect();
    }

    private void redirect() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // get user data from prefrence
                Intent intent = null;
                if (globals.ifCommercialViewed()) {
                    if (userModel == null)
                        intent = new Intent(SplashActivity.this, LoginActivity.class);
                    else {
                        switch (UserRole.getEnum(userModel.Data.userRole - 1)) {
                            case BUYER:
                                intent = new Intent(SplashActivity.this, UserLandingActivity.class);
                                break;
                            case SELLER:
                                if (!userModel.Data.isCompanyProfileCreated) {
                                    globals.setUserData(null);
                                    intent = new Intent(SplashActivity.this, LoginActivity.class);
                                } else
                                    intent = new Intent(SplashActivity.this, SellerLandingActivity.class);
                                break;
                        }
                    }
                } else {
                    intent = new Intent(SplashActivity.this, VideoCommercialActivity.class);
                }
                startActivity(intent);
                finish();
            }
        }, 1000);
    }
}
