package com.shopbyuber.enums;


public enum ServiceStatus {

    Requested(0),
    Confirmed(1),
    PayDone(2),
    OnRouteToClient(3),
    StartService(4),
    Completed(5),
    Cancelled(6);

    int service;

    ServiceStatus(int service) {
        this.service = service;
    }

    public int getService() {
        return service;
    }

    public static ServiceStatus getEnum(int ordinal) {
        return ServiceStatus.values()[ordinal];
    }
}
