package com.shopbyuber.enums;


public enum NotificationTypeSeller {
    Messaged_Receive(0),
    Play_sound_on_notification(1),
    Vibrate_on_notification(2),
    Pulse_on_notification(3),
    User_has_made_request(4),
    User_has_made_payment_for_confirmed_service(5),
    User_has_canceled_request_for_confirmed_service(6),
    New_app_features_are_available(7),
    You_have_new_buyer_activity(8),
    Service_completed(9),
    User_has_request_wanted_service(10);

    int type;

    NotificationTypeSeller(int type) {
        this.type = type;
    }

    public int getNotificationType() {
        return type;
    }

    public static NotificationTypeSeller fromInt(int i) {
        for (NotificationTypeSeller b : NotificationTypeSeller.values()) {
            if (b.getNotificationType() == i) {
                return b;
            }
        }
        return null;
    }
}
