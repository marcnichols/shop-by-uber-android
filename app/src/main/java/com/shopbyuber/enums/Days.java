package com.shopbyuber.enums;

import java.util.ArrayList;
import java.util.Calendar;


public class Days {

    private static ArrayList<Days> daysArrayList;
    private String dayName;
    private String shortDayName;
    private int dayID;
    private int calDayID;

    private Days() {
        daysArrayList = new ArrayList<>();
        daysArrayList.add(new Days(0, "Monday", "Mo", Calendar.MONDAY));
        daysArrayList.add(new Days(1, "Tuesday", "Tu", Calendar.TUESDAY));
        daysArrayList.add(new Days(2, "Wednesday", "We", Calendar.WEDNESDAY));
        daysArrayList.add(new Days(3, "Thursday", "Th", Calendar.THURSDAY));
        daysArrayList.add(new Days(4, "Friday", "Fr", Calendar.FRIDAY));
        daysArrayList.add(new Days(5, "Saturday", "Sa", Calendar.SATURDAY));
        daysArrayList.add(new Days(6, "Sunday", "Su", Calendar.SUNDAY));
    }

    public static String getDay(int id) {
        if (daysArrayList == null)
            new Days();
        for (Days days : Days.daysArrayList) {
            if (days.dayID == id) {
                return days.dayName;
            }
        }
        return "";
    }

    public static String getShortDay(int id) {
        if (daysArrayList == null)
            new Days();
        for (Days days : Days.daysArrayList) {
            if (days.dayID == id) {
                return days.shortDayName;
            }
        }
        return "";
    }

    public static int getCalendarDay(int id) {
        if (daysArrayList == null)
            new Days();
        for (Days days : Days.daysArrayList) {
            if (days.dayID == id) {
                return days.calDayID;
            }
        }
        return -1;
    }

    private Days(int dayID, String dayName, String shortDayName, int calDay) {
        this.dayID = dayID;
        this.dayName = dayName;
        this.shortDayName = shortDayName;
        this.calDayID = calDay;
    }
}
