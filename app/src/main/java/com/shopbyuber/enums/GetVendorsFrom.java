package com.shopbyuber.enums;


public enum GetVendorsFrom {
    FROM_LOCAL(1),
    FROM_CATEGORIES(2),
    FROM_SEARCH(3);

    int from;

    GetVendorsFrom(int from) {
        this.from = from;
    }
}
