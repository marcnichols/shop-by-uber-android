package com.shopbyuber.enums;


public enum ImageType {
    USER_PROFILE(1),
    COMPANY_PROFILE(2),
    SERVICE_IMAGE(3);

    int type;

    ImageType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
