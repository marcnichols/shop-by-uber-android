package com.shopbyuber.enums;

public enum ServiceSpinnerType {
    DURATION(1),
    DAYS(2),
    TIMES(3),
    CATEGORY(4),
    DAY_SELECTION(5),
    SUB_CATEGORY(6);
    int type;

    ServiceSpinnerType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
