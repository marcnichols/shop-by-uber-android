package com.shopbyuber.enums;


public enum NotificationTypeBuyer {
    Messaged_Receive(0),
    Play_sound_on_notification(1),
    Vibrate_on_notification(2),
    Pulse_on_notification(3),
    Vendor_has_confirmed_your_request(4),
    Vendor_is_on_route_to_your_location(5),
    Vendor_has_arrived_and_your_service_has_been_processed(6),
    Service_has_been_completed(7),
    Vendor_has_cancelled_request(8),
    I_can_review_my_recent_purchases(9),
    New_app_features_are_available(10),
    There_are_items_left_in_my_cart(11),
    New_updates_in_my_favourite_service(12),
    User_has_made_request(13),
    Service_completed(14),
    Vendor_create_wanted_service(15);

    int type;

    NotificationTypeBuyer(int type) {
        this.type = type;
    }

    public int getNotificationType() {
        return type;
    }

    public static NotificationTypeBuyer fromInt(int i) {
        for (NotificationTypeBuyer b : NotificationTypeBuyer.values()) {
            if (b.getNotificationType() == i) {
                return b;
            }
        }
        return null;
    }
}
