package com.shopbyuber.enums;


public enum UserRole {
    BUYER(1),
    SELLER(2);
    int role;

    UserRole(int role) {
        this.role = role;
    }

    public int getRole() {
        return role;
    }

    public static UserRole getEnum(int ordinal) {
        return UserRole.values()[ordinal];
    }
}
