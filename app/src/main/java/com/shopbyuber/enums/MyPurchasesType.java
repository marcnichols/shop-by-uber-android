package com.shopbyuber.enums;

public enum MyPurchasesType {

    Requested(0),
    Confirmed(1),
    NotStarted(2),
    OnRoute(3),
    InProgress(4),
    Completed(5),
    Cancelled(6);

    int purchaseType;

    MyPurchasesType(int purchaseType) {
        this.purchaseType = purchaseType;
    }

    public int getPurchaseType() {
        return purchaseType;
    }

    public static MyPurchasesType getEnum(int ordinal) {
        return MyPurchasesType.values()[ordinal];
    }

    public static MyPurchasesType fromInt(int i) {
        for (MyPurchasesType b : MyPurchasesType.values()) {
            if (b.getPurchaseType() == i) {
                return b;
            }
        }
        return null;
    }
}
