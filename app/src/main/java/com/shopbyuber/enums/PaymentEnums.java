package com.shopbyuber.enums;


public enum  PaymentEnums {

    SELECT_PAYMENT_TYPE(0, "Select payment options"),
    PAYPAL(1, "Paypal"),
    BANK_ACCOUNT(2, "Bank Account (ACH/EFT)"),
    PAY_BY_CHEQUE(3, "Pay by Check");

    private String PaymentType;
    private int id;

    PaymentEnums(int id, String PaymentType) {
        this.id = id;
        this.PaymentType = PaymentType;
    }

    @Override
    public String toString() {
        return PaymentType;
    }

    public int getId() {
        return id;
    }

    public static PaymentEnums fromInt(int i) {
        for (PaymentEnums b : PaymentEnums.values()) {
            if (b.getId() == i) {
                return b;
            }
        }
        return null;

    }
}
