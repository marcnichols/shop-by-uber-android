package com.shopbyuber.enums;


public enum AddServiceFrom {
    FROM_REGISTER(1),
    FROM_MY_SERVICE(2),
    FROM_EDIT_SERVICE(3);

    int from;

    AddServiceFrom(int from) {
        this.from = from;
    }
}
