package com.shopbyuber.forgotpassword;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.AddServiceFrom;
import com.shopbyuber.enums.UserRole;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.model.UserModel;
import com.shopbyuber.profile.AddNewService;
import com.shopbyuber.profile.CreateProfileActivity;
import com.shopbyuber.seller.SellerLandingActivity;
import com.shopbyuber.user.UserLandingActivity;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;

public class ResetNewPasswordActivity extends BaseActivity {
    Globals globals;

    @BindView(R.id.edt_new_password)
    EditText edt_new_password;

    @BindView(R.id.edt_confirm_password)
    EditText edt_confirm_password;

    String UserId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_new_password);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        globals = (Globals) getApplicationContext();
        Bundle bundle = getIntent().getExtras();
        if (bundle == null)
            return;
        UserId = bundle.getString(Constants.SBU_UserId);
    }

    @OnClick(R.id.btn_reset)
    public void doResetRequest() {
        if (doValidation()) {
            doResetPasswordRequest();
        }
    }

    public void doResetPasswordRequest() {
        if (!Globals.internetCheck(ResetNewPasswordActivity.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getResetPasswordParams(
                UserId,
                edt_new_password.getText().toString().trim(),
                globals.getFCM_DeviceToken());
        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(ResetNewPasswordActivity.this);

        HttpRequestHandler.getInstance().post(ResetNewPasswordActivity.this, getString(R.string.resetPassword), params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                UserModel userModel = new Gson().fromJson(response.toString(), UserModel.class);
                if (userModel.Result) {
                    gotoHomeScreen(userModel);
                } else {
                    Toaster.shortToast(userModel.Message);
                    if (userModel.Message.equals(getString(R.string.invalid_session))) {
                        globals.doLogout(ResetNewPasswordActivity.this);
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void gotoHomeScreen(UserModel userModel) {
        globals.setUserData(userModel);
        Intent intent;
        if (userModel.Data.userRole == UserRole.SELLER.getRole() && !userModel.Data.isCompanyProfileCreated) {
            // if company profile is not created for seller
            intent = new Intent(ResetNewPasswordActivity.this, CreateProfileActivity.class);
            intent.putExtra(Constants.SBU_OwnerName, userModel.Data.name);
            intent.putExtra(Constants.SBU_CompanyName, userModel.Data.legalBusinessName);
            intent.putExtra(Constants.SBU_MobilePhone, userModel.Data.phoneNumber);
            intent.putExtra(Constants.SBU_CountryCode, userModel.Data.countryCode);
        } else if (userModel.Data.userRole == UserRole.BUYER.getRole()) {
            intent = new Intent(ResetNewPasswordActivity.this, UserLandingActivity.class);
        } else if (userModel.Data.userRole == UserRole.SELLER.getRole() && userModel.Data.isServicesCreated) {
            intent = new Intent(this, SellerLandingActivity.class);
        } else {
            gotoAddNewServie();
            intent = new Intent(this, AddNewService.class).putExtra(Constants.SBU_Add_service_from, AddServiceFrom.FROM_MY_SERVICE);
        }
        startActivity(intent);
        finishAffinity();

    }


    private void gotoAddNewServie() {
        UserModel userModel = globals.getUserData();
        userModel.Data.isCompanyProfileCreated = true;
        globals.setUserData(userModel);
    }

    public boolean doValidation() {
        if (edt_new_password.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_password);
            return false;
        }
        if (edt_confirm_password.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_confirm_password);
            return false;
        }
        if (!edt_new_password.getText().toString().trim().equalsIgnoreCase(edt_confirm_password.getText().toString().trim())) {
            Toaster.shortToast(R.string.err_confirm_password_should_be_same_as_password);
            return false;
        }
        return true;
    }
}
