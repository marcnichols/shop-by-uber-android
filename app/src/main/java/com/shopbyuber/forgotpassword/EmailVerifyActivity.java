package com.shopbyuber.forgotpassword;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;

public class EmailVerifyActivity extends BaseActivity {
    Globals globals;

    @BindView(R.id.edt_email)
    EditText edt_email;

    @BindView(R.id.btn_verify)
    Button btn_verify;

    @BindView(R.id.activity_email_verify)
    LinearLayout contentView;

    @BindView(R.id.lnr_send_again)
    LinearLayout lnr_send_again;

    @BindView(R.id.view_send_again)
    View view_send_again;

    String UserId = "";
    String email = "";
    boolean isCodeSent = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_verify);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        globals = (Globals) getApplicationContext();
    }

    @OnClick(R.id.btn_verify)
    public void onVerify() {
        if (doValidation(false)) {
            if (isCodeSent)
                doCodeVerificationRequest();
            else
                doEmailVerificationRequest();
        }
    }

    @OnClick(R.id.lnr_send_again)
    public void onSendAgain() {
        if (isCodeSent)
            doEmailVerificationRequest();
    }

    public void doEmailVerificationRequest() {
        if (!Globals.internetCheck(EmailVerifyActivity.this))
            return;

        if (!isCodeSent)
            email = edt_email.getText().toString().trim();
        JSONObject params = HttpRequestHandler.getInstance().getVerifyEmailParams(
                email);
        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(EmailVerifyActivity.this);

        HttpRequestHandler.getInstance().post(EmailVerifyActivity.this, getString(R.string.recoverPassword), params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                Toaster.shortToast(dataModel.Message);
                if (dataModel.Result) {
                    isCodeSent = true;
                    UserId = dataModel.Data.userId;
                    setUpUIforVerifyCode();
                } else if (dataModel.Message.equals(getString(R.string.invalid_session)))
                    globals.doLogout(EmailVerifyActivity.this);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    public void doCodeVerificationRequest() {
        if (!Globals.internetCheck(EmailVerifyActivity.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getVerifyCodeParams(
                UserId,
                Integer.parseInt(edt_email.getText().toString().trim()));
        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(EmailVerifyActivity.this);

        HttpRequestHandler.getInstance().post(EmailVerifyActivity.this, getString(R.string.recoverPasswordVerifyCode), params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel model = new Gson().fromJson(response.toString(), DataModel.class);
                Toaster.shortToast(model.Message);
                if (model.Result) {
                    gotoResetPassword(UserId);
                } else {
                    if (model.Message.equals(getString(R.string.invalid_session))) {
                        globals.doLogout(EmailVerifyActivity.this);
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void setUpUIforVerifyCode() {
        edt_email.setHint(R.string.enter_verification_code);
        edt_email.setText("");
        edt_email.setInputType(InputType.TYPE_CLASS_NUMBER |
                InputType.TYPE_NUMBER_FLAG_DECIMAL |
                InputType.TYPE_NUMBER_FLAG_SIGNED);
        edt_email.setFilters(new InputFilter[]{new InputFilter.LengthFilter(4)});
        btn_verify.setText(R.string.verify);
        super.hideUnhide(contentView, lnr_send_again);
        view_send_again.setVisibility(View.VISIBLE);
        lnr_send_again.setVisibility(View.VISIBLE);
    }

    private void gotoResetPassword(String UserId) {
        startActivity(new Intent(this, ResetNewPasswordActivity.class)
                .putExtra(Constants.SBU_UserId, UserId));
    }

    public boolean doValidation(boolean fromSenAgain) {
        if (!isCodeSent) {
            if (edt_email.getText().toString().trim().isEmpty()) {
                Toaster.shortToast(R.string.err_enter_email);
                return false;
            }
            if (!Globals.validateEmailAddress(edt_email.getText().toString().trim())) {
                Toaster.shortToast(R.string.err_enter_valid_email);
                return false;
            }
        } else {
            if (!fromSenAgain && edt_email.getText().toString().trim().isEmpty()) {
                Toaster.shortToast(R.string.enter_verification_code);
                return false;
            }
        }
        return true;
    }
}
