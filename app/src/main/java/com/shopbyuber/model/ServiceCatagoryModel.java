package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class ServiceCatagoryModel implements Serializable {
    @SerializedName("Data")
    public Data Data;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Result")
    public boolean Result;

    public static class Data implements Serializable {
        @SerializedName("Categories")
        public ArrayList<Categories> categories;

        @SerializedName("subCategories")
        public List<SubCategories> subCategories;

        @SerializedName("isServiceCreated")
        public boolean isServiceCreated;


    }

    public static class Categories implements Serializable {
        public boolean isAllocate;

        @SerializedName("_id")
        public String _id;
        @SerializedName("catName")
        public String catName;

        @Override
        public String toString() {
            return catName;
        }
    }


    public static class SubCategories {
        public boolean isAllocate;

        @SerializedName("_id")
        public String _id;
        @SerializedName("subcatName")
        public String subcatName;
        @SerializedName("subCatImage")
        public String subCatImage;
    }

}
