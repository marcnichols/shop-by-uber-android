package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class PayPalResponseModel implements Serializable{

    @SerializedName("client")
    public Client client;
    @SerializedName("response")
    public Response response;
    @SerializedName("response_type")
    public String response_type;

    public static class Client implements Serializable{
        @SerializedName("environment")
        public String environment;
        @SerializedName("paypal_sdk_version")
        public String paypal_sdk_version;
        @SerializedName("platform")
        public String platform;
        @SerializedName("product_name")
        public String product_name;
    }

    public static class Response implements Serializable{
        @SerializedName("create_time")
        public String create_time;
        @SerializedName("id")
        public String id;
        @SerializedName("intent")
        public String intent;
        @SerializedName("state")
        public String state;
    }
}
