package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class SalesListModel implements Serializable {

    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class Sales implements Serializable {
        @SerializedName("requestId")
        public String requestId;
        @SerializedName("screenName")
        public String screenName;
        @SerializedName("status")
        public int status;
        @SerializedName("serviceImage")
        public String serviceImage;
        @SerializedName("duration")
        public String duration;
        @SerializedName("flatFee")
        public String flatFee;
        @SerializedName("bookedBy")
        public String bookedBy;
    }

    public static class Data implements Serializable {
        @SerializedName("sales")
        public List<Sales> sales;
        @SerializedName("recentServices")
        public List<Sales> recentServices;
        @SerializedName("recordCount")
        public int recordCount;
    }
}
