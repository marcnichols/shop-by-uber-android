package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class MyServicesModel implements Serializable {
    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class Data implements Serializable {
        @SerializedName("services")
        public ArrayList<services> services;
        @SerializedName("recordCount")
        public int recordCount;
    }

    public static class services implements Serializable {
        @SerializedName("serviceId")
        public String serviceId;
        @SerializedName("screenName")
        public String screenName;
        @SerializedName("flatFee")
        public String flatFee;
        @SerializedName("isAvailable")
        public boolean isAvailable;
        @SerializedName("serviceImage")
        public String serviceImage;
        @SerializedName("editAble")
        public boolean editAble;
    }
}
