package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class RateReviewModel implements Serializable {

    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class Data implements Serializable{
        @SerializedName("companyId")
        public String companyId;
        @SerializedName("vendorId")
        public String vendorId;
        @SerializedName("name")
        public String name;
        @SerializedName("profileImage")
        public String profileImage;
        @SerializedName("rating")
        public float rating;
        @SerializedName("feedback")
        public String feedback;
        @SerializedName("requestId")
        public String requestId;
        @SerializedName("serviceId")
        public String serviceId;
    }
}
