package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class OrderDetailModel implements Serializable {

    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class Data implements Serializable {
        @SerializedName("isFeedbackGiven")
        public boolean isFeedbackGiven;
        @SerializedName("requestId")
        public String requestId;
        @SerializedName("serviceId")
        public String serviceId;
        @SerializedName("companyId")
        public String companyId;
        @SerializedName("screenName")
        public String screenName;
        @SerializedName("serviceImage")
        public String serviceImage;
        @SerializedName("status")
        public int status;
        @SerializedName("pubChannelName")
        public String pubChannelName;
        @SerializedName("paymentMethod")
        public String paymentMethod;
        @SerializedName("amountPaid")
        public String amountPaid;
        @SerializedName("serviceProvider")
        public String serviceProvider;
        @SerializedName("serviceProviderImage")
        public String serviceProviderImage;
        @SerializedName("userLatitude")
        public double userLatitude;
        @SerializedName("userLongitude")
        public double userLongitude;
        @SerializedName("catName")
        public String catName;
        @SerializedName("subCatName")
        public String subCatName;
        @SerializedName("schedule")
        public String schedule;
        @SerializedName("isCancelled")
        public boolean isCancelled;

        @SerializedName("isRefund")
        public boolean isRefund;

        @SerializedName("cancelReason")
        public String cancelReason;

        @SerializedName("serviceDetail")
        public String serviceDetail;

        @SerializedName("cancelByText")
        public String cancelByText;
    }
}
