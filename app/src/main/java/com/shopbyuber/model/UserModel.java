package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class UserModel implements Serializable {

    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class Settings implements Serializable {
        @SerializedName("enableHistory")
        public boolean enableHistory;
        @SerializedName("currency")
        public String currency;
    }


    public static class Data implements Serializable {
        @SerializedName("name")
        public String name;
        @SerializedName("speciality")
        public String speciality;
        @SerializedName("description")
        public String description;
        @SerializedName("mobilePhone")
        public String mobilePhone;
        @SerializedName("phoneNumber")
        public String phoneNumber;
        @SerializedName("countryCode")
        public String countryCode;
        @SerializedName("email")
        public String email;
        @SerializedName("userRole")
        public int userRole;
        @SerializedName("deviceToken")
        public String deviceToken;
        @SerializedName("settings")
        public Settings settings;
        @SerializedName("profileImage")
        public String profileImage;
        @SerializedName("sessionId")
        public String sessionId;
        @SerializedName("userId")
        public String userId;
        @SerializedName("number_verified")
        public boolean number_verified;
        @SerializedName("verficationCode")
        public String verficationCode;
        @SerializedName("is_new_user")
        public boolean is_new_user;
        @SerializedName("isCompanyProfileCreated")
        public boolean isCompanyProfileCreated;
        @SerializedName("appUdate")
        public boolean appUdate;
        @SerializedName("legalBusinessName")
        public String legalBusinessName;
        @SerializedName("ezListScreenName")
        public String ezListScreenName;
        @SerializedName("address")
        public String address;
        @SerializedName("latitude")
        public double latitude;
        @SerializedName("longitude")
        public double longitude;
        @SerializedName("firstName")
        public String firstName;
        @SerializedName("lastName")
        public String lastName;
        @SerializedName("promoCode")
        public String promoCode;
        @SerializedName("city")
        public String city;
        @SerializedName("state")
        public String state;
        @SerializedName("isSalesperson")
        public boolean isSalesperson;
        @SerializedName("quickBloxId")
        public int quickBloxId;
        @SerializedName("isServicesCreated")
        public boolean isServicesCreated;
    }
}
