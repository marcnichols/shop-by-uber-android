package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class DataModel implements Serializable {

    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;


    public static class Data implements Serializable {
        @SerializedName("verficationCode")
        public String verficationCode;
        @SerializedName("fileName")
        public String fileName;
        @SerializedName("companyId")
        public String companyId;
        @SerializedName("serviceId")
        public String serviceId;
        @SerializedName("userId")
        public String userId;
        @SerializedName("userUnreadNotificationCount")
        public int userUnreadNotificationCount;
        @SerializedName("latitude")
        public double latitude;
        @SerializedName("longitude")
        public double longitude;
        @SerializedName("pubChannelName")
        public String pubChannelName;

        @SerializedName("redirectUrl")
        public String redirectUrl;
        @SerializedName("PayKey")
        public String PayKey;
        @SerializedName("checkUpdate")
        public boolean checkUpdate;
        @SerializedName("version")
        public double version;

        @SerializedName("status")
        public int status;
    }

}


