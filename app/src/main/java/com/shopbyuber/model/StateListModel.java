package com.shopbyuber.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class StateListModel implements Serializable{

    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class States implements Serializable{
        @SerializedName("_id")
        public String _id;
        @SerializedName("name")
        public String name;

        @Override
        public String toString() {
            return this.name;            // What to display in the Spinner list.
        }
    }

    public static class Cities implements Serializable{
        @SerializedName("_id")
        public String _id;
        @SerializedName("city")
        public String city;

        @Override
        public String toString() {
            return this.city;            // What to display in the Spinner list.
        }
    }

    public static class Data implements Serializable{
        @SerializedName("States")
        public List<States> States;

        @SerializedName("Cities")
        public List<Cities> Cities;
    }


}
