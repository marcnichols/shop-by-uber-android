package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class ServiceOccupiedDataModel implements Serializable {
    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public int month;
    public int year;

    public static class Data implements Serializable {
        @SerializedName("timeslot")
        public ArrayList<timeslots> timeslots;
        @SerializedName("availableDays")
        public ArrayList<availableDays> availableDays;
        @SerializedName("allocatedDates")
        public ArrayList<allocatedDates> allocatedDates;
        @SerializedName("serviceDetail")
        public serviceDetail serviceDetail;
        @SerializedName("allocate")
        public ArrayList<timeslots> allocate;

        @SerializedName("paypalEmail")
        public String paypalEmail;
        @SerializedName("paymentMethod")
        public int paymentMethod;
        @SerializedName("paymentDetail")
        public PaymentDetail paymentDetail;
    }

    public static class timeslots implements Serializable {
        public boolean isSelected;

        @SerializedName("duration")
        public int duration;
        @SerializedName("timeslot")
        public ArrayList<slotes> slotes;
    }

    public static class allocatedDates implements Serializable {
        public int intdate = -1;
        public int month = -1;
        public int year = -1;

        @SerializedName("date")
        public String date;
        @SerializedName("timeslot")
        public ArrayList<slotes> slotes = new ArrayList<>();
    }

    public static class slotes implements Serializable {
        @SerializedName("time")
        public String time;
        @SerializedName("allocated")
        public boolean isAllocate;
    }

    public static class availableDays implements Serializable {
        @SerializedName("day")
        public int day;
        @SerializedName("isAllocate")
        public boolean isAllocate;
    }

    public static class serviceDetail implements Serializable {
        @SerializedName("availableDay")
        public ArrayList<Integer> availableDay;

        @SerializedName("appointmentDuration")
        public int appointmentDuration;
        @SerializedName("categoryId")
        public String categoryId;

        @SerializedName("subCategoryId")
        public String subCategoryId;

        @SerializedName("screenName")
        public String screenName;
        @SerializedName("availableTimes")
        public ArrayList<String> availableTimes;

        public ArrayList<availableSchedule> availableSchedules;

        @SerializedName("flatFee")
        public String flatFee;

        @SerializedName("availableYear")
        public ArrayList<Integer> availableYear;
        @SerializedName("availableMonth")
        public ArrayList<Integer> availableMonth;

        @SerializedName("serviceDetail")
        public String serviceDetail;
        @SerializedName("companyId")
        public String companyId;
        @SerializedName("serviceImage")
        public String serviceImage;
        @SerializedName("dayScheduling")
        public ArrayList<allocatedDates> dayScheduling;
        @SerializedName("profileImage")
        public String profileImage;

        @SerializedName("keywords")
        public String keywords;
    }

    public static class availableSchedule implements Serializable {
        @SerializedName("month")
        public int month;
        @SerializedName("Year")
        public int Year;
        @SerializedName("duration")
        public int duration;
        @SerializedName("day")
        public ArrayList<Integer> day = new ArrayList<>();
        @SerializedName("timings")
        public ArrayList<String> timings = new ArrayList<>();
    }

    public static class preAssignedDay implements Serializable {
        public int month;
        public int Year;
        public ArrayList<Integer> day = new ArrayList<>();
    }

    public static class PaymentDetail implements Serializable {
        @SerializedName("firstName")
        public String firstName;
        @SerializedName("lastName")
        public String lastName;
        @SerializedName("streetAddress")
        public String streetAddress;
        @SerializedName("city")
        public String city;
        @SerializedName("state")
        public String state;
        @SerializedName("zipCode")
        public String zipcode;
        @SerializedName("routingNumber")
        public String routingNumber;
        @SerializedName("accountNumber")
        public String accountNumber;
    }
}
