package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class ShopByUberPicksModel implements Serializable {

    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class CompanyListing implements Serializable {
        @SerializedName("_id")
        public String _id;
        @SerializedName("companyName")
        public String companyName;
        @SerializedName("comapnyScreenName")
        public String comapnyScreenName;
        @SerializedName("serviceDescription")
        public String serviceDescription;
        @SerializedName("companyProfileImage")
        public String companyProfileImage;
        @SerializedName("categories")
        public String categories;
        @SerializedName("latitude")
        public double latitude;
        @SerializedName("longitude")
        public double longitude;
    }

    public static class Data implements Serializable {
        @SerializedName("companyListing")
        public ArrayList<CompanyListing> companyListing;
        @SerializedName("recordCount")
        public int recordCount;
    }
}
