package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class ViewRequestDetailModel implements Serializable {


    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class Data implements Serializable {
        @SerializedName("requestId")
        public String requestId;
        @SerializedName("screenName")
        public String screenName;
        @SerializedName("serviceImage")
        public String serviceImage;
        @SerializedName("amountPaid")
        public String amountPaid;
        @SerializedName("refundMethod")
        public String refundMethod;
        @SerializedName("status")
        public int status;
        @SerializedName("requestFrom")
        public String requestFrom;
        @SerializedName("catName")
        public String catName;
        @SerializedName("subCatName")
        public String subCatName;
        @SerializedName("serviceProvider")
        public String serviceProvider;
        @SerializedName("serviceProviderImage")
        public String serviceProviderImage;
        @SerializedName("serviceDetail")
        public String serviceDetail;
        @SerializedName("companyAddress")
        public String companyAddress;
        @SerializedName("companyLatitude")
        public double companyLatitude;
        @SerializedName("companyLongitude")
        public double companyLongitude;
        @SerializedName("schedule")
        public String schedule;
        @SerializedName("isCancelled")
        public boolean isCancelled;

        @SerializedName("isRefund")
        public boolean isRefund;
        @SerializedName("userProfileImage")
        public String userProfileImage;
        @SerializedName("pubChannelName")
        public String pubChannelName;
        @SerializedName("userLatitude")
        public double userLatitude;
        @SerializedName("userLongitude")
        public double userLongitude;
        @SerializedName("userAddress")
        public String userAddress;

        @SerializedName("cancelReason")
        public String cancelReason;

        @SerializedName("receiverId")
        public String receiverId;

        @SerializedName("cancelByText")
        public String cancelByText;
    }
}
