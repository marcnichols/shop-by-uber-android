package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class MyRequestModel implements Serializable {

    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class Requests implements Serializable {
        @SerializedName("requestId")
        public String requestId;
        @SerializedName("screenName")
        public String screenName;
        @SerializedName("status")
        public int status;
        @SerializedName("serviceImage")
        public String serviceImage;
        @SerializedName("duration")
        public String duration;
    }

    public static class Data implements Serializable {
        @SerializedName("requests")
        public List<Requests> requests;
        @SerializedName("recordCount")
        public int recordCount;
    }
}
