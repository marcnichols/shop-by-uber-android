package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class CancelOrderModel implements Serializable {

    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class Details implements Serializable {
        @SerializedName("profileImage")
        public String profileImage;
        @SerializedName("name")
        public String name;
        @SerializedName("description")
        public String description;
        @SerializedName("speciality")
        public String speciality;
        @SerializedName("vendorId")
        public String vendorId;
    }

    public static class ConfirmServices implements Serializable {
        @SerializedName("flatFee")
        public String flatFee;
        @SerializedName("serviceId")
        public String serviceId;
        @SerializedName("screenName")
        public String screenName;
        @SerializedName("serviceImage")
        public String serviceImage;
        @SerializedName("duration")
        public String duration;
        @SerializedName("requestId")
        public String requestId;
        @SerializedName("timeSlotId")
        public String timeSlotId;
        @SerializedName("data")
        public Details details;
    }

    public static class Data implements Serializable {
        @SerializedName("confirmServices")
        public List<ConfirmServices> confirmServices;
    }
}
