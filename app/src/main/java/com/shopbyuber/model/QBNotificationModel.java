package com.shopbyuber.model;


import java.io.Serializable;

public class QBNotificationModel implements Serializable {
    public String dialogId;
    public String profileImage;
    public String userName;
    public String userId;
    public int quickBloxId = 0;
    public String requestId = "";
    public int unreadCount = 0;
    public boolean isChat = false;

    public QBNotificationModel() {
        
    }
}
