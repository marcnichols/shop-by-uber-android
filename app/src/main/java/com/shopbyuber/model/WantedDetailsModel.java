package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class WantedDetailsModel implements Serializable {
    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class Data implements Serializable {
        @SerializedName("requestId")
        public String requestId;
        @SerializedName("userId")
        public String userId;
        @SerializedName("serviceName")
        public String serviceName;
        @SerializedName("serviceDescription")
        public String serviceDescription;
        @SerializedName("categoryId")
        public String categoryId;
        @SerializedName("catName")
        public String catName;
        @SerializedName("image")
        public String image;
        @SerializedName("name")
        public String name;
        @SerializedName("subCatName")
        public String subCatName;
        @SerializedName("subCategoryId")
        public String subCategoryId;
        @SerializedName("cateImage")
        public String cateImage;
        @SerializedName("state")
        public String state;
        @SerializedName("city")
        public String city;
        @SerializedName("isServiceCreated")
        public boolean isServiceCreated;
    }
}
