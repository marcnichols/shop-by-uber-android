package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class WantedServiceModel implements Serializable {


    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class WantedList implements Serializable {
        @SerializedName("requestId")
        public String requestId;
        @SerializedName("userId")
        public String userId;
        @SerializedName("serviceName")
        public String serviceName;
        @SerializedName("serviceDescription")
        public String serviceDescription;
        @SerializedName("categoryId")
        public String categoryId;
        @SerializedName("catName")
        public String catName;
        @SerializedName("image")
        public String image;
        @SerializedName("isServiceCreated")
        public boolean isServiceCreated;
        @SerializedName("serviceAvailableNow")
        public String serviceAvailableNow;
    }

    public static class Data implements Serializable {
        @SerializedName("wantedList")
        public ArrayList<WantedList> wantedList;
        @SerializedName("recordCount")
        public int recordCount;
    }
}
