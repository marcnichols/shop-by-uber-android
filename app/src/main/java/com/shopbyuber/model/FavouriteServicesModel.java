package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class FavouriteServicesModel implements Serializable {


    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class Services implements Serializable {
        @SerializedName("screenName")
        public String screenName;
        @SerializedName("flatFee")
        public String flatFee;
        @SerializedName("serviceImage")
        public String serviceImage;
        @SerializedName("serviceId")
        public String serviceId;
        @SerializedName("companyId")
        public String companyId;
        @SerializedName("categoryId")
        public String categoryId;
        @SerializedName("subCategoryId")
        public String subCategoryId;
    }

    public static class Data implements Serializable {
        @SerializedName("services")
        public List<Services> services;
        @SerializedName("recordCount")
        public int recordCount;
    }
}
