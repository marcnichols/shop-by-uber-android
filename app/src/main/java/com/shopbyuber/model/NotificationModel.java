package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class NotificationModel implements Serializable {


    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class Notifications implements Serializable {
        @SerializedName("message")
        public String message;
        @SerializedName("isRead")
        public boolean isRead;
        @SerializedName("createdDatetime")
        public String createdDatetime;
        @SerializedName("notifyId")
        public int notifyId;
        @SerializedName("requestId")
        public String requestId;
        @SerializedName("serviceId")
        public String serviceId;
        @SerializedName("screenName")
        public String screenName;
        @SerializedName("serviceDetail")
        public String serviceDetail;
        @SerializedName("serviceImage")
        public String serviceImage;
        @SerializedName("notificationId")
        public String notificationId;
        @SerializedName("categoryId")
        public String categoryId;
        @SerializedName("companyId")
        public String companyId;
        @SerializedName("vendorId")
        public String vendorId;
        @SerializedName("flatFee")
        public double flatFee;
        @SerializedName("status")
        public int status;
        @SerializedName("pubChannelName")
        public String pubChannelName;
        @SerializedName("userAddress")
        public String userAddress;
       @SerializedName("subCategoryId")
        public String subCategoryId;
        @SerializedName("userLatitude")
        public double userLatitude;
        @SerializedName("userLongitude")
        public double userLongitude;

    }

    public static class Data implements Serializable {
        @SerializedName("notifications")
        public ArrayList<Notifications> notifications;
        @SerializedName("activities")
        public ArrayList<Notifications> activities;
        @SerializedName("recordCount")
        public int recordCount;
    }
}
