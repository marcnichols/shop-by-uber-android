package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class LocalCategoriesModel implements Serializable {

    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class Data implements Serializable {
        @SerializedName("categories")
        public ArrayList<LocalCategories> categories;
        @SerializedName("localCategories")
        public ArrayList<LocalCategories> localCategories;

        @SerializedName("subCategories")
        public ArrayList<SubCategories> subCategories;

        @SerializedName("companies")
        public ArrayList<CompanyListing> companyListing;

        @SerializedName("recordCount")
        public int recordCount;
    }

    public static class SubCategories implements Serializable {
        public boolean isAllocate;

        @SerializedName("_id")
        public String _id;
        @SerializedName("subcatName")
        public String subcatName;
        @SerializedName("subCatImage")
        public String subCatImage;
    }


    public static class LocalCategories implements Serializable {

        public boolean isAllocate;


        @SerializedName("catName")
        public String catName;
        @SerializedName("categoryImage")
        public String categoryImage;
        @SerializedName("_id")
        public String _id;
        @SerializedName("companyData")
        public ArrayList<companyData> companyDatas;
    }

    public static class companyData implements Serializable {
        @SerializedName("companyId")
        public String companyId;
        @SerializedName("comapnyScreenName")
        public String comapnyScreenName;
        @SerializedName("companyName")
        public String companyName;
        @SerializedName("latitude")
        public double latitude;
        @SerializedName("longitude")
        public double longitude;
        @SerializedName("companyProfileImage")
        public String companyProfileImage;
        @SerializedName("serviceDescription")
        public String serviceDescription;

        @Override
        public String toString() {
            return companyName;
        }
    }

    public static class CompanyListing implements Serializable {
        @SerializedName("_id")
        public String _id;
        @SerializedName("companyName")
        public String companyName;
        @SerializedName("comapnyScreenName")
        public String comapnyScreenName;
        @SerializedName("serviceDescription")
        public String serviceDescription;
        @SerializedName("companyProfileImage")
        public String companyProfileImage;
        @SerializedName("categories")
        public String categories;
    }

}
