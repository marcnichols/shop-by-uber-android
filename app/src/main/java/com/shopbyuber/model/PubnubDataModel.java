package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class PubnubDataModel implements Serializable {

    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class MapData implements Serializable {
        @SerializedName("pubChannelName")
        public String pubChannelName;
        @SerializedName("status")
        public int status;
        @SerializedName("screenName")
        public String screenName;
        @SerializedName("requestFee")
        public String requestFee;
        @SerializedName("appointmentTimeId")
        public String appointmentTimeId;
        @SerializedName("requestId")
        public String requestId;
        @SerializedName("displayMessage")
        public String displayMessage;
        @SerializedName("duration")
        public String duration;
        @SerializedName("serviceProvider")
        public String serviceProvider;
        @SerializedName("serviceProviderImage")
        public String serviceProviderImage;

        @SerializedName("companyAddress")
        public String companyAddress;
        @SerializedName("companyLatitude")
        public double companyLatitude;
        @SerializedName("companyLongitude")
        public double companyLongitude;

        @SerializedName("catName")
        public String catName;
        @SerializedName("address")
        public String address;
        @SerializedName("userLatitude")
        public double userLatitude;
        @SerializedName("userLongitude")
        public double userLongitude;
        @SerializedName("paymentMethod")
        public String paymentMethod;
        @SerializedName("serviceId")
        public String serviceId;
        @SerializedName("companyId")
        public String companyId;
    }

    public static class Data implements Serializable {
        @SerializedName("mapData")
        public List<MapData> mapData;
    }
}
