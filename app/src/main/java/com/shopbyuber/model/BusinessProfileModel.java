package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class BusinessProfileModel implements Serializable {


    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class PaymentDetail implements Serializable {
        @SerializedName("firstName")
        public String firstName;
        @SerializedName("lastName")
        public String lastName;
        @SerializedName("streetAddress")
        public String streetAddress;
        @SerializedName("city")
        public String city;
        @SerializedName("state")
        public String state;
        @SerializedName("zipCode")
        public String zipcode;
        @SerializedName("routingNumber")
        public String routingNumber;
        @SerializedName("accountNumber")
        public String accountNumber;
    }

    public static class VendorData implements Serializable {
        @SerializedName("email")
        public String email;
        @SerializedName("userRole")
        public int userRole;
        @SerializedName("mobilePhone")
        public String mobilePhone;
        @SerializedName("timeZone")
        public String timeZone;
        @SerializedName("profileImage")
        public String profileImage;
        @SerializedName("name")
        public String name;
        @SerializedName("legalBusinessName")
        public String legalBusinessName;
        @SerializedName("description")
        public String description;
        @SerializedName("speciality")
        public String speciality;
        @SerializedName("countryCode")
        public String countryCode;
        @SerializedName("phoneNumber")
        public String phoneNumber;
        @SerializedName("isSalesperson")
        public boolean isSalesperson;

    }


    public static class ServiceReviews {
        @SerializedName("rating")
        public double rating;
        @SerializedName("feedback")
        public String Feedback;
        @SerializedName("givenBy")
        public String givenBy;
        @SerializedName("feedbackDateTime")
        public String feedbackDateTime;
        @SerializedName("givenByUser")
        public String givenByUser;


    }

    public static class Data {
        @SerializedName("companyName")
        public String companyName;
        @SerializedName("comapnyScreenName")
        public String comapnyScreenName;
        @SerializedName("ownerName")
        public String ownerName;
        @SerializedName("companyAddress")
        public String companyAddress;
        @SerializedName("latitude")
        public double latitude;
        @SerializedName("longitude")
        public double longitude;
        @SerializedName("zipcode")
        public String zipcode;
        @SerializedName("companyPhone")
        public String companyPhone;
        @SerializedName("websiteUrl")
        public String websiteUrl;
        @SerializedName("serviceDescription")
        public String serviceDescription;
        @SerializedName("companyProfileImage")
        public String companyProfileImage;
        @SerializedName("companyId")
        public String companyId;
        @SerializedName("serviceImage")
        public String serviceImage;
        @SerializedName("serviceReviews")
        public List<ServiceReviews> serviceReviews;
        @SerializedName("overAllRating")
        public float overAllRating;

        @SerializedName("companyPhoneNumber")
        public String companyPhoneNumber;
        @SerializedName("countryCode")
        public String countryCode;
        @SerializedName("paypalEmail")
        public String paypalEmail;
        @SerializedName("vendorData")
        public VendorData vendorData;

        @SerializedName("firstName")
        public String firstName;
        @SerializedName("lastName")
        public String lastName;

        @SerializedName("paymentMethod")
        public int paymentMethod;

        @SerializedName("paymentDetail")
        public PaymentDetail paymentDetail;


        @SerializedName("promoCode")
        public String promoCode;
    }

}
