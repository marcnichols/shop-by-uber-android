package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class SelectedCategoryModel implements Serializable {
    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class Data implements Serializable {
        @SerializedName("companyListing")
        public ArrayList<CompanyListing> companyListing;
        @SerializedName("recordCount")
        public int recordCount;
    }

    public static class CompanyListing implements Serializable {
        @SerializedName("companyId")
        public String companyId;
        @SerializedName("isFavourite")
        public boolean isFavourite;
        @SerializedName("comapnyScreenName")
        public String comapnyScreenName;
        @SerializedName("companyProfileImage")
        public String companyProfileImage;
        @SerializedName("isAvailable")
        public boolean isAvailable;
        @SerializedName("latitude")
        public double latitude;
        @SerializedName("longitude")
        public double longitude;
        @SerializedName("zipcode")
        public String zipcode;
        @SerializedName("categoryId")
        public String categoryId;
        @SerializedName("subCategoryId")
        public String subCategoryId;
        @SerializedName("serviceDescription")
        public String serviceDescription;
    }

}