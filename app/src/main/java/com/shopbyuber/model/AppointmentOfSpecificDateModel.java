package com.shopbyuber.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class AppointmentOfSpecificDateModel implements Serializable{

    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public ArrayList<Data> Data;

    public static class Data implements Serializable{
        @SerializedName("requestId")
        public String requestId;
        @SerializedName("bookByUserId")
        public String bookByUserId;
        @SerializedName("serviceId")
        public String serviceId;
        @SerializedName("timeSlotId")
        public String timeSlotId;
        @SerializedName("userName")
        public String userName;
        @SerializedName("screenName")
        public String screenName;
        @SerializedName("serviceDetail")
        public String serviceDetail;
        @SerializedName("status")
        public int status;
        @SerializedName("flatFee")
        public String flatFee;
        @SerializedName("userImage")
        public String userImage;
        @SerializedName("duration")
        public String duration;
        @SerializedName("address")
        public String address;
        @SerializedName("latitude")
        public double latitude;
        @SerializedName("longitude")
        public double longitude;
    }
}
