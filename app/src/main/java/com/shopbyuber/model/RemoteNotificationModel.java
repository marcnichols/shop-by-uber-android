package com.shopbyuber.model;

import java.io.Serializable;

public class RemoteNotificationModel implements Serializable {
    public String alert;
    public String title;
    public String requestId;
    public String companyId;
    public String categoryId;
    public String subCategoryId;
    public boolean playSound;
    public boolean vibrate;
    public boolean pulseLight;
    public int notifyType;
    public String notificationId;
    public String pubChannelName;
    public String dialogId;
    public String profileImage;
    public String userName;
    public String userId;
    public String serviceId;
    public int quickBloxId;
}
