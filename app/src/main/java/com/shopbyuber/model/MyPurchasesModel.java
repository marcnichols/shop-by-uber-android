package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MyPurchasesModel implements Serializable {

    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class Purchases implements Serializable {
        @SerializedName("serviceId")
        public String serviceId;
        @SerializedName("screenName")
        public String screenName;
        @SerializedName("serviceImage")
        public String serviceImage;
        @SerializedName("flatFee")
        public String flatFee;
        @SerializedName("status")
        public int status;
        @SerializedName("requestId")
        public String requestId;
    }

    public static class Data implements Serializable {
        @SerializedName("purchases")
        public List<Purchases> purchases;
        @SerializedName("recordCount")
        public int recordCount;
    }
}
