package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class FavouritesOfUserModel implements Serializable {

    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class Details implements Serializable {
        @SerializedName("categoryId")
        public String categoryId;
        @SerializedName("screenName")
        public String screenName;
        @SerializedName("flatFee")
        public String flatFee;
        @SerializedName("companyId")
        public String companyId;
        @SerializedName("serviceId")
        public String serviceId;
        @SerializedName("profileImage")
        public String profileImage;
        @SerializedName("isFavourite")
        public boolean isFavourite;
        @SerializedName("zipcode")
        public String zipcode;
        @SerializedName("isAvailable")
        public boolean isAvailable;
        @SerializedName("serviceDescription")
        public String serviceDescription;
        @SerializedName("latitude")
        public Double latitude;
        @SerializedName("longitude")
        public Double longitude;
    }

    public static class Favourites implements Serializable {
        @SerializedName("type")
        public int type;
        @SerializedName("data")
        public Details details;
    }

    public static class Data implements Serializable {
        @SerializedName("favourites")
        public ArrayList<Favourites> favourites;
        @SerializedName("notifications")
        public ArrayList<NotificationModel.Notifications> notifications;
    }
}
