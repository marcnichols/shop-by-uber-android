package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class CompanyServiceDetailModel implements Serializable {


    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class CompanyInfo implements Serializable {
        @SerializedName("companyId")
        public String companyId;
        @SerializedName("isFavourite")
        public boolean isFavourite;
        @SerializedName("comapnyScreenName")
        public String comapnyScreenName;
        @SerializedName("companyProfileImage")
        public String companyProfileImage;
        @SerializedName("categoryId")
        public String categoryId;
        @SerializedName("catName")
        public String catName;
        @SerializedName("isAvailable")
        public boolean isAvailable;
        @SerializedName("companyDescription")
        public String companyDescription;
        @SerializedName("companyAddress")
        public String companyAddress;
    }

    public static class AvailableTimeSlot implements Serializable {
        @SerializedName("timeslotId")
        public String timeslotId;
        @SerializedName("duration")
        public String duration;
        public boolean isAvailable;

        @Override
        public String toString() {
            return duration;
        }
    }

    public static class Services implements Serializable {
        @SerializedName("screenName")
        public String screenName;
        @SerializedName("flatFee")
        public String flatFee;
        @SerializedName("serviceDetail")
        public String serviceDetail;
        @SerializedName("serviceImage")
        public String serviceImage;
        @SerializedName("serviceId")
        public String serviceId;
        @SerializedName("isFavourite")
        public boolean isFavourite;
        @SerializedName("availableTimeSlot")
        public List<AvailableTimeSlot> availableTimeSlot;
        @SerializedName("serviceAvailable")
        public boolean serviceAvailable;
        @SerializedName("bookNowDone")
        public boolean bookNowDone;
        @SerializedName("companyAddress")
        public String companyAddress;
        @SerializedName("requestId")
        public String requestId;

        public String address = "";
        public double latitude;
        public double longitude;
    }

    public static class Reviews implements Serializable {
        @SerializedName("rating")
        public float rating;
        @SerializedName("feedback")
        public String Feedback;
        @SerializedName("givenBy")
        public String givenBy;
        @SerializedName("feedbackDateTime")
        public String feedbackDateTime;
        @SerializedName("givenByUser")
        public String givenByUser;
    }

    public static class Data implements Serializable {
        @SerializedName("companyInfo")
        public CompanyInfo companyInfo;
        @SerializedName("services")
        public List<Services> services;
        @SerializedName("reviews")
        public List<Reviews> reviews;
        @SerializedName("overAllRating")
        public float overAllRating;
    }


}
