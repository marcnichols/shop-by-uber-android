package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class GetPicksCompanyServiceModel implements Serializable {


    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class CompanyInfo implements Serializable {
        @SerializedName("comapnyScreenName")
        public String comapnyScreenName;
        @SerializedName("companyProfileImage")
        public String companyProfileImage;
        @SerializedName("companyId")
        public String companyId;
        @SerializedName("isFavourite")
        public boolean isFavourite;
        @SerializedName("companyDescription")
        public String companyDescription;
    }

    public static class AvailableTimeSlot implements Serializable {
        @SerializedName("timeslotId")
        public String timeslotId;
        @SerializedName("duration")
        public String duration;
        public boolean isAvailable;

        @Override
        public String toString() {
            return duration;
        }
    }

    public static class Services {
        @SerializedName("categoryId")
        public String categoryId;
        @SerializedName("catName")
        public String catName;
        @SerializedName("screenName")
        public String screenName;
        @SerializedName("serviceAvailable")
        public boolean serviceAvailable;
        @SerializedName("flatFee")
        public String flatFee;
        @SerializedName("serviceDetail")
        public String serviceDetail;
        @SerializedName("serviceImage")
        public String serviceImage;
        @SerializedName("serviceId")
        public String serviceId;
        @SerializedName("isFavourite")
        public boolean isFavourite;
        @SerializedName("availableTimeSlot")
        public List<AvailableTimeSlot> availableTimeSlot;
        @SerializedName("bookNowDone")
        public boolean bookNowDone;
        @SerializedName("companyAddress")
        public String companyAddress;
        public String address = "";
        public double latitude;
        public double longitude;
    }

    public static class Reviews implements Serializable {
        @SerializedName("feedback")
        public String feedback;
        @SerializedName("rating")
        public float rating;
        @SerializedName("givenBy")
        public String givenBy;
        @SerializedName("feedbackDateTime")
        public String feedbackDateTime;

        @SerializedName("givenByUser")
        public String givenByUser;
    }

    public static class Data implements Serializable {
        @SerializedName("companyInfo")
        public CompanyInfo companyInfo;
        @SerializedName("services")
        public ArrayList<Services> services;
        @SerializedName("recordCount")
        public int recordCount;
        @SerializedName("reviews")
        public List<Reviews> reviews;
        @SerializedName("overAllRating")
        public float overAllRating;
    }
}
