package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class NotificationSettingsModel implements Serializable {

    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class Data implements Serializable {
        @SerializedName("basicNotification")
        public List<Notification> basicNotification;
        @SerializedName("otherNotification")
        public List<Notification> otherNotification;
    }

    public static class Notification implements Serializable {
        @SerializedName("notifyId")
        public int notifyId;
        @SerializedName("notifyLabel")
        public String notifyLabel;
        @SerializedName("isEnabled")
        public boolean isEnabled;
    }

    public static class NotificationSection implements Serializable {
        public String section;
        public List<Notification> notifications;
    }
}
