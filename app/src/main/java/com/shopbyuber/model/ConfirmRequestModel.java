package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;



public class ConfirmRequestModel implements Serializable {
    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class UserInfo implements Serializable{
        @SerializedName("latitude")
        public double latitude;
        @SerializedName("longitude")
        public double longitude;
        @SerializedName("address")
        public String address;
    }

    public static class Requests implements Serializable{
        @SerializedName("requestId")
        public String requestId;
        @SerializedName("timeSlotId")
        public String timeSlotId;
        @SerializedName("duration")
        public String duration;
        @SerializedName("userInfo")
        public UserInfo userInfo;
    }


    public static class VendorData implements Serializable{
        @SerializedName("userRole")
        public int userRole;
        @SerializedName("name")
        public String name;
        @SerializedName("profileImage")
        public String profileImage;
        @SerializedName("description")
        public String description;
        @SerializedName("speciality")
        public String speciality;
        @SerializedName("vendorId")
        public String vendorId;
    }

    public static class ConfirmServices implements Serializable {
        @SerializedName("flatFee")
        public String flatFee;
        @SerializedName("serviceId")
        public String serviceId;
        @SerializedName("screenName")
        public String screenName;
        @SerializedName("serviceImage")
        public String serviceImage;
        @SerializedName("requests")
        public List<Requests> requests;
        @SerializedName("vendorData")
        public VendorData vendorData;
        @SerializedName("serviceDetail")
        public String serviceDetail;
    }

    public static class Data implements Serializable {
        @SerializedName("confirmServices")
        public ArrayList<ConfirmServices> confirmServices;
        @SerializedName("payment_configuration")
        public Payment_configuration payment_configuration;
    }

    public static class Payment_configuration implements Serializable{
        @SerializedName("key")
        public String key;
        @SerializedName("secret")
        public String secret;
    }
}
