package com.shopbyuber.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class AppointmentModel implements Serializable {

    @SerializedName("Result")
    public boolean Result;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class Appointment implements Serializable {
        @SerializedName("requestId")
        public String requestId;
        @SerializedName("screenName")
        public String screenName;
        @SerializedName("status")
        public int status;
        @SerializedName("serviceImage")
        public String serviceImage;
        @SerializedName("duration")
        public String duration;
        @SerializedName("address")
        public String address;
        @SerializedName("latitude")
        public double latitude;
        @SerializedName("longitude")
        public double longitude;
        @SerializedName("pubChannelName")
        public String pubChannelName;
    }

    public static class Data implements Serializable {
        @SerializedName("appointment")
        public List<Appointment> appointment;
        @SerializedName("recordCount")
        public int recordCount;
    }
}
