package com.shopbyuber.fcm;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.orhanobut.logger.Logger;
import com.shopbyuber.utils.Globals;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    Globals globals = (Globals) Globals.getContext();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        if (!refreshedToken.isEmpty()) {
            // store fcm token to preference
            globals.setFCM_DeviceToken(refreshedToken);
            Logger.e("FCMDeviceToken : " + refreshedToken);
        }
    }
}
