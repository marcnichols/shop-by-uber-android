package com.shopbyuber.fcm;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.orhanobut.logger.Logger;
import com.shopbyuber.model.RemoteNotificationModel;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.NotificationUtils;

import org.json.JSONObject;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Logger.e("onMessageReceived" + remoteMessage.getData());

        if (remoteMessage.getData() != null) {
            try {
                RemoteNotificationModel remoteNotificationModel = new RemoteNotificationModel();
                remoteNotificationModel.alert = remoteMessage.getData().get(Constants.SBU_Alert);
                remoteNotificationModel.title = remoteMessage.getData().get(Constants.SBU_Title);
                JSONObject jsonObject = new JSONObject(remoteMessage.getData().get(Constants.SBU_Params));
                if (jsonObject.has(Constants.SBU_PlaySound))
                    remoteNotificationModel.playSound = jsonObject.getBoolean(Constants.SBU_PlaySound);
                if (jsonObject.has(Constants.SBU_Vibrate))
                    remoteNotificationModel.vibrate = jsonObject.getBoolean(Constants.SBU_Vibrate);
                if (jsonObject.has(Constants.SBU_PulseLight))
                    remoteNotificationModel.pulseLight = jsonObject.getBoolean(Constants.SBU_PulseLight);
                if (jsonObject.has(Constants.SBU_NotifyType))
                    remoteNotificationModel.notifyType = jsonObject.getInt(Constants.SBU_NotifyType);
                if (jsonObject.has(Constants.SBU_NotificationId))
                    remoteNotificationModel.notificationId = jsonObject.getString(Constants.SBU_NotificationId);
                if (jsonObject.has(Constants.SBU_ProfileImage))
                    remoteNotificationModel.profileImage = jsonObject.getString(Constants.SBU_ProfileImage);
                if (jsonObject.has(Constants.SBU_UserName))
                    remoteNotificationModel.userName = jsonObject.getString(Constants.SBU_UserName);
                if (jsonObject.has(Constants.SBU_UserId))
                    remoteNotificationModel.userId = jsonObject.getString(Constants.SBU_UserId);
                if (jsonObject.has(Constants.SBU_QuickBloxId))
                    remoteNotificationModel.quickBloxId = jsonObject.getInt(Constants.SBU_QuickBloxId);
                if (jsonObject.has(Constants.SBU_RequestId))
                    remoteNotificationModel.requestId = jsonObject.getString(Constants.SBU_RequestId);
                if (jsonObject.has(Constants.SBU_CompanyId))
                    remoteNotificationModel.companyId = jsonObject.getString(Constants.SBU_CompanyId);
                if (jsonObject.has(Constants.SBU_CategoryId))
                    remoteNotificationModel.categoryId = jsonObject.getString(Constants.SBU_CategoryId);
                if (jsonObject.has(Constants.SBU_SubCategoryId))
                    remoteNotificationModel.subCategoryId = jsonObject.getString(Constants.SBU_SubCategoryId);
                if (jsonObject.has(Constants.SBU_ServiceId))
                    remoteNotificationModel.serviceId = jsonObject.getString(Constants.SBU_ServiceId);
                if (jsonObject.has(Constants.SBU_PubChannelName))
                    remoteNotificationModel.pubChannelName = jsonObject.getString(Constants.SBU_PubChannelName);
                if (jsonObject.has(Constants.SBU_DialogId))
                    remoteNotificationModel.dialogId = jsonObject.getString(Constants.SBU_DialogId);
                NotificationUtils.showNotification(Globals.getContext(), remoteNotificationModel);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
