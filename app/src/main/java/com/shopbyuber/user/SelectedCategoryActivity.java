package com.shopbyuber.user;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemSpanLookup;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.GetVendorsFrom;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.model.SelectedCategoryModel;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.FusedLocationManager;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;
import com.shopbyuber.utils.paginationProgressBarAdapter;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

public class SelectedCategoryActivity extends BaseActivity implements AdapterView.OnItemClickListener,
        OnMapReadyCallback,
        Paginate.Callbacks, SwipeRefreshLayout.OnRefreshListener,
        GoogleMap.OnMarkerClickListener {

    Globals globals;

    @BindView(R.id.list)
    RecyclerView selectedCatRecyclerView;

    @BindView(R.id.map_container)
    RelativeLayout map_container;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tv_no_record_found)
    TextView tv_no_record_found;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private SelectedCategoryModel selectedCategoryModel;
    private ArrayList<SelectedCategoryModel.CompanyListing> dataArrayList;
    private AdapterSelectedCategory adapterSelectedCategory;

    GoogleMap mGoogleMap;
    SupportMapFragment mapFragment;

    private Paginate paginate;
    int pageNo = 1;
    private boolean loading = false;
    private String catId, catTitle, subCategoryId;
    FusedLocationManager fusedLocationManager;
    GetVendorsFrom vendorsFrom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_category);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        fusedLocationManager = new FusedLocationManager(this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            catId = bundle.getString(Constants.SBU_CategoryId);
            catTitle = bundle.getString(Constants.SBU_CatName);
            subCategoryId = bundle.getString(Constants.SBU_SubCategoryId);
            vendorsFrom = (GetVendorsFrom) bundle.getSerializable(Constants.SBU_GetVendorFrom);
        }

        globals = ((Globals) getApplicationContext());
        map_container.setVisibility(View.VISIBLE);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(catTitle);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectedCategoryActivity.this.onBackPressed();
            }
        });
        pageNo = 1;
        dataArrayList = new ArrayList<>();

        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorToolbarTitle);
        swipeRefreshLayout.setOnRefreshListener(this);

        setupMap();
        dogetLocalRequest(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;
            case R.id.action_cart:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleMap == null) {
            mapFragment.getMapAsync(this);
        }
    }

    private void setupMap() {
        // add map fragment to container
        FragmentManager fm = getSupportFragmentManager();
        mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map_container);
        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.map_container, mapFragment).commit();
        }
    }

    private void showNoRecordFound(String message) {
        loading = false;
        if (dataArrayList.isEmpty()) {
            selectedCatRecyclerView.setVisibility(View.GONE);
            if (tv_no_record_found.getVisibility() == View.GONE)
                tv_no_record_found.setText(message);
            tv_no_record_found.setVisibility(View.VISIBLE);
        }
    }

    private void hideNoRecordFound() {
        selectedCatRecyclerView.setVisibility(View.VISIBLE);
        if (tv_no_record_found.getVisibility() == View.VISIBLE)
            tv_no_record_found.setVisibility(View.GONE);
    }

    public void dogetLocalRequest(final boolean showProgress) {
        if (!Globals.internetCheck(this)) {
            showNoRecordFound(getString(R.string.no_vendors_are_available_in_this_category));
            return;
        }
        String zipcode = "";
        if (vendorsFrom == GetVendorsFrom.FROM_LOCAL)
            zipcode = globals.getPostal(fusedLocationManager.getLatLng());

        JSONObject params = HttpRequestHandler.getInstance().getSelectedCategory(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId, catId,
                zipcode,
                pageNo, subCategoryId);

        HttpRequestHandler.getInstance().post(this, getString(R.string.getCategoryVendors), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                if (!swipeRefreshLayout.isRefreshing() && showProgress)
                    progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (progressBar.getVisibility() == View.VISIBLE)
                    progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                selectedCategoryModel = new Gson().fromJson(response.toString(), SelectedCategoryModel.class);
                if (selectedCategoryModel.Result) {
                    if (swipeRefreshLayout.isRefreshing()) {
                        dataArrayList.clear();
                        if (adapterSelectedCategory != null)
                            adapterSelectedCategory.notifyDataSetChanged();
                    }
                    setupList(selectedCategoryModel);
                } else {
                    showNoRecordFound(selectedCategoryModel.Message);
                    if (selectedCategoryModel.Message.contains(getString(R.string.invalid_session))) {
                        Toaster.shortToast(selectedCategoryModel.Message);
                        globals.doLogout(SelectedCategoryActivity.this);
                    }
                }
                stopRefreshing();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                stopRefreshing();
                Logger.d(errorResponse);
                showNoRecordFound(getString(R.string.no_vendors_are_available_in_this_category));
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                stopRefreshing();
                Logger.d(responseString);
                showNoRecordFound(getString(R.string.no_vendors_are_available_in_this_category));
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void stopRefreshing() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void setupList(SelectedCategoryModel selectedCategoryModel) {
        ArrayList<SelectedCategoryModel.CompanyListing> localCategory = selectedCategoryModel.Data.companyListing;
        if (localCategory != null && !localCategory.isEmpty()) {
            dataArrayList.addAll(localCategory);
            setAdapter();
        } else
            showNoRecordFound(selectedCategoryModel.Message);
    }

    private void plotMarkers() {
        if (!dataArrayList.isEmpty()) {
            for (SelectedCategoryModel.CompanyListing companyListing : dataArrayList) {

                if (mGoogleMap != null) {
                    drawMarker(new LatLng(companyListing.latitude, companyListing.longitude),
                            companyListing.companyId);
                }
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mGoogleMap != null) {
                        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(fusedLocationManager.getLatLng()
                                , 8));
                    }
                }
            }, 1000);
        }
    }

    private void drawMarker(LatLng point, String companyId) {
        // Creating an instance of MarkerOptions
        MarkerOptions markerOptions = new MarkerOptions();

        // Setting latitude and longitude for the marker
        markerOptions.position(point);

        // Adding marker on the Google Map
        Marker marker = mGoogleMap.addMarker(markerOptions);
        marker.setTag(companyId);
        marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.map_green));
    }

    private void setAdapter() {
        hideNoRecordFound();
        plotMarkers();
        if (adapterSelectedCategory == null) {
            adapterSelectedCategory = new AdapterSelectedCategory(this);
            adapterSelectedCategory.setOnItemClickListener(this);
            if (paginate != null) {
                paginate.unbind();
            }
        }
        loading = false;
        adapterSelectedCategory.doRefresh(dataArrayList, catTitle);

        if (selectedCatRecyclerView.getAdapter() == null) {
            selectedCatRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            selectedCatRecyclerView.setItemAnimator(new DefaultItemAnimator());
            selectedCatRecyclerView.setAdapter(adapterSelectedCategory);
            if (dataArrayList.size() < selectedCategoryModel.Data.recordCount) {
                // initialize paginate
                paginate = Paginate.with(selectedCatRecyclerView, this)
                        .setLoadingTriggerThreshold(Constants.progress_threshold_2)
                        .addLoadingListItem(Constants.addLoadingRow)
                        .setLoadingListItemCreator(new paginationProgressBarAdapter())
                        .setLoadingListItemSpanSizeLookup(new LoadingListItemSpanLookup() {
                            @Override
                            public int getSpanSize() {
                                return Constants.GRID_SPAN;
                            }
                        })
                        .build();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        startActivity(new Intent(SelectedCategoryActivity.this, ServiceDetailActivity.class)
                .putExtra(Constants.SBU_CompanyId, dataArrayList.get(position).companyId)
                .putExtra(Constants.SBU_CategoryId, dataArrayList.get(position).categoryId)
                .putExtra(Constants.SBU_SubCategoryId, dataArrayList.get(position).subCategoryId));
    }


    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        // called when map is ready
        this.mGoogleMap = googleMap;
        this.mGoogleMap.setOnMarkerClickListener(this);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.setMyLocationEnabled(true);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Constants.SBU_LOS_ANGELES, 5));
    }

    @Override
    public void onLoadMore() {
        loading = true;
        pageNo++;
        dogetLocalRequest(false);
    }

    @Override
    public boolean isLoading() {
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return dataArrayList.size() == selectedCategoryModel.Data.recordCount;
    }

    @Override
    public void onRefresh() {
        pageNo = 1;
        dogetLocalRequest(true);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        SelectedCategoryModel.CompanyListing companyListing = getMarkerData(String.valueOf(marker.getTag()));
        if (companyListing != null) {
            Company_detail_popup company_detail_popup = new Company_detail_popup(SelectedCategoryActivity.this);
            company_detail_popup.showPopup(companyListing.comapnyScreenName, companyListing.serviceDescription,
                    companyListing.companyProfileImage, companyListing.companyId);
        }
        return false;
    }

    private SelectedCategoryModel.CompanyListing getMarkerData(String companyId) {
        for (SelectedCategoryModel.CompanyListing companyData : dataArrayList) {
            if (companyData.companyId.equalsIgnoreCase(companyId))
                return companyData;
        }
        return null;
    }
}
