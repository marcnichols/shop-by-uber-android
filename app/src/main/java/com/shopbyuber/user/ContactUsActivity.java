package com.shopbyuber.user;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

public class ContactUsActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.edt_name)
    EditText edtName;

    @BindView(R.id.edt_email)
    EditText edtEmail;

    @BindView(R.id.edt_description)
    EditText edtDescription;

    @BindView(R.id.btn_contact_us)
    Button btnContactUs;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    Globals globals;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.contact_us);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        globals = (Globals) getApplicationContext();
    }


    @OnClick(R.id.btn_contact_us)
    public void onViewClicked() {
        if (doValidation())
            doContactUs(true);
    }

    public boolean doValidation() {
        if (edtName.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_your_name);
            return false;
        }
        if (edtEmail.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_email);
            return false;
        }
        if (!Globals.validateEmailAddress(edtEmail.getText().toString().trim())) {
            Toaster.shortToast(R.string.err_enter_valid_email);
            return false;
        }
        if (edtDescription.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_description);
            return false;
        }
        return true;
    }

    public void doContactUs(final boolean showProgress) {
        if (!Globals.internetCheck(this)) {
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().getContactUsParam(
                edtName.getText().toString(),
                edtEmail.getText().toString(),
                edtDescription.getText().toString());

        HttpRequestHandler.getInstance().post(this, getString(R.string.contactUs), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                if (showProgress)
                    progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (progressBar.getVisibility() == View.VISIBLE)
                    progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                Toaster.shortToast(dataModel.Message);
                if (dataModel.Result)
                    finish();
                else if (dataModel.Message.equals(getString(R.string.invalid_session)))
                    globals.doLogout(ContactUsActivity.this);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}