package com.shopbyuber.user;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.shopbyuber.R;
import com.shopbyuber.enums.MyPurchasesType;
import com.shopbyuber.model.MyPurchasesModel.Purchases;
import com.shopbyuber.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


class AdapterMyPurchases extends RecyclerView.Adapter<AdapterMyPurchases.ViewHolder> {

    private ArrayList<Purchases> mValues;
    private final Context mContext;
    private AdapterView.OnItemClickListener onItemClickListener;

    AdapterMyPurchases(Context context) {
        mContext = context;
    }

    public void doRefresh(ArrayList<Purchases> purchases) {
        mValues = purchases;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_purchases_item, parent, false);
        return new ViewHolder(view, this);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private AdapterMyPurchases adapterMyPurchases;
        @BindView(R.id.img_profile)
        ImageView img_profile;
        @BindView(R.id.tv_title)
        TextView tv_title;
        @BindView(R.id.tv_price)
        TextView tv_price;
        @BindView(R.id.tv_status)
        TextView tv_status;

        public ViewHolder(View itemView, AdapterMyPurchases adapterMyServices) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.adapterMyPurchases = adapterMyServices;
            itemView.setOnClickListener(this);
        }

        void setDataToView(Purchases mItem, ViewHolder viewHolder) {
            viewHolder.tv_title.setText(mItem.screenName);
            viewHolder.tv_price.setText(Constants.SBU_Currency + " " + mItem.flatFee);
            MyPurchasesType purchasesType = MyPurchasesType.fromInt(mItem.status);

            String status = "";
            switch (purchasesType) {
                case NotStarted:
                    status = mContext.getString(R.string.not_started_black);
                    break;
                case OnRoute:
                    status = mContext.getString(R.string.on_route_orange);
                    break;
                case InProgress:
                    status = mContext.getString(R.string.inprogress_green);
                    break;
                case Completed:
                    status = mContext.getString(R.string.completed_gray);
                    break;
                case Cancelled:
                    status = mContext.getString(R.string.cancelled_red);
                    break;
                default:
                    break;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                viewHolder.tv_status.setText(Html.fromHtml(status, Html.FROM_HTML_MODE_COMPACT));
            } else {
                viewHolder.tv_status.setText(Html.fromHtml(status));
            }

            Glide.with(mContext)
                    .load(mItem.serviceImage)
                    .centerCrop()
                    .placeholder(R.drawable.image_place_holder)
                    .crossFade()
                    .dontAnimate()
                    .into(viewHolder.img_profile);
        }

        @Override
        public void onClick(View v) {
            adapterMyPurchases.onItemHolderClick(ViewHolder.this);
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Purchases mItem = mValues.get(position);
        holder.setDataToView(mItem, holder);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void onItemHolderClick(ViewHolder holder) {
        if (onItemClickListener != null)
            onItemClickListener.onItemClick(null, holder.itemView, holder.getAdapterPosition(), holder.getItemId());
    }
}

