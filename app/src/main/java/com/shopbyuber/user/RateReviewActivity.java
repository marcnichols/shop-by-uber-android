package com.shopbyuber.user;


import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.model.RateReviewModel;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

public class RateReviewActivity extends BaseActivity {

    @BindView(R.id.layout_rate_review)
    LinearLayout contentView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.profile_img)
    CircleImageView profileImg;
    @BindView(R.id.profile_name)
    TextView profileName;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;
    @BindView(R.id.edt_feedback)
    EditText edtFeedback;
    @BindView(R.id.btn_submit)
    Button btn_submit;
    @BindView(R.id.layout_submit)
    LinearLayout layoutSubmit;

    Globals globals;

    String serviceId = "", requestId = "", companyId = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_review);
        ButterKnife.bind(this);
        init();
    }

    public void init() {
        super.hideUnhide(contentView, layoutSubmit);
        globals = (Globals) getApplicationContext();

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.toolbar_title_rate_review_us);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RateReviewActivity.this.onBackPressed();
            }
        });

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            serviceId = bundle.getString(Constants.SBU_ServiceId) != null ? bundle.getString(Constants.SBU_ServiceId) : "";
            requestId = bundle.getString(Constants.SBU_RequestId) != null ? bundle.getString(Constants.SBU_RequestId) : "";
            doGetRatingScreenData();
        }
    }

    public void doGetRatingScreenData() {
        if (!Globals.internetCheck(this)) {
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().getRatingScreenData(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                serviceId,
                requestId
        );

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(RateReviewActivity.this);

        HttpRequestHandler.getInstance().post(RateReviewActivity.this, getString(R.string.getRatingScreenData), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                RateReviewModel rateReviewModel = new Gson().fromJson(response.toString(), RateReviewModel.class);
                if (rateReviewModel.Result) {
                    setData(rateReviewModel.Data);
                } else {
                    Toaster.shortToast(rateReviewModel.Message);
                    if (rateReviewModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(RateReviewActivity.this);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    public void setData(RateReviewModel.Data data) {
        Glide.with(this)
                .load(data.profileImage)
                .centerCrop()
                .placeholder(R.drawable.image_place_holder)
                .crossFade()
                .dontAnimate()
                .into(profileImg);

        edtFeedback.setText(data.feedback != null ? data.feedback : "");
        profileName.setText(data.name != null ? data.name : "");
        companyId = data.companyId;

        if (data.rating > 0) {
            ratingBar.setRating(data.rating);
            btn_submit.setEnabled(true);
            btn_submit.setAlpha(1);
        } else {
            btn_submit.setEnabled(false);
            btn_submit.setAlpha(.5f);
        }
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar1, float rating, boolean fromUser) {
                if (rating > 0) {
                    btn_submit.setEnabled(true);
                    btn_submit.setAlpha(1);
                } else {
                    btn_submit.setEnabled(false);
                    btn_submit.setAlpha(.5f);
                }
            }
        });

    }


    @OnClick({R.id.profile_img, R.id.btn_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.profile_img:
                break;
            case R.id.btn_submit:
                doGiveRatingFeedbackService();
                break;
        }
    }

    public void doGiveRatingFeedbackService() {
        if (!Globals.internetCheck(this)) {
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().giveRatingFeedbackService(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                serviceId,
                companyId,
                requestId,
                ratingBar.getRating(),
                edtFeedback.getText().toString().trim());

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(RateReviewActivity.this);

        HttpRequestHandler.getInstance().post(RateReviewActivity.this, getString(R.string.giveRatingFeedbackService), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel.Result) {
                    setResult(RESULT_OK);
                    finish();
                } else if (dataModel.Message.contains(getString(R.string.invalid_session)))
                    globals.doLogout(RateReviewActivity.this);
                Toaster.shortToast(dataModel.Message);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }
}
