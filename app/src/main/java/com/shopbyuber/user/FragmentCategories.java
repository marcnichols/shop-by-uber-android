package com.shopbyuber.user;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemSpanLookup;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.GetVendorsFrom;
import com.shopbyuber.model.LocalCategoriesModel;
import com.shopbyuber.model.LocalCategoriesModel.LocalCategories;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.FusedLocationManager;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;
import com.shopbyuber.utils.paginationProgressBarAdapter;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

public class FragmentCategories extends Fragment implements AdapterView.OnItemClickListener,
        OnMapReadyCallback,
        Paginate.Callbacks, SwipeRefreshLayout.OnRefreshListener,
        GoogleMap.OnMarkerClickListener {
    private static final String TAG = FragmentLocal.class.getSimpleName();
    Globals globals;

    @BindView(R.id.list)
    RecyclerView categoriesRecyclerView;

    @BindView(R.id.map_container)
    RelativeLayout map_container;

    @BindView(R.id.view_map_border)
    View view_map_border;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tv_no_record_found)
    TextView tv_no_record_found;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    ImageView imgCategory;

    LocalCategoriesModel catagoryModel;
    LocalCategoriesModel subCategoriesModel;

    private AdapterCategories adapterLocalCategories;
    private ArrayList<LocalCategories> categories;
    GoogleMap mGoogleMap;
    SupportMapFragment mapFragment;

    private Paginate paginate;
    int pageNo = 1;
    private boolean loading = false;

    FusedLocationManager fusedLocationManager;

    private String categoryId = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        fusedLocationManager = new FusedLocationManager(getActivity());
        globals = ((Globals) getActivity().getApplicationContext());
        map_container.setVisibility(View.VISIBLE);
        view_map_border.setVisibility(View.VISIBLE);

        pageNo = 1;
        categories = new ArrayList<>();

        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorToolbarTitle);
        swipeRefreshLayout.setOnRefreshListener(this);

        setupMap();
        dogetCatagoriesRequest(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleMap == null) {
            mapFragment.getMapAsync(this);
        }
    }

    private void setupMap() {
        // add map fragment to container
        FragmentManager fm = getChildFragmentManager();
        mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map_container);
        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.map_container, mapFragment).commit();
        }
    }

    private void showNoRecordFound() {
        loading = false;
        if (categories.isEmpty()) {
            categoriesRecyclerView.setVisibility(View.GONE);
            if (tv_no_record_found.getVisibility() == View.GONE)
                tv_no_record_found.setText(getString(R.string.err_no_category));
            tv_no_record_found.setVisibility(View.VISIBLE);
        }
    }

    private void hideNoRecordFound() {
        categoriesRecyclerView.setVisibility(View.VISIBLE);
        if (tv_no_record_found.getVisibility() == View.VISIBLE)
            tv_no_record_found.setVisibility(View.GONE);
    }

    public void dogetCatagoriesRequest(final boolean showProgress) {
        if (!Globals.internetCheck(getActivity())) {
            showNoRecordFound();
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().getCatagoryParams(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                pageNo);

        HttpRequestHandler.getInstance().post(getActivity(), getString(R.string.getCategories), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                if (!swipeRefreshLayout.isRefreshing() && showProgress)
                    progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (progressBar.getVisibility() == View.VISIBLE)
                    progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                catagoryModel = new Gson().fromJson(response.toString(), LocalCategoriesModel.class);
                if (catagoryModel.Result) {
                    if (swipeRefreshLayout.isRefreshing()) {
                        categories.clear();
                        if (adapterLocalCategories != null)
                            adapterLocalCategories.notifyDataSetChanged();
                    }
                    setupList(catagoryModel.Data.categories);
                } else {
                    showNoRecordFound();
                    if (catagoryModel.Message.contains(getString(R.string.invalid_session))) {
                        Toaster.shortToast(catagoryModel.Message);
                        globals.doLogout(getActivity());
                    }
                }
                stopRefreshing();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                stopRefreshing();
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                stopRefreshing();
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void setupList(ArrayList<LocalCategories> category) {
        if (category != null && !category.isEmpty()) {
            categories.addAll(category);
            setAdapter();
        } else
            showNoRecordFound();
    }

    private void stopRefreshing() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void plotMarkers() {
        if (!categories.isEmpty()) {
            for (LocalCategoriesModel.LocalCategories category : categories) {
                if (!category.companyDatas.isEmpty()) {
                    for (LocalCategoriesModel.companyData companyData : category.companyDatas) {
                        drawMarker(new LatLng(companyData.latitude, companyData.longitude),
                                companyData.companyId);
                    }
                }
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(fusedLocationManager.getLatLng()
                                , 8));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 1000);
        }
    }

    private void drawMarker(LatLng point, String companyId) {
        try {
            // Creating an instance of MarkerOptions
            MarkerOptions markerOptions = new MarkerOptions();

            // Setting latitude and longitude for the marker
            markerOptions.position(point);

            // Adding marker on the Google Map
            Marker marker = mGoogleMap.addMarker(markerOptions);
            marker.setTag(companyId);
            marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.map_green));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        hideNoRecordFound();
        plotMarkers();
        if (adapterLocalCategories == null) {
            if (paginate != null) {
                paginate.unbind();
            }
            adapterLocalCategories = new AdapterCategories(getActivity(), false);
            adapterLocalCategories.setOnItemClickListener(this);
        }
        loading = false;
        adapterLocalCategories.doRefresh(categories);

        if (categoriesRecyclerView.getAdapter() == null) {
            categoriesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            categoriesRecyclerView.setItemAnimator(new DefaultItemAnimator());
            categoriesRecyclerView.setAdapter(adapterLocalCategories);
            if (categories.size() < catagoryModel.Data.recordCount) {
                paginate = Paginate.with(categoriesRecyclerView, this)
                        .setLoadingTriggerThreshold(Constants.progress_threshold_2)
                        .addLoadingListItem(Constants.addLoadingRow)
                        .setLoadingListItemCreator(new paginationProgressBarAdapter())
                        .setLoadingListItemSpanSizeLookup(new LoadingListItemSpanLookup() {
                            @Override
                            public int getSpanSize() {
                                return Constants.GRID_SPAN;
                            }
                        })
                        .build();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Logger.d(TAG, categories.get(position));
        categoryId = categories.get(position)._id;
        dogetSubCatagoriesRequest(true, categories.get(position)._id);

    }


    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        // called when map is ready
        this.mGoogleMap = googleMap;
        this.mGoogleMap.setOnMarkerClickListener(this);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.setMyLocationEnabled(true);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Constants.SBU_LOS_ANGELES, 5));
    }

    @Override
    public void onLoadMore() {
        loading = true;
        pageNo++;
        dogetCatagoriesRequest(false);
    }

    @Override
    public boolean isLoading() {
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return categories.size() == catagoryModel.Data.recordCount;
    }

    @Override
    public void onRefresh() {
        pageNo = 1;
        dogetCatagoriesRequest(true);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        LocalCategoriesModel.companyData data = getMarkerData(String.valueOf(marker.getTag()));
        if (data != null) {
            Company_detail_popup company_detail_popup = new Company_detail_popup(getActivity());
            company_detail_popup.showPopup(data.comapnyScreenName, data.serviceDescription,
                    data.companyProfileImage, data.companyId);
        }
        return false;
    }

    private LocalCategoriesModel.companyData getMarkerData(String companyId) {
        for (LocalCategories localCategory : categories) {
            for (LocalCategoriesModel.companyData data : localCategory.companyDatas) {
                if (data.companyId.equalsIgnoreCase(companyId))
                    return data;
            }
        }
        return null;
    }


    public void showSubCatagoryDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog_layout);
        dialog.setCancelable(true);

        TextView tv_dg_main_title = dialog.findViewById(R.id.tv_dg_main_title);
        tv_dg_main_title.setText(getString(R.string.select_a_sub_category));
        final ListView listview = dialog.findViewById(R.id.listview);
        TextView tv_no_record_found = dialog.findViewById(R.id.tv_no_record_found);
        imgCategory = dialog.findViewById(R.id.img_category);

        if (subCategoriesModel != null && !subCategoriesModel.Data.subCategories.isEmpty()) {

            subCategoriesModel.Data.subCategories.get(0).isAllocate = true;

            final AdapterSubCategoryDialog adapterSubCategoryDialog = new AdapterSubCategoryDialog(subCategoriesModel.Data.subCategories, getActivity());
            listview.setAdapter(adapterSubCategoryDialog);
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override// displaySelectedImage(position, localCategoriesModel);
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    for (int i = 0; i < subCategoriesModel.Data.subCategories.size(); i++) {
                        LocalCategoriesModel.SubCategories categories = subCategoriesModel.Data.subCategories.get(i);
                        categories.isAllocate = i == position;
                    }


                    adapterSubCategoryDialog.notifyDataSetChanged();
                }
            });
        } else {
            listview.setVisibility(View.GONE);
            tv_no_record_found.setVisibility(View.VISIBLE);
        }

        dialog.show();

        Button btn_submit = dialog.findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (subCategoriesModel != null && !subCategoriesModel.Data.subCategories.isEmpty())
                    displaySelectedCategory(getSelectedSubCategory());
                dialog.dismiss();
            }
        });
    }

    private LocalCategoriesModel.SubCategories getSelectedSubCategory() {
        for (int i = 0; i < subCategoriesModel.Data.subCategories.size(); i++) {
            LocalCategoriesModel.SubCategories categories = subCategoriesModel.Data.subCategories.get(i);
            if (categories.isAllocate)
                return categories;
        }
        return null;
    }

    private void displaySelectedImage(int position, final LocalCategoriesModel localCategoriesModel) {
        Glide.with(getActivity())
                .load(localCategoriesModel.Data.categories.get(position).categoryImage)
                .centerCrop()
                .placeholder(R.drawable.image_place_holder)
                .crossFade()
                .dontAnimate()
                .into(imgCategory);
    }

    public void dogetSubCatagoriesRequest(final boolean showProgress, String categoryId) {
        if (!Globals.internetCheck(getActivity())) {
            showNoRecordFound();
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().doSubCategoryParam(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                categoryId, true);

        HttpRequestHandler.getInstance().post(getActivity(), getString(R.string.getSubSubcategories), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                if (!swipeRefreshLayout.isRefreshing() && showProgress)
                    progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (progressBar.getVisibility() == View.VISIBLE)
                    progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                subCategoriesModel = new Gson().fromJson(response.toString(), LocalCategoriesModel.class);
                if (subCategoriesModel.Result) {
                    showSubCatagoryDialog();
                } else {
                    Toaster.shortToast(subCategoriesModel.Message);
                    if (subCategoriesModel.Message.contains(getString(R.string.invalid_session))) {
                        globals.doLogout(getActivity());
                    }
                }
                stopRefreshing();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                stopRefreshing();
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                stopRefreshing();
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void displaySelectedCategory(LocalCategoriesModel.SubCategories subCategories) {
        startActivity(new Intent(getActivity(), SelectedCategoryActivity.class)
                .putExtra(Constants.SBU_CategoryId, categoryId)
                .putExtra(Constants.SBU_SubCategoryId, subCategories._id)
                .putExtra(Constants.SBU_CatName, subCategories.subcatName)
                .putExtra(Constants.SBU_GetVendorFrom, GetVendorsFrom.FROM_CATEGORIES));
    }
}
