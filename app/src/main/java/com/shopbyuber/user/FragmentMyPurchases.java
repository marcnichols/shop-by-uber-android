package com.shopbyuber.user;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemSpanLookup;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.model.MyPurchasesModel;
import com.shopbyuber.model.MyPurchasesModel.Purchases;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;
import com.shopbyuber.utils.paginationProgressBarAdapter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;


public class FragmentMyPurchases extends Fragment implements AdapterView.OnItemClickListener, Paginate.Callbacks, SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = FragmentMyPurchases.class.getSimpleName();
    Globals globals;

    @BindView(R.id.list)
    RecyclerView myPurchasesRecyclerView;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tv_no_record_found)
    TextView tv_no_record_found;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    MyPurchasesModel myPurchasesModel;
    ArrayList<Purchases> arrPurchases;
    AdapterMyPurchases adapterMyPurchases;

    private Paginate paginate;
    int pageNo = 1;
    private boolean loading = false;
    int reqCode = 1;

    public static FragmentMyPurchases mContext;

    public static FragmentMyPurchases getInstance() {
        return mContext;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_purchases, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        mContext = this;
        arrPurchases = new ArrayList<>();
        globals = ((Globals) getActivity().getApplicationContext());

        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorToolbarTitle);
        swipeRefreshLayout.setOnRefreshListener(this);

        doGetMyServicesRequest(true);
    }

    @OnClick(R.id.continue_shopping)
    public void doContinueShopping() {
        UserLandingActivity.getInstance().setToolbarTitle(R.string.purchase_title);
        UserLandingActivity.getInstance().setFragment(new FragmentUserHome());
    }

    private void showNoRecordFound() {
        loading = false;
        if (arrPurchases.isEmpty()) {
            myPurchasesRecyclerView.setVisibility(View.GONE);
            if (tv_no_record_found.getVisibility() == View.GONE)
                tv_no_record_found.setText(getString(R.string.you_have_not_made_any_purchases_yet));
            tv_no_record_found.setVisibility(View.VISIBLE);
        }
    }

    private void hideNoRecordFound() {
        myPurchasesRecyclerView.setVisibility(View.VISIBLE);
        if (tv_no_record_found.getVisibility() == View.VISIBLE) {
            tv_no_record_found.setVisibility(View.GONE);
        }
    }

    public void doGetMyServicesRequest(final boolean showProgress) {
        if (!Globals.internetCheck(getActivity())) {
            showNoRecordFound();
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().getMyServicesParams(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                pageNo);

        HttpRequestHandler.getInstance().post(getActivity(), getString(R.string.getMyPurchases), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                if (!swipeRefreshLayout.isRefreshing() && showProgress)
                    progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (progressBar.getVisibility() == View.VISIBLE)
                    progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                myPurchasesModel = new Gson().fromJson(response.toString(), MyPurchasesModel.class);
                if (myPurchasesModel.Result) {
                    if (swipeRefreshLayout.isRefreshing()) {
                        arrPurchases.clear();
                        if (adapterMyPurchases != null)
                            adapterMyPurchases.notifyDataSetChanged();
                    }
                    setupList(myPurchasesModel.Data.purchases);
                } else {
                    showNoRecordFound();
                    Toaster.shortToast(myPurchasesModel.Message);
                    if (myPurchasesModel.Message.contains(getString(R.string.invalid_session))) {
                        globals.doLogout(getActivity());
                    }
                }
                stopRefreshing();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                stopRefreshing();
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                stopRefreshing();
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void stopRefreshing() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void setupList(List<Purchases> servicesArrayList) {
        if (servicesArrayList != null && !servicesArrayList.isEmpty()) {
            arrPurchases.addAll(servicesArrayList);
            setAdapter();
        } else
            showNoRecordFound();
    }

    private void setAdapter() {
        hideNoRecordFound();
        if (adapterMyPurchases == null) {
            if (paginate != null) {
                paginate.unbind();
            }
            adapterMyPurchases = new AdapterMyPurchases(getActivity());
            adapterMyPurchases.setOnItemClickListener(this);
        }
        loading = false;
        adapterMyPurchases.doRefresh(arrPurchases);

        if (myPurchasesRecyclerView.getAdapter() == null) {
            myPurchasesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            myPurchasesRecyclerView.setItemAnimator(new DefaultItemAnimator());
            myPurchasesRecyclerView.setAdapter(adapterMyPurchases);
            if (arrPurchases.size() < myPurchasesModel.Data.recordCount) {
                paginate = Paginate.with(myPurchasesRecyclerView, this)
                        .setLoadingTriggerThreshold(Constants.progress_threshold_2)
                        .addLoadingListItem(Constants.addLoadingRow)
                        .setLoadingListItemCreator(new paginationProgressBarAdapter())
                        .setLoadingListItemSpanSizeLookup(new LoadingListItemSpanLookup() {
                            @Override
                            public int getSpanSize() {
                                return Constants.GRID_SPAN;
                            }
                        })
                        .build();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Logger.d(TAG, arrPurchases.get(position));
        startActivityForResult(new Intent(getActivity(), OrderDetailActivity.class)
                .putExtra(Constants.SBU_RequestId, arrPurchases.get(position).requestId)
                .putExtra(Constants.SBU_Status, arrPurchases.get(position).status), reqCode);
    }

    @Override
    public void onLoadMore() {
        loading = true;
        pageNo++;
        doGetMyServicesRequest(false);
    }

    @Override
    public boolean isLoading() {
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return arrPurchases.size() == myPurchasesModel.Data.recordCount;
    }

    @Override
    public void onRefresh() {
        pageNo = 1;
        doGetMyServicesRequest(true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == reqCode && resultCode == Activity.RESULT_OK) {
            pageNo = 1;
            doGetMyServicesRequest(true);
        }
    }
}
