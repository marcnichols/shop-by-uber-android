package com.shopbyuber.user;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.RadioButton;

import com.shopbyuber.R;
import com.shopbyuber.model.LocalCategoriesModel;

import java.util.ArrayList;

public class AdapterSubCategoryDialog extends BaseAdapter implements AdapterView.OnItemClickListener {
    private ArrayList<LocalCategoriesModel.SubCategories> categoriesArrayList;
    private Context mContext;

    public AdapterSubCategoryDialog(ArrayList<LocalCategoriesModel.SubCategories> categoriesArrayList, Context mContext) {
        this.categoriesArrayList = categoriesArrayList;
        this.mContext = mContext;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }


    @Override
    public int getCount() {
        return categoriesArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return categoriesArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final AdapterSubCategoryDialog.ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new AdapterSubCategoryDialog.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.custom_dialog_list_item, parent, false);
            viewHolder.radioButton = convertView.findViewById(R.id.radiobutton);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (AdapterSubCategoryDialog.ViewHolder) convertView.getTag();
        }


        viewHolder.radioButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.rbtn_selector, 0);
        viewHolder.radioButton.setText(categoriesArrayList.get(position).subcatName == null ? "" : categoriesArrayList.get(position).subcatName);

        viewHolder.radioButton.setChecked(categoriesArrayList.get(position).isAllocate);

        return convertView;
    }


    // View lookup cache
    private static class ViewHolder {
        RadioButton radioButton;
    }
}
