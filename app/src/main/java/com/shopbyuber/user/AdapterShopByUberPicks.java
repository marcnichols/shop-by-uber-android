package com.shopbyuber.user;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.shopbyuber.R;
import com.shopbyuber.model.ShopByUberPicksModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterShopByUberPicks extends RecyclerView.Adapter<AdapterShopByUberPicks.ViewHolder> {

    private ArrayList<ShopByUberPicksModel.CompanyListing> mValues;
    private final Context mContext;
    private AdapterView.OnItemClickListener onItemClickListener;
    private boolean isFromLocal;

    public AdapterShopByUberPicks(Context context, boolean isFromLocal) {
        mContext = context;
        this.isFromLocal = isFromLocal;
    }

    public void doRefresh(ArrayList<ShopByUberPicksModel.CompanyListing> companyListings) {
        this.mValues = companyListings;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (isFromLocal)
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.local_item, parent, false);
        else
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shopbyuberpicks_item, parent, false);
        return new ViewHolder(view, this);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private AdapterShopByUberPicks mAdapterShopByUberPicks;
        @BindView(R.id.img_profile)
        ImageView img_profile;
        @BindView(R.id.content)
        TextView content;
        @BindView(R.id.detail)
        TextView detail;
        @BindView(R.id.categories)
        TextView categories;

        public ViewHolder(View itemView, AdapterShopByUberPicks adapterShopByUberPicks) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.mAdapterShopByUberPicks = adapterShopByUberPicks;
            itemView.setOnClickListener(this);
        }

        void setDataToView(ShopByUberPicksModel.CompanyListing mItem, ViewHolder viewHolder) {
            viewHolder.content.setText(mItem.comapnyScreenName);
            viewHolder.detail.setVisibility(View.VISIBLE);
            viewHolder.detail.setText(mItem.serviceDescription);
            viewHolder.detail.setVisibility(View.GONE);

            viewHolder.categories.setVisibility(View.VISIBLE);
            viewHolder.categories.setText(mItem.categories);

            Glide.with(mContext)
                    .load(mItem.companyProfileImage)
                    .centerCrop()
                    .placeholder(R.drawable.image_place_holder)
                    .crossFade()
                    .dontAnimate()
                    .into(viewHolder.img_profile);
        }

        @Override
        public void onClick(View v) {
            mAdapterShopByUberPicks.onItemHolderClick(ViewHolder.this);
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        ShopByUberPicksModel.CompanyListing mItem = mValues.get(position);
        holder.setDataToView(mItem, holder);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void onItemHolderClick(ViewHolder holder) {
        if (onItemClickListener != null)
            onItemClickListener.onItemClick(null, holder.itemView, holder.getAdapterPosition(), holder.getItemId());
    }
}
