package com.shopbyuber.user;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.shopbyuber.R;
import com.shopbyuber.utils.Constants;

class Company_detail_popup {
    static Dialog dialog = null;
    private Activity activity;

    public Company_detail_popup(Activity activity) {
        this.activity = activity;
    }

    public void showPopup(final String companyName, String companyDescription,
                          String companyImage, final String companyId) {
        try {
            View v;
            final AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.CustomDialogTransparentTheme);
            v = activity.getLayoutInflater().inflate(R.layout.layout_marker_popup, null);

            TextView name, description, cancel, processed;
            ImageView img_company;

            name = v.findViewById(R.id.name);
            description = v.findViewById(R.id.description);

            cancel = v.findViewById(R.id.cancel);
            processed = v.findViewById(R.id.processed);
            img_company = v.findViewById(R.id.img_company);

            name.setText(companyName);
            description.setText(companyDescription);
            description.setMovementMethod(new ScrollingMovementMethod());

            Glide.with(activity)
                    .load(companyImage)
                    .centerCrop()
                    .placeholder(R.drawable.image_place_holder)
                    .crossFade()
                    .dontAnimate()
                    .into(img_company);

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v1) {
                    dialog.dismiss();
                }
            });
            processed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v1) {
                    dialog.dismiss();
                    activity.startActivity(new Intent(activity, GetPicksCompanyServiceActivity.class)
                            .putExtra(Constants.SBU_CompanyId, companyId)
                            .putExtra(Constants.SBU_CompanyName, companyName));
                }
            });

            builder.setView(v);
            if (dialog != null)
                dialog = null;
            dialog = builder.create();
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
