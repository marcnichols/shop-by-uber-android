package com.shopbyuber.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.paginate.Paginate;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.model.GetPicksCompanyServiceModel;
import com.shopbyuber.model.GetPicksCompanyServiceModel.Reviews;
import com.shopbyuber.model.GetPicksCompanyServiceModel.Services;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.FusedLocationManager;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.PaginationProgressBarListAdapter;
import com.shopbyuber.utils.Toaster;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

public class GetPicksCompanyServiceActivity extends BaseActivity implements Paginate.Callbacks, SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.lv_service_detail)
    ListView lvServiceDetail;

    Globals globals;

    //Footer
    RatingBar ratingBar;
    LinearLayout layoutListReview;

    String companyId = "", companyName = "";
    private int positionForAddress;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tv_no_record_found)
    TextView tv_no_record_found;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private Paginate paginate;
    int pageNo = 1;
    private boolean loading = false;

    private GetPicksCompanyServiceModel getPicksCompanyServiceModel;
    private ArrayList<GetPicksCompanyServiceModel.Services> servicesArrayList;
    private AdapterServicePicks adapter;
    View footerView;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 548;

    View layout2;

    private FusedLocationManager fusedLocationManager;

    public static GetPicksCompanyServiceActivity mContext;

    public static GetPicksCompanyServiceActivity getInstance() {
        return mContext;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_picks_service);
        ButterKnife.bind(this);

        init();
    }

    private void init() {
        mContext = this;

        fusedLocationManager = new FusedLocationManager(mContext);
        fusedLocationManager.setAddress();

        globals = (Globals) getApplicationContext();
        if (getIntent() != null) {
            companyId = getIntent().getStringExtra(Constants.SBU_CompanyId);
            companyName = getIntent().getStringExtra(Constants.SBU_CompanyName);

            if (getIntent().getExtras().containsKey(Constants.SBU_NotificationId) && Globals.isNetworkAvailable(this)) {
                globals.doMakeNotificationUnread(getIntent().getExtras().getString(Constants.SBU_NotificationId));
            }
        }

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(companyName);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetPicksCompanyServiceActivity.this.onBackPressed();
            }
        });
        footerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.layout_review_rating, null, false);

        lvServiceDetail.addFooterView(footerView);
        ratingBar = footerView.findViewById(R.id.ratingBar);
        layoutListReview = footerView.findViewById(R.id.layout_list_review);

        pageNo = 1;
        servicesArrayList = new ArrayList<>();

        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorToolbarTitle);
        swipeRefreshLayout.setOnRefreshListener(this);
        getPicksCompanyService(true);
    }

    public void getPicksCompanyService(final boolean showProgress) {
        if (!Globals.internetCheck(GetPicksCompanyServiceActivity.this)) {
            showNoRecordFound(getString(R.string.no_data_found_ez_list_picks));
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().getPicksCompanyServiceParam(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                companyId,
                pageNo);

        HttpRequestHandler.getInstance().post(GetPicksCompanyServiceActivity.this, getString(R.string.getPicksCompanyService), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                if (!swipeRefreshLayout.isRefreshing() && showProgress)
                    progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (progressBar.getVisibility() == View.VISIBLE)
                    progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                getPicksCompanyServiceModel = new Gson().fromJson(response.toString(), GetPicksCompanyServiceModel.class);
                if (getPicksCompanyServiceModel.Result) {
                    if (swipeRefreshLayout.isRefreshing()) {
                        servicesArrayList.clear();
                        getPicksCompanyServiceModel.Data.reviews.clear();
                        if (adapter != null) adapter.notifyDataSetChanged();
                    }
                    setupList(getPicksCompanyServiceModel.Data.services);

                } else {
                    showNoRecordFound(getPicksCompanyServiceModel.Message);
                    Toaster.shortToast(getPicksCompanyServiceModel.Message);
                    if (getPicksCompanyServiceModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(GetPicksCompanyServiceActivity.this);
                }
                stopRefreshing();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                showNoRecordFound(getString(R.string.no_data_found_ez_list_picks));
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                showNoRecordFound(getString(R.string.no_data_found_ez_list_picks));
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void stopRefreshing() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void setupList(ArrayList<Services> localCategory) {
        if (localCategory != null && !localCategory.isEmpty()) {
            hideNoRecordFound();
            servicesArrayList.addAll(localCategory);
            setData(getPicksCompanyServiceModel);

        } else
            showNoRecordFound(getPicksCompanyServiceModel.Message);
    }

    public void setData(GetPicksCompanyServiceModel data) {
        ratingBar.setRating(data.Data.overAllRating);

        addReviewLayout(data.Data.reviews);
        setAdapter(data);
    }

    private void addReviewLayout(List<Reviews> serviceReviews) {

        for (int i = 0; i < serviceReviews.size(); i++) {
            layout2 = LayoutInflater.from(this).inflate(R.layout.review_list_item, layoutListReview, false);

            TextView tv_date = layout2.findViewById(R.id.tv_date);
            TextView tv_review = layout2.findViewById(R.id.tv_review);
            TextView tv_given_user = layout2.findViewById(R.id.tv_given_user);

            tv_date.setText(serviceReviews.get(i).feedbackDateTime != null && !serviceReviews.get(i).feedbackDateTime.isEmpty() ? serviceReviews.get(i).feedbackDateTime : "");
            tv_review.setText(serviceReviews.get(i).feedback != null && !serviceReviews.get(i).feedback.isEmpty() ? serviceReviews.get(i).feedback : "");
            tv_given_user.setText(serviceReviews.get(i).givenByUser != null && !serviceReviews.get(i).givenByUser.isEmpty() ? serviceReviews.get(i).givenByUser : "");
            layoutListReview.addView(layout2);
        }
    }

    public void getAddressFromMap(int position) throws GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException {
        positionForAddress = position;
        Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                .build(this);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                getPicksCompanyServiceModel.Data.services.get(positionForAddress).address = place.getAddress().toString();
                getPicksCompanyServiceModel.Data.services.get(positionForAddress).latitude = place.getLatLng().latitude;
                getPicksCompanyServiceModel.Data.services.get(positionForAddress).longitude = place.getLatLng().longitude;
                adapter.notifyDataSetChanged();
            }
        }
    }

    private void showNoRecordFound(String message) {
        loading = false;
        if (servicesArrayList.isEmpty()) {
            lvServiceDetail.setVisibility(View.GONE);
            if (tv_no_record_found.getVisibility() == View.GONE)
                tv_no_record_found.setText(message);
            tv_no_record_found.setVisibility(View.VISIBLE);
        }
    }

    private void hideNoRecordFound() {
        lvServiceDetail.setVisibility(View.VISIBLE);
        if (tv_no_record_found.getVisibility() == View.VISIBLE)
            tv_no_record_found.setVisibility(View.GONE);
    }

    private void setAdapter(GetPicksCompanyServiceModel data) {
        hideNoRecordFound();
        if (adapter == null) {
            adapter = new AdapterServicePicks(data.Data, this, fusedLocationManager);
            if (paginate != null) {
                paginate.unbind();
            }
        }
        loading = false;

        if (lvServiceDetail.getAdapter() == null) {
            lvServiceDetail.setAdapter(adapter);
            if (servicesArrayList.size() < getPicksCompanyServiceModel.Data.services.size()) {
                // initialize paginate
                paginate = Paginate.with(lvServiceDetail, this)
                        .setLoadingTriggerThreshold(Constants.progress_threshold_2)
                        .addLoadingListItem(Constants.addLoadingRow)
                        .setLoadingListItemCreator(new PaginationProgressBarListAdapter())
                        .build();
            }
        }
    }

    @Override
    public void onRefresh() {
        pageNo = 1;
        getPicksCompanyService(true);
    }

    @Override
    public void onLoadMore() {
        loading = true;
        pageNo++;
        getPicksCompanyService(false);
    }

    @Override
    public boolean isLoading() {
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return servicesArrayList.size() == getPicksCompanyServiceModel.Data.recordCount;
    }
}
