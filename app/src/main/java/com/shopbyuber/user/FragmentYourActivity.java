package com.shopbyuber.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemSpanLookup;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.ServiceStatus;
import com.shopbyuber.model.NotificationModel;
import com.shopbyuber.model.NotificationModel.Notifications;
import com.shopbyuber.seller.SellerLocationUpdateActivity;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;
import com.shopbyuber.utils.YourActivityEvent;
import com.shopbyuber.utils.paginationProgressBarAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

public class FragmentYourActivity extends Fragment implements AdapterView.OnItemClickListener,
        SwipeRefreshLayout.OnRefreshListener, Paginate.Callbacks {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.rcycler_notification)
    RecyclerView recyclerYourActivity;

    @BindView(R.id.layout_main)
    LinearLayout layout_main;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tv_no_record_found)
    TextView tv_no_record_found;

    Globals globals;
    private Paginate paginate;
    int pageNo = 1;
    private boolean loading = false;

    private NotificationModel notificationModel;
    private ArrayList<Notifications> notificationModelArrayList;
    private AdapterServiceNotification adapterServiceNotification;

    ServiceStatus serviceStatus;

    public static FragmentYourActivity mContext;

    public static FragmentYourActivity getInstance() {
        return mContext;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_notification, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }


    private void init() {
        globals = ((Globals) getActivity().getApplicationContext());

        pageNo = 1;
        notificationModelArrayList = new ArrayList<>();

        layout_main.setBackgroundColor(getResources().getColor(R.color.white));

        toolbar.setVisibility(View.GONE);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorToolbarTitle);
        swipeRefreshLayout.setOnRefreshListener(this);

        dogetServiceNotificion(true);
    }


    private void showNoRecordFound() {
        loading = false;
        if (notificationModelArrayList.isEmpty()) {
            recyclerYourActivity.setVisibility(View.GONE);
            if (tv_no_record_found.getVisibility() == View.GONE) {
                tv_no_record_found.setText(getString(R.string.err_no_activity));
                tv_no_record_found.setVisibility(View.VISIBLE);
            }
        }
    }

    private void hideNoRecordFound() {
        recyclerYourActivity.setVisibility(View.VISIBLE);
        if (tv_no_record_found.getVisibility() == View.VISIBLE)
            tv_no_record_found.setVisibility(View.GONE);
    }

    public void dogetServiceNotificion(final boolean showProgress) {
        if (!Globals.internetCheck(getActivity())) {
            showNoRecordFound();
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().getNotificationList(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                pageNo);

        HttpRequestHandler.getInstance().post(getActivity(), getResources().getString(R.string.yourActivity), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                if (!swipeRefreshLayout.isRefreshing() && showProgress)
                    progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (progressBar.getVisibility() == View.VISIBLE)
                    progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                notificationModel = new Gson().fromJson(response.toString(), NotificationModel.class);
                if (notificationModel.Result) {
                    if (swipeRefreshLayout.isRefreshing()) {
                        notificationModelArrayList.clear();
                        if (adapterServiceNotification != null)
                            adapterServiceNotification.notifyDataSetChanged();
                    }
                    setupList(notificationModel.Data.activities);
                } else {
                    showNoRecordFound();
                    if (notificationModel.Message.contains(getString(R.string.invalid_session))) {
                        Toaster.shortToast(notificationModel.Message);
                        globals.doLogout(getActivity());
                    }
                }
                stopRefreshing();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                stopRefreshing();
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                stopRefreshing();
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void stopRefreshing() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void setupList(ArrayList<Notifications> notificationsArrayList) {
        if (notificationsArrayList != null && !notificationsArrayList.isEmpty()) {
            notificationModelArrayList.addAll(notificationsArrayList);
            setAdapter();
        } else {
            notificationModel.Data.recordCount = notificationModelArrayList.size();
            showNoRecordFound();
            if (adapterServiceNotification != null)
                adapterServiceNotification.notifyDataSetChanged();
        }
    }

    private void setAdapter() {
        hideNoRecordFound();

        if (adapterServiceNotification == null) {
            adapterServiceNotification = new AdapterServiceNotification(getActivity(), false);
            adapterServiceNotification.setOnItemClickListener(this);
            if (paginate != null) {
                paginate.unbind();
            }
        }
        loading = false;
        adapterServiceNotification.doRefresh(notificationModelArrayList);

        if (recyclerYourActivity.getAdapter() == null) {
            recyclerYourActivity.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerYourActivity.setItemAnimator(new DefaultItemAnimator());
            recyclerYourActivity.setAdapter(adapterServiceNotification);
            if (notificationModelArrayList.size() < notificationModel.Data.recordCount) {
                // initialize paginate
                paginate = Paginate.with(recyclerYourActivity, this)
                        .setLoadingTriggerThreshold(Constants.progress_threshold_2)
                        .addLoadingListItem(Constants.addLoadingRow)
                        .setLoadingListItemCreator(new paginationProgressBarAdapter())
                        .setLoadingListItemSpanSizeLookup(new LoadingListItemSpanLookup() {
                            @Override
                            public int getSpanSize() {
                                return Constants.GRID_SPAN;
                            }
                        })
                        .build();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        doRedirection(notificationModelArrayList.get(position));
    }

    private void doRedirection(Notifications notification) {
        //redirection based on service status
        serviceStatus = ServiceStatus.getEnum(notification.status);
        Intent intent = null;
        switch (serviceStatus) {
            case Requested:
                intent = new Intent(getActivity(), OrderDetailActivity.class);
                intent.putExtra(Constants.SBU_RequestId, notification.requestId);
                break;
            case Confirmed:
                intent = new Intent(getActivity(), PaymentConfirmationActivity.class);
                break;
            case PayDone:
            case OnRouteToClient:
                ArrayList<String> createdPubChannelNames = new ArrayList<>();
                createdPubChannelNames.add(notification.requestId);
                intent = new Intent(getActivity(), SellerLocationUpdateActivity.class);
                intent.putStringArrayListExtra(Constants.SBU_CreatedPubChannelNames, createdPubChannelNames);
                break;
            case StartService:
            case Completed:
            case Cancelled:
                intent = new Intent(getActivity(), OrderDetailActivity.class);
                intent.putExtra(Constants.SBU_RequestId, notification.requestId);
                break;
            default:
                break;
        }
        if (intent != null)
            startActivity(intent);
    }

    @Override
    public void onLoadMore() {
        loading = true;
        pageNo++;
        dogetServiceNotificion(false);
    }

    @Override
    public boolean isLoading() {
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return notificationModelArrayList.size() == notificationModel.Data.recordCount;
    }

    @Override
    public void onRefresh() {
        pageNo = 1;
        dogetServiceNotificion(true);
    }

    @Override
    public void onStart() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onStart();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(YourActivityEvent event) {
        //event bus refresh screen
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(true);
        if (notificationModelArrayList != null && !notificationModelArrayList.isEmpty())
            notificationModelArrayList.clear();
        onRefresh();
    }
}
