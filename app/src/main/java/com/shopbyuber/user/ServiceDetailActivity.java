package com.shopbyuber.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.UserRole;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.model.CompanyServiceDetailModel;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.seller.SellerLandingActivity;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.FusedLocationManager;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;

public class ServiceDetailActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.lv_service_detail)
    ListView lvServiceDetail;

    @BindView(R.id.tv_no_record_found)
    TextView tv_no_record_found;

    //Header
    ImageView imgProfile;
    TextView tvContent, tv_header_vendor_location;
    TextView tvDetail, tvAvailable;
    RadioButton rbFavourite;

    //Footer
    RatingBar ratingBar;
    LinearLayout layoutListReview;

    Globals globals;

    AdapterServiceDetail adapter;
    View footerView, headerView;
    String categoryId = "", companyId = "", subCategoryId = "", serviceId = "", requestId = "";

    CompanyServiceDetailModel serviceDetailModel;
    private int positionForAddress;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 548;

    private FusedLocationManager fusedLocationManager;

    public static ServiceDetailActivity mContext;

    public static ServiceDetailActivity getInstance() {
        return mContext;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_detail);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        mContext = this;

        fusedLocationManager = new FusedLocationManager(mContext);
        fusedLocationManager.setAddress();

        globals = (Globals) getApplicationContext();
        if (getIntent() != null) {
            companyId = getIntent().getStringExtra(Constants.SBU_CompanyId);
            categoryId = getIntent().getStringExtra(Constants.SBU_CategoryId);
            subCategoryId = getIntent().getStringExtra(Constants.SBU_SubCategoryId);
            if (getIntent().getExtras().containsKey(Constants.SBU_NotificationId) && Globals.isNetworkAvailable(this)) {
                globals.doMakeNotificationUnread(getIntent().getExtras().getString(Constants.SBU_NotificationId));
            }
            if (getIntent().getExtras().containsKey(Constants.SBU_ServiceId)) {
                serviceId = getIntent().getExtras().getString(Constants.SBU_ServiceId);
            }
            if (getIntent().getExtras().containsKey(Constants.SBU_RequestId)) {
                requestId = getIntent().getExtras().getString(Constants.SBU_RequestId);
            }
        }

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        headerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.service_detail_header_layout, null, false);
        footerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.layout_review_rating, null, false);

        lvServiceDetail.addHeaderView(headerView);
        lvServiceDetail.addFooterView(footerView);

        imgProfile = headerView.findViewById(R.id.img_profile);
        tvContent = headerView.findViewById(R.id.tv_content);
        tv_header_vendor_location = headerView.findViewById(R.id.tv_header_vendor_location);

        tvDetail = headerView.findViewById(R.id.tv_detail);
        tvDetail.setVisibility(View.VISIBLE);
        tvAvailable = headerView.findViewById(R.id.tv_available);
        rbFavourite = headerView.findViewById(R.id.rb_favourite);

        ratingBar = footerView.findViewById(R.id.ratingBar);
        layoutListReview = footerView.findViewById(R.id.layout_list_review);

        doGetCompanyServiceRequest();
    }

    @Override
    public void onBackPressed() {
        if (isTaskRoot()) {
            if (globals.getUserData().Data.userRole == UserRole.BUYER.getRole())
                startActivity(new Intent(this, UserLandingActivity.class));
            else
                startActivity(new Intent(this, SellerLandingActivity.class));
            finish();
        } else
            super.onBackPressed();
    }

    public void doGetCompanyServiceRequest() {
        if (!Globals.internetCheck(ServiceDetailActivity.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getCompanyServiceParams(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                companyId,
                categoryId, subCategoryId, serviceId);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(ServiceDetailActivity.this);

        HttpRequestHandler.getInstance().post(ServiceDetailActivity.this, getString(R.string.getCompanyService)
                , params
                , new JsonHttpResponseHandler() {

                    @Override
                    public void onStart() {
                        super.onStart();
                        dialog.show();
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        if (dialog != null && dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Logger.json(response.toString());
                        serviceDetailModel = new Gson().fromJson(response.toString(), CompanyServiceDetailModel.class);
                        if (serviceDetailModel.Result) {
                            setData(serviceDetailModel);
                        } else {
                            showNoRecordFound(serviceDetailModel.Message);
                            Toaster.shortToast(serviceDetailModel.Message);
                            if (serviceDetailModel.Message.contains(getString(R.string.invalid_session)))
                                globals.doLogout(ServiceDetailActivity.this);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        showNoRecordFound(getString(R.string.no_data_found_ez_list_picks));
                        Toaster.shortToast(R.string.error_went_wrong);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        showNoRecordFound(getString(R.string.no_data_found_ez_list_picks));
                        Toaster.shortToast(R.string.error_went_wrong);
                    }
                });
    }

    public void setData(CompanyServiceDetailModel data) {
        hideNoRecordFound();
        Glide.with(this)
                .load(data.Data.companyInfo.companyProfileImage)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .centerCrop()
                .placeholder(R.drawable.image_place_holder)
                .crossFade()
                .dontAnimate()
                .into(imgProfile);

        rbFavourite.setChecked(data.Data.companyInfo.isFavourite);

        tvContent.setText(data.Data.companyInfo.comapnyScreenName);
        if (data.Data.companyInfo.companyAddress.isEmpty()) {
            tv_header_vendor_location.setVisibility(View.GONE);
            tv_header_vendor_location.setText(data.Data.companyInfo.companyAddress.isEmpty() ? "" : data.Data.companyInfo.companyAddress);
        } else {
            tv_header_vendor_location.setVisibility(View.VISIBLE);
            tv_header_vendor_location.setText(data.Data.companyInfo.companyAddress);
        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(data.Data.companyInfo.comapnyScreenName);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        tvDetail.setText(data.Data.companyInfo.catName);
        tvAvailable.setText(data.Data.companyInfo.isAvailable ? getString(R.string.available_now) : getString(R.string.available_by_appointment));
        ratingBar.setRating(data.Data.overAllRating);

        addReviewLayout(data.Data.reviews);

        adapter = new AdapterServiceDetail(data.Data, this, fusedLocationManager, requestId);
        lvServiceDetail.setAdapter(adapter);

        rbFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServiceDetailActivity.this.doAddToFavoriteServiceRequest();
            }
        });
    }

    private void addReviewLayout(List<CompanyServiceDetailModel.Reviews> serviceReviews) {
        for (int i = 0; i < serviceReviews.size(); i++) {
            View layout2 = LayoutInflater.from(this).inflate(R.layout.review_list_item, layoutListReview, false);

            TextView tv_date = layout2.findViewById(R.id.tv_date);
            TextView tv_review = layout2.findViewById(R.id.tv_review);
            TextView tv_given_user = layout2.findViewById(R.id.tv_given_user);


            tv_date.setText(serviceReviews.get(i).feedbackDateTime != null && !serviceReviews.get(i).feedbackDateTime.isEmpty() ? serviceReviews.get(i).feedbackDateTime : "");
            tv_review.setText(serviceReviews.get(i).Feedback != null && !serviceReviews.get(i).Feedback.isEmpty() ? serviceReviews.get(i).Feedback : "");
            tv_given_user.setText(serviceReviews.get(i).givenByUser != null && !serviceReviews.get(i).givenByUser.isEmpty() ? serviceReviews.get(i).givenByUser : "");
            layoutListReview.addView(layout2);
        }
    }

    public void doAddToFavoriteServiceRequest() {
        if (!Globals.internetCheck(this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().AddToFavoriteCompanyParams(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                companyId);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(this);

        HttpRequestHandler.getInstance().post(this, getString(R.string.addToFavoriteCompany), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel.Result) {
                    serviceDetailModel.Data.companyInfo.isFavourite = !serviceDetailModel.Data.companyInfo.isFavourite;
                    rbFavourite.setChecked(serviceDetailModel.Data.companyInfo.isFavourite);
                } else {
                    if (dataModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(ServiceDetailActivity.this);
                }
                Toaster.shortToast(dataModel.Message);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_search:
                return true;
            case R.id.action_cart:
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            companyId = bundle.getString(Constants.SBU_CompanyId);
            categoryId = bundle.getString(Constants.SBU_CategoryId);
            subCategoryId = bundle.getString(Constants.SBU_SubCategoryId);
            doGetCompanyServiceRequest();
        }
    }

    public void getAddressFromMap(int position) throws GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException {
        positionForAddress = position;
        Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                .build(this);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                serviceDetailModel.Data.services.get(positionForAddress).address = place.getAddress().toString();
                serviceDetailModel.Data.services.get(positionForAddress).latitude = place.getLatLng().latitude;
                serviceDetailModel.Data.services.get(positionForAddress).longitude = place.getLatLng().longitude;
                adapter.notifyDataSetChanged();
            }
        }
    }

    private void showNoRecordFound(String message) {
        if (serviceDetailModel == null || serviceDetailModel.Data == null ||
                serviceDetailModel.Data.services.isEmpty()) {
            lvServiceDetail.setVisibility(View.GONE);
            if (tv_no_record_found.getVisibility() == View.GONE)
                tv_no_record_found.setText(message);
            tv_no_record_found.setVisibility(View.VISIBLE);
        }
    }

    private void hideNoRecordFound() {
        lvServiceDetail.setVisibility(View.VISIBLE);
        if (tv_no_record_found.getVisibility() == View.VISIBLE)
            tv_no_record_found.setVisibility(View.GONE);
    }
}
