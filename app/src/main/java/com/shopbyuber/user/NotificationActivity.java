package com.shopbyuber.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemSpanLookup;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.NotificationTypeBuyer;
import com.shopbyuber.enums.NotificationTypeSeller;
import com.shopbyuber.enums.UserRole;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.model.NotificationModel;
import com.shopbyuber.model.NotificationModel.Notifications;
import com.shopbyuber.seller.SellerLocationUpdateActivity;
import com.shopbyuber.seller.ViewRequestActivity;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.NotificationCountHolder;
import com.shopbyuber.utils.Toaster;
import com.shopbyuber.utils.paginationProgressBarAdapter;
import com.shopbyuber.wantedservice.WantedServiceDetailActivity;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

public class NotificationActivity extends BaseActivity implements AdapterView.OnItemClickListener,
        Paginate.Callbacks, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.rcycler_notification)
    RecyclerView rcyclerNotification;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tv_no_record_found)
    TextView tv_no_record_found;

    Globals globals;
    private Paginate paginate;
    int pageNo = 1;
    private boolean loading = false;

    private NotificationModel notificationModel;
    private ArrayList<Notifications> notificationModelArrayList;
    private AdapterServiceNotification adapterServiceNotification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        globals = ((Globals) getApplicationContext());

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.notifications);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

        }

        pageNo = 1;
        notificationModelArrayList = new ArrayList<>();

        swipeRefreshLayout.setColorSchemeResources(R.color.colorToolbarTitle);
        swipeRefreshLayout.setOnRefreshListener(this);

        dogetServiceNotificion(true);
    }

    private void showNoRecordFound() {
        loading = false;
        if (notificationModelArrayList.isEmpty()) {
            rcyclerNotification.setVisibility(View.GONE);
            if (tv_no_record_found.getVisibility() == View.GONE)
                tv_no_record_found.setText(getString(R.string.you_do_not_have_any_current_notifications));
            tv_no_record_found.setVisibility(View.VISIBLE);
        }
    }

    private void hideNoRecordFound() {
        rcyclerNotification.setVisibility(View.VISIBLE);
        if (tv_no_record_found.getVisibility() == View.VISIBLE)
            tv_no_record_found.setVisibility(View.GONE);
    }

    public void dogetServiceNotificion(final boolean showProgress) {
        if (!Globals.internetCheck(this)) {
            showNoRecordFound();
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().getNotificationList(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                pageNo);

        HttpRequestHandler.getInstance().post(this, getString(R.string.getNotificationList), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                if (!swipeRefreshLayout.isRefreshing() && showProgress)
                    progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (progressBar.getVisibility() == View.VISIBLE)
                    progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                notificationModel = new Gson().fromJson(response.toString(), NotificationModel.class);
                if (notificationModel.Result) {
                    if (swipeRefreshLayout.isRefreshing()) {
                        notificationModelArrayList.clear();
                        if (adapterServiceNotification != null)
                            adapterServiceNotification.notifyDataSetChanged();
                    }
                    setupList(notificationModel.Data.notifications);
                } else {
                    showNoRecordFound();
                    Toaster.shortToast(notificationModel.Message);
                    if (notificationModel.Message.contains(getString(R.string.invalid_session))) {
                        globals.doLogout(NotificationActivity.this);
                    }
                }
                stopRefreshing();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                stopRefreshing();
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                stopRefreshing();
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void stopRefreshing() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void setupList(ArrayList<Notifications> notificationsArrayList) {
        if (notificationsArrayList != null && !notificationsArrayList.isEmpty()) {
            notificationModelArrayList.addAll(notificationsArrayList);
            setAdapter();
        } else {
            notificationModel.Data.recordCount = notificationModelArrayList.size();
            showNoRecordFound();
            if (adapterServiceNotification != null)
                adapterServiceNotification.notifyDataSetChanged();
        }
    }

    private void setAdapter() {
        hideNoRecordFound();

        if (adapterServiceNotification == null) {
            adapterServiceNotification = new AdapterServiceNotification(this, true);
            adapterServiceNotification.setOnItemClickListener(this);
            if (paginate != null) {
                paginate.unbind();
            }
        }
        loading = false;
        adapterServiceNotification.doRefresh(notificationModelArrayList);

        if (rcyclerNotification.getAdapter() == null) {
            rcyclerNotification.setLayoutManager(new LinearLayoutManager(this));
            rcyclerNotification.setItemAnimator(new DefaultItemAnimator());
            rcyclerNotification.setAdapter(adapterServiceNotification);
            if (notificationModelArrayList.size() < notificationModel.Data.recordCount) {
                // initialize paginate
                paginate = Paginate.with(rcyclerNotification, this)
                        .setLoadingTriggerThreshold(Constants.progress_threshold_2)
                        .addLoadingListItem(Constants.addLoadingRow)
                        .setLoadingListItemCreator(new paginationProgressBarAdapter())
                        .setLoadingListItemSpanSizeLookup(new LoadingListItemSpanLookup() {
                            @Override
                            public int getSpanSize() {
                                return Constants.GRID_SPAN;
                            }
                        })
                        .build();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (!notificationModelArrayList.get(position).isRead) {
            if (Globals.isNetworkAvailable(this)) {
                globals.doMakeNotificationUnread(notificationModelArrayList.get(position).notificationId);
            }
            notificationModelArrayList.get(position).isRead = true;
            NotificationCountHolder.getInstance().setUnreads(NotificationCountHolder.getInstance().getUnreads() - 1);
            adapterServiceNotification.notifyDataSetChanged();
        }
        doRedirection(notificationModelArrayList.get(position));
    }

    private void doRedirection(Notifications notification) {
        //manage redirection as per notification type
        Intent intent = null;
        if (globals.getUserData().Data.userRole == UserRole.BUYER.getRole()) {
            switch (NotificationTypeBuyer.fromInt(notification.notifyId)) {
                case Vendor_has_confirmed_your_request:
                    intent = new Intent(this, PaymentConfirmationActivity.class);
                    break;
                case Vendor_is_on_route_to_your_location:
                    ArrayList<String> createdPubChannelNames = new ArrayList<>();
                    createdPubChannelNames.add(notification.requestId);
                    intent = new Intent(this, SellerLocationUpdateActivity.class);
                    intent.putStringArrayListExtra(Constants.SBU_CreatedPubChannelNames, createdPubChannelNames);
                    break;
                case User_has_made_request:
                case Vendor_has_arrived_and_your_service_has_been_processed:
                case Service_has_been_completed:
                case Vendor_has_cancelled_request: {
                    intent = new Intent(this, OrderDetailActivity.class);
                    intent.putExtra(Constants.SBU_RequestId, notification.requestId);
                    break;
                }
                case I_can_review_my_recent_purchases:
                    break;
                case New_app_features_are_available:
                    break;
                case There_are_items_left_in_my_cart:
                    break;
                case New_updates_in_my_favourite_service:
                    intent = new Intent(this, ServiceDetailActivity.class);
                    intent.putExtra(Constants.SBU_CompanyId, notification.companyId);
                    intent.putExtra(Constants.SBU_CategoryId, notification.categoryId);
                    break;
                case Vendor_create_wanted_service:
                    intent = new Intent(this, ServiceDetailActivity.class);
                    intent.putExtra(Constants.SBU_CompanyId, notification.companyId);
                    intent.putExtra(Constants.SBU_CategoryId, notification.categoryId);
                    intent.putExtra(Constants.SBU_SubCategoryId, notification.subCategoryId);
                    intent.putExtra(Constants.SBU_ServiceId, notification.serviceId);
                    intent.putExtra(Constants.SBU_RequestId, notification.requestId);
                    break;
                default:
                    break;
            }
        } else if (globals.getUserData().Data.userRole == UserRole.SELLER.getRole()) {
            switch (NotificationTypeSeller.fromInt(notification.notifyId)) {
                case User_has_made_request:
                case User_has_made_payment_for_confirmed_service:
                case User_has_canceled_request_for_confirmed_service: {
                    intent = new Intent(this, ViewRequestActivity.class);
                    intent.putExtra(Constants.SBU_RequestId, notification.requestId);
                    break;
                }
                case New_app_features_are_available:
                    break;
                case User_has_request_wanted_service:
                    intent = new Intent(this, WantedServiceDetailActivity.class)
                            .putExtra(Constants.SBU_RequestId, notification.requestId)
                            .putExtra(Constants.SBU_GetAll, true);
                    break;
                default:
                    break;
            }
        }
        if (intent != null)
            startActivity(intent);
    }

    @Override
    public void onLoadMore() {
        loading = true;
        pageNo++;
        dogetServiceNotificion(false);
    }

    @Override
    public boolean isLoading() {
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return notificationModelArrayList.size() == notificationModel.Data.recordCount;
    }

    @Override
    public void onRefresh() {
        pageNo = 1;
        dogetServiceNotificion(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
