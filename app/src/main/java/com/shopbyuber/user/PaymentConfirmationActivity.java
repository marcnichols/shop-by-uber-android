
package com.shopbyuber.user;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.UserRole;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.model.ConfirmRequestModel;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.model.PayPalResponseModel;
import com.shopbyuber.seller.SellerLandingActivity;
import com.shopbyuber.seller.SellerLocationUpdateActivity;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;

public class PaymentConfirmationActivity extends BaseActivity {
    private static final String TAG = PaymentConfirmationActivity.class.getName();
    private static final String TAG_PAYPAL = "PayPal : ";
    private static final int CODE_PAYPAL = 1;

    @BindView(R.id.tv_cart_empty)
    TextView tv_cart_empty;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.lv_payment_confirm)
    ListView lvPaymentConfirm;
    @BindView(R.id.btn_con_shop)
    TextView btn_con_shop;

    private PayPalConfiguration config;
    Globals globals;
    private ConfirmRequestModel confirmRequestModel;
    private ConfirmRequestAdapter adapter;
    double total;

    View footerView;
    Button btnConfirmPayment;
    private int positionForAddress;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 548;

    int REQUEST_CODE_FOR_STRIPE_PAYMENT = 2;
    public static PaymentConfirmationActivity mContext;
    String PayKey;

    public static PaymentConfirmationActivity getInstance() {
        return mContext;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_confirmation);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        mContext = this;
        globals = (Globals) getApplicationContext();

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.shopping_cart);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaymentConfirmationActivity.this.onBackPressed();
            }
        });

        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey(Constants.SBU_NotificationId) && Globals.isNetworkAvailable(this)) {
            globals.doMakeNotificationUnread(bundle.getString(Constants.SBU_NotificationId));
        }

        footerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_confirm_detail_layout, null);
        lvPaymentConfirm.addFooterView(footerView);
        btnConfirmPayment = footerView.findViewById(R.id.btn_pay_service);

        doGetCompanyServiceRequest();
    }

    @Override
    public void onBackPressed() {
        if (isTaskRoot()) {
            if (globals.getUserData().Data.userRole == UserRole.BUYER.getRole())
                startActivity(new Intent(this, UserLandingActivity.class));
            else
                startActivity(new Intent(this, SellerLandingActivity.class));
            finish();
        } else
            super.onBackPressed();
    }

    public void doGetCompanyServiceRequest() {
        if (!Globals.internetCheck(PaymentConfirmationActivity.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getConfirmServiceDetails(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(PaymentConfirmationActivity.this);

        HttpRequestHandler.getInstance().post(PaymentConfirmationActivity.this, getString(R.string.getConfirmRequestData), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                confirmRequestModel = new Gson().fromJson(response.toString(), ConfirmRequestModel.class);
                if (confirmRequestModel.Result) {
                    Constants.SBU_StripeKey = confirmRequestModel.Data.payment_configuration.key;
                    Constants.SBU_StripeScreteKey = confirmRequestModel.Data.payment_configuration.secret;
                    setupList();
                } else {
                    showNoRecordFound();
                    if (confirmRequestModel.Message.contains(getString(R.string.invalid_session))) {
                        Toaster.shortToast(confirmRequestModel.Message);
                        globals.doLogout(PaymentConfirmationActivity.this);
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
                showNoRecordFound();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
                showNoRecordFound();
            }
        });
    }

    private void setupList() {
        if (confirmRequestModel == null || confirmRequestModel.Data.confirmServices.isEmpty())
            showNoRecordFound();
        else
            setData(confirmRequestModel);
    }

    private void showNoRecordFound() {
        tv_cart_empty.setText(getString(R.string.your_shopping_cart_is_empty));
        tv_cart_empty.setVisibility(View.VISIBLE);
    }

    private void hideNoRecordFound() {
        tv_cart_empty.setVisibility(View.GONE);
    }

    public void setData(ConfirmRequestModel data) {
        hideNoRecordFound();

        adapter = new ConfirmRequestAdapter(data.Data.confirmServices, this);
        lvPaymentConfirm.setAdapter(adapter);

        btnConfirmPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                total = 0;
                for (int i = 0; i < confirmRequestModel.Data.confirmServices.size(); i++) {
                    total += Double.parseDouble(confirmRequestModel.Data.confirmServices.get(i).flatFee);
                }

                Intent intent = new Intent(PaymentConfirmationActivity.this, PaymentStripeActivity.class);
                intent.putExtra(Constants.SBU_ConfirmService, confirmRequestModel.Data.confirmServices);
                startActivityForResult(intent, REQUEST_CODE_FOR_STRIPE_PAYMENT);
            }
        });
        btn_con_shop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PaymentConfirmationActivity.this, UserLandingActivity.class);
                intent.putExtra(Constants.SBU_For, Constants.SBU_LoadLocal);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                        Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                PaymentConfirmationActivity.this.startActivity(intent);
                PaymentConfirmationActivity.this.finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                //change location
                Place place = PlacePicker.getPlace(this, data);
                confirmRequestModel.Data.confirmServices.get(positionForAddress).requests.get(0).userInfo.address = place.getAddress().toString();
                confirmRequestModel.Data.confirmServices.get(positionForAddress).requests.get(0).userInfo.latitude = place.getLatLng().latitude;
                confirmRequestModel.Data.confirmServices.get(positionForAddress).requests.get(0).userInfo.longitude = place.getLatLng().longitude;
                adapter.notifyDataSetChanged();
            }
        } else if (requestCode == REQUEST_CODE_FOR_STRIPE_PAYMENT) {
            if (resultCode == RESULT_OK) {
                //handle stripe redirection
                ArrayList<String> createdPubChannelNames = new ArrayList<>();
                for (int i = 0; i < confirmRequestModel.Data.confirmServices.size(); i++) {
                    for (int i1 = 0; i1 < confirmRequestModel.Data.confirmServices.get(i).requests.size(); i1++) {
                        createdPubChannelNames.add(confirmRequestModel.Data.confirmServices.get(i).requests.get(i1).requestId);
                    }
                }
                finish();
                startActivity(new Intent(PaymentConfirmationActivity.this, SellerLocationUpdateActivity.class)
                        .putStringArrayListExtra(Constants.SBU_CreatedPubChannelNames, createdPubChannelNames));
            }

        } else if (requestCode == CODE_PAYPAL) {
            if (resultCode == RESULT_OK) {
                doPayForServiceAdaptive();
            }
        } else if (resultCode == Activity.RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                Logger.d(TAG_PAYPAL + confirm.toJSONObject());
                PayPalResponseModel palResponseModel = new Gson().fromJson(confirm.toJSONObject().toString(), PayPalResponseModel.class);
                doPayForService(palResponseModel.response.id
                        , palResponseModel.response.create_time);
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            Logger.d(TAG_PAYPAL + "The user canceled.");
        } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
            Logger.d(TAG_PAYPAL + "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
        }
    }

    private void doPayForServiceAdaptive() {
        if (!Globals.internetCheck(PaymentConfirmationActivity.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().payForServiceAdaptive(globals.getUserData().Data.userId
                , globals.getUserData().Data.sessionId, confirmRequestModel.Data.confirmServices.get(0).requests.get(0),
                confirmRequestModel.Data.confirmServices.get(0).requests.get(0).requestId,
                confirmRequestModel.Data.confirmServices.get(0).serviceId,
                PayKey);
        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(PaymentConfirmationActivity.this);
        HttpRequestHandler.getInstance().post(PaymentConfirmationActivity.this, getString(R.string.payForServiceAdaptive), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel.Result) {
                    ArrayList<String> createdPubChannelNames = new ArrayList<>();
                    for (int i = 0; i < confirmRequestModel.Data.confirmServices.size(); i++) {
                        for (int i1 = 0; i1 < confirmRequestModel.Data.confirmServices.get(i).requests.size(); i1++) {
                            createdPubChannelNames.add(confirmRequestModel.Data.confirmServices.get(i).requests.get(i1).requestId);
                        }
                    }
                    finish();
                    startActivity(new Intent(PaymentConfirmationActivity.this, SellerLocationUpdateActivity.class)
                            .putStringArrayListExtra(Constants.SBU_CreatedPubChannelNames, createdPubChannelNames));
                } else {
                    Toaster.shortToast(dataModel.Message);
                    if (dataModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(PaymentConfirmationActivity.this);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void doPayForService(String transactionId, String create_time) {
        if (!Globals.internetCheck(PaymentConfirmationActivity.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().payForService(confirmRequestModel.Data.confirmServices, transactionId, total, globals.getUserData().Data.userId
                , globals.getUserData().Data.sessionId, create_time);
        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(PaymentConfirmationActivity.this);
        HttpRequestHandler.getInstance().post(PaymentConfirmationActivity.this, getString(R.string.payForService), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel.Result) {
                    ArrayList<String> createdPubChannelNames = new ArrayList<>();
                    for (int i = 0; i < confirmRequestModel.Data.confirmServices.size(); i++) {
                        for (int i1 = 0; i1 < confirmRequestModel.Data.confirmServices.get(i).requests.size(); i1++) {
                            createdPubChannelNames.add(confirmRequestModel.Data.confirmServices.get(i).requests.get(i1).requestId);
                        }
                    }
                    finish();
                    startActivity(new Intent(PaymentConfirmationActivity.this, SellerLocationUpdateActivity.class)
                            .putStringArrayListExtra(Constants.SBU_CreatedPubChannelNames, createdPubChannelNames));
                } else {
                    Toaster.shortToast(dataModel.Message);
                    if (dataModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(PaymentConfirmationActivity.this);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    public void getAddressFromMap(int position) throws GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException {
        positionForAddress = position;
        Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).zzdB(confirmRequestModel.Data.confirmServices.get(position).requests.get(0).userInfo.address)
                .build(this);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }
}
