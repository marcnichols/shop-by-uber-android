package com.shopbyuber.user;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemSpanLookup;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.model.LocalCategoriesModel;
import com.shopbyuber.model.ShopByUberPicksModel;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.FusedLocationManager;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;
import com.shopbyuber.utils.paginationProgressBarAdapter;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

public class FragmentLocal extends Fragment implements AdapterView.OnItemClickListener,
        OnMapReadyCallback,
        Paginate.Callbacks, SwipeRefreshLayout.OnRefreshListener,
        GoogleMap.OnMarkerClickListener {
    Globals globals;

    @BindView(R.id.list)
    RecyclerView localListRecyclerView;

    @BindView(R.id.map_container)
    RelativeLayout map_container;

    @BindView(R.id.view_map_border)
    View view_map_border;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tv_no_record_found)
    TextView tv_no_record_found;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private ShopByUberPicksModel localVendorsModel;
    private ArrayList<ShopByUberPicksModel.CompanyListing> vendorListings;
    private AdapterShopByUberPicks adapterLocalVendors;
    private LocalCategoriesModel subCategoriesModel;

    GoogleMap mGoogleMap;
    SupportMapFragment mapFragment;

    private Paginate paginate;
    int pageNo = 1;
    private boolean loading = false;
    FusedLocationManager fusedLocationManager;

    ImageView imgCategory;

    private String categoryId = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_main, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        fusedLocationManager = new FusedLocationManager(getActivity());
        globals = ((Globals) getActivity().getApplicationContext());
        map_container.setVisibility(View.VISIBLE);
        view_map_border.setVisibility(View.VISIBLE);

        pageNo = 1;
        vendorListings = new ArrayList<>();

        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorToolbarTitle);
        swipeRefreshLayout.setOnRefreshListener(this);

        setupMap();
        dogetLocalRequest(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleMap == null) {
            mapFragment.getMapAsync(this);
        }
    }

    private void setupMap() {
        // add map fragment to container
        FragmentManager fm = getChildFragmentManager();
        mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map_container);
        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.map_container, mapFragment).commit();
        }
    }

    private void showNoRecordFound(String message) {
        loading = false;
        if (vendorListings.isEmpty()) {
            localListRecyclerView.setVisibility(View.GONE);
            if (tv_no_record_found.getVisibility() == View.GONE)
                tv_no_record_found.setText(message);
            tv_no_record_found.setVisibility(View.VISIBLE);
        }
    }

    private void hideNoRecordFound() {
        localListRecyclerView.setVisibility(View.VISIBLE);
        if (tv_no_record_found.getVisibility() == View.VISIBLE)
            tv_no_record_found.setVisibility(View.GONE);
    }

    public void dogetLocalRequest(final boolean showProgress) {
        if (!Globals.internetCheck(getActivity())) {
            showNoRecordFound(getString(R.string.err_no_matching_vendors_found));
            return;
        }
        LatLng latLng = fusedLocationManager.getLatLng();
        JSONObject params = HttpRequestHandler.getInstance().getLocalCatagoriesParams(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                latLng.latitude, latLng.longitude,
                globals.getPostal(latLng),
                pageNo);

        HttpRequestHandler.getInstance().post(getActivity(), getString(R.string.getLocalVendors), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                if (!swipeRefreshLayout.isRefreshing() && showProgress)
                    progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (progressBar.getVisibility() == View.VISIBLE)
                    progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                localVendorsModel = new Gson().fromJson(response.toString(), ShopByUberPicksModel.class);
                if (localVendorsModel.Result) {
                    if (swipeRefreshLayout.isRefreshing()) {
                        vendorListings.clear();
                        if (adapterLocalVendors != null)
                            adapterLocalVendors.notifyDataSetChanged();
                    }
                    setupList(localVendorsModel.Data.companyListing);
                } else {
                    showNoRecordFound(getString(R.string.err_no_matching_vendors_found));
                    if (localVendorsModel.Message.contains(getString(R.string.invalid_session))) {
                        Toaster.shortToast(localVendorsModel.Message);
                        globals.doLogout(getActivity());
                    }
                }
                stopRefreshing();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                stopRefreshing();
                showNoRecordFound(getString(R.string.err_no_matching_vendors_found));
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                stopRefreshing();
                showNoRecordFound(getString(R.string.err_no_matching_vendors_found));
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void stopRefreshing() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void setupList(ArrayList<ShopByUberPicksModel.CompanyListing> localCategory) {
        if (localCategory != null && !localCategory.isEmpty()) {
            vendorListings.addAll(localCategory);
            setAdapter();
        } else
            showNoRecordFound(getString(R.string.err_no_matching_vendors_found));
    }

    private void plotMarkers() {
        if (!vendorListings.isEmpty()) {
            for (ShopByUberPicksModel.CompanyListing vendorListing : vendorListings) {
                drawMarker(new LatLng(vendorListing.latitude, vendorListing.longitude),
                        vendorListing._id);

            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(fusedLocationManager.getLatLng()
                                , 8));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 1000);
        }
    }

    private void drawMarker(LatLng point, String companyId) {
        try {
            // Creating an instance of MarkerOptions
            MarkerOptions markerOptions = new MarkerOptions();

            // Setting latitude and longitude for the marker
            markerOptions.position(point);

            // Adding marker on the Google Map
            Marker marker = mGoogleMap.addMarker(markerOptions);
            marker.setTag(companyId);
            marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.map_green));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        hideNoRecordFound();
        plotMarkers();
        if (adapterLocalVendors == null) {
            adapterLocalVendors = new AdapterShopByUberPicks(getActivity(), true);
            adapterLocalVendors.setOnItemClickListener(this);
            if (paginate != null) {
                paginate.unbind();
            }
        }
        loading = false;
        adapterLocalVendors.doRefresh(vendorListings);

        if (localListRecyclerView.getAdapter() == null) {
            localListRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            localListRecyclerView.setItemAnimator(new DefaultItemAnimator());
            localListRecyclerView.setAdapter(adapterLocalVendors);
            if (vendorListings.size() < localVendorsModel.Data.recordCount) {
                // initialize paginate
                paginate = Paginate.with(localListRecyclerView, this)
                        .setLoadingTriggerThreshold(Constants.progress_threshold_2)
                        .addLoadingListItem(Constants.addLoadingRow)
                        .setLoadingListItemCreator(new paginationProgressBarAdapter())
                        .setLoadingListItemSpanSizeLookup(new LoadingListItemSpanLookup() {
                            @Override
                            public int getSpanSize() {
                                return Constants.GRID_SPAN;
                            }
                        })
                        .build();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        startActivity(new Intent(getContext(), GetPicksCompanyServiceActivity.class)
                .putExtra(Constants.SBU_CompanyId, vendorListings.get(position)._id)
                .putExtra(Constants.SBU_CompanyName, vendorListings.get(position).comapnyScreenName));
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        // called when map is ready
        this.mGoogleMap = googleMap;
        this.mGoogleMap.setOnMarkerClickListener(this);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.setMyLocationEnabled(true);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Constants.SBU_LOS_ANGELES, 5));
    }

    @Override
    public void onLoadMore() {
        loading = true;
        pageNo++;
        dogetLocalRequest(false);
    }

    @Override
    public boolean isLoading() {
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return vendorListings.size() == localVendorsModel.Data.recordCount;
    }

    @Override
    public void onRefresh() {
        pageNo = 1;

        dogetLocalRequest(true);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        ShopByUberPicksModel.CompanyListing data = getMarkerData(String.valueOf(marker.getTag()));
        if (data != null) {
            Company_detail_popup company_detail_popup = new Company_detail_popup(getActivity());
            company_detail_popup.showPopup(data.comapnyScreenName, data.serviceDescription,
                    data.companyProfileImage, data._id);
        }
        return false;
    }

    private ShopByUberPicksModel.CompanyListing getMarkerData(String companyId) {
        for (ShopByUberPicksModel.CompanyListing vendorListing : vendorListings) {
            if (vendorListing._id.equalsIgnoreCase(companyId))
                return vendorListing;
        }
        return null;
    }
}
