package com.shopbyuber.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemSpanLookup;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.model.ShopByUberPicksModel;
import com.shopbyuber.model.ShopByUberPicksModel.CompanyListing;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.FusedLocationManager;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;
import com.shopbyuber.utils.paginationProgressBarAdapter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

public class FragmentShopByUberPicks extends Fragment implements AdapterView.OnItemClickListener, Paginate.Callbacks, SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = FragmentShopByUberPicks.class.getSimpleName();
    Globals globals;

    @BindView(R.id.list)
    RecyclerView picksRecyclerView;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tv_no_record_found)
    TextView tv_no_record_found;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private ShopByUberPicksModel modelShopByUberPicks;
    private ArrayList<CompanyListing> companyListings;
    private AdapterShopByUberPicks adapterShopByUberPicks;

    private Paginate paginate;
    int pageNo = 1;
    private boolean loading = false;

    FusedLocationManager fusedLocationManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_main, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        globals = ((Globals) getActivity().getApplicationContext());
        fusedLocationManager = new FusedLocationManager(getActivity());
        fusedLocationManager.getLatLng();
        pageNo = 1;
        companyListings = new ArrayList<>();
        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorToolbarTitle);
        swipeRefreshLayout.setOnRefreshListener(this);

        dogetShopbyUberPicksRequest(true);
    }

    private void showNoRecordFound() {
        loading = false;
        if (companyListings.isEmpty()) {
            picksRecyclerView.setVisibility(View.GONE);
            if (tv_no_record_found.getVisibility() == View.GONE)
                tv_no_record_found.setText(getString(R.string.err_no_vendors));
            tv_no_record_found.setVisibility(View.VISIBLE);
        }
    }

    private void hideNoRecordFound() {
        picksRecyclerView.setVisibility(View.VISIBLE);
        if (tv_no_record_found.getVisibility() == View.VISIBLE)
            tv_no_record_found.setVisibility(View.GONE);
    }

    public void dogetShopbyUberPicksRequest(final boolean showProgress) {
        if (!Globals.internetCheck(getActivity())) {
            showNoRecordFound();
            return;
        }

        LatLng latLng = fusedLocationManager.getLatLng();

        JSONObject params = HttpRequestHandler.getInstance().getShopbyUberPicksParams(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                pageNo, latLng.latitude, latLng.longitude);

        HttpRequestHandler.getInstance().post(getActivity(), getString(R.string.getShopbyUberPicks), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                if (!swipeRefreshLayout.isRefreshing() && showProgress)
                    progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (progressBar.getVisibility() == View.VISIBLE)
                    progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                modelShopByUberPicks = new Gson().fromJson(response.toString(), ShopByUberPicksModel.class);
                if (modelShopByUberPicks.Result) {
                    if (swipeRefreshLayout.isRefreshing()) {
                        companyListings.clear();
                        if (adapterShopByUberPicks != null)
                            adapterShopByUberPicks.notifyDataSetChanged();
                    }
                    setupList(modelShopByUberPicks.Data.companyListing);
                } else {
                    showNoRecordFound();
                    if (modelShopByUberPicks.Message.contains(getString(R.string.invalid_session))) {
                        Toaster.shortToast(modelShopByUberPicks.Message);
                        globals.doLogout(getActivity());
                    }
                }
                stopRefreshing();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                stopRefreshing();
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                stopRefreshing();
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void stopRefreshing() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void setupList(List<CompanyListing> companyListing) {
        if (companyListing != null && !companyListing.isEmpty()) {
            companyListings.addAll(companyListing);
            setAdapter();
        } else
            showNoRecordFound();
    }

    private void setAdapter() {
        hideNoRecordFound();
        if (adapterShopByUberPicks == null) {
            if (paginate != null) {
                paginate.unbind();
            }
            adapterShopByUberPicks = new AdapterShopByUberPicks(getActivity(), false);
            adapterShopByUberPicks.setOnItemClickListener(this);
        }
        loading = false;
        adapterShopByUberPicks.doRefresh(companyListings);

        if (picksRecyclerView.getAdapter() == null) {
            picksRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            picksRecyclerView.setItemAnimator(new DefaultItemAnimator());
            picksRecyclerView.setAdapter(adapterShopByUberPicks);
            if (companyListings.size() < modelShopByUberPicks.Data.recordCount) {
                paginate = Paginate.with(picksRecyclerView, this)
                        .setLoadingTriggerThreshold(Constants.progress_threshold_2)
                        .addLoadingListItem(Constants.addLoadingRow)
                        .setLoadingListItemCreator(new paginationProgressBarAdapter())
                        .setLoadingListItemSpanSizeLookup(new LoadingListItemSpanLookup() {
                            @Override
                            public int getSpanSize() {
                                return Constants.GRID_SPAN;
                            }
                        })
                        .build();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Logger.d(TAG, companyListings.get(position));
        startActivity(new Intent(getContext(), GetPicksCompanyServiceActivity.class)
                .putExtra(Constants.SBU_CompanyId, companyListings.get(position)._id)
                .putExtra(Constants.SBU_CompanyName, companyListings.get(position).comapnyScreenName));
    }

    @Override
    public void onLoadMore() {
        loading = true;
        pageNo++;
        dogetShopbyUberPicksRequest(false);
    }

    @Override
    public boolean isLoading() {
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return companyListings.size() == modelShopByUberPicks.Data.recordCount;
    }

    @Override
    public void onRefresh() {
        pageNo = 1;
        dogetShopbyUberPicksRequest(true);
    }
}
