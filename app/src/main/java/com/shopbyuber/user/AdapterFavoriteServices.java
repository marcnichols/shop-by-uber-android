package com.shopbyuber.user;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.shopbyuber.R;
import com.shopbyuber.model.FavouriteServicesModel.Services;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

class AdapterFavoriteServices extends RecyclerView.Adapter<AdapterFavoriteServices.ViewHolder> {

    private ArrayList<Services> mValues;
    private final Context mContext;
    private AdapterView.OnItemClickListener onItemClickListener;

    AdapterFavoriteServices(Context context) {
        mContext = context;
    }

    public void doRefresh(ArrayList<Services> companyListings) {
        this.mValues = companyListings;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.favorite_services_item, parent, false);
        return new ViewHolder(view, this);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private AdapterFavoriteServices mAdapterFavoriteServices;
        @BindView(R.id.img_profile)
        ImageView img_profile;
        @BindView(R.id.tv_service_title)
        TextView tv_service_title;
        @BindView(R.id.tv_price)
        TextView tv_price;
        @BindView(R.id.rb_favourite)
        RadioButton rb_favourite;

        public ViewHolder(View itemView, AdapterFavoriteServices adapterShopByUberPicks) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.mAdapterFavoriteServices = adapterShopByUberPicks;
            itemView.setOnClickListener(this);
        }

        void setDataToView(Services mItem, ViewHolder viewHolder) {
            viewHolder.tv_service_title.setText(mItem.screenName);
            if (Double.parseDouble(mItem.flatFee) > 0)
                viewHolder.tv_price.setText(mContext.getString(R.string.currency_symbol) + " " + mItem.flatFee);
            else
                viewHolder.tv_price.setText("");
            Glide.with(mContext)
                    .load(mItem.serviceImage)
                    .centerCrop()
                    .placeholder(R.drawable.image_place_holder)
                    .crossFade()
                    .dontAnimate()
                    .into(viewHolder.img_profile);
        }

        @Override
        public void onClick(View v) {
            mAdapterFavoriteServices.onItemHolderClick(ViewHolder.this);
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Services mItem = mValues.get(position);
        holder.setDataToView(mItem, holder);
        holder.rb_favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentFavoriteServices.getInstance().doAddToFavoriteServiceRequest(mItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void onItemHolderClick(ViewHolder holder) {
        if (onItemClickListener != null)
            onItemClickListener.onItemClick(null, holder.itemView, holder.getAdapterPosition(), holder.getItemId());
    }
}

