package com.shopbyuber.user;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.model.SelectedCategoryModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

class AdapterSelectedCategory extends RecyclerView.Adapter<AdapterSelectedCategory.ViewHolder> {
    private List<SelectedCategoryModel.CompanyListing> mDataSet;
    private final Context mContext;
    private AdapterView.OnItemClickListener onItemClickListener;
    private String catTitle;

    AdapterSelectedCategory(Context context) {
        mContext = context;
    }

    public void doRefresh(List<SelectedCategoryModel.CompanyListing> arrCategories, String catTitle) {
        mDataSet = arrCategories;
        this.catTitle = catTitle;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.local_item, parent, false);
        return new ViewHolder(view, this);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private AdapterSelectedCategory adapterSelectedCategory;
        @BindView(R.id.img_profile)
        ImageView img_profile;
        @BindView(R.id.content)
        TextView tv_content;


        ViewHolder(View itemView, AdapterSelectedCategory adapterShopByUberPicks) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.adapterSelectedCategory = adapterShopByUberPicks;
            itemView.setOnClickListener(this);
        }

        void setDataToView(SelectedCategoryModel.CompanyListing mItem, ViewHolder viewHolder) {

            viewHolder.tv_content.setText(mItem.comapnyScreenName);

            Glide.with(mContext)
                    .load(mItem.companyProfileImage)
                    .centerCrop()
                    .placeholder(R.drawable.image_place_holder)
                    .crossFade()
                    .dontAnimate()
                    .into(viewHolder.img_profile);
        }

        @Override
        public void onClick(View v) {
            adapterSelectedCategory.onItemHolderClick(ViewHolder.this);
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        SelectedCategoryModel.CompanyListing mItem = mDataSet.get(position);
        Logger.d(mItem.comapnyScreenName);
        holder.setDataToView(mItem, holder);
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void onItemHolderClick(ViewHolder holder) {
        if (onItemClickListener != null)
            onItemClickListener.onItemClick(null, holder.itemView, holder.getAdapterPosition(), holder.getItemId());
    }
}
