package com.shopbyuber.user;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.shopbyuber.R;
import com.shopbyuber.model.LocalCategoriesModel.LocalCategories;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AdapterCategories extends RecyclerView.Adapter<AdapterCategories.ViewHolder> {
    private List<LocalCategories> mDataSet;
    private final Context mContext;
    private AdapterView.OnItemClickListener onItemClickListener;
    private String NO_DATA_DOTS = "...";
    private boolean isFromSearch;

    public AdapterCategories(Context context, boolean isFromSearch) {
        this.isFromSearch = isFromSearch;
        mContext = context;
    }

    public void doRefresh(List<LocalCategories> arrCategories) {
        mDataSet = arrCategories;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        if (!isFromSearch) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);

        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shopbyuberpicks_item, parent, false);
        }

        return new ViewHolder(view, this);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private AdapterCategories mAdapterLocalCategories;
        @BindView(R.id.img_profile)
        ImageView img_profile;
        @BindView(R.id.content)
        TextView content;
        @BindView(R.id.detail)
        TextView detail;

        public ViewHolder(View itemView, AdapterCategories adapterShopByUberPicks) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.mAdapterLocalCategories = adapterShopByUberPicks;
            itemView.setOnClickListener(this);
        }

        void setDataToView(LocalCategories mItem, ViewHolder viewHolder) {
            if (isFromSearch)
                viewHolder.detail.setVisibility(View.VISIBLE);
            else
                viewHolder.detail.setVisibility(View.GONE);
            viewHolder.content.setText(mItem.catName);
            if (mItem.companyDatas != null && !mItem.companyDatas.isEmpty())
                viewHolder.detail.setText(TextUtils.join(", ", mItem.companyDatas));
            else
                viewHolder.detail.setText(NO_DATA_DOTS);
            Glide.with(mContext)
                    .load(mItem.categoryImage)
                    .centerCrop()
                    .placeholder(R.drawable.image_place_holder)
                    .crossFade()
                    .dontAnimate()
                    .into(viewHolder.img_profile);
        }

        @Override
        public void onClick(View v) {
            mAdapterLocalCategories.onItemHolderClick(ViewHolder.this);
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        LocalCategories mItem = mDataSet.get(position);
        holder.setDataToView(mItem, holder);
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void onItemHolderClick(ViewHolder holder) {
        if (onItemClickListener != null)
            onItemClickListener.onItemClick(null, holder.itemView, holder.getAdapterPosition(), holder.getItemId());
    }
}
