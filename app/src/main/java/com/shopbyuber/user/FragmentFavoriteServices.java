package com.shopbyuber.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemSpanLookup;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.model.FavouriteServicesModel;
import com.shopbyuber.model.FavouriteServicesModel.Services;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;
import com.shopbyuber.utils.paginationProgressBarAdapter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;

public class FragmentFavoriteServices extends Fragment implements AdapterView.OnItemClickListener, Paginate.Callbacks, SwipeRefreshLayout.OnRefreshListener {
    Globals globals;

    @BindView(R.id.list)
    RecyclerView favoriteRecyclerView;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tv_no_record_found)
    TextView tv_no_record_found;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.continue_shopping)
    TextView continue_shopping;

    private FavouriteServicesModel favouriteServicesModel;
    private ArrayList<Services> favouriteServices;
    private AdapterFavoriteServices adapterFavoriteServices;

    private Paginate paginate;
    int pageNo = 1;
    private boolean loading = false;

    public static FragmentFavoriteServices mContext;

    public static FragmentFavoriteServices getInstance() {
        return mContext;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_main, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        mContext = this;
        continue_shopping.setVisibility(View.VISIBLE);
        globals = ((Globals) getActivity().getApplicationContext());
        pageNo = 1;
        favouriteServices = new ArrayList<>();
        swipeRefreshLayout.setColorSchemeResources(R.color.colorToolbarTitle);
        swipeRefreshLayout.setOnRefreshListener(this);
        doGetFavouriteServicesRequest(true);
    }

    @OnClick(R.id.continue_shopping)
    public void doContinueShopping() {
        UserLandingActivity.getInstance().setToolbarTitle(R.string.home_title);
        UserLandingActivity.getInstance().setFragment(new FragmentUserHome());
    }

    private void showNoRecordFound() {
        loading = false;
        if (favouriteServices.isEmpty()) {
            swipeRefreshLayout.setVisibility(View.GONE);
            if (tv_no_record_found.getVisibility() == View.GONE)
                tv_no_record_found.setText(getString(R.string.you_do_not_have_any_favorites));
            tv_no_record_found.setVisibility(View.VISIBLE);
        }
    }

    private void hideNoRecordFound() {
        swipeRefreshLayout.setVisibility(View.VISIBLE);
        if (tv_no_record_found.getVisibility() == View.VISIBLE)
            tv_no_record_found.setVisibility(View.GONE);
    }

    public void doGetFavouriteServicesRequest(final boolean showProgress) {
        if (!Globals.internetCheck(getActivity())) {
            showNoRecordFound();
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().getFavoritesParams(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                pageNo);

        HttpRequestHandler.getInstance().post(getActivity(), getString(R.string.getFavouriteServices), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                if (!swipeRefreshLayout.isRefreshing() && showProgress)
                    progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (progressBar.getVisibility() == View.VISIBLE)
                    progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                favouriteServicesModel = new Gson().fromJson(response.toString(), FavouriteServicesModel.class);
                if (favouriteServicesModel.Result) {
                    if (swipeRefreshLayout.isRefreshing()) {
                        favouriteServices.clear();
                        if (adapterFavoriteServices != null)
                            adapterFavoriteServices.notifyDataSetChanged();
                    }
                    setupList(favouriteServicesModel.Data.services);
                } else {
                    showNoRecordFound();
                    Toaster.shortToast(favouriteServicesModel.Message);
                    if (favouriteServicesModel.Message.contains(getString(R.string.invalid_session))) {
                        globals.doLogout(getActivity());
                    }
                }
                stopRefreshing();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                stopRefreshing();
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                stopRefreshing();
                showNoRecordFound();
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void stopRefreshing() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void setupList(List<Services> servicesList) {
        if (servicesList != null && !servicesList.isEmpty()) {
            favouriteServices.addAll(servicesList);
            setAdapter();
        } else
            showNoRecordFound();
    }

    private void setAdapter() {
        hideNoRecordFound();
        if (adapterFavoriteServices == null) {
            if (paginate != null) {
                paginate.unbind();
            }
            adapterFavoriteServices = new AdapterFavoriteServices(getActivity());
            adapterFavoriteServices.setOnItemClickListener(this);
        }
        loading = false;
        adapterFavoriteServices.doRefresh(favouriteServices);

        if (favoriteRecyclerView.getAdapter() == null) {
            favoriteRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            favoriteRecyclerView.setItemAnimator(new DefaultItemAnimator());
            favoriteRecyclerView.setAdapter(adapterFavoriteServices);
            if (favouriteServices.size() < favouriteServicesModel.Data.recordCount) {
                paginate = Paginate.with(favoriteRecyclerView, this)
                        .setLoadingTriggerThreshold(Constants.progress_threshold_2)
                        .addLoadingListItem(Constants.addLoadingRow)
                        .setLoadingListItemCreator(new paginationProgressBarAdapter())
                        .setLoadingListItemSpanSizeLookup(new LoadingListItemSpanLookup() {
                            @Override
                            public int getSpanSize() {
                                return Constants.GRID_SPAN;
                            }
                        })
                        .build();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        startActivity(new Intent(getActivity(), ServiceDetailActivity.class)
                .putExtra(Constants.SBU_CompanyId, favouriteServices.get(position).companyId)
                .putExtra(Constants.SBU_SubCategoryId, favouriteServices.get(position).subCategoryId)
                .putExtra(Constants.SBU_CategoryId, favouriteServices.get(position).categoryId));
    }

    @Override
    public void onLoadMore() {
        loading = true;
        pageNo++;
        doGetFavouriteServicesRequest(false);
    }

    @Override
    public boolean isLoading() {
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return favouriteServices.size() == favouriteServicesModel.Data.recordCount;
    }

    @Override
    public void onRefresh() {
        pageNo = 1;
        doGetFavouriteServicesRequest(true);
    }

    public void doAddToFavoriteServiceRequest(final Services mItem) {
        if (!Globals.internetCheck(getActivity()))
            return;

        JSONObject params = HttpRequestHandler.getInstance().AddToFavoriteServiceParams(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                mItem.serviceId);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(getActivity());

        HttpRequestHandler.getInstance().post(getActivity(), getActivity().getString(R.string.addToFavoriteService), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel.Result) {
                    favouriteServices.remove(mItem);
                    adapterFavoriteServices.notifyDataSetChanged();
                    if (favouriteServices.isEmpty())
                        showNoRecordFound();
                } else {
                    Toaster.shortToast(dataModel.Message);
                    if (dataModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(getActivity());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }
}
