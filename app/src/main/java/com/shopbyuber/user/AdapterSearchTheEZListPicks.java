package com.shopbyuber.user;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.shopbyuber.R;
import com.shopbyuber.model.LocalCategoriesModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AdapterSearchTheEZListPicks extends RecyclerView.Adapter<AdapterSearchTheEZListPicks.ViewHolder> {

    private ArrayList<LocalCategoriesModel.CompanyListing> mValues;
    private final Context mContext;
    private AdapterView.OnItemClickListener onItemClickListener;

    public AdapterSearchTheEZListPicks(Context context) {
        mContext = context;
    }

    public void doRefresh(ArrayList<LocalCategoriesModel.CompanyListing> companyListings) {
        this.mValues = companyListings;
        notifyDataSetChanged();
    }

    @Override
    public AdapterSearchTheEZListPicks.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shopbyuberpicks_item, parent, false);
        return new AdapterSearchTheEZListPicks.ViewHolder(view, this);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private AdapterSearchTheEZListPicks mAdapterShopByUberPicks;
        @BindView(R.id.img_profile)
        ImageView img_profile;
        @BindView(R.id.content)
        TextView content;
        @BindView(R.id.detail)
        TextView detail;
        @BindView(R.id.categories)
        TextView categories;

        public ViewHolder(View itemView, AdapterSearchTheEZListPicks adapterShopByUberPicks) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.mAdapterShopByUberPicks = adapterShopByUberPicks;
            itemView.setOnClickListener(this);
        }

        void setDataToView(LocalCategoriesModel.CompanyListing mItem, AdapterSearchTheEZListPicks.ViewHolder viewHolder) {
            viewHolder.content.setText(mItem.comapnyScreenName);
            viewHolder.detail.setVisibility(View.VISIBLE);
            viewHolder.detail.setText(mItem.serviceDescription);
            viewHolder.detail.setVisibility(View.GONE);

            viewHolder.categories.setVisibility(View.VISIBLE);
            viewHolder.categories.setText(mItem.categories);

            Glide.with(mContext)
                    .load(mItem.companyProfileImage)
                    .centerCrop()
                    .placeholder(R.drawable.image_place_holder)
                    .crossFade()
                    .dontAnimate()
                    .into(viewHolder.img_profile);
        }

        @Override
        public void onClick(View v) {
            mAdapterShopByUberPicks.onItemHolderClick(AdapterSearchTheEZListPicks.ViewHolder.this);
        }
    }

    @Override
    public void onBindViewHolder(final AdapterSearchTheEZListPicks.ViewHolder holder, int position) {
        LocalCategoriesModel.CompanyListing mItem = mValues.get(position);
        holder.setDataToView(mItem, holder);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void onItemHolderClick(AdapterSearchTheEZListPicks.ViewHolder holder) {
        if (onItemClickListener != null)
            onItemClickListener.onItemClick(null, holder.itemView, holder.getAdapterPosition(), holder.getItemId());
    }
}
