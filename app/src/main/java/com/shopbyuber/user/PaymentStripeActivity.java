package com.shopbyuber.user;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.model.ConfirmRequestModel;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.model.UserModel;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputListener;
import com.stripe.android.view.CardMultilineWidget;
import com.stripe.android.view.StripeEditText;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;

public class PaymentStripeActivity extends BaseActivity {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.tv_delete_service)
    TextView tvDeleteService;
    @BindView(R.id.auto_complete_search)
    AutoCompleteTextView autoCompleteSearch;
    @BindView(R.id.img_clear)
    ImageView imgClear;
    @BindView(R.id.img_search)
    ImageView imgSearch;
    @BindView(R.id.layout_search)
    LinearLayout layoutSearch;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.card_input_widget)
    CardMultilineWidget cardInputWidget;
    @BindView(R.id.btn_done)
    Button btnDone;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    Card card;
    Stripe stripe;
    Globals globals;
    double total;
    ArrayList<ConfirmRequestModel.ConfirmServices> confirmServices;
    UserModel userModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_stripe);
        ButterKnife.bind(this);
        init();

    }

    private void init() {
        globals = (Globals) getApplicationContext();
        userModel = globals.getUserData();
        confirmServices = new ArrayList<>();

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.card_details));
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey(Constants.SBU_ConfirmService)) {
            confirmServices.addAll((Collection<? extends ConfirmRequestModel.ConfirmServices>) bundle.getSerializable(Constants.SBU_ConfirmService));
        }
        stripe = new Stripe(this, Constants.SBU_StripeKey);
        cardInputWidget.setCardInputListener(new CardInputListener() {
            @Override
            public void onFocusChange(String focusField) {

            }

            @Override
            public void onCardComplete() {
                //et_add_source_expiry_ml Id from card_multiline_widget.xml
                cardInputWidget.findViewById(R.id.et_add_source_expiry_ml).setFocusable(true);
            }

            @Override
            public void onExpirationComplete() {
                //et_add_source_cvc_ml Id from card_multiline_widhet.xml
                StripeEditText stripeEditText = cardInputWidget.findViewById(R.id.et_add_source_cvc_ml);
                stripeEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
            }

            @Override
            public void onCvcComplete() {

            }

            @Override
            public void onPostalCodeComplete() {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick(R.id.btn_done)
    public void onClickDone() {
        //generate stripe token for payment
        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(PaymentStripeActivity.this);
        card = cardInputWidget.getCard();
        if (card != null) {
            dialog.show();
            if (card.validateCard()) {
                stripe.createToken(card, new TokenCallback() {
                    @Override
                    public void onError(Exception error) {
                        if (dialog != null && dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        Toast.makeText(PaymentStripeActivity.this, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess(Token token) {
                        if (dialog != null && dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        total = 0;
                        for (int i = 0; i < confirmServices.size(); i++) {
                            total += Double.parseDouble(confirmServices.get(i).flatFee);
                        }
                        doPayForService(token.getId());
                    }
                });
            }
        }
    }

    @OnClick(R.id.btn_cancel)
    public void onClickCancel() {
        onBackPressed();
    }

    private void doPayForService(String token) {
        if (!Globals.internetCheck(PaymentStripeActivity.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().payment(confirmServices, token,
                total, globals.getUserData().Data.userId
                , globals.getUserData().Data.sessionId);
        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(PaymentStripeActivity.this);
        HttpRequestHandler.getInstance().post(PaymentStripeActivity.this, getString(R.string.payment), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel.Result) {
                    setResult(RESULT_OK);
                    finish();
                } else {
                    Toaster.shortToast(dataModel.Message);
                    if (dataModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(PaymentStripeActivity.this);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }
}
