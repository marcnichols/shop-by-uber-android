package com.shopbyuber.user;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.shopbyuber.R;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.YourActivityEvent;
import com.shopbyuber.wantedservice.WantedServiceTabFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentUserHome extends Fragment {
    @BindView(R.id.item_viewpager)
    ViewPager item_viewpager;
    @BindView(R.id.tb_list)
    TabLayout tb_list;

    boolean isLoaded = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_home, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        registerBroadcast();
        setupViewPager(item_viewpager);
        tb_list.setupWithViewPager(item_viewpager);

        tb_list.getTabAt(1).setText(Html.fromHtml(getResources().getString(R.string.the_ez_list_picks)));

        item_viewpager.setOffscreenPageLimit(3);
        item_viewpager.setCurrentItem(0);

        //use to display wanted service information dialog
        tb_list.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 2 && !isLoaded) {
                    Globals.showInfoDialog(getActivity());
                    isLoaded = true;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        viewPagerAdapter.addFrag(new FragmentCategories(), getString(R.string.catagories));
        viewPagerAdapter.addFrag(new FragmentShopByUberPicks(), getString(R.string.shopbyuberpicks));
        viewPagerAdapter.addFrag(new WantedServiceTabFragment(), getString(R.string.wanted_services));
        viewPagerAdapter.addFrag(new FragmentYourActivity(), getString(R.string.youractivity));
        viewPager.setAdapter(viewPagerAdapter);
    }

    private BroadcastReceiver mReloadActivityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            if (intent.getAction() != null) {
                if (intent.getAction().equals(Constants.SBU_LoadYourActivity)) {
                    if (item_viewpager != null) {
                        item_viewpager.setCurrentItem(3);
                        Intent intentLoadYourActivity = new Intent(Constants.SBU_RefreshList);
                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intentLoadYourActivity);
                    }
                } else if (intent.getAction().equals(Constants.SBU_LoadLocal)) {
                    if (item_viewpager != null)
                        item_viewpager.setCurrentItem(2);
                } else if (intent.getAction().equals(Constants.SBU_LoadWanted)) {
                    if (item_viewpager != null)
                        item_viewpager.setCurrentItem(2);
                }
            }
        }
    };

    public void registerBroadcast() {
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mReloadActivityReceiver,
                new IntentFilter(Constants.SBU_LoadLocal));
    }

    @Override
    public void onStart() {

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onStart();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(YourActivityEvent event) {
        if (item_viewpager != null)
            item_viewpager.setCurrentItem(3);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage1(YourActivityEvent.WantedService event) {
        if (item_viewpager != null)
            item_viewpager.setCurrentItem(2);
    }


}

