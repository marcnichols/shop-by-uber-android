package com.shopbyuber.user;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.model.ConfirmRequestModel;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;

import static com.facebook.FacebookSdk.getApplicationContext;


class ConfirmRequestAdapter extends ArrayAdapter<ConfirmRequestModel.ConfirmServices> implements View.OnClickListener {

    private List<ConfirmRequestModel.ConfirmServices> dataSet;
    private Context mContext;
    private Globals globals;

    ConfirmRequestAdapter(List<ConfirmRequestModel.ConfirmServices> data, Context context) {
        super(context, R.layout.confirm_details_item, data);
        this.dataSet = data;
        this.mContext = context;
        globals = (Globals) getApplicationContext();
    }

    @Override
    public void onClick(View v) {

    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.confirm_details_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        setData(viewHolder, dataSet.get(position));

        viewHolder.tv_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmRequestAdapter.this.cancleSignleAppointment(position,
                        dataSet.get(position).requests.get(0).requestId,
                        dataSet.get(position).requests.get(0).timeSlotId);
            }
        });
        viewHolder.tvAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    PaymentConfirmationActivity.getInstance().getAddressFromMap(position);
                } catch (GooglePlayServicesNotAvailableException | GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                }
            }
        });
        return convertView;
    }

    private void setData(ViewHolder viewHolder, ConfirmRequestModel.ConfirmServices confirmServices) {
        try {
            Glide.with(mContext)
                    .load(confirmServices.serviceImage)
                    .centerCrop()
                    .placeholder(R.drawable.image_place_holder)
                    .crossFade()
                    .dontAnimate()
                    .into(viewHolder.img_profile);
            viewHolder.tv_title.setText(confirmServices.screenName);
            viewHolder.tv_price.setText(mContext.getString(R.string.currency_symbol) + " " + confirmServices.flatFee);
            viewHolder.tv_provider_name.setText(confirmServices.vendorData.name);

            viewHolder.tvAddress.setText(confirmServices.requests.get(0).userInfo.address);
            Glide.with(mContext)
                    .load(confirmServices.vendorData.profileImage)
                    .centerCrop()
                    .placeholder(R.drawable.image_place_holder_payment)
                    .crossFade()
                    .dontAnimate()
                    .into(viewHolder.img_service_provider);
            viewHolder.tv_description.setText(confirmServices.vendorData.description);
            viewHolder.tv_speciality.setText(confirmServices.vendorData.speciality);

            viewHolder.tv_confirm_day_time.setText(confirmServices.requests.get(0).duration);

            if (!confirmServices.serviceDetail.isEmpty() || confirmServices.serviceDetail != null)
                viewHolder.tvServiceDescription.setText(confirmServices.serviceDetail);
            else
                viewHolder.tvServiceDescription.setText(mContext.getString(R.string.service_details_null));

        } catch (Exception e) {
            Logger.d(e);
        }
    }

    private void cancleSignleAppointment(final int position, String requestId, String timeSlotId) {
        if (!Globals.internetCheck(mContext))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getCancleSignleRequest(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                requestId, timeSlotId);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(mContext);

        HttpRequestHandler.getInstance().post(mContext, mContext.getString(R.string.cancelSingleAppointments), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel.Result) {
                    dataSet.remove(position);
                    notifyDataSetChanged();
                    if (dataSet.isEmpty()) {
                        ((Activity) mContext).finish();
                    }
                } else if (dataModel.Message.contains(mContext.getString(R.string.invalid_session)))
                    globals.doLogout((Activity) mContext);
                Toaster.shortToast(dataModel.Message);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    public static class ViewHolder {
        @BindView(R.id.img_profile)
        ImageView img_profile;
        @BindView(R.id.img_service_provider)
        ImageView img_service_provider;
        @BindView(R.id.tv_title)
        TextView tv_title;
        @BindView(R.id.tv_description)
        TextView tv_description;
        @BindView(R.id.tv_speciality)
        TextView tv_speciality;
        @BindView(R.id.tv_price)
        TextView tv_price;
        @BindView(R.id.tv_provider_name)
        TextView tv_provider_name;
        @BindView(R.id.tv_remove)
        TextView tv_remove;
        @BindView(R.id.tv_confirm_day_time)
        TextView tv_confirm_day_time;
        @BindView(R.id.tv_service_description)
        TextView tvServiceDescription;
        @BindView(R.id.tv_address)
        TextView tvAddress;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}

