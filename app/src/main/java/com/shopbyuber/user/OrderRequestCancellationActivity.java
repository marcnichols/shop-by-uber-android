package com.shopbyuber.user;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.model.CancelOrderModel;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;

public class OrderRequestCancellationActivity extends BaseActivity {

    Globals globals;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.lv_order)
    ListView lvOrder;
    @BindView(R.id.btn_cancel)
    Button btnCancel;

    View footerView;

    EditText edtFeedback,edtReasonToCancel;

    boolean isOrder = true;
    String requestId = "";
    CancelOrderModel cancelOrderModel;
    AdapterCancelOrderRequest adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_cancellation);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        globals = (Globals) getApplicationContext();

        if (getIntent() != null) {
            requestId = getIntent().getStringExtra(Constants.SBU_RequestId);
        }
        isOrder = globals.getUserData().Data.userRole == 1;

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.request_cancellation);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            btnCancel.setText(R.string.cancel_this_request);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderRequestCancellationActivity.this.onBackPressed();
            }
        });

        footerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_order_cancellation, null);
        lvOrder.addFooterView(footerView);
        edtReasonToCancel = footerView.findViewById(R.id.edt_reason_to_cancel);
        edtFeedback = footerView.findViewById(R.id.edt_feedback);

        doGetCancelOrderScreenData();

    }

    public void doGetCancelOrderScreenData() {
        if (!Globals.internetCheck(OrderRequestCancellationActivity.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getCancelOrderScreenData(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                requestId, //"595cbe98ca72d51ed8001a62",
                globals.getUserData().Data.userRole);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(OrderRequestCancellationActivity.this);

        HttpRequestHandler.getInstance().post(OrderRequestCancellationActivity.this, getString(R.string.getCancelOrderScreenData), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                cancelOrderModel = new Gson().fromJson(response.toString(), CancelOrderModel.class);
                if (cancelOrderModel.Result) {
                    setData();
                } else {
                    Toaster.shortToast(cancelOrderModel.Message);
                    if (cancelOrderModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(OrderRequestCancellationActivity.this);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    public void setData() {
        adapter = new AdapterCancelOrderRequest(cancelOrderModel.Data.confirmServices, this, isOrder);
        lvOrder.setAdapter(adapter);
    }

    @OnClick(R.id.btn_cancel)
    public void doCancelRequest() {
        if (!Globals.internetCheck(OrderRequestCancellationActivity.this))
            return;

        if (edtReasonToCancel.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.please_enter_reason_to_cancel);
            return;
        }

        JSONObject params = HttpRequestHandler.getInstance().doCancelRequest(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                requestId,
                cancelOrderModel.Data.confirmServices.get(0).serviceId,
                cancelOrderModel.Data.confirmServices.get(0).timeSlotId,
                edtReasonToCancel.getText().toString(),
                cancelOrderModel.Data.confirmServices.get(0).flatFee);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(OrderRequestCancellationActivity.this);

        HttpRequestHandler.getInstance().post(OrderRequestCancellationActivity.this, getString(R.string.doCancelRequest), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel.Result) {
                    setResult(RESULT_OK);
                    finish();
                } else {
                    Toaster.shortToast(dataModel.Message);
                    if (dataModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(OrderRequestCancellationActivity.this);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }
}
