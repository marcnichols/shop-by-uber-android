package com.shopbyuber.user;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.MyPurchasesType;
import com.shopbyuber.enums.UserRole;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.model.OrderDetailModel;
import com.shopbyuber.seller.SellerLandingActivity;
import com.shopbyuber.seller.SellerLocationUpdateActivity;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;

import static com.shopbyuber.enums.MyPurchasesType.NotStarted;
import static com.shopbyuber.enums.MyPurchasesType.OnRoute;
import static com.shopbyuber.enums.MyPurchasesType.Requested;

public class OrderDetailActivity extends BaseActivity {

    Globals globals;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.img_order)
    ImageView imgOrder;
    @BindView(R.id.tv_request_title)
    TextView tvRequestTitle;
    @BindView(R.id.tv_lbl_amount)
    TextView tvLblAmount;
    @BindView(R.id.tv_amount_paid)
    TextView tvAmountPaid;
    @BindView(R.id.layout_amount_paid)
    LinearLayout layoutAmountPaid;
    @BindView(R.id.tv_payment_method)
    TextView tvPaymentMethod;
    @BindView(R.id.layout_payment_method)
    LinearLayout layoutPaymentMethod;
    @BindView(R.id.tv_service_provider)
    TextView tvServiceProvider;
    @BindView(R.id.layout_service_provider)
    LinearLayout layoutServiceProvider;
    @BindView(R.id.tv_category)
    TextView tvCategory;

    @BindView(R.id.tv_sub_category)
    TextView tv_sub_category;

    @BindView(R.id.layout_category)
    LinearLayout layoutCategory;
    @BindView(R.id.tv_status)
    TextView tvStatus;
    @BindView(R.id.layout_status)
    LinearLayout layoutStatus;
    @BindView(R.id.tv_schedule)
    TextView tvSchedule;
    @BindView(R.id.layout_schedule)
    LinearLayout layoutSchedule;
    @BindView(R.id.btn_order)
    Button btnOrder;
    @BindView(R.id.tv_cancel_order)
    TextView tvCancelOrder;
    @BindView(R.id.layout_main)
    LinearLayout layoutMain;

    @BindView(R.id.tv_service_detail)
    TextView tv_service_detail;

    @BindView(R.id.layout_reason)
    LinearLayout layoutReason;
    @BindView(R.id.tv_reason)
    TextView tvReason;

    MyPurchasesType purchasesType;

    String requestId = "", pubChannelName = "";
    OrderDetailModel orderDetailModel;
    int reqCode = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        globals = (Globals) getApplicationContext();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            requestId = bundle.getString(Constants.SBU_RequestId) != null ? bundle.getString(Constants.SBU_RequestId) : "";
            if (bundle.containsKey(Constants.SBU_NotificationId) && Globals.isNetworkAvailable(this)) {
                globals.doMakeNotificationUnread(bundle.getString(Constants.SBU_NotificationId));
            }
        }

        setSupportActionBar(toolbar);
        setUpData();
    }

    private void setUpData() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.toolbar_title_view_request));
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderDetailActivity.this.onBackPressed();
            }
        });
        doGetOrderDetailRequest();
    }

    @Override
    public void onBackPressed() {
        if (isTaskRoot()) {
            if (globals.getUserData().Data.userRole == UserRole.BUYER.getRole())
                startActivity(new Intent(this, UserLandingActivity.class));
            else
                startActivity(new Intent(this, SellerLandingActivity.class));
            finish();
        } else
            super.onBackPressed();
    }

    public void doGetOrderDetailRequest() {
        if (!Globals.internetCheck(OrderDetailActivity.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().GetOrderDetailParams(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                requestId);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(OrderDetailActivity.this);

        HttpRequestHandler.getInstance().post(OrderDetailActivity.this, getString(R.string.getOrderDetail), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                orderDetailModel = new Gson().fromJson(response.toString(), OrderDetailModel.class);
                if (orderDetailModel.Result) {
                    layoutMain.setVisibility(View.VISIBLE);
                    setData(orderDetailModel);
                } else {
                    Toaster.shortToast(orderDetailModel.Message);
                    if (orderDetailModel.Message.contains(getString(R.string.invalid_session)))
                        globals.doLogout(OrderDetailActivity.this);
                    else
                        onBackPressed();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
                onBackPressed();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
                onBackPressed();
            }
        });
    }

    public void setData(OrderDetailModel data) {
        Glide.with(this)
                .load(data.Data.serviceImage)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .centerCrop()
                .placeholder(R.drawable.image_place_holder)
                .crossFade()
                .dontAnimate()
                .into(imgOrder);

        tvRequestTitle.setText(data.Data.screenName);
        tvAmountPaid.setText(getString(R.string.currency_symbol) + " " + data.Data.amountPaid);
        tvPaymentMethod.setText(data.Data.paymentMethod);
        tvServiceProvider.setText(data.Data.serviceProvider);
        tvCategory.setText(data.Data.catName);
        tv_sub_category.setText(data.Data.subCatName == null ? "" : data.Data.subCatName);
        tv_service_detail.setText(data.Data.serviceDetail);

        tvSchedule.setText(data.Data.schedule);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            tvReason.setText(Html.fromHtml(getString(R.string.lbl_reason_to_cancel_with_color, data.Data.cancelReason == null ? "" : data.Data.cancelReason),
                    Html.FROM_HTML_MODE_COMPACT));
        else
            tvReason.setText(Html.fromHtml(getString(R.string.lbl_reason_to_cancel_with_color, data.Data.cancelReason == null ? "" : data.Data.cancelReason)));

        purchasesType = MyPurchasesType.fromInt(data.Data.status);
        pubChannelName = data.Data.pubChannelName;

        //layout changes as per service status
        String status = "";
        switch (purchasesType) {
            case Requested:
                status = getString(R.string.not_started_black);
                tvLblAmount.setText(getString(R.string.amount));
                tvCancelOrder.setVisibility(View.VISIBLE);
                layoutPaymentMethod.setVisibility(View.GONE);
                btnOrder.setVisibility(View.GONE);
                break;
            case Confirmed:
                startActivity(new Intent(this, PaymentConfirmationActivity.class));
                finish();
                break;
            case NotStarted:
                status = getString(R.string.not_started_black);
                tvCancelOrder.setVisibility(View.VISIBLE);
                btnOrder.setVisibility(View.GONE);
                break;
            case OnRoute:
                status = getString(R.string.on_the_way_black);
                btnOrder.setText(getString(R.string.start_tracking));
                tvCancelOrder.setVisibility(View.VISIBLE);
                break;
            case InProgress:
                status = getString(R.string.inprogress_green);
                btnOrder.setVisibility(View.GONE);
                break;
            case Completed:
                status = getString(R.string.completed_black);
                if (data.Data.isFeedbackGiven)
                    btnOrder.setVisibility(View.GONE);
                btnOrder.setText(getString(R.string.rate_review_us));
                break;
            case Cancelled:
                if (!data.Data.isRefund)
                    layoutAmountPaid.setVisibility(View.GONE);
                status = getString(R.string.cancelled_by_text, data.Data.cancelByText);

                btnOrder.setVisibility(View.GONE);
                layoutReason.setVisibility(View.VISIBLE);
                break;
            default:
                break;

        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvStatus.setText(Html.fromHtml(status, Html.FROM_HTML_MODE_COMPACT));
        } else {
            tvStatus.setText(Html.fromHtml(status));
        }
    }

    @OnClick(R.id.btn_order)
    public void onOrderClicked() {
        switch (purchasesType) {
            case Completed:
                startActivityForResult(new Intent(this, RateReviewActivity.class)
                        .putExtra(Constants.SBU_ServiceId, orderDetailModel.Data.serviceId)
                        .putExtra(Constants.SBU_RequestId, orderDetailModel.Data.requestId), reqCode);
                break;
            case OnRoute:
                ArrayList<String> createdPubChannelNames = new ArrayList<>();
                createdPubChannelNames.add(orderDetailModel.Data.requestId);
                startActivity(new Intent(this, SellerLocationUpdateActivity.class)
                        .putStringArrayListExtra(Constants.SBU_CreatedPubChannelNames, createdPubChannelNames));
                break;
            default:
                break;
        }
    }

    @OnClick(R.id.tv_cancel_order)
    public void onCancelClicked() {
        if (purchasesType == OnRoute || purchasesType == NotStarted || purchasesType == Requested) {
            startActivityForResult(new Intent(this, OrderRequestCancellationActivity.class)
                    .putExtra(Constants.SBU_RequestId, orderDetailModel.Data.requestId), reqCode);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            requestId = bundle.getString(Constants.SBU_RequestId) != null ? bundle.getString(Constants.SBU_RequestId) : "";
            setUpData();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == reqCode && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            if (isTaskRoot()) {
                if (globals.getUserData().Data.userRole == UserRole.BUYER.getRole())
                    startActivity(new Intent(this, UserLandingActivity.class));
                else
                    startActivity(new Intent(this, SellerLandingActivity.class));
                finish();
            } else
                finish();
        }
    }
}
