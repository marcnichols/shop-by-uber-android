package com.shopbyuber.user;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.ServiceStatus;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.model.GetPicksCompanyServiceModel.AvailableTimeSlot;
import com.shopbyuber.model.GetPicksCompanyServiceModel.Data;
import com.shopbyuber.model.GetPicksCompanyServiceModel.Services;
import com.shopbyuber.utils.FusedLocationManager;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;
import info.hoang8f.android.segmented.SegmentedGroup;


class AdapterServicePicks extends ArrayAdapter<Services> implements OnClickListener {

    private Data dataSet;
    private Activity mContext;
    private Globals globals;
    private FusedLocationManager fusedLocationManager;

    AdapterServicePicks(Data data, Activity context, FusedLocationManager fusedLocationManager) {
        super(context, R.layout.get_picks_service_item, data.services);
        this.dataSet = data;
        this.mContext = context;
        globals = (Globals) Globals.getContext();
        this.fusedLocationManager = fusedLocationManager;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Services dataModel = getItem(position);
        final ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.get_picks_service_item, parent, false);
            viewHolder = new ViewHolder(convertView);

            if (!fusedLocationManager.getAddress().isEmpty()) {
                dataModel.address = fusedLocationManager.getAddress();
                dataModel.latitude = fusedLocationManager.getLatLng().latitude;
                dataModel.longitude = fusedLocationManager.getLatLng().longitude;
            }

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            if (dataModel.address.isEmpty()) {
                if (!fusedLocationManager.getAddress().isEmpty()) {
                    dataModel.address = fusedLocationManager.getAddress();
                    dataModel.latitude = fusedLocationManager.getLatLng().latitude;
                    dataModel.longitude = fusedLocationManager.getLatLng().longitude;
                }
            }
        }
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        if (position > 0) {
            int topMargin = (int) TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    mContext.getResources().getDimension(R.dimen.dim_10),
                    mContext.getResources().getDisplayMetrics()
            );
            params.setMargins(0, topMargin, 0, 0);
        } else {
            params.setMargins(0, 0, 0, 0);
        }

        if (viewHolder.layoutDayTime.getParent() != null) viewHolder.layoutDayTime.removeAllViews();

        if (dataModel.availableTimeSlot.isEmpty() && dataModel.serviceAvailable) {
            viewHolder.btnSendRequestToBook.setEnabled(true);
            viewHolder.layoutDayTime.setVisibility(View.GONE);
        } else if (!dataModel.availableTimeSlot.isEmpty()) {
            viewHolder.layoutDayTime.setVisibility(View.VISIBLE);
            addDayTimeLayout(viewHolder.layoutDayTime, position);
            viewHolder.btnSendRequestToBook.setEnabled(true);
        } else {
            viewHolder.layoutDayTime.setVisibility(View.VISIBLE);
            viewHolder.btnSendRequestToBook.setEnabled(false);
            viewHolder.layoutDayTime.addView(LayoutInflater.from(mContext).inflate(R.layout.no_day_time_layout,
                    viewHolder.layoutDayTime, false));
        }
        viewHolder.tvDetail.setVisibility(View.VISIBLE);
        viewHolder.tvAddAnotherDayBooking.setVisibility(View.GONE);
        viewHolder.rbFavouriteService.setChecked(dataModel.isFavourite);

        setData(viewHolder, dataSet.services.get(position));

        viewHolder.imgInfo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dataSet.companyInfo.companyDescription == "" && dataSet.companyInfo.companyDescription == null)
                    infoDialog(mContext.getString(R.string.no_description_available));
                else
                    infoDialog(dataSet.companyInfo.companyDescription);
            }
        });


        viewHolder.rbFavouriteService.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AdapterServicePicks.this.doAddToFavoriteServiceRequest(viewHolder, position);
            }
        });
        viewHolder.rbFavourite.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AdapterServicePicks.this.doAddToFavoriteServiceCompany(viewHolder);
            }
        });

        viewHolder.btnSendRequestToBook.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Integer> selectedItem = new ArrayList<>();
                List<String> selectedTimeSlot = new ArrayList<>();
                String duration = "";

                if (dataModel.availableTimeSlot.isEmpty() && dataModel.serviceAvailable) {
                    showNoDayTimeAvailable();
                    return;
                }
                for (int i = 0; i < viewHolder.layoutDayTime.getChildCount(); i++) {
                    Spinner sp = viewHolder.layoutDayTime.getChildAt(i).findViewById(R.id.sp_day_time);
                    selectedItem.add(i, sp.getSelectedItemPosition());
                    duration = dataSet.services.get(position).availableTimeSlot.get(sp.getSelectedItemPosition()).duration;
                    selectedTimeSlot.add(dataSet.services.get(position).availableTimeSlot.get(sp.getSelectedItemPosition()).timeslotId);
                }

                Set<Integer> set = new HashSet<>(selectedItem);
                if (set.size() < selectedItem.size()) {
                    Toaster.shortToast(R.string.please_select_different_time_slot);
                } else {
                    if (!dataSet.services.get(position).address.isEmpty())
                        showConfirmTimeAlert(position, selectedTimeSlot, duration);
                    else Toaster.shortToast(mContext.getString(R.string.please_enter_address));

                }
            }
        });

        viewHolder.segmentCurrentLocation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!fusedLocationManager.getAddress().isEmpty()) {
                    dataModel.address = fusedLocationManager.getAddress();
                    LatLng latLng = fusedLocationManager.getLatLng();
                    dataModel.latitude = latLng.latitude;
                    dataModel.longitude = latLng.longitude;
                    AdapterServicePicks.this.notifyDataSetChanged();
                }
            }
        });
        viewHolder.segmentEnterLocation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    GetPicksCompanyServiceActivity.getInstance().getAddressFromMap(position);
                } catch (GooglePlayServicesNotAvailableException | GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                }
            }
        });
        viewHolder.btn_book_now.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!dataSet.services.get(position).address.isEmpty())
                    showConfirmBookNow(position);
                else Toaster.shortToast(mContext.getString(R.string.please_enter_address));


            }
        });

        return convertView;
    }

    private void showBookNowDone(String msg) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        alertDialog.setMessage(msg);
        alertDialog.setCancelable(false);
        // On pressing Settings button
        alertDialog.setPositiveButton(mContext.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                return;
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    private void showConfirmTimeAlert(final int position, final List<String> selectedTimeSlot, String duration) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setTitle(mContext.getString(R.string.sure));
        // Setting Dialog Message
        alertDialog.setMessage(mContext.getString(R.string.you_want_to_request_an_appointment, duration));
        alertDialog.setCancelable(false);
        // On pressing Settings button
        alertDialog.setPositiveButton(mContext.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                doSendRequestToBookRequest(position, new JSONArray(selectedTimeSlot));
            }
        });

        alertDialog.setNegativeButton(mContext.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    private void showConfirmBookNow(final int position) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setTitle(mContext.getString(R.string.sure));
        // Setting Dialog Message
        alertDialog.setMessage(mContext.getString(R.string.booknow_confirm_service_provider_to_your_location));
        alertDialog.setCancelable(false);
        // On pressing Settings button
        alertDialog.setPositiveButton(mContext.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                List<String> selectedTimeSlot = new ArrayList<>();
                selectedTimeSlot.add("");
                doSendRequestToBookRequest(position, new JSONArray(selectedTimeSlot));
            }
        });

        alertDialog.setNegativeButton(mContext.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    private void showNoDayTimeAvailable() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        // Setting Dialog Message
        alertDialog.setMessage(mContext.getString(R.string.no_day_time_available_for_booking));
        alertDialog.setCancelable(false);
        // On pressing Settings button
        alertDialog.setPositiveButton(mContext.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    private void setData(ViewHolder viewHolder, Services service) {
        if (service.serviceAvailable)
            viewHolder.btn_book_now.setVisibility(View.VISIBLE);
        else
            viewHolder.btn_book_now.setVisibility(View.GONE);
        if (VERSION.SDK_INT >= VERSION_CODES.N)
            viewHolder.tvNote.setText(Html.fromHtml(mContext.getString(R.string.service_detail_note), Html.FROM_HTML_MODE_COMPACT));
        else
            viewHolder.tvNote.setText(Html.fromHtml(mContext.getString(R.string.service_detail_note)));

        if (service.serviceAvailable) {
            viewHolder.tvAvailableService.setTextColor(ContextCompat.getColor(mContext, R.color.available_orange));
            viewHolder.tvAvailableService.setText(mContext.getString(R.string.available_now));
        } else {
            viewHolder.tvAvailableService.setTextColor(ContextCompat.getColor(mContext, R.color.green));
            viewHolder.tvAvailableService.setText(mContext.getString(R.string.available_by_appointment));
        }

        Glide.with(mContext)
                .load(service.serviceImage)
                .centerCrop()
                .placeholder(R.drawable.image_place_holder)
                .crossFade()
                .dontAnimate()
                .into(viewHolder.imgService);

        viewHolder.tvServiceTitle.setText(service.screenName);
        try {
            if (Double.parseDouble(service.flatFee) > 0)
                viewHolder.tvPrice.setText(mContext.getString(R.string.currency_symbol) + " " + service.flatFee);
            else
                viewHolder.tvPrice.setText("");
        } catch (Exception e) {
            Logger.d(e.getMessage());
        }

        Glide.with(mContext)
                .load(dataSet.companyInfo.companyProfileImage)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .centerCrop()
                .placeholder(R.drawable.image_place_holder)
                .crossFade()
                .dontAnimate()
                .into(viewHolder.imgProfile);

        viewHolder.rbFavourite.setChecked(dataSet.companyInfo.isFavourite);

        viewHolder.tvContent.setText(dataSet.companyInfo.comapnyScreenName);

        viewHolder.tvDetail.setText(service.catName);
        viewHolder.tv_offer_details.setText(service.serviceDetail);
        viewHolder.tvEnterLocation.setText(service.address);
        viewHolder.tv_vendor_location.setText(service.companyAddress);

        if (service.companyAddress.isEmpty()) {
            viewHolder.tv_header_vendor_location.setVisibility(View.GONE);
        } else {
            viewHolder.tv_header_vendor_location.setVisibility(View.VISIBLE);
            viewHolder.tv_header_vendor_location.setText(service.companyAddress);
        }
    }

    private void infoDialog(String description) {
        final Dialog dialog = new Dialog(mContext, R.style.CustomDialogTheme);
        dialog.setContentView(R.layout.service_info_dialog);
        dialog.setCancelable(false);

        TextView tv_detail = dialog.findViewById(R.id.tv_detail);
        tv_detail.setText(description);
        tv_detail.setMovementMethod(new ScrollingMovementMethod());

        dialog.show();

        Button btn_ok = dialog.findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void addDayTimeLayout(LinearLayout layoutDayTime, int position) {
        View layout = LayoutInflater.from(mContext).inflate(R.layout.day_time_layout, layoutDayTime, false);

        Spinner sp_day_time = layout.findViewById(R.id.sp_day_time);
        ArrayAdapter<AvailableTimeSlot> adapter = new ArrayAdapter<>(
                mContext, android.R.layout.simple_spinner_item, dataSet.services.get(position).availableTimeSlot);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_day_time.setAdapter(adapter);
        layoutDayTime.addView(layout);
    }

    private void doAddToFavoriteServiceRequest(final ViewHolder viewHolder, final int position) {
        if (!Globals.internetCheck(mContext))
            return;

        JSONObject params = HttpRequestHandler.getInstance().AddToFavoriteServiceParams(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                dataSet.services.get(position).serviceId);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(mContext);

        HttpRequestHandler.getInstance().post(mContext, mContext.getString(R.string.addToFavoriteService), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel.Result) {
                    dataSet.services.get(position).isFavourite = !dataSet.services.get(position).isFavourite;
                    viewHolder.rbFavouriteService.setChecked(dataSet.services.get(position).isFavourite);
                } else if (dataModel.Message.contains(mContext.getString(R.string.invalid_session)))
                    globals.doLogout(mContext);
                Toaster.shortToast(dataModel.Message);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void doAddToFavoriteServiceCompany(final ViewHolder viewHolder) {
        if (!Globals.internetCheck(mContext))
            return;

        JSONObject params = HttpRequestHandler.getInstance().AddToFavoriteCompanyParams(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                dataSet.companyInfo.companyId);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(mContext);

        HttpRequestHandler.getInstance().post(mContext, mContext.getString(R.string.addToFavoriteCompany), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);
                if (dataModel.Result) {
                    dataSet.companyInfo.isFavourite = !dataSet.companyInfo.isFavourite;
                    viewHolder.rbFavourite.setChecked(dataSet.companyInfo.isFavourite);
                } else {
                    if (dataModel.Message.contains(mContext.getString(R.string.invalid_session)))
                        globals.doLogout(mContext);
                }
                Toaster.shortToast(dataModel.Message);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void doSendRequestToBookRequest(int position, JSONArray jsonArray) {
        if (!Globals.internetCheck(mContext))
            return;

        JSONObject params = HttpRequestHandler.getInstance().sendRequestToBookParams(
                globals.getUserData().Data.userId,
                globals.getUserData().Data.sessionId,
                dataSet.services.get(position).serviceId,
                jsonArray,
                ServiceStatus.Requested.getService()
                , dataSet.services.get(position).flatFee
                , dataSet.services.get(position).address
                , dataSet.services.get(position).latitude
                , dataSet.services.get(position).longitude
                , "");

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(mContext);

        HttpRequestHandler.getInstance().post(mContext, mContext.getString(R.string.sendRequestToBook), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);

                if (dataModel.Result) {
                    Intent intent = new Intent(mContext, UserLandingActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    mContext.startActivity(intent);
                    (mContext).finish();
                } else if (dataModel.Message.contains(mContext.getString(R.string.invalid_session)))
                    globals.doLogout(mContext);
                else showBookNowDone(dataModel.Message);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    static class ViewHolder {
        @BindView(R.id.img_profile)
        ImageView imgProfile;
        @BindView(R.id.tv_content)
        TextView tvContent;
        @BindView(R.id.tv_detail)
        TextView tvDetail;
        @BindView(R.id.tv_available)
        TextView tvAvailable;
        @BindView(R.id.rb_favourite)
        RadioButton rbFavourite;
        @BindView(R.id.img_service)
        ImageView imgService;
        @BindView(R.id.tv_service_title)
        TextView tvServiceTitle;
        @BindView(R.id.rb_favourite_service)
        RadioButton rbFavouriteService;
        @BindView(R.id.tv_available_service)
        TextView tvAvailableService;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_flat_fee_only)
        TextView tvFlatFeeOnly;
        @BindView(R.id.img_info)
        ImageView imgInfo;
        @BindView(R.id.layout_day_time)
        LinearLayout layoutDayTime;
        @BindView(R.id.segment_current_location)
        RadioButton segmentCurrentLocation;
        @BindView(R.id.segment_enter_location)
        RadioButton segmentEnterLocation;
        @BindView(R.id.segmented_group)
        SegmentedGroup segmentedGroup;
        @BindView(R.id.btn_send_request_to_book)
        Button btnSendRequestToBook;
        @BindView(R.id.tv_add_another_day_booking)
        TextView tvAddAnotherDayBooking;
        @BindView(R.id.tv_note)
        TextView tvNote;
        @BindView(R.id.tv_enter_location)
        TextView tvEnterLocation;
        @BindView(R.id.tv_offer_details)
        TextView tv_offer_details;
        @BindView(R.id.card_title)
        CardView card_title;
        @BindView(R.id.btn_book_now)
        Button btn_book_now;
        @BindView(R.id.tv_vendor_location)
        TextView tv_vendor_location;
        @BindView(R.id.tv_header_vendor_location)
        TextView tv_header_vendor_location;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
