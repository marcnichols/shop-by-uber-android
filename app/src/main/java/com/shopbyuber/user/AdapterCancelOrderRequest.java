package com.shopbyuber.user;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.model.CancelOrderModel;
import com.shopbyuber.model.CancelOrderModel.ConfirmServices;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

class AdapterCancelOrderRequest extends ArrayAdapter<ConfirmServices> implements View.OnClickListener {

    private List<ConfirmServices> dataSet;
    private Context mContext;
    private boolean isOrder = false;

    AdapterCancelOrderRequest(List<ConfirmServices> data, Context context, boolean isOrder) {
        super(context, isOrder ? R.layout.order_cancellation_user_item : R.layout.request_cancellation_vendor_item, data);
        this.dataSet = data;
        this.mContext = context;
        this.isOrder = isOrder;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        OrderViewHolder orderViewHolder;
        RequestViewHolder requestViewHolder;

        if (isOrder) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.order_cancellation_user_item, parent, false);
                orderViewHolder = new OrderViewHolder(convertView);
                convertView.setTag(orderViewHolder);
            } else {
                orderViewHolder = (OrderViewHolder) convertView.getTag();
            }
            setOrderData(orderViewHolder, dataSet.get(position));
        } else {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.request_cancellation_vendor_item, parent, false);
                requestViewHolder = new RequestViewHolder(convertView);
                convertView.setTag(requestViewHolder);
            } else {
                requestViewHolder = (RequestViewHolder) convertView.getTag();
            }
            setRequestData(requestViewHolder, dataSet.get(position));
        }
        return convertView;
    }

    private void setOrderData(OrderViewHolder viewHolder, ConfirmServices confirmServices) {
        try {
            Glide.with(mContext)
                    .load(confirmServices.serviceImage)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .centerCrop()
                    .placeholder(R.drawable.image_place_holder)
                    .crossFade()
                    .dontAnimate()
                    .into(viewHolder.imgProfile);
            viewHolder.tvServiceTitle.setText(confirmServices.screenName);
            viewHolder.tvServicePrice.setText(mContext.getString(R.string.currency_symbol) + " " + confirmServices.flatFee);
            viewHolder.tvConfirmDayTime.setText(confirmServices.duration);

            Glide.with(mContext)
                    .load(confirmServices.details.profileImage)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .centerCrop()
                    .placeholder(R.drawable.image_place_holder_payment)
                    .crossFade()
                    .dontAnimate()
                    .into(viewHolder.imgServiceProvider);
            viewHolder.tvProviderName.setText(confirmServices.details.name);
            viewHolder.tvSpeciality.setText(confirmServices.details.speciality);
            viewHolder.tvDescription.setText(confirmServices.details.description);

        } catch (Exception e) {
            Logger.d(e);
        }
    }

    private void setRequestData(RequestViewHolder viewHolder, CancelOrderModel.ConfirmServices confirmServices) {
        try {
            Glide.with(mContext)
                    .load(confirmServices.serviceImage)
                    .centerCrop()
                    .placeholder(R.drawable.image_place_holder)
                    .crossFade()
                    .dontAnimate()
                    .into(viewHolder.imgProfile);
            viewHolder.tvServiceTitle.setText(confirmServices.screenName);
            viewHolder.tvServicePrice.setText(mContext.getString(R.string.currency_symbol) + " " + confirmServices.flatFee);

            Glide.with(mContext)
                    .load(confirmServices.details.profileImage)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .centerCrop()
                    .placeholder(R.drawable.image_place_holder_payment)
                    .crossFade()
                    .dontAnimate()
                    .into(viewHolder.imgBuyer);

            viewHolder.tvConfirmDayTime.setText(confirmServices.duration);
            viewHolder.tvRequestedBy.setText(confirmServices.details.name);

        } catch (Exception e) {
            Logger.d(e);
        }
    }

    @Override
    public void onClick(View v) {

    }

    static class OrderViewHolder {
        @BindView(R.id.img_profile)
        ImageView imgProfile;
        @BindView(R.id.tv_service_title)
        TextView tvServiceTitle;
        @BindView(R.id.tv_service_price)
        TextView tvServicePrice;
        @BindView(R.id.tv_confirm_day_time)
        TextView tvConfirmDayTime;
        @BindView(R.id.tv_provider_name)
        TextView tvProviderName;
        @BindView(R.id.tv_speciality)
        TextView tvSpeciality;
        @BindView(R.id.tv_description)
        TextView tvDescription;
        @BindView(R.id.img_service_provider)
        ImageView imgServiceProvider;

        OrderViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    static class RequestViewHolder {
        @BindView(R.id.img_profile)
        ImageView imgProfile;
        @BindView(R.id.tv_service_title)
        TextView tvServiceTitle;
        @BindView(R.id.tv_service_price)
        TextView tvServicePrice;
        @BindView(R.id.img_buyer)
        CircleImageView imgBuyer;
        @BindView(R.id.tv_confirm_day_time)
        TextView tvConfirmDayTime;
        @BindView(R.id.tv_requested_by)
        TextView tvRequestedBy;

        RequestViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}

