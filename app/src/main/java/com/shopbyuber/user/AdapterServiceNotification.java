package com.shopbyuber.user;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.shopbyuber.R;
import com.shopbyuber.model.NotificationModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.shopbyuber.R.id.tvDecription;


public class AdapterServiceNotification extends RecyclerView.Adapter<AdapterServiceNotification.ViewHolder> {

    private List<NotificationModel.Notifications> mDataSet;
    private final Context mContext;
    private AdapterView.OnItemClickListener onItemClickListener;
    private boolean fromNotification;

    public AdapterServiceNotification(Context context, boolean fromNotification) {
        mContext = context;
        this.fromNotification = fromNotification;
    }

    public void doRefresh(List<NotificationModel.Notifications> notificationModels) {
        mDataSet = notificationModels;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_item, parent, false);
        return new ViewHolder(view, this);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private AdapterServiceNotification adapterServiceNotification;
        @BindView(R.id.img_service)
        ImageView img_service;
        @BindView(R.id.tv_service_title)
        TextView tv_service_title;
        @BindView(R.id.tv_service_detail)
        TextView tv_service_detail;
        @BindView(tvDecription)
        TextView tv_decription;
        @BindView(R.id.tv_notification)
        TextView tv_notification;
        @BindView(R.id.cv_notification)
        CardView cv_notification;

        @BindView(R.id.lnr_desc)
        LinearLayout lnr_desc;

        public ViewHolder(View itemView, AdapterServiceNotification adapterShopByUberPicks) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.adapterServiceNotification = adapterShopByUberPicks;
            itemView.setOnClickListener(this);
        }

        void setDataToView(NotificationModel.Notifications mItem, ViewHolder viewHolder) {
            viewHolder.tv_service_title.setText(mItem.screenName);

            if (fromNotification) {
                if (mItem.isRead)
                    cv_notification.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
                else
                    cv_notification.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.light_green));
            } else {
                viewHolder.tv_decription.setEllipsize(null);
                viewHolder.tv_decription.setMaxLines(2);
            }
            viewHolder.tv_decription.setText(mItem.message);
            Glide.with(mContext)
                    .load(mItem.serviceImage)
                    .centerCrop()
                    .placeholder(R.drawable.image_place_holder)
                    .crossFade()
                    .dontAnimate()
                    .into(viewHolder.img_service);
        }

        @Override
        public void onClick(View v) {
            adapterServiceNotification.onItemHolderClick(ViewHolder.this);
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        NotificationModel.Notifications mItem = mDataSet.get(position);
        holder.setDataToView(mItem, holder);
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void onItemHolderClick(ViewHolder holder) {
        if (onItemClickListener != null)
            onItemClickListener.onItemClick(null, holder.itemView, holder.getAdapterPosition(), holder.getItemId());
    }
}
