package com.shopbyuber.api;

import android.content.Context;
import android.graphics.Color;

import com.google.android.gms.maps.model.LatLng;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.enums.PaymentEnums;
import com.shopbyuber.enums.UserRole;
import com.shopbyuber.model.ConfirmRequestModel;
import com.shopbyuber.model.ConfirmRequestModel.ConfirmServices;
import com.shopbyuber.model.ServiceOccupiedDataModel.allocatedDates;
import com.shopbyuber.model.ServiceOccupiedDataModel.availableSchedule;
import com.shopbyuber.model.ServiceOccupiedDataModel.slotes;
import com.shopbyuber.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.entity.StringEntity;


public class HttpRequestHandler {
    String TAG = getClass().getName();
    private static final String HEADER_TYPE_JSON = "application/json";
    private static HttpRequestHandler mInstance = null;
    private final AsyncHttpClient client;

    public static HttpRequestHandler getInstance() {
        if (mInstance == null) {
            mInstance = new HttpRequestHandler();
        }
        return mInstance;
    }

    public HttpRequestHandler() {
        client = new AsyncHttpClient();
        client.setTimeout(60000);
    }

    public void postFile(Context context, String api, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        try {
            String url = getURL(context, api);
            Logger.d(TAG, "URL :: " + url);
            Logger.d(TAG, "Request :: " + params.toString());

            client.post(url, params, responseHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void post(Context context, String api, JSONObject params, AsyncHttpResponseHandler responseHandler) {
        try {
            String url = getURL(context, api);
            Logger.d(url);
            Logger.json(params.toString());
            StringEntity entity = new StringEntity(params.toString(), "UTF-8");

            client.post(context, url, entity, HEADER_TYPE_JSON, responseHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void get(Context context, String url, AsyncHttpResponseHandler responseHandler) {
        try {
            Logger.d(url);
            client.get(context, getURL(context, url), responseHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getURL(Context context, String serviceName) {
        return context.getString(R.string.base_server_url) + context.getString(R.string.server_url) + serviceName;
    }

    public ACProgressFlower getProgressBar(Context context) {
        ACProgressFlower dialog = new ACProgressFlower.Builder(context)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build();
        dialog.setCancelable(false);
        return dialog;
    }

    public JSONObject getRegisterParams(String firstName, String lastName,
                                        String countryCode, String phoneNumber,
                                        String mobilePhone, String facebookId, String twitterId, String email,
                                        String password, int userRole, String deviceToken, String userLocation, LatLng latLng, String ezlistScreenName,
                                        String city, String state, String country) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_FirstName, firstName);
            params.put(Constants.SBU_LastName, lastName);
            params.put(Constants.SBU_MobilePhone, mobilePhone);
            params.put(Constants.SBU_CountryCode, countryCode);
            params.put(Constants.SBU_PhoneNumber, phoneNumber);
            params.put(Constants.SBU_FacebookId, facebookId);
            params.put(Constants.SBU_TwitterId, twitterId);
            params.put(Constants.SBU_Email, email);
            params.put(Constants.SBU_Password, password);
            params.put(Constants.SBU_UserRole, userRole);
            params.put(Constants.SBU_TimeZone, TimeZone.getDefault().getID());
            params.put(Constants.SBU_DeviceType, 1);
            params.put(Constants.SBU_DeviceToken, deviceToken);
            params.put(Constants.SBU_RegisterFrom, Constants.SBU_Android);
            params.put(Constants.SBU_EzListScreenName, ezlistScreenName);

            if (UserRole.BUYER.getRole() == userRole) {
                params.put(Constants.SBU_Address, userLocation);
                params.put(Constants.SBU_Latitude, latLng.latitude);
                params.put(Constants.SBU_Longitude, latLng.longitude);

                params.put(Constants.SBU_City, city);
                params.put(Constants.SBU_State, state);
                params.put(Constants.SBU_Country, country);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getLoginParams(String email, String password, String deviceToken) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_Email, email);
            params.put(Constants.SBU_Password, password);
            params.put(Constants.SBU_DeviceType, 1);
            params.put(Constants.SBU_DeviceToken, deviceToken);
            params.put(Constants.SBU_TimeZone, TimeZone.getDefault().getID());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getSessionValidateParams(String userId, String sessionId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getServiceStatusParams(String userId, String sessionId, String requestId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_RequestId, requestId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getLogoutParams(String userId, String sessionId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject setNotificationDataParams(String userId, String sessionId, int notifyId, boolean isEnabled) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_NotifyId, notifyId);
            params.put(Constants.SBU_IsEnabled, isEnabled);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getAddNewServiceParams1(String userId, String sessionId, String categoryId, String screenName,
                                              String flatFee, String serviceDetail, ArrayList<availableSchedule> availableSchedules,
                                              ArrayList<allocatedDates> dayScheduling, String profileImage, String serviceId, boolean forAdd, boolean editAble, String paypalEmail, int paymentMethod, JSONObject jsnPaymentDetail, String subCategoryId, String searchKeyword, String requestId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_CategoryId, categoryId);
            params.put(Constants.SBU_ScreenName, screenName);
            params.put(Constants.SBU_ServiceEditAble, editAble);
            params.put(Constants.SBU_FlatFee, flatFee);
            params.put(Constants.SBU_ServiceDetail, serviceDetail);
            params.put(Constants.SBU_ProfileImage, profileImage);

            if (!forAdd) params.put(Constants.SBU_ServiceId, serviceId);
            else params.put(Constants.SBU_RequestId, requestId);

            JSONArray daySchedulingJsonArray = new JSONArray();
            for (allocatedDates allocatedDates : dayScheduling) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.SBU_Date, allocatedDates.date);

                // Prepare time slotes array for day
                JSONArray timeSlotesJsonArray = new JSONArray();
                for (slotes slote : allocatedDates.slotes) {
                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put(Constants.SBU_Time, slote.time);
                    jsonObj.put(Constants.SBU_Allocated, slote.isAllocate);
                    timeSlotesJsonArray.put(jsonObj);
                }
                jsonObject.put(Constants.SBU_Timeslot, timeSlotesJsonArray);

                daySchedulingJsonArray.put(jsonObject);
            }
            params.put(Constants.SBU_DayScheduling, daySchedulingJsonArray);

            JSONArray availableScheduleJsonArray = new JSONArray();
            for (availableSchedule schedule : availableSchedules) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(Constants.SBU_Month, schedule.month);
                jsonObject.put(Constants.SBU_Year, schedule.Year);
                jsonObject.put(Constants.SBU_Duration, schedule.duration);
                jsonObject.put(Constants.SBU_Day, new JSONArray(schedule.day));
                jsonObject.put(Constants.SBU_Timings, new JSONArray(schedule.timings));
                availableScheduleJsonArray.put(jsonObject);
            }
            params.put(Constants.SBU_AvailableSchedule, availableScheduleJsonArray);

            params.put(Constants.SBU_PaypalEmail, paypalEmail);
            params.put(Constants.SBU_PaymentMethod, paymentMethod);
            if (paymentMethod != PaymentEnums.PAYPAL.getId() || paymentMethod != PaymentEnums.SELECT_PAYMENT_TYPE.getId())
                params.put(Constants.SBU_PaymentDetail, jsnPaymentDetail);

            params.put(Constants.SBU_SubCategoryId, subCategoryId);
            params.put(Constants.SBU_Keywords, searchKeyword);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getServiceCatagoryParams(String userId, String sessionId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getServiceOccupiedParams(String userId, String sessionId, int month, int year) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_Month, month);
            params.put(Constants.SBU_Year, year);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getServiceOccupiedEditParams(String userId, String sessionId, String serviceId, int month, int year) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_ServiceId, serviceId);
            params.put(Constants.SBU_Month, month);
            params.put(Constants.SBU_Year, year);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getMobileVerificationParams(String userId, String verifyCode, String mobilePhone, String deviceToken) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_VerifyCode, verifyCode);
            params.put(Constants.SBU_MobilePhone, mobilePhone);
            params.put(Constants.SBU_DeviceType, 1);
            params.put(Constants.SBU_DeviceToken, deviceToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getSendAgainCodeParams(String userId, String mobilePhone) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_MobilePhone, mobilePhone);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public RequestParams getUploadCompanyProfileImageParams(File selectedFile, int imageType) {
        RequestParams params = new RequestParams();
        try {
            params.put(Constants.SBU_ProfileImage, selectedFile);
            params.put(Constants.SBU_ImageType, imageType);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getAddCompanyProfileParams(String sessionId, String vendorId, String companyName, String comapnyScreenName, String ownerName, String companyAddress,
                                                 String postal, double latitude, double longitude, String countryCode, String phoneNumber, /*String companyPhone,*/ String websiteUrl, String serviceDescription, String fileName, String paypalEmail, String firstName, String lastName, int paymentMethod, JSONObject jsnPaymentDetail, String promoCode, String state, String city) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_VendorId, vendorId);
            params.put(Constants.SBU_CompanyName, companyName);
            params.put(Constants.SBU_ComapnyScreenName, comapnyScreenName);
            params.put(Constants.SBU_OwnerName, ownerName);
            params.put(Constants.SBU_CompanyAddress, companyAddress);
            params.put(Constants.SBU_Zipcode, postal);
            params.put(Constants.SBU_CompanyPhoneNumber, phoneNumber);
            params.put(Constants.SBU_CountryCode, countryCode);
            params.put(Constants.SBU_Latitude, latitude);
            params.put(Constants.SBU_Longitude, longitude);
            params.put(Constants.SBU_WebsiteUrl, websiteUrl);
            params.put(Constants.SBU_ServiceDescription, serviceDescription);
            params.put(Constants.SBU_ProfileImage, fileName);
            params.put(Constants.SBU_PaypalEmail, paypalEmail);
            params.put(Constants.SBU_FirstName, firstName);
            params.put(Constants.SBU_LastName, lastName);
            params.put(Constants.SBU_PaymentMethod, paymentMethod);
            params.put(Constants.SBU_PromoCode, promoCode);
            params.put(Constants.SBU_State, state);
            params.put(Constants.SBU_City, city);
            if (paymentMethod != PaymentEnums.PAYPAL.getId() || paymentMethod != PaymentEnums.SELECT_PAYMENT_TYPE.getId())
                params.put(Constants.SBU_PaymentDetail, jsnPaymentDetail);
            Logger.e(TAG, params.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getVerifyEmailParams(String email) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_Email, email);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getVerifyCodeParams(String userId, int code) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_VerifyCode, code);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getResetPasswordParams(String userId, String password, String deviceToken) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_Password, password);
            params.put(Constants.SBU_DeviceType, 1);
            params.put(Constants.SBU_DeviceToken, deviceToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getEditProfileParams(String userId, String sessionId, String name,
                                           String countryCode, String phoneNumber,
                                           String mobilePhone, String password, String profileImage,
                                           String description, String speciality, String firstName, String lastName, String eZListScreenName) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_Name, name);
            params.put(Constants.SBU_CountryCode, countryCode);
            params.put(Constants.SBU_PhoneNumber, phoneNumber);
            params.put(Constants.SBU_Password, password);
            params.put(Constants.SBU_Mobile, mobilePhone);
            params.put(Constants.SBU_Description, description);
            params.put(Constants.SBU_Speciality, speciality);
            params.put(Constants.SBU_ProfileImage, profileImage);
            params.put(Constants.SBU_FirstName, firstName);
            params.put(Constants.SBU_LastName, lastName);
            params.put(Constants.SBU_EzListScreenName, eZListScreenName);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getShopbyUberPicksParams(String userId, String sessionId, int PageNo, double latitude, double longitude) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_PageNo, PageNo);
            params.put(Constants.SBU_NumOfRecords, Constants.NumberOfRecords);
            params.put(Constants.SBU_Latitude, latitude);
            params.put(Constants.SBU_Longitude, longitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getFavoritesParams(String userId, String sessionId, int PageNo) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_PageNo, PageNo);
            params.put(Constants.SBU_NumOfRecords, Constants.NumberOfRecords);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getMyServicesParams(String userId, String sessionId, int PageNo) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_PageNo, PageNo);
            params.put(Constants.SBU_NumOfRecords, Constants.NumberOfRecords);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getLocalCatagoriesParams(String userId, String sessionId,
                                               double latitude, double longitude,
                                               String zipcode, int pageNo) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_Latitude, latitude);
            params.put(Constants.SBU_Longitude, longitude);
            params.put(Constants.SBU_PageNo, pageNo);
            params.put(Constants.SBU_NumOfRecords, Constants.NumberOfRecords);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }


    public JSONObject getSelectedCategory(String userId, String sessionId, String catId, String zipcode, int pageNo, String subCategoryId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_CategoryId, catId);
            params.put(Constants.SBU_Zipcode, zipcode);
            params.put(Constants.SBU_PageNo, pageNo);
            params.put(Constants.SBU_NumOfRecords, Constants.NumberOfRecords);
            params.put(Constants.SBU_SubCategoryId, subCategoryId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getSearchCategory(String userId, String sessionId, String searchText, int pageNo, double latitude, double longitude) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_SearchText, searchText);
            params.put(Constants.SBU_PageNo, pageNo);
            params.put(Constants.SBU_NumOfRecords, Constants.NumberOfRecords);
            params.put(Constants.SBU_Latitude, latitude);
            params.put(Constants.SBU_Longitude, longitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getCatagoryParams(String userId, String sessionId, int PageNo) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_PageNo, PageNo);
            params.put(Constants.SBU_NumOfRecords, Constants.NumberOfRecords);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getSetServiceAvailabilityParams(String userId, String sessionId, String serviceId, boolean availability) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_ServiceId, serviceId);
            params.put(Constants.SBU_IsAvailable, availability);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getBusinessProfileParams(String userId, String sessionId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getRecentServiceRequestParam(String userId, String sessionId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }


    public JSONObject getUpdateBusinessProfileParams(String sessionId, String vendorId, String companyId, String companyName, String comapnyScreenName, String ownerName,
                                                     String companyAddress, String postal, double latitude, double longitude, String countryCode, String phoneNumber, String companyPhone, String websiteUrl,
                                                     String serviceDescription, String fileName, JSONObject jsnVendorData, String paypalEmail, String firstName, String lastName, int paymentMethod, JSONObject jsnPaymentDetail, String promoCode) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_VendorId, vendorId);
            params.put(Constants.SBU_CompanyId, companyId);
            params.put(Constants.SBU_CompanyName, companyName);
            params.put(Constants.SBU_ComapnyScreenName, comapnyScreenName);
            params.put(Constants.SBU_OwnerName, ownerName);
            params.put(Constants.SBU_CompanyAddress, companyAddress);
            params.put(Constants.SBU_Zipcode, postal);
            params.put(Constants.SBU_CompanyPhoneNumber, phoneNumber);
            params.put(Constants.SBU_CountryCode, countryCode);
            params.put(Constants.SBU_Latitude, latitude);
            params.put(Constants.SBU_Longitude, longitude);
            params.put(Constants.SBU_WebsiteUrl, websiteUrl);
            params.put(Constants.SBU_ServiceDescription, serviceDescription);
            params.put(Constants.SBU_CompanyProfileImage, fileName);
            params.put(Constants.SBU_VendorData, jsnVendorData);
            params.put(Constants.SBU_PaypalEmail, paypalEmail);
            params.put(Constants.SBU_FirstName, firstName);
            params.put(Constants.SBU_LastName, lastName);
            params.put(Constants.SBU_PaymentMethod, paymentMethod);
            params.put(Constants.SBU_PromoCode, promoCode);
            if (paymentMethod != PaymentEnums.PAYPAL.getId() || paymentMethod != PaymentEnums.SELECT_PAYMENT_TYPE.getId())
                params.put(Constants.SBU_PaymentDetail, jsnPaymentDetail);
            Logger.e(TAG, params.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getCompanyServiceParams(String userId, String sessionId, String companyId, String categoryId, String subCategoryId, String serviceId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_CompanyId, companyId);
            params.put(Constants.SBU_CategoryId, categoryId);
            params.put(Constants.SBU_SubCategoryId, subCategoryId);
            params.put(Constants.SBU_ServiceId, serviceId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject AddToFavoriteCompanyParams(String userId, String sessionId, String companyId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_CompanyId, companyId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject AddToFavoriteServiceParams(String userId, String sessionId, String serviceId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_ServiceId, serviceId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject sendRequestToBookParams(String userId, String sessionId, String serviceId, JSONArray appointmentTimeId, int status, String flatFee, String address, double latitude, double longitude, String requestId) {
        JSONObject params = new JSONObject();
        try {
            JSONObject userInfo = new JSONObject();
            userInfo.put(Constants.SBU_Address, address);
            userInfo.put(Constants.SBU_Latitude, latitude);
            userInfo.put(Constants.SBU_Longitude, longitude);

            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_ServiceId, serviceId);
            params.put(Constants.SBU_AppointmentTimeId, appointmentTimeId);
            params.put(Constants.SBU_Status, status);
            params.put(Constants.SBU_RequestFee, flatFee);
            params.put(Constants.SBU_UserInfo, userInfo);
            params.put(Constants.SBU_RequestId, requestId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getMyRequestParams(String userId, String sessionId, int PageNo) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_PageNo, PageNo);
            params.put(Constants.SBU_NumOfRecords, Constants.NumberOfRecords);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject ViewRequestDetailParams(String userId, String sessionId, String requestId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_RequestId, requestId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject ConfirmRequestParams(String userId, String sessionId, String requestId, int status, String pubChannelName) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_RequestId, requestId);
            params.put(Constants.SBU_Status, status);
            params.put(Constants.SBU_PubChannelName, pubChannelName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getRecentAppointmentsParams(String userId, String sessionId, int PageNo) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_PageNo, PageNo);
            params.put(Constants.SBU_NumOfRecords, Constants.NumberOfRecords);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getConfirmServiceDetails(String userId, String sessionId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getCancleSignleRequest(String userId, String sessionId, String requestId, String timeSlotId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_RequestId, requestId);
            params.put(Constants.SBU_TimeSlotId, timeSlotId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject cancelServiceAppointments(String userId, String sessionId, String serviceId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_ServiceId, serviceId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject initPaymentForService(double total, String sessionId, String userId, String requestId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_Total, total);
            params.put(Constants.SBU_RequestId, requestId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject payForServiceAdaptive(String userId, String sessionId, ConfirmRequestModel.Requests requests,
                                            String requestId, String serviceId, String payKey) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_ServiceId, serviceId);
            params.put(Constants.SBU_RequestId, requestId);

            JSONObject jsnUserInfo = new JSONObject();
            jsnUserInfo.put(Constants.SBU_Latitude, requests.userInfo.latitude);
            jsnUserInfo.put(Constants.SBU_Longitude, requests.userInfo.longitude);
            jsnUserInfo.put(Constants.SBU_Address, requests.userInfo.address);
            params.put(Constants.SBU_UserInfo, jsnUserInfo);

            params.put(Constants.SBU_PayKey, payKey);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject payForService(ArrayList<ConfirmServices> confirmServices, String transactionId, double totle, String userId, String sessionId, String create_time) {
        JSONObject params = new JSONObject();
        try {
            JSONArray services = new JSONArray();
            for (int i = 0; i < confirmServices.size(); i++) {
                ArrayList<String> list = new ArrayList<>();
                JSONObject jsnRequestId = new JSONObject();
                JSONObject serviceData = new JSONObject();
                try {
                    serviceData.put(Constants.SBU_ServiceId, confirmServices.get(i).serviceId);
                    for (int i1 = 0; i1 < confirmServices.get(i).requests.size(); i1++) {
                        JSONObject jsnUserInfo = new JSONObject();
                        jsnUserInfo.put(Constants.SBU_Latitude, confirmServices.get(i).requests.get(i1).userInfo.latitude);
                        jsnUserInfo.put(Constants.SBU_Longitude, confirmServices.get(i).requests.get(i1).userInfo.longitude);
                        jsnUserInfo.put(Constants.SBU_Address, confirmServices.get(i).requests.get(i1).userInfo.address);
                        jsnRequestId.put(Constants.SBU_Id, confirmServices.get(i).requests.get(i1).requestId);
                        jsnRequestId.put(Constants.SBU_UserInfo, jsnUserInfo);
                        jsnRequestId.put(Constants.SBU_FlatFee, confirmServices.get(i).flatFee);
                    }
                    serviceData.put(Constants.SBU_RequestId, new JSONArray().put(jsnRequestId));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                services.put(serviceData);
                Logger.d(services);
            }

            params.put(Constants.SBU_Services, services);
            params.put(Constants.SBU_TransactionId, transactionId);
            params.put(Constants.SBU_Total, totle);
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_PaymentDateTime, create_time);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject payment(ArrayList<ConfirmServices> confirmServices, String token, double totle, String userId, String sessionId) {
        JSONObject params = new JSONObject();
        try {
            JSONArray services = new JSONArray();
            for (int i = 0; i < confirmServices.size(); i++) {
                JSONObject jsnRequestId = new JSONObject();
                JSONObject serviceData = new JSONObject();
                try {
                    serviceData.put(Constants.SBU_ServiceId, confirmServices.get(i).serviceId);
                    for (int i1 = 0; i1 < confirmServices.get(i).requests.size(); i1++) {
                        JSONObject jsnUserInfo = new JSONObject();
                        jsnUserInfo.put(Constants.SBU_Latitude, confirmServices.get(i).requests.get(i1).userInfo.latitude);
                        jsnUserInfo.put(Constants.SBU_Longitude, confirmServices.get(i).requests.get(i1).userInfo.longitude);
                        jsnUserInfo.put(Constants.SBU_Address, confirmServices.get(i).requests.get(i1).userInfo.address);
                        jsnRequestId.put(Constants.SBU_Id, confirmServices.get(i).requests.get(i1).requestId);
                        jsnRequestId.put(Constants.SBU_UserInfo, jsnUserInfo);
                        jsnRequestId.put(Constants.SBU_FlatFee, confirmServices.get(i).flatFee);
                    }
                    serviceData.put(Constants.SBU_RequestId, new JSONArray().put(jsnRequestId));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                services.put(serviceData);
                Logger.d(services);
            }

            params.put(Constants.SBU_Services, services);
            params.put(Constants.SBU_Token, token);
            params.put(Constants.SBU_Total, totle);
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject GetOrderDetailParams(String userId, String sessionId, String requestId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_RequestId, requestId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getNotificationList(String userId, String sessionId, int PageNo) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_PageNo, PageNo);
            params.put(Constants.SBU_NumOfRecords, Constants.NumberOfRecords);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getNotificationListFromRequestId(String userId, String sessionId, String requestId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_RequestId, requestId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getUnreadNotification(String userId, String sessionId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getMySalesParam(String userId, String sessionId, int PageNo) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_NumOfRecords, Constants.NumberOfRecords);
            params.put(Constants.SBU_PageNo, PageNo);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject giveRatingFeedbackService(String userId, String sessionId, String serviceId, String companyId, String requestId, float rating, String Feedback) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_ServiceId, serviceId);
            params.put(Constants.SBU_CompanyId, companyId);
            params.put(Constants.SBU_RequestId, requestId);
            params.put(Constants.SBU_Rating, rating);
            params.put(Constants.SBU_Feedback, Feedback);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject makeNotificationUnread(String userId, String sessionId, String notificationId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_NotificationId, notificationId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getRatingScreenData(String userId, String sessionId, String serviceId, String requestId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_ServiceId, serviceId);
            params.put(Constants.SBU_RequestId, requestId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }


    public JSONObject getCancelOrderScreenData(String userId, String sessionId, String requestId, int userRole) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_RequestId, requestId);
            params.put(Constants.SBU_UserRole, userRole);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject doCancelRequest(String userId, String sessionId, String requestId, String serviceId, String timeSlotId, String reason, String flatFee) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_RequestId, requestId);
            params.put(Constants.SBU_ServiceId, serviceId);
            params.put(Constants.SBU_TimeSlotId, timeSlotId);
            params.put(Constants.SBU_CancelReason, reason);
            params.put(Constants.SBU_FlatFee, flatFee);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getContactUsParam(String name, String email, String description) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_Name, name);
            params.put(Constants.SBU_Email, email);
            params.put(Constants.SBU_Description, description);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getPicksCompanyServiceParam(String userId, String sessionId, String companyId, int pageNo) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_CompanyId, companyId);
            params.put(Constants.SBU_PageNo, pageNo);
            params.put(Constants.SBU_NumOfRecords, Constants.NumberOfRecords);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getPubnubData(String userId, String sessionId, List createdPubChannelNames) {
        JSONObject params = new JSONObject();
        JSONArray requestIds = new JSONArray();
        for (int i = 0; i < createdPubChannelNames.size(); i++) {
            requestIds.put(createdPubChannelNames.get(i));
        }
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_RequestIds, requestIds);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getClearDataParams(String userId, String sessionId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getRemoveServiceeParams(String userId, String sessionId, String serviceId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_ServiceId, serviceId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getSwitchUserParam(String userId, String sessionId, int switchTo) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_SwitchTo, switchTo);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getCityListParam(String stateId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_StateId, stateId);
            Logger.e(TAG, params.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getAppointmentOfSpecificDateParam(String userId, String sessionId, String serviceId, String checkDate) {
        JSONObject params = new JSONObject();
        try {

            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_ServiceId, serviceId);
            params.put(Constants.SBU_CheckDate, checkDate);
            Logger.e(TAG, params.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }


    public JSONObject doSubCategoryParam(String userId, String sessionId, String categoryId, boolean allSubcategoryRequired) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_CategoryId, categoryId);
            params.put(Constants.SBU_AllSubcategoryRequired, allSubcategoryRequired);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject doVerifyPromoCode(String sessionId, String userId, String promoCode) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_PromoCode, promoCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getPushNotificationParam(String userId, String sessionId,
                                               String msg, String dialogId, String dialogName,
                                               String receiverId, int quickBloxId, String requestId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_Message, msg);
            params.put(Constants.SBU_DialogId, dialogId);
            params.put(Constants.SBU_DialogName, dialogName);
            params.put(Constants.SBU_ReceiverId, receiverId);
            params.put(Constants.SBU_QuickBloxId, quickBloxId);
            params.put(Constants.SBU_RequestId, requestId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getUpdateQuickBloxIdParam(String userId, String sessionId, int quickBloxId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_QuickBloxId, quickBloxId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getUsersFromQuickbloxIds(String userId, String sessionId, JSONArray quickBloxIds) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_QuickBloxIds, quickBloxIds);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getRequestServiceParams(String userId, String sessionId, String serviceName, String categoryId, String subCategoryId, String description, boolean isForUpdate, String requestId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_ServiceName, serviceName);
            params.put(Constants.SBU_CategoryId, categoryId);
            params.put(Constants.SBU_SubCategoryId, subCategoryId);
            params.put(Constants.SBU_ServiceDescription, description);
            if (isForUpdate)
                params.put(Constants.SBU_RequestId, requestId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getAllWantedListParams(String userId, String sessionId, int PageNo, boolean getAll) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_PageNo, PageNo);
            params.put(Constants.SBU_NumOfRecords, Constants.NumberOfRecords);
            params.put(Constants.SBU_GetAll, getAll);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public JSONObject getWantedDetailsParams(String userId, String sessionId, String requestId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_RequestId, requestId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }
    public JSONObject getWantedReponseListParams(String userId, String sessionId, String requestId) {
        JSONObject params = new JSONObject();
        try {
            params.put(Constants.SBU_UserId, userId);
            params.put(Constants.SBU_SessionId, sessionId);
            params.put(Constants.SBU_RequestId, requestId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

}