package com.shopbyuber.login;

import android.Manifest;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.AddServiceFrom;
import com.shopbyuber.enums.UserRole;
import com.shopbyuber.forgotpassword.EmailVerifyActivity;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.model.UserModel;
import com.shopbyuber.profile.AddNewService;
import com.shopbyuber.profile.CreateProfileActivity;
import com.shopbyuber.seller.SellerLandingActivity;
import com.shopbyuber.user.UserLandingActivity;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.FusedLocationManager;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;
import retrofit2.Call;

public class LoginActivity extends BaseActivity implements PermissionListener {

    private static final String TAG_CANCEL = "Facebook Cancel";

    private static final String TAG_ERROR = "Facebook Error";
    private static final String TAG = LoginActivity.class.getName();

    Globals globals;

    @BindView(R.id.edt_email)
    EditText edt_email;

    @BindView(R.id.edt_password)
    EditText edt_password;

    @BindView(R.id.activity_login_new)
    LinearLayout contentView;

    @BindView(R.id.lnr_register)
    LinearLayout lnr_register;

    // Creating Facebook CallbackManager Value
    public static CallbackManager callbackmanager;

    boolean isFacebook = false;
    TwitterAuthClient mTwitterAuthClient;
    TwitterSession twitterSession;
    PermissionListener permissionListener;
    private FusedLocationManager fusedLocationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        init();
    }

    private void init() {

        super.hideUnhide(contentView, lnr_register);
        globals = (Globals) getApplicationContext();
        configTwitter();
        mTwitterAuthClient = new TwitterAuthClient();
    }

    public void configTwitter() {
        // Configure twitter
        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(Constants.TWITTER_KEY, Constants.TWITTER_SECRET))
                .debug(true)
                .build();
        Twitter.initialize(config);
    }

    @OnClick(R.id.btn_login)
    public void doLogin() {
        if (validation())
            doLoginRequest();
    }

    @OnClick(R.id.tv_forget_password)
    public void verifyEmail() {
        startActivity(new Intent(this, EmailVerifyActivity.class));
    }

    public boolean validation() {
        if (edt_email.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_email);
            return false;
        }
        if (!Globals.validateEmailAddress(edt_email.getText().toString().trim())) {
            Toaster.shortToast(R.string.err_enter_valid_email);
            return false;
        }
        if (edt_password.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_password);
            return false;
        }
        return true;
    }

    @OnClick(R.id.lnr_register)
    public void doRegister() {
        startActivity(new Intent(this, RegisterActivity.class));
    }

    public void doLoginRequest() {
        if (!Globals.internetCheck(LoginActivity.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getLoginParams(
                edt_email.getText().toString().trim(),
                edt_password.getText().toString().trim(),
                globals.getFCM_DeviceToken());

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(LoginActivity.this);

        HttpRequestHandler.getInstance().post(LoginActivity.this, getString(R.string.loginURL), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                UserModel userModel = new Gson().fromJson(response.toString(), UserModel.class);
                if (userModel.Result) {
                    gotoHomeScreen(userModel);
                } else {
                    Toaster.shortToast(userModel.Message);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void gotoHomeScreen(UserModel userModel) {
        Intent intent;
        if (!userModel.Data.number_verified) {
            // If mobile number not verified
            Toaster.shortToast(userModel.Message);
            intent = new Intent(LoginActivity.this, MobileVerifyActivity.class).
                    putExtra(Constants.SBU_MobilePhone, userModel.Data.mobilePhone).
                    putExtra(Constants.SBU_UserId, userModel.Data.userId).
                    putExtra(Constants.SBU_VerifyCode, userModel.Data.verficationCode);
        } else if (userModel.Data.userRole == UserRole.SELLER.getRole() && !userModel.Data.isCompanyProfileCreated) {
            // if company profile is not created for seller
            globals.setUserData(userModel);
            intent = new Intent(LoginActivity.this, CreateProfileActivity.class);
            intent.putExtra(Constants.SBU_OwnerName, userModel.Data.name);
            intent.putExtra(Constants.SBU_CompanyName, userModel.Data.legalBusinessName);
            intent.putExtra(Constants.SBU_MobilePhone, userModel.Data.phoneNumber);
            intent.putExtra(Constants.SBU_CountryCode, userModel.Data.countryCode);
        } else if (userModel.Data.userRole == UserRole.SELLER.getRole() && userModel.Data.isServicesCreated) {
            globals.setUserData(userModel);
            intent = new Intent(LoginActivity.this, SellerLandingActivity.class);
        } else {
            globals.setUserData(userModel);
            if (userModel.Data.userRole == UserRole.BUYER.getRole()) {
                intent = new Intent(LoginActivity.this, UserLandingActivity.class);
            } else {
                gotoAddNewServie();
                intent = new Intent(this, AddNewService.class).putExtra(Constants.SBU_Add_service_from, AddServiceFrom.FROM_MY_SERVICE);
            }
        }
        startActivity(intent);
        finishAffinity();

    }

    @OnClick(R.id.img_facebook)
    public void onFacebookClick() {
        isFacebook = true;
        checkPermission();
    }

    public void doFacebookLogin() {
        Globals.hideKeyboard(LoginActivity.this);

        if (!Globals.internetCheck(LoginActivity.this))
            return;

        callbackmanager = CallbackManager.Factory.create();

        // Set permissions
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email")); //, "user_about_me"

        LoginManager.getInstance().registerCallback(callbackmanager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                System.out.println("Success");

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Logger.i("LoginActivity", response.toString());

                        // Get facebook data from login
                        String id = "", first_name = "", last_name = "", email = "";
                        try {
                            if (object.has("id"))
                                id = object.getString("id");
                            if (object.has("first_name"))
                                first_name = object.getString("first_name");
                            if (object.has("last_name"))
                                last_name = object.getString("last_name");
                            if (object.has("email"))
                                email = object.getString("email");

                            if (!id.trim().isEmpty()) {
                                doConnectWithSocialRequest(id, first_name, last_name, email, true);
                            }
                        } catch (Exception e) {
                            Log.d(TAG, "Error parsing JSON");
                            Toaster.shortToast(R.string.error_went_wrong);
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, first_name, last_name, email"); // gender, birthday, location"
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Logger.d(TAG_CANCEL, "On cancel");
            }

            @Override
            public void onError(FacebookException error) {
                Logger.d(TAG_ERROR, error.toString());
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    public void doConnectWithSocialRequest(String id, String first_name, String last_name, String email, boolean isFacebook) {

        if (!Globals.internetCheck(LoginActivity.this))
            return;
        Address address = fusedLocationManager.getAdminArea();

        JSONObject params = HttpRequestHandler.getInstance().getRegisterParams(
                first_name,
                last_name,
                "", "",
                "",// Mobile
                isFacebook ? id : "",// SBU_FacebookId
                !isFacebook ? id : "",// SBU_TwitterId
                email,
                "",// password
                1, // userRole
                globals.getFCM_DeviceToken(),
                fusedLocationManager.getAddress(), fusedLocationManager.getLatLng(), "",
                address == null || address.getLocality() == null ? "" : address.getLocality(),
                address == null || address.getAdminArea() == null ? "" : address.getAdminArea(),
                address == null || address.getCountryName() == null ? "" : address.getCountryName());

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(LoginActivity.this);

        HttpRequestHandler.getInstance().post(LoginActivity.this,
                isFacebook ? getString(R.string.connectWithFacebookURL) : getString(R.string.connectWithTwitter),
                params, new JsonHttpResponseHandler() {

                    @Override
                    public void onStart() {
                        super.onStart();
                        dialog.show();
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        if (dialog != null && dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        UserModel userModel = new Gson().fromJson(response.toString(), UserModel.class);
                        if (userModel.Result) {
                            globals.setUserData(userModel);
                            startActivity(new Intent(LoginActivity.this, UserLandingActivity.class));
                            finish();
                        } else {
                            Toaster.shortToast(userModel.Message);
                            globals.disconnectFromFacebook();
                            globals.logoutTwitter();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        Toaster.shortToast(R.string.error_went_wrong);
                        globals.disconnectFromFacebook();
                        globals.logoutTwitter();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        Toaster.shortToast(R.string.error_went_wrong);
                        globals.disconnectFromFacebook();
                        globals.logoutTwitter();
                    }
                });
    }

    @OnClick(R.id.img_twitter)
    public void onTwitterClick() {
        isFacebook = false;
        checkPermission();
    }

    public void authTwitter() {

        mTwitterAuthClient.authorize(this, new com.twitter.sdk.android.core.Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> twitterSessionResult) {
                //get user Data
                getUserData(twitterSessionResult);
            }

            @Override
            public void failure(TwitterException e) {
                e.printStackTrace();
                globals.logoutTwitter();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == mTwitterAuthClient.getRequestCode()) {
            mTwitterAuthClient.onActivityResult(requestCode, resultCode, data);
        } else {
            callbackmanager.onActivityResult(requestCode, resultCode, data);
        }
    }

    void getUserData(Result<TwitterSession> twitterSessionResult) {
        if (twitterSessionResult.data != null)
            twitterSession = twitterSessionResult.data;

        // get username and userID
        final String userID = String.valueOf(twitterSession.getUserId());

        // get additional userdata
        TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();
        Call<User> userResult = twitterApiClient.getAccountService().verifyCredentials(true, false, true);
        userResult.enqueue(new Callback<User>() {

            @Override
            public void success(Result<User> result) {
                User user = result.data;
                try {
                    String[] flname = user.name.split("\\s+");
                    if (!userID.trim().isEmpty()) {
                        doConnectWithSocialRequest(userID, flname[0], flname.length > 1 ? flname[1] : "", user.email, false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    globals.logoutTwitter();
                }
            }

            @Override
            public void failure(TwitterException exception) {
                Toaster.shortToast(R.string.error_went_wrong);
                globals.logoutTwitter();
            }
        });
    }

    private void checkPermission() {
        // Check if location permission is granted??
        permissionListener = this;
        new TedPermission(this)
                .setPermissionListener(permissionListener)
                .setRationaleMessage(R.string.rationale_message)
                .setDeniedMessage(R.string.denied_message)
                .setGotoSettingButtonText(R.string.ok)
                .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                .check();
    }

    @Override
    public void onPermissionGranted() {
        fusedLocationManager = new FusedLocationManager(this);
        fusedLocationManager.setAddress();
        if (isFacebook)
            doFacebookLogin();
        else
            authTwitter();
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

    }

    private void gotoAddNewServie() {
        UserModel userModel = globals.getUserData();
        userModel.Data.isCompanyProfileCreated = true;
        globals.setUserData(userModel);
    }
}

