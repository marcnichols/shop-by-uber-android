package com.shopbyuber.login;

import android.content.Intent;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.orhanobut.logger.Logger;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.AddServiceFrom;
import com.shopbyuber.enums.UserRole;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.model.DataModel;
import com.shopbyuber.model.UserModel;
import com.shopbyuber.profile.AddNewService;
import com.shopbyuber.profile.CreateProfileActivity;
import com.shopbyuber.user.UserLandingActivity;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;


public class MobileVerifyActivity extends BaseActivity {
    Globals globals;

    @BindView(R.id.tv_mobile_phone)
    TextView tv_mobile_phone;

    @BindView(R.id.edt_verification_code)
    EditText edt_verification_code;

    @BindView(R.id.activity_mobile_verify)
    LinearLayout contentView;

    @BindView(R.id.lnr_send_again)
    LinearLayout lnr_send_again;

    String UserId = "";
    String registerPhoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_verify);
        ButterKnife.bind(this);
        init();
    }

    @RequiresApi(api = VERSION_CODES.LOLLIPOP)
    private void init() {

        super.hideUnhide(contentView, lnr_send_again);
        globals = (Globals) getApplicationContext();
        if (getIntent() != null) {
            tv_mobile_phone.setText(getIntent().getStringExtra(Constants.SBU_MobilePhone));
            registerPhoneNumber = getIntent().getStringExtra(Constants.SBU_MobilePhone).replace(getString(R.string.service_details_null), "");
            UserId = getIntent().getStringExtra(Constants.SBU_UserId);
        }
    }

    @OnClick(R.id.btn_verify)
    public void onVerify() {
        if (validation())
            doVerifyRequest();
    }

    @OnClick(R.id.lnr_send_again)
    public void onSendAgain() {
        doSendAgainRequest();
    }

    public boolean validation() {
        if (edt_verification_code.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_verification_code);
            return false;
        }
        return true;
    }

    public void doVerifyRequest() {
        if (!Globals.internetCheck(MobileVerifyActivity.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getMobileVerificationParams(
                UserId,
                edt_verification_code.getText().toString().trim(),
                registerPhoneNumber,
                globals.getFCM_DeviceToken());

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(MobileVerifyActivity.this);

        HttpRequestHandler.getInstance().post(MobileVerifyActivity.this, getString(R.string.verifyCodeURL), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                UserModel userModel = new Gson().fromJson(response.toString(), UserModel.class);
                if (userModel.Result)
                    gotoHomeScreen(userModel);
                else {
                    Toaster.shortToast(userModel.Message);
                    if (userModel.Message.equals(getString(R.string.invalid_session))) {
                        globals.doLogout(MobileVerifyActivity.this);
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void gotoHomeScreen(UserModel userModel) {
        globals.setUserData(userModel);
        Intent intent;
        //Redirection based on profile created
        if (userModel.Data.userRole == UserRole.SELLER.getRole()) {
            if (!userModel.Data.isCompanyProfileCreated) {
                intent = new Intent(MobileVerifyActivity.this, CreateProfileActivity.class);
                intent.putExtra(Constants.SBU_OwnerName, userModel.Data.name);
                intent.putExtra(Constants.SBU_CompanyName, userModel.Data.legalBusinessName);
                intent.putExtra(Constants.SBU_MobilePhone, userModel.Data.phoneNumber);
                intent.putExtra(Constants.SBU_CountryCode, userModel.Data.countryCode);
            } else {
                gotoAddNewServie();
                intent=new Intent(this, AddNewService.class).putExtra(Constants.SBU_Add_service_from, AddServiceFrom.FROM_MY_SERVICE);
            }


        } else {
            intent = new Intent(MobileVerifyActivity.this, UserLandingActivity.class);

        }
        startActivity(intent);
        finishAffinity();
    }

    public void doSendAgainRequest() {
        if (!Globals.internetCheck(MobileVerifyActivity.this))
            return;

        JSONObject params = HttpRequestHandler.getInstance().getSendAgainCodeParams(
                UserId, registerPhoneNumber);

        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(MobileVerifyActivity.this);

        HttpRequestHandler.getInstance().post(MobileVerifyActivity.this, getString(R.string.sendVerificationCodeURL), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Logger.json(response.toString());
                DataModel dataModel = new Gson().fromJson(response.toString(), DataModel.class);

                Toaster.shortToast(dataModel.Message);
                if (!dataModel.Result && dataModel.Message.equals(getString(R.string.invalid_session))) {
                    globals.doLogout(MobileVerifyActivity.this);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }


    private void gotoAddNewServie() {
        UserModel userModel = globals.getUserData();
        userModel.Data.isCompanyProfileCreated = true;
        globals.setUserData(userModel);
    }
}
