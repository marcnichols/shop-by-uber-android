package com.shopbyuber.login;

import android.Manifest;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.shopbyuber.R;
import com.shopbyuber.api.HttpRequestHandler;
import com.shopbyuber.enums.UserRole;
import com.shopbyuber.main.BaseActivity;
import com.shopbyuber.model.UserModel;
import com.shopbyuber.user.UserLandingActivity;
import com.shopbyuber.utils.Constants;
import com.shopbyuber.utils.FusedLocationManager;
import com.shopbyuber.utils.Globals;
import com.shopbyuber.utils.Toaster;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;
import cz.msebera.android.httpclient.Header;

public class RegisterActivity extends BaseActivity implements PermissionListener {

    @BindView(R.id.edt_first_name)
    EditText edt_first_name;

    @BindView(R.id.edt_last_name)
    EditText edt_last_name;


    @BindView(R.id.tv_country_code)
    TextView tvCountryCode;

    @BindView(R.id.edt_mobile_phone)
    EditText edt_mobile_phone;

    @BindView(R.id.edt_ezlist_screen_name)
    EditText edt_ezlist_screen_name;

    @BindView(R.id.edt_email)
    EditText edt_email;

    @BindView(R.id.edt_password)
    EditText edt_password;

    @BindView(R.id.edt_confirm_password)
    EditText edt_confirm_password;

    @BindView(R.id.activity_register)
    LinearLayout contentView;

    @BindView(R.id.lnr_login)
    LinearLayout lnr_login;

    @BindView(R.id.segment_seller)
    RadioButton segment_seller;

    boolean isSeller = false;
    Globals globals;
    UserRole role;
    String registerPhoneNumber;
    PermissionListener permissionListener;
    private FusedLocationManager fusedLocationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        checkPermission();
    }

    private void init() {

        super.hideUnhide(contentView, lnr_login);
        globals = (Globals) getApplicationContext();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey(Constants.SBU_UserRole)) {
            role = (UserRole) bundle.getSerializable(Constants.SBU_UserRole);
            if (role == UserRole.SELLER)
                segment_seller.performClick();
        }
        fusedLocationManager = new FusedLocationManager(this);
        fusedLocationManager.setAddress();

        edt_mobile_phone.addTextChangedListener(new TextWatcher() {
            private boolean mFormatting; // this is a flag which prevents the  stack overflow.
            private int mAfter;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                mAfter = after;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!mFormatting) {
                    mFormatting = true;
                    // using US formatting...
                    if (mAfter != 0) // in case back space ain't clicked...
                        PhoneNumberUtils.formatNumber(s, PhoneNumberUtils.getFormatTypeForLocale(Locale.US));
                    mFormatting = false;
                }
            }
        });
    }

    @OnClick(R.id.segment_buyer)
    public void onBuyer() {
        isSeller = false;
        edt_first_name.setHint(getString(R.string.first_name));
        edt_last_name.setHint(getString(R.string.last_name));
        cleardata();
    }

    @OnClick(R.id.segment_seller)
    public void onSeller() {
        isSeller = true;

        cleardata();
    }

    @OnClick(R.id.btn_register)
    public void onRegister() {
        if (doValidation()) {
            doRegisterRequest();
        }

    }

    public boolean doValidation() {
        if (edt_first_name.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(isSeller ? getString(R.string.err_enter_owner_name) : getString(R.string.err_enter_first_name));
            return false;
        }
        if (edt_last_name.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(isSeller ? getString(R.string.err_enter_business_name) : getString(R.string.err_enter_last_name));
            return false;
        }
        if (edt_mobile_phone.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_mobile_number);
            return false;
        }
        if (edt_mobile_phone.getText().toString().trim()
                .replace(getString(R.string.service_details_null), "")
                .length() < 10) {
            Toaster.shortToast(R.string.err_enter_valid_mobile_number);
            return false;
        }
        if (edt_email.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_email);
            return false;
        }
        if (!Globals.validateEmailAddress(edt_email.getText().toString().trim())) {
            Toaster.shortToast(R.string.err_enter_valid_email);
            return false;
        }
        if (edt_password.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_password);
            return false;
        }
        if (edt_confirm_password.getText().toString().trim().isEmpty()) {
            Toaster.shortToast(R.string.err_enter_confirm_password);
            return false;
        }
        if (!edt_confirm_password.getText().toString().trim().equals(edt_password.getText().toString().trim())) {
            Toaster.shortToast(R.string.err_confirm_password_should_be_same_as_password);
            return false;
        }
        return true;
    }

    public void doRegisterRequest() {
        if (!Globals.internetCheck(RegisterActivity.this))
            return;

        registerPhoneNumber = edt_mobile_phone.getText().toString().trim();
        Address address = fusedLocationManager.getAdminArea();

        JSONObject params = HttpRequestHandler.getInstance().getRegisterParams(
                edt_first_name.getText().toString().trim(),
                edt_last_name.getText().toString().trim(),
                tvCountryCode.getText().toString(), registerPhoneNumber.replace(getString(R.string.service_details_null), ""),
                tvCountryCode.getText().toString() + registerPhoneNumber.replace(getString(R.string.service_details_null), ""),
                "",// SBU_FacebookId
                "",// SBU_TwitterId
                edt_email.getText().toString().trim(),
                edt_password.getText().toString().trim(),
                isSeller ? 2 : 1,
                globals.getFCM_DeviceToken(),
                fusedLocationManager.getAddress(),
                fusedLocationManager.getLatLng(), isSeller ? "" : edt_ezlist_screen_name.getText().toString().trim(),
                address == null || address.getLocality() == null ? "" : address.getLocality(),
                address == null || address.getAdminArea() == null ? "" : address.getAdminArea(),
                address == null || address.getCountryName() == null ? "" : address.getCountryName());
        final ACProgressFlower dialog = HttpRequestHandler.getInstance().getProgressBar(RegisterActivity.this);

        HttpRequestHandler.getInstance().post(RegisterActivity.this, getString(R.string.registerURL), params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                UserModel userModel = new Gson().fromJson(response.toString(), UserModel.class);
                if (userModel.Result) {
                    gotoHomeScreen(userModel);
                } else {
                    Toaster.shortToast(userModel.Message);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toaster.shortToast(R.string.error_went_wrong);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toaster.shortToast(R.string.error_went_wrong);
            }
        });
    }

    private void gotoHomeScreen(UserModel userModel) {
        Intent intent;
        //Redirect based on mobile number verified
        if (!userModel.Data.number_verified) {
            intent = new Intent(RegisterActivity.this, MobileVerifyActivity.class).
                    putExtra(Constants.SBU_MobilePhone,
                            tvCountryCode.getText().toString() + edt_mobile_phone.getText().toString().trim()).
                    putExtra(Constants.SBU_UserId, userModel.Data.userId).
                    putExtra(Constants.SBU_VerifyCode, userModel.Data.verficationCode);
        } else {
            intent = new Intent(RegisterActivity.this, UserLandingActivity.class);
        }
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.lnr_login)
    public void onLogin() {
        startActivity(new Intent(RegisterActivity.this, LoginActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
        finish();
    }

    public void cleardata() {
        edt_first_name.setText("");
        edt_last_name.setText("");
        edt_mobile_phone.setText("");
        edt_email.setText("");
        edt_password.setText("");
        edt_confirm_password.setText("");
    }

    private void checkPermission() {
        // Check if location permission is granted??
        permissionListener = this;
        new TedPermission(this)
                .setPermissionListener(permissionListener)
                .setRationaleMessage(R.string.rationale_message)
                .setDeniedMessage(R.string.denied_message)
                .setGotoSettingButtonText(R.string.ok)
                .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                .check();
    }

    @Override
    public void onPermissionGranted() {
        init();
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermissions) {
        //location permission not granted
        if (isTaskRoot())
            startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

}
